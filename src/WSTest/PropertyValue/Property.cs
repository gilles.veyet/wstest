﻿using System.Xml;
using System.Xml.Linq;
using WSTest.Helpers;

namespace WSTest;

enum UrlParamLocations
{
	Path,
	Query,
	Cookie, // not handled
	Header  // not handled
}

enum SimpleTypes
{
	Boolean,
	Integer,    // formats: 'int32', 'int64'
	Number,     // formats: 'float', 'double', 'decimal'
				//Binary,     // byte array.  TODO handle Binary.
	Date,
	DateTime,
	Time,       // SOAP only
	Duration,   // SOAP only
}

public enum SoapPropertyHandlingEnum
{
	/// <summary>
	/// Handle as element (normal handling).
	/// </summary>
	Element = 0,

	/// <summary>
	/// Handle as attribute. 
	/// </summary>
	Attribute = 1,

	/// <summary>
	/// Handle as inner text of parent
	/// Only one element inside <see cref="ObjectPropertyType"/> can use this handling.
	/// </summary>
	InnerText = 2
}


record Property
{
	public XmlQualifiedName QualifiedName { get; init; } = null!;

	public PropertyType Type { get; init; } = null!;

	[Newtonsoft.Json.JsonIgnore]
	public ActualPropertyType ActualType => Type.ActualType;
	public string? Description { get; init; }   // SOAP Annotation/Documentation (string/html) - OpenAPI description (string only, CommonMark/MarkDown not handled).

	public bool Required { get; init; }
	public bool Nullable { get; init; }

	public bool IsArray { get; init; }

	[Newtonsoft.Json.JsonIgnore]
	public bool IsPropertyOrActualTypeArray => IsArray || Type.IsArray || ActualType.IsArray;

	[Newtonsoft.Json.JsonIgnore]
	public bool Mandatory => Required && !Nullable;

	[Newtonsoft.Json.JsonIgnore]
	public string Name => QualifiedName.Name;

	[Newtonsoft.Json.JsonIgnore]
	public XName XName => XmlHelper.ToXName(QualifiedName);

	public SoapPropertyHandlingEnum SoapHandling { get; init; }


	[Newtonsoft.Json.JsonConstructor]
	protected Property() { }

	public Property(XmlQualifiedName qualifiedName, PropertyType type, bool isArray)
	{
		this.QualifiedName = qualifiedName;
		this.Type = type;
		this.IsArray = isArray;
	}

	public override string ToString()
	{
		string name = $"{Name}{(Nullable ? '?' : ' ')}";
		return $"{(Required ? '*' : ' ')}{name}: {Type}";
	}

	public static Property MakeChoiceProperty(ChoicePropertyType choicePropertyType, bool isRequired)
	{
		// isArray is false in Property constructor but it can be true on PropertyType.
		return new Property(new XmlQualifiedName("Choice"), choicePropertyType, false) { Required = isRequired };
	}
}
record UrlParam : Property
{
	public UrlParamLocations Location { get; init; }

	[Newtonsoft.Json.JsonConstructor]
	protected UrlParam() { }

	public UrlParam(Property property, UrlParamLocations location) : base(property) // using record default copy constructor.
	{
		this.Location = location;
	}

	public override string ToString()
	{
		return $"{Location,-6} {base.ToString()}";
	}
}

record RequestProperty : Property
{
	[Newtonsoft.Json.JsonConstructor]
	protected RequestProperty() { }

	public RequestProperty(XmlQualifiedName qualifiedName, PropertyType type) : base(qualifiedName, type, false)
	{
		this.Required = true;
	}

	public RequestProperty(Property property) : base(property)  // using record default copy constructor.
	{ }

	public override string ToString()
	{
		return base.ToString();
	}

}
