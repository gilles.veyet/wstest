﻿using System.Xml;
using System.Xml.Linq;
using WSTest.Helpers;

#pragma warning disable IDE0130 // Namespace does not match folder structure
namespace WSTest
#pragma warning restore IDE0130 // Namespace does not match folder structure
{

	abstract record PropertyType
	{
		public bool IsArray { get; init; }      // SOAP: always false.
		public XmlQualifiedName? QualifiedName { get; init; }

		public string? Description { get; init; }   // SOAP Annotation/Documentation (string/html) - OpenAPI description (string only, CommonMark/MarkDown not handled).

		[Newtonsoft.Json.JsonIgnore]
		public string Name => QualifiedName?.Name ?? string.Empty;

		[Newtonsoft.Json.JsonIgnore]
		public XName? XName => QualifiedName != null ? XmlHelper.ToXName(QualifiedName) : null;

		[Newtonsoft.Json.JsonIgnore]
		public ActualPropertyType ActualType => this is ReferencePropertyType referencePropertyType ? referencePropertyType._ActualType : (ActualPropertyType)this;

		public override string ToString()
		{
			return Name;
		}
	}

	record ReferencePropertyType : PropertyType
	{
		[Newtonsoft.Json.JsonIgnore]
		public XmlQualifiedName RefQualifiedName => QualifiedName!;

		[Newtonsoft.Json.JsonIgnore]
		public ActualPropertyType _ActualType { get; set; } = null!;

		public ReferencePropertyType(XmlQualifiedName refId)
		{
			this.QualifiedName = refId;
		}

		public override string ToString()
		{
			return base.ToString();
		}
	}

	abstract record ActualPropertyType : PropertyType
	{
		public bool IsAbstract { get; init; }

		public XmlQualifiedName? BaseTypeName { get; init; }

		// ===============
		// SOAP specific

		public List<XmlQualifiedName>? DerivedTypeNames { get; set; }

		public void AddDerivedTypeName(XmlQualifiedName qualifiedName)
		{
			DerivedTypeNames ??= [];
			DerivedTypeNames.Add(qualifiedName);
		}

		//public IEnumerable<ActualPropertyType> GetDescendantTypes(ILookupReferenceType lookup)
		//{
		//	if (DerivedTypeNames == null)
		//		return [];

		//	return DerivedTypeNames.Select(t => lookup.GetPropertyTypeRef(t));
		//}

		//public IEnumerable<ActualPropertyType> GetAllDescendantTypes(ILookupReferenceType lookup)
		//{
		//	var types = GetDescendantTypes(lookup);
		//	return types.Concat(types.SelectMany(t => t.GetDescendantTypes(lookup)));
		//}

		/// <summary>
		/// SOAP XmlSchemaSimpleTypeList => single XML element with values separated by one space.
		/// </summary>
		public bool SoapArrayAsList { get; init; }

		public IReadOnlyList<ActualPropertyType>? SoapAnyOfPropertyTypes { get; set; } = null;

		// END SOAP specific
		// ===============

		public override string ToString()
		{
			return base.ToString();
		}
	}

	/// <summary>
	/// BasicType : StringPropertyType & SimplePropertyType
	/// </summary>
	record BasicPropertyType : ActualPropertyType
	{
		public override string ToString()
		{
			return base.ToString();
		}
	}

	record SimplePropertyType : BasicPropertyType
	{
		public SimpleTypes Type { get; init; }

		public SimplePropertyType()
		{ }

		public SimplePropertyType(SimpleTypes type)
		{
			this.Type = type;
		}

		public Value MakeValue(Property property, bool isArrayItem, Value? parent)
		{
			return Type switch
			{
				SimpleTypes.Boolean => new BooleanValue(property, isArrayItem, parent),
				SimpleTypes.Integer => new IntegerValue(property, isArrayItem, parent),
				SimpleTypes.Number => new NumberValue(property, isArrayItem, parent),
				SimpleTypes.DateTime => new DateTimeValue(property, isArrayItem, parent),
				SimpleTypes.Date => new DateValue(property, isArrayItem, parent),
				SimpleTypes.Time => new TimeValue(property, isArrayItem, parent),
				SimpleTypes.Duration => new DurationValue(property, isArrayItem, parent),
				//SimpleTypes.Binary => new ByteArrayValue(property, isArrayItem),
				_ => throw new Exception($"Invalid SimplePropertyType '{Type}'")
			};
		}

		public static SimplePropertyType TypeBoolean => new(SimpleTypes.Boolean);
		public static SimplePropertyType TypeInteger => new(SimpleTypes.Integer);
		public static SimplePropertyType TypeNumber => new(SimpleTypes.Number);
		public static SimplePropertyType TypeDateTime => new(SimpleTypes.DateTime);
		public static SimplePropertyType TypeDate => new(SimpleTypes.Date);
		public static SimplePropertyType TypeTime => new(SimpleTypes.Time);
		public static SimplePropertyType TypeDuration => new(SimpleTypes.Duration);

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record StringPropertyType : BasicPropertyType
	{
		public int? MinLength { get; init; }
		public int? MaxLength { get; init; }
		public string? Pattern { get; init; }

		public StringPropertyType()
		{ }

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record StringBase64PropertyType : BasicPropertyType
	{
		public StringBase64PropertyType()
		{ }

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record StringEnumPropertyType : ActualPropertyType
	{
		public IReadOnlyList<string> Values { get; init; } = null!;

		public StringEnumPropertyType()
		{ }

		public StringEnumPropertyType(IEnumerable<string> values)
		{
			this.Values = values.ToArray();
		}

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record IntEnumPropertyType : ActualPropertyType
	{
		public IReadOnlyList<int> Values { get; init; } = null!;

		public IntEnumPropertyType()
		{ }

		public IntEnumPropertyType(IEnumerable<int> values)
		{
			this.Values = values.ToArray();
		}
		public override string ToString()
		{
			return base.ToString();
		}
	}

	record ObjectPropertyType : ActualPropertyType
	{
		/// <summary>
		/// For SOAP array: complex object / sequence that contains a single element (unbounded).
		/// </summary>
		public bool SoapLikeArray { get; init; }

		public IReadOnlyList<Property> Properties { get; init; } = null!;

		public ObjectPropertyType()
		{ }

		public ObjectPropertyType(IEnumerable<Property> properties)
		{
			this.Properties = properties.ToArray();
		}

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record OneOfPropertyType : ActualPropertyType
	{
		public IReadOnlyList<PropertyType> PropertyTypes { get; init; } = null!;

		public ActualPropertyType? SoapBaseType { get; init; }

		public OneOfPropertyType()
		{ }

		public OneOfPropertyType(IEnumerable<PropertyType> types)
		{
			PropertyTypes = types.ToArray();
			//Debug.Assert(types.FirstOrDefault(t => t.ActualType.IsArray) == null, $"{this.GetType().Name} cannot have array element '{this}'");
		}

		public bool IsSoapBaseType(ActualPropertyType actualType) => SoapBaseType != null && SoapBaseType.QualifiedName == actualType.QualifiedName;

		public static OneOfPropertyType MakeAnySimpleType(bool isArray, XmlQualifiedName? qualifiedName) =>
			new([ new StringPropertyType() , SimplePropertyType.TypeInteger, SimplePropertyType.TypeNumber, SimplePropertyType.TypeBoolean,
			SimplePropertyType.TypeDate, SimplePropertyType.TypeDateTime, SimplePropertyType.TypeTime ])
			{ IsArray = isArray, QualifiedName = qualifiedName };

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record ChoicePropertyType : ActualPropertyType
	{
		public IReadOnlyList<Property> Properties { get; init; } = null!;

		public ChoicePropertyType()
		{ }

		public ChoicePropertyType(IEnumerable<Property> properties)
		{
			this.Properties = properties.ToArray();
		}

		public override string ToString()
		{
			return base.ToString();
		}
	}
}
