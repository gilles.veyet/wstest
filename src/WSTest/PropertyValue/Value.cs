﻿using System.Diagnostics;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Xml;
using System.Xml.Linq;
using WSTest.Extensions;
using WSTest.Helpers;

namespace WSTest;

interface IJsonSerialize
{
	JsonNode? ToJsonNode();
	bool ParseJson(JsonElement element);
}

interface ISoapSerialize
{
	bool ParseXml(XElement element);
}

internal static class ValueExtensions
{
	public static List<Value> CloneList(this IEnumerable<Value> list)
	{
		return list.Select(item => item with { }).ToList();
	}
}

abstract record Value : IJsonSerialize, ISoapSerialize
{
	public bool IsArrayItem { get; }

	public Property Property { get; private init; }

	public ActualPropertyType ActualType => Property.ActualType;

	public bool Specified { get; set; }

	public Value? Parent { get; init; }

	public Value(Property property, bool isArrayItem, Value? parent)
	{
		this.Property = property;
		this.IsArrayItem = isArrayItem;
		this.Specified = Property.Required || HandleEmptyAsNull;
		this.Parent = parent;
	}

	public bool HandleEmptyAsNull => this switch
	{
		StringValue => true,
		ArrayValue => true,
		ChoiceValue choiceValue => choiceValue.IsArray,
		ObjectValue objectValue => objectValue.PropertyType.SoapLikeArray,
		_ => false
	};

	public bool IsEmptyArray => this switch
	{
		ArrayValue arrayValue => !arrayValue.Specified || arrayValue.Values.Count == 0,
		ChoiceValue choiceValue => !choiceValue.Specified || choiceValue.SelectedValues.Count == 0,
		_ => false
	};

	public abstract bool HasChildren { get; }

	public abstract JsonNode? ToJsonNode();
	public abstract bool ParseJson(JsonElement element);

	public JsonNode? ToNotSpecifiedJsonNode()
	{
		// if value is not specified then property must be Nullable or not Required.
		return Property.Required ? "null" : null;
	}

	public void SoapSerialize(XElement parent)
	{
		if (this is SingleValue singleValue)
			singleValue.SoapSerializeSingle(parent);
		else if (this is ArrayValue arrayValue)
			arrayValue.SoapSerializeArray(parent);
	}

	public void SoapSerializeEx(XElement parent, bool addType)
	{
		if (this is SingleValue singleValue)
		{
			var e = singleValue.SoapSerializeSingle(parent);
			if (e != null && addType)
				singleValue.AddXsiType(e);
		}
		else if (this is ArrayValue arrayValue)
		{
			arrayValue.SoapSerializeArray(parent);
		}
	}


	public abstract bool ParseXml(XElement element);

	public virtual bool MarkSpecified(bool present)
	{
		this.Specified = present || HandleEmptyAsNull;
		return !Property.Mandatory || this.Specified;
	}

	public override string ToString()
	{
		return $"{this.GetType().Name} {Property}";
	}

	public bool IsInfiniteLooping(XmlQualifiedName? propertyTypeQualifiedName)
	{
		if (propertyTypeQualifiedName == null)
			return false;

		for (var value = this; value != null; value = value.Parent)
		{
			var prop = value.Property;

			if (!prop.Required || value.IsArrayItem)
				return false;

			if (value.ActualType.QualifiedName == propertyTypeQualifiedName)
				return true;
		}

		return false;
	}

	public List<Value> GetSegments()
	{
		List<Value> segments = [];

		for (var value = this; value != null; value = value.Parent)
		{
			if (value is not ArrayValue and not OneOfValue and not ChoiceValue)
				segments.Add(value);
		}

		segments.Reverse();
		return segments;
	}

	public string GetPropertyChain()
	{
		return new StringBuilder().AppendJoin('|', GetSegments().Select(v => v.Property.QualifiedName.ToString())).ToString();
	}

	public XElement AppendXmlElement(XElement parent)
	{
		var elem = new XElement(Property.XName);
		parent.Add(elem);   // add element before calling AddXsiType (need to know the root element).
		return elem;
	}

	protected XElement SetXmlValue(XElement parent, string? text)
	{
		switch (Property.SoapHandling)
		{
			case SoapPropertyHandlingEnum.Attribute:
				parent.SetAttributeValue(Property.XName, text ?? String.Empty);
				return parent;

			case SoapPropertyHandlingEnum.InnerText:
				parent.SetValue(text ?? String.Empty);
				return parent;

			default:
				var elem = AppendXmlElement(parent);
				elem.Value = text ?? String.Empty;
				return elem;
		}
	}

	public void AddXsiType(XElement element)
	{
		var q = ActualType.QualifiedName;

		if (q is null)
			return;

		string typeName = element.GetDefaultNamespace() == q.Namespace ? q.Name : XmlHelper.MakeQualifiedNameWithPrefix(element, q);
		element.SetAttributeValue(XmlHelper.XNameTypeAttribute, typeName);
	}
}


abstract record SingleValue : Value
{
	public SingleValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent)
	{
		// Arrays of arrays are not handled by PropertyEditor so Property must not be an array unless value is an ArrayItem.
		// ChoiceValue is currently handled as SingleValue although it can hold an array
		// TODO : removed test because if failed with RecursiveWsdl / CreateOrderItems 
		//Debug.Assert(this is ChoiceValue || isArrayItem || !property.IsArray, $"'{this.GetType().Name}' property must not be an array");
	}

	public abstract XElement? SoapSerializeSingle(XElement parent);

	public override string ToString()
	{
		return base.ToString();
	}
}


abstract record BasicValue : SingleValue
{
	public BasicValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent) { }

	public abstract bool ParseString(string? text);

	public abstract string SerializeToString();

	public override string ToString()
	{
		return base.ToString();
	}
}


abstract record SimpleValue<T> : BasicValue where T : struct
{
	public abstract T Value { get; set; }

	protected virtual T DefaultValue => default;

	public SimplePropertyType PropertyType => (SimplePropertyType)ActualType;
	public override bool HasChildren => false;

	public SimpleValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType, $"'{this.GetType().Name}' property  must be of type '{typeof(SimplePropertyType).Name}'");
	}

	public override JsonNode? ToJsonNode()
	{
		if (!Specified)
			return ToNotSpecifiedJsonNode();

		return JsonHelper.ConvertToJsonNode<T>(Value);
	}

	public override string SerializeToString()
	{
		return XmlHelper.Serialize<T>(Value);
	}

	public override XElement? SoapSerializeSingle(XElement parent)
	{
		if (!Specified)
			return null;

		return SetXmlValue(parent, XmlHelper.Serialize<T>(Value));
	}

	public override bool ParseString(string? text)
	{
		Nullable<T> value = text != null ? XmlHelper.ParseString<T>(text) : null;
		Value = value ?? DefaultValue;
		return MarkSpecified(value.HasValue);
	}

	public override bool ParseXml(XElement element)
	{
		return ParseString(element.Value);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}


record BooleanValue : SimpleValue<bool>
{
	public override bool Value { get; set; }

	public BooleanValue(Property property, bool isArrayItem, Value? parent, bool value = false) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType simplePropertyType && simplePropertyType.Type == SimpleTypes.Boolean, $"'{this.GetType().Name}' property  must be of type '{SimpleTypes.Boolean}'");
		Value = value;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.True || element.ValueKind == JsonValueKind.False)
		{
			Value = element.GetBoolean();
			present = true;
		}
		else
			Value = false;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}

record IntegerValue : SimpleValue<long>
{
	public override long Value { get; set; }

	public IntegerValue(Property property, bool isArrayItem, Value? parent, long value = 0) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType simplePropertyType && simplePropertyType.Type == SimpleTypes.Integer, $"'{this.GetType().Name}' property  must be of type '{SimpleTypes.Integer}'");
		Value = value;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.Number && element.TryGetInt64(out long value))
		{
			Value = value;
			present = true;
		}
		else
			Value = 0;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}

record NumberValue : SimpleValue<decimal>
{
	public override decimal Value { get; set; }

	public NumberValue(Property property, bool isArrayItem, Value? parent, decimal value = 0) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType simplePropertyType && simplePropertyType.Type == SimpleTypes.Number, $"'{this.GetType().Name}' property  must be of type '{SimpleTypes.Number}'");
		Value = value;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.Number && element.TryGetDecimal(out decimal value))
		{
			Value = value;
			present = true;
		}
		else
			Value = 0;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}

record DateTimeValue : SimpleValue<DateTime>
{
	protected override DateTime DefaultValue
	{
		get
		{
			var now = DateTime.Now;
			return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, now.Kind);
		}
	}

	public override DateTime Value { get; set; }

	public DateTimeValue(Property property, bool isArrayItem, Value? parent, DateTime? value = null) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType simplePropertyType && simplePropertyType.Type == SimpleTypes.DateTime, $"'{this.GetType().Name}' property  must be of type '{SimpleTypes.DateTime}'");
		Value = value ?? DefaultValue;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.String && element.TryGetDateTime(out DateTime value))
		{
			Value = value;
			present = true;
		}
		else
			Value = DefaultValue;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}

record DateValue : SimpleValue<DateOnly>
{
	protected override DateOnly DefaultValue => DateOnly.FromDateTime(DateTime.Now);

	public override DateOnly Value { get; set; }

	public DateValue(Property property, bool isArrayItem, Value? parent, DateOnly? value = null) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType simplePropertyType && simplePropertyType.Type == SimpleTypes.Date, $"'{this.GetType().Name}' property  must be of type '{SimpleTypes.Date}'");
		Value = value ?? DefaultValue;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.String && element.TryGetDateTime(out DateTime value))
		{
			Value = DateOnly.FromDateTime(value);
			present = true;
		}
		else
			Value = DefaultValue;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}

record TimeValue : SimpleValue<TimeOnly>
{
	public override TimeOnly Value { get; set; }

	public TimeValue(Property property, bool isArrayItem, Value? parent, TimeOnly? value = null) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType simplePropertyType && simplePropertyType.Type == SimpleTypes.Time, $"'{this.GetType().Name}' property  must be of type '{SimpleTypes.Time}'");
		Value = value ?? TimeOnly.MinValue;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.String && element.TryGetDateTime(out DateTime value))
		{
			Value = TimeOnly.FromDateTime(value);
			present = true;
		}
		else
			Value = TimeOnly.MinValue;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}

record DurationValue : SimpleValue<TimeSpan>
{
	public override TimeSpan Value { get; set; }

	public DurationValue(Property property, bool isArrayItem, Value? parent, TimeSpan? value = null) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is SimplePropertyType simplePropertyType && simplePropertyType.Type == SimpleTypes.Duration, $"'{this.GetType().Name}' property  must be of type '{SimpleTypes.Duration}'");
		Value = value ?? TimeSpan.Zero;
	}

	public override bool ParseJson(JsonElement element)
	{
		throw new Exception($"{nameof(DurationValue)} does not implement {nameof(ParseJson)}");
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}


abstract record StringBaseValue : BasicValue
{
	public string Value { get; set; }

	public override bool HasChildren => false;

	public StringBaseValue(Property property, bool isArrayItem, Value? parent, string value) : base(property, isArrayItem, parent)
	{
		Value = value;
	}

	public override JsonNode? ToJsonNode()
	{
		if (!Specified || string.IsNullOrEmpty(Value))
			return (Property.Required || IsArrayItem) ? string.Empty : null;

		return Value;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.String)
		{
			Value = element.GetString() ?? string.Empty;
			present = true;
		}
		else
			Value = string.Empty;

		return MarkSpecified(present);  // always set Specified to true since HandleEmptyAsNull is true => return True.
	}

	public override XElement? SoapSerializeSingle(XElement parent)
	{
		if (!Specified || string.IsNullOrEmpty(Value) && !Property.Required)
			return null;

		// TODO handle required & nullable with xsi:nil="true" ? see https://support.google.com/code/answer/64736?hl=en
		return SetXmlValue(parent, Value);
	}

	public override bool ParseXml(XElement element)
	{
		return ParseString(element.Value);
	}

	public override string SerializeToString()
	{
		return Value;
	}

	public override bool ParseString(string? text)
	{
		Value = text ?? string.Empty;
		return MarkSpecified(true);
	}

	public override string ToString()
	{
		return $"{this.GetType().GetFormattedName()} '{Value}'";
	}
}

record StringValue : StringBaseValue
{
	public StringPropertyType PropertyType => (StringPropertyType)ActualType;

	public StringValue(Property property, bool isArrayItem, Value? parent, string value = "") : base(property, isArrayItem, parent, value)
	{
		Debug.Assert(property.ActualType is StringPropertyType, $"'{this.GetType().Name}' property  must be of type '{nameof(StringPropertyType)}'");
	}
}

record StringBase64Value : StringBaseValue
{
	public StringBase64PropertyType PropertyType => (StringBase64PropertyType)ActualType;

	public StringBase64Value(Property property, bool isArrayItem, Value? parent, string value = "") : base(property, isArrayItem, parent, value)
	{
		Debug.Assert(property.ActualType is StringBase64PropertyType, $"'{this.GetType().Name}' property  must be of type '{nameof(StringBase64PropertyType)}'");
	}
}

record StringEnumValue : BasicValue
{
	private string DefaultValue => PropertyType.Values[0];

	public string Value { get; set; }

	public StringEnumPropertyType PropertyType => (StringEnumPropertyType)ActualType;
	public override bool HasChildren => false;

	public StringEnumValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is StringEnumPropertyType, $"'{this.GetType().Name}' property  must be of type '{typeof(StringEnumPropertyType).Name}'");
		Value = DefaultValue;
	}

	public override JsonNode? ToJsonNode()
	{
		if (!Specified)
			return ToNotSpecifiedJsonNode();

		return Value;
	}

	public override bool ParseJson(JsonElement element)
	{
		string value = element.ValueKind == JsonValueKind.String ? element.GetString() ?? string.Empty : string.Empty;
		return ParseString(value);
	}

	public override XElement? SoapSerializeSingle(XElement parent)
	{
		if (!Specified)
			return null;

		return SetXmlValue(parent, Value);
	}

	public override bool ParseXml(XElement element)
	{
		return ParseString(element.Value);
	}

	public override string SerializeToString()
	{
		return Value;
	}

	public override bool ParseString(string? text)
	{
		bool present = false;

		if (!string.IsNullOrEmpty(text) && PropertyType.Values.Contains(text))
		{
			Value = text;
			present = true;
		}

		if (!present)
			Value = DefaultValue;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().Name} '{Value}'";
	}
}

record IntEnumValue : BasicValue
{
	private int DefaultValue => PropertyType.Values[0];

	public int Value { get; set; }

	public IntEnumPropertyType PropertyType => (IntEnumPropertyType)ActualType;
	public override bool HasChildren => false;

	public IntEnumValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is IntEnumPropertyType, $"'{this.GetType().Name}' property  must be of type '{typeof(IntEnumPropertyType).Name}'");
		Value = PropertyType.Values[0];
	}

	public override JsonNode? ToJsonNode()
	{
		if (!Specified)
			return ToNotSpecifiedJsonNode();

		return Value;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.Number && element.TryGetInt32(out int value))
		{
			if (PropertyType.Values.Contains(value))
			{
				Value = value;
				present = true;
			}
		}

		if (!present)
			Value = DefaultValue;

		return MarkSpecified(present);
	}

	public override XElement? SoapSerializeSingle(XElement parent)
	{
		if (!Specified)
			return null;

		return SetXmlValue(parent, XmlHelper.Serialize(Value));
	}

	public override bool ParseXml(XElement element)
	{
		return ParseString(element.Value);
	}

	public override string SerializeToString()
	{
		return Value.ToString();
	}

	public override bool ParseString(string? text)
	{
		int? value = text != null ? XmlHelper.ParseString<int>(text) : null;

		bool present = false;

		if (value != null)
		{
			if (PropertyType.Values.Contains(value.Value))
			{
				Value = value.Value;
				present = true;
			}
		}

		if (!present)
			Value = DefaultValue;

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return $"{this.GetType().Name} {Value}";
	}
}


record ObjectValue : SingleValue
{
	public ObjectPropertyType PropertyType => (ObjectPropertyType)ActualType;
	public override bool HasChildren => true;

	private List<Value>? _PropertyValues = null;
	public IReadOnlyList<Value> PropertyValues => _PropertyValues ??= PropertyType.Properties.Select(p => ValueFactory.MakeValueFromPropertyInternal(p, this)).ToList();

	public ObjectValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is ObjectPropertyType, $"'{this.GetType().Name}' property  must be of type '{typeof(ObjectPropertyType).Name}'");
	}

	// custom copy constructor for deep copy : use default copy constructor of base class.
	public ObjectValue(ObjectValue other) : base(other)
	{
		_PropertyValues = other._PropertyValues?.CloneList();
	}

	public override JsonNode? ToJsonNode()
	{
		if (!Specified)
			return ToNotSpecifiedJsonNode();

		JsonObject jsonObject = [];

		foreach (var v in PropertyValues)
		{
			var node = v.ToJsonNode();
			if (v.Property.Required || node != null)    // if property is required, then set value as null if property is not set.
				jsonObject.Add(v.Property.Name, node);
		}

		return jsonObject;
	}

	public override bool ParseJson(JsonElement element)
	{
		bool present = false;

		if (element.ValueKind == JsonValueKind.Object)
		{
			bool match = true;

			foreach (var e in element.EnumerateObject())
			{
				if (!PropertyValues.Any(v => v.Property.Name == e.Name))
				{
					match = false;
					break;
				}
			}

			if (match)
			{
				present = true;

				foreach (var v in PropertyValues)
				{
					var prop = v.Property;

					if (element.TryGetProperty(prop.Name, out JsonElement e))
					{
						if (!v.ParseJson(e))
							present = false;
					}
					else
					{
						if (!v.MarkSpecified(false))
							present = false;
					}
				}
			}
		}

		return MarkSpecified(present);
	}


	public override XElement? SoapSerializeSingle(XElement parent)
	{
		if (!Specified)
			return null;

		if (PropertyType.SoapLikeArray && PropertyValues.Count == 1 && PropertyValues[0].IsEmptyArray)
			return null;

		var elem = AppendXmlElement(parent);

		foreach (var child in PropertyValues)
		{
			child.SoapSerialize(elem);
		};

		return elem;
	}

	public override bool ParseXml(XElement element)
	{
		bool present = true;

		if (element.Name == Property.XName)
		{
			foreach (var v in PropertyValues)
			{
				if (v is BasicValue basicValue)
				{
					string? text = GetXmlValue(element, v.Property);
					if (!basicValue.ParseString(text))
					{
						present = false;
					}
				}
				else if (v is ArrayValue || v is ChoiceValue choiceValue)
				{
					if (!v.ParseXml(element))
						present = false;
				}
				else
				{
					var e = element.Element(v.Property.XName);
					if (e != null)
					{
						if (!v.ParseXml(e))
							present = false;
					}
					else
					{
						if (!v.MarkSpecified(false))
							present = false;
					}
				}
			}
		}

		return MarkSpecified(present);
	}

	private static string? GetXmlValue(XElement element, Property property)
	{
		return property.SoapHandling switch
		{
			SoapPropertyHandlingEnum.Attribute => element.Attribute(property.XName)?.Value,
			SoapPropertyHandlingEnum.InnerText => element.Value,
			_ => element.Element(property.XName)?.Value,
		};
	}

	public override string ToString()
	{
		return base.ToString();
	}
}

record OneOfValue : SingleValue
{
	public OneOfPropertyType PropertyType => (OneOfPropertyType)ActualType;
	public override bool HasChildren => SelectedValue != null;

	public IReadOnlyList<PropertyType> PropertyTypeList => PropertyType.PropertyTypes;

	public Value SelectedValue { get; private set; } = null!;   // not nullable because constructor is always followed by SetValue

	public void SetValue(Value value)
	{
		SelectedValue = value;
	}

	public OneOfValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is OneOfPropertyType, $"'{this.GetType().Name}' property must be of type '{typeof(OneOfPropertyType).Name}'");
	}

	// custom copy constructor for deep copy : use default copy constructor of base class.
	public OneOfValue(OneOfValue other) : base(other)
	{
		SelectedValue = other.SelectedValue with { };
	}

	public override JsonNode? ToJsonNode()
	{
		if (!Specified)
			return ToNotSpecifiedJsonNode();

		return SelectedValue?.ToJsonNode();
	}

	public override bool ParseJson(JsonElement element)
	{
		static int DeserializePriority(PropertyType p) => p switch
		{
			// Since Date, DateTime & Time have ValueKind 'String' then StringValue must be attempted after date/time types.
			// all Integer (long) values fit into Number (decimal) so probably does not make sense to declare both Integer & Number in OneOfValue.
			SimplePropertyType simplePropertyType => simplePropertyType.Type switch
			{
				SimpleTypes.Integer => 1,
				SimpleTypes.Number => 2,
				SimpleTypes.Date => 3,
				SimpleTypes.Time => 4,
				SimpleTypes.DateTime => 5,
				_ => 10
			},
			_ => 100
		};

		var present = false;

		var ptypes = PropertyTypeList.OrderBy(p => DeserializePriority(p));

		foreach (var ptype in ptypes)
		{
			var v = ValueFactory.GetOneOfValue(this, ptype);

			if (v.ParseJson(element))
			{
				SelectedValue = v;
				present = true;
				break;
			}
		}

		if (!present)
			SelectedValue = ValueFactory.GetOneOfValue(this, PropertyTypeList[0]);

		return MarkSpecified(present);
	}


	public override XElement? SoapSerializeSingle(XElement parent)
	{
		if (!Specified)
			return null;

		SelectedValue.SoapSerializeEx(parent, SelectedValue.ActualType.QualifiedName != null && !PropertyType.IsSoapBaseType(SelectedValue.ActualType));
		return null; // return value is useless. Only used to addType which is already handled here.
	}

	public override bool ParseXml(XElement element)
	{
		var present = false;
		PropertyType? ptype = null;

		var typeAttr = element.Attribute(XmlHelper.XNameTypeAttribute);

		if (typeAttr != null)
		{
			var type = XmlHelper.MakeXmlQualifiedName(element, typeAttr.Value);
			ptype = PropertyTypeList.FirstOrDefault(t => t.QualifiedName == type);
		}
		else if (PropertyType.SoapBaseType != null && !PropertyType.SoapBaseType.IsAbstract)
		{
			ptype = PropertyType.SoapBaseType;
		}

		if (ptype != null)
		{
			var v = ValueFactory.GetOneOfValue(this, ptype);

			if (v.ParseXml(element))
			{
				SelectedValue = v;
				present = true;
			}
			else
			{
				//TODO better handle parse error : if xsi:type was specified then parse should have succeeded.
				if (typeAttr != null)
					throw new Exception($"{nameof(ParseXml)}: {Property} - Fail to parse '{element}'");
			}
		}

		if (!present)
			SelectedValue = ValueFactory.GetOneOfValue(this, PropertyTypeList[0]);

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return base.ToString();
	}
}

record ChoiceValue : SingleValue
{
	public bool IsArray => Property.IsPropertyOrActualTypeArray;

	public ChoicePropertyType PropertyType => (ChoicePropertyType)ActualType;
	public IReadOnlyList<Property> PropertyList => PropertyType.Properties;

	public override bool HasChildren => SelectedValues.Count > 0;

	public List<Value> SelectedValues = [];

	public Value? SelectedValue => IsArray ? null : SelectedValues.FirstOrDefault();

	public void SetValue(Value value)
	{
		SelectedValues.Clear();
		AddValue(value);
	}

	public void AddValue(Value value)
	{
		SelectedValues.Add(value);
	}

	public void RemoveValue(Value value)
	{
		SelectedValues.Remove(value);
	}

	public ChoiceValue(Property property, bool isArrayItem, Value? parent) : base(property, isArrayItem, parent)
	{
		Debug.Assert(property.ActualType is ChoicePropertyType, $"'{this.GetType().Name}' property must be of type '{typeof(ChoicePropertyType).Name}'");
	}

	// custom copy constructor for deep copy : use default copy constructor of base class.
	public ChoiceValue(ChoiceValue other) : base(other)
	{
		SelectedValues = other.SelectedValues.CloneList();
	}

	public override JsonNode? ToJsonNode()
	{
		throw new Exception($"{nameof(ToJsonNode)} is not handled for {nameof(ChoiceValue)}");
	}

	public override bool ParseJson(JsonElement element)
	{
		throw new Exception($"{nameof(ToJsonNode)} is not handled for {nameof(ChoiceValue)}");
	}

	public override XElement? SoapSerializeSingle(XElement parent)
	{
		if (!Specified)
			return null;

		foreach (var v in SelectedValues)
		{
			var inner = parent;

			if (v is ChoiceValue)
				inner = v.AppendXmlElement(parent);

			v.SoapSerialize(inner);
		}

		return null;    // return value is useless. Only used to addType in case of OneOfType so it does not apply to ChoiceValue.
	}

	public override bool ParseXml(XElement element)
	{
		SelectedValues.Clear();

		var present = false;

		foreach (var e in element.Elements())
		{
			var name = e.Name.ToXmlQualifiedName();
			var prop = PropertyList.FirstOrDefault(p => p.QualifiedName == name);

			if (prop != null)
			{
				var v = ValueFactory.GetChoiceValue(this, prop, IsArray);
				if (!v.ParseXml(e))
				{
					//TODO better handle parse error
					//stack overflow but why? - throw new Exception($"{MethodBase.GetCurrentMethod()}: {this} / {v} - fail to parse '{e}'");
					throw new Exception($"{nameof(ParseXml)}: {Property} / {v.Property} - Fail to parse '{e}'");
				}

				AddValue(v);
				present = true;

				if (!IsArray)
					break;
			}
		}

		if (!present && !IsArray)
			this.SetValue(ValueFactory.GetChoiceValue(this, PropertyType.Properties[0], false));

		return MarkSpecified(present);
	}

	public override string ToString()
	{
		return base.ToString();
	}
}

record ArrayValue : Value
{
	public override bool HasChildren => Values.Count > 0;

	// In case of Array of OneOfPropertyType  then Values are not OneOfValue (they are e.g. ObjectValue). Change to have list of OneOfValue? 
	public List<Value> Values { get; } = [];

	public void AddValue(Value value)
	{
		Values.Add(value);
	}

	public void RemoveValue(Value value)
	{
		Values.Remove(value);
	}

	public ArrayValue(Property property, Value? parent) : base(property, false, parent)
	{ }

	// custom copy constructor for deep copy : use default copy constructor of base class.
	public ArrayValue(ArrayValue other) : base(other)
	{
		Values = other.Values.CloneList();
	}

	public override JsonNode? ToJsonNode()
	{
		if (!Specified || Values.Count == 0)
			return null;

		return new JsonArray(Values.Select(v => v.ToJsonNode()).ToArray());
	}

	public override bool ParseJson(JsonElement element)
	{
		Values.Clear();

		if (element.ValueKind == JsonValueKind.Array)
		{
			foreach (var e in element.EnumerateArray())
			{
				var v = ValueFactory.MakeArrayItem(this);

				if (v.ParseJson(e))
				{
					Values.Add(v);
				}
				else
				{
					//TODO better handle parse error
					throw new Exception($"{nameof(ParseJson)}: {Property} - Fail to parse '{e}'");
				}
			}
		}

		return true;
	}

	public void SoapSerializeArray(XElement parent)
	{
		if (!Specified || Values.Count == 0)
			return;

		if (ActualType.SoapArrayAsList)
		{
			StringBuilder sb = new();

			foreach (var v in Values)
			{
				if (v is BasicValue basicValue)
				{
					sb.Append(basicValue.SerializeToString());
					sb.Append(' ');
				}
				else
				{
					throw new Exception($"Value {v} is not handled as Soap list");
				}
			}

			--sb.Length;    // remove last space

			SetXmlValue(parent, sb.ToString());
		}
		else
		{
			bool addOneOfType = false;
			XmlQualifiedName? soapBaseTypeName = null;

			if (ActualType is OneOfPropertyType oneOfPropertyType)
			{
				addOneOfType = true;
				soapBaseTypeName = oneOfPropertyType.SoapBaseType?.QualifiedName;
			}

			foreach (var v in Values)
			{
				v.SoapSerializeEx(parent, addOneOfType && v.ActualType.QualifiedName != null && v.ActualType.QualifiedName != soapBaseTypeName);
			}
		}
	}

	public override bool ParseXml(XElement element)
	{
		//Note: Specified is always true for ArrayValue, see Value constructor / HandleEmptyAsNull
		Values.Clear();

		if (ActualType.SoapArrayAsList)
		{
			var e = element.Element(Property.XName);
			if (e != null)
			{
				string[] texts = e.Value.Split(' ');

				foreach (string text in texts)
				{
					var v = ValueFactory.MakeArrayItem(this);

					if (v is BasicValue basicValue)
					{
						if (basicValue.ParseString(text))
						{
							Values.Add(v);
						}
					}
					else
					{
						throw new Exception($"{nameof(ParseXml)}: {v} cannot parse Soap list (not {nameof(BasicValue)})");
					}
				}
			}
		}
		else
		{
			var xPropertyName = XmlHelper.ToXName(Property.QualifiedName);

			foreach (var e in element.Elements())
			{
				if (e.Name != xPropertyName)
					continue;

				var v = ValueFactory.MakeArrayItem(this);

				if (v.ParseXml(e))
				{
					Values.Add(v);
				}
				else
				{
					//TODO better handle parse error
					// cause a stack overflow but why? - throw new Exception($"{nameof(ParseXml)}: {this} fail to parse '{e}'");
					throw new Exception($"{nameof(ParseXml)}: {v} - Fail to parse '{e}'");
				}
			}
		}

		return true;
	}

	public void ParseStrings(IEnumerable<string> texts)
	{
		Values.Clear();

		foreach (string text in texts)
		{
			var v = ValueFactory.MakeArrayItem(this);
			if (v is BasicValue basicValue)
			{
				if (basicValue.ParseString(text))
				{
					Values.Add(v);
				}
			}
		}
	}

	public override string ToString()
	{
		return base.ToString();
	}
}
