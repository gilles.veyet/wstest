﻿using System.Reflection;
using System.Xml;
using WSTest.Data;
using WSTest.Helpers;

namespace WSTest
{
	static class ValueFactory
	{
		public static Value MakeValueFromProperty(Property property)
		{
			return MakeValueFromPropertyInternal(property, null);
		}

		public static Value MakeValueFromPropertyInternal(Property property, Value? parent)
		{
			ActualPropertyType actualType = property.ActualType;

			bool isArray = property.IsPropertyOrActualTypeArray && actualType is not ChoicePropertyType;

			if (isArray)
			{
				if (actualType.SoapAnyOfPropertyTypes != null)
				{
					// Do not use ",SoapBaseType = actualType};" because SoapBaseType.SoapAnyOfPropertyTypes must be null as in all items of SoapAnyOfPropertyTypes.
					var ptype = new OneOfPropertyType(actualType.SoapAnyOfPropertyTypes) { IsArray = actualType.IsArray, SoapBaseType = actualType with { SoapAnyOfPropertyTypes = null } };
					property = property with { Type = ptype };
				}

				return new ArrayValue(property, parent);
			}
			else
				return MakeValueFromPropertyBase(property, false, parent);
		}


		public static Value MakeArrayItem(ArrayValue arrayValue)
		{
			var property = arrayValue.Property;

			var value = MakeValueFromPropertyBase(property, true, arrayValue);
			value.Specified = true;
			return value;
		}

		public static Value AddArrayItem(ArrayValue arrayValue)
		{
			var value = MakeArrayItem(arrayValue);
			arrayValue.AddValue(value);
			return value;
		}

		public static Value AddChoiceItem(ChoiceValue choiceValue, Property selectedProperty)
		{
			var value = GetChoiceValue(choiceValue, selectedProperty, true);
			value.Specified = true;
			choiceValue.AddValue(value);
			return value;
		}

		public static Value AddArrayOneOfItem(ArrayValue arrayValue, PropertyType propertyType)
		{
			var property = arrayValue.Property with { Type = propertyType };
			var value = MakeValueFromPropertyBase(property, true, arrayValue);
			value.Specified = true;
			arrayValue.AddValue(value);

			return value;
		}

		private static Value MakeValueFromPropertyBase(Property property, bool isArrayItem, Value? parent)
		{
			var ptype = property.ActualType;

			if (ptype.SoapAnyOfPropertyTypes != null)
			{
				var oneOfPropertyType = new OneOfPropertyType(ptype.SoapAnyOfPropertyTypes) { IsArray = ptype.IsArray, SoapBaseType = ptype with { SoapAnyOfPropertyTypes = null } };
				return MakeOneOfValue(property, oneOfPropertyType, isArrayItem, parent);
			}

			return ptype switch
			{
				SimplePropertyType simplePropertyType => MakeSimpleValue(simplePropertyType, property, isArrayItem, parent),
				StringPropertyType => new StringValue(property, isArrayItem, parent),
				StringBase64PropertyType => new StringBase64Value(property, isArrayItem, parent),
				StringEnumPropertyType => new StringEnumValue(property, isArrayItem, parent),
				IntEnumPropertyType => new IntEnumValue(property, isArrayItem, parent),
				ObjectPropertyType objectPropertyType => MakeObjectValue(property, objectPropertyType, isArrayItem, parent),
				ChoicePropertyType choicePropertyType => MakeChoiceValue(property, choicePropertyType, isArrayItem, parent),
				OneOfPropertyType oneOfPropertyType => MakeOneOfValue(property, oneOfPropertyType, isArrayItem, parent),
				_ => throw new Exception($"Unknown PropertyType '{ptype.GetType().Name}' in property '{property}'")
			};
		}

		private static Value MakeSimpleValue(SimplePropertyType simplePropertyType, Property property, bool isArrayItem, Value? parent)
		{
			var value = simplePropertyType.MakeValue(property, isArrayItem, parent);

			string propertyChain = value.GetPropertyChain();

			foreach (var fill in Program.Settings.PropertyFills)
			{
				if (fill.Pattern.RegexProperty.IsMatch(propertyChain))
				{
					if (value is StringValue stringValue)
					{
						stringValue.Value = fill.Value;
					}
					else if (value is IntegerValue longValue)
					{
						int? val = fill.DecimalValue;

						if (val != null)
							longValue.Value = (long)val;
						else
							TraceHelper.WriteWarning($"{MethodBase.GetCurrentMethod()}: {nameof(PropertyFill)} '{fill}' value does not match Property:'{property}'");
					}
					else
					{
						TraceHelper.WriteWarning($"{MethodBase.GetCurrentMethod()}: {nameof(PropertyFill)} '{fill}' not handled for Property:'{property}'");
					}
				}
			}

			return value;
		}

		private static ObjectValue MakeObjectValue(Property property, ObjectPropertyType propertyType, bool isArrayItem, Value? parent)
		{
			var value = new ObjectValue(property, isArrayItem, parent);

			if (value.Specified)
			{
				bool recurse = !isArrayItem && parent != null && parent.IsInfiniteLooping(property.ActualType.QualifiedName);

				if (recurse)
				{
					value.Specified = false;
					TraceHelper.WriteWarning($"{MethodBase.GetCurrentMethod()}: Reset Specified to avoid infinite loop Property:'{property}' - PropertyType:{propertyType}");
				}
			}

			return value;
		}

		private static ChoiceValue MakeChoiceValue(Property property, ChoicePropertyType propertyType, bool isArrayItem, Value? parent)
		{
			var value = new ChoiceValue(property, isArrayItem, parent);
			if (!value.IsArray)
			{
				value.SetValue(GetChoiceValue(value, propertyType.Properties[0], false));
			}

			return value;
		}

		public static Value GetChoiceValue(ChoiceValue choiceValue, Property selectedProperty, bool isArrayItem)
		{
			return MakeValueFromPropertyBase(selectedProperty, isArrayItem, choiceValue);
		}

		private static List<Value> ExpandProperties(IEnumerable<Property> properties, ObjectValue parent)
		{
			return properties.Select(p => MakeValueFromPropertyInternal(p, parent)).ToList();
		}

		private static OneOfValue MakeOneOfValue(Property property, OneOfPropertyType propertyType, bool isArrayItem, Value? parent)
		{
			property = property with { Type = propertyType };
			var value = new OneOfValue(property, isArrayItem, parent);

			value.SetValue(GetOneOfValue(value, propertyType.PropertyTypes[0]));
			return value;
		}

		public static Value GetOneOfValue(OneOfValue oneOfValue, PropertyType propertyType)
		{
			var property = oneOfValue.Property with { Type = propertyType };
			return MakeValueFromPropertyBase(property, false, oneOfValue);
		}

		public static Value SetOneOfValue(OneOfValue oneOfValue, PropertyType propertyType)
		{
			var value = GetOneOfValue(oneOfValue, propertyType);
			oneOfValue.SetValue(value);
			return value;
		}

		public static IEnumerable<StringValue> MakeValuesFromSimpleMappings(List<KeyValuePair<string, ProtectableValue>> mappings)
		{
			return MakeValuesFromKeyValues(mappings.Select(kv => new KeyValuePair<string, string>(kv.Key, kv.Value.Value)).ToList());
		}

		public static IEnumerable<StringValue> MakeValuesFromKeyValues(List<KeyValuePair<string, string>> mappings)
		{
			return mappings.Select(kv =>
			   {
				   var typ = new StringPropertyType();
				   var p = new Property(new XmlQualifiedName(kv.Key), typ, false);
				   var v = new StringValue(p, false, null)
				   {
					   Value = kv.Value,
					   Specified = true
				   };

				   return v;
			   }).ToList();
		}

		public static void UpdateSimpleMappingsWithValues(Dictionary<string, ProtectableValue> mappings, IEnumerable<StringValue> values)
		{
			foreach (var stringValue in values)
			{
				var map = mappings[stringValue.Property.Name];
				map.Value = stringValue.Value;
			}
		}

	}
}
