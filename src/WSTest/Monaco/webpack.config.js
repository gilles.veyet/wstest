const path = require('path');
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');

module.exports = {
	mode: process.env.NODE_ENV,
	entry: './index.js',
	mode: 'development',
	optimization: {
		minimize: false
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.ttf$/,
				type: 'asset/resource',
			}
		]
	},
	plugins: [
		new MonacoWebpackPlugin({
            globalAPI : true,
			languages: ['xml', 'json' ]
		})
	]
};
