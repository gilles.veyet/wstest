﻿using System.Xml;
using System.Xml.Linq;

namespace WSTest.Extensions
{
	internal static class XNameExtensions
	{
		public static XmlQualifiedName ToXmlQualifiedName(this XName xname) => new(xname.LocalName, xname.NamespaceName);
	}
}
