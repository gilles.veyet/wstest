﻿namespace WSTest.Extensions;

internal static class StringExtensions
{
	public static string? NullIfEmpty(this string s)
	{
		return string.IsNullOrEmpty(s) ? null : s;
	}
}
