﻿using System.Xml.Linq;

namespace WSTest.Extensions
{
	internal static class XNodeExtensions
	{
		public static string InnerXml(this XNode node) => node.ToString();
	}
}
