﻿namespace WSTest.Extensions
{
	internal static class TypeExtensions
	{
		/// <summary>
		/// Returns the type name. 
		/// If this is a generic type, appends the list of generic type arguments between angle brackets.
		/// (Does not account for embedded / inner generic arguments.)
		/// From https://stackoverflow.com/questions/2581642/how-do-i-get-the-type-name-of-a-generic-type-argument
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns>System.String.</returns>
		public static string GetFormattedName(this Type type)
		{
			if (type.IsGenericType)
			{
				string genericArguments = string.Join(",", type.GetGenericArguments().Select(GetFormattedName));
				string typeName = type.Name;
				int index = typeName.IndexOf('`');
				string name = index > 0 ? typeName[..index] : typeName;
				return $"{name}<{genericArguments}>";
			}
			else
				return type.Name;
		}
	}
}
