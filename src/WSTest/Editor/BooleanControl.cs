﻿namespace WSTest.Editor
{
	class BooleanControl : CheckBox, IPropertyControl
	{
		readonly BooleanValue BooleanValue;

		public BooleanControl(BooleanValue booleanValue)
		{
			this.AutoSize = true;

			this.BooleanValue = booleanValue;
			RefreshValue();

			this.CheckedChanged += BooleanControl_CheckedChanged;
		}

		public void RefreshValue()
		{
			this.Checked = BooleanValue.Value;
		}

		private void BooleanControl_CheckedChanged(object? sender, EventArgs e)
		{
			this.BooleanValue.Value = this.Checked;
		}
	}
}
