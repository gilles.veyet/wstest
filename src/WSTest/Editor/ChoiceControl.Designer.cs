﻿namespace WSTest.Editor
{
	partial class ChoiceControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.comboProperty = new System.Windows.Forms.ComboBox();
			this.btnAdd = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// comboProperty
			// 
			this.comboProperty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboProperty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboProperty.FormattingEnabled = true;
			this.comboProperty.Location = new System.Drawing.Point(25, 0);
			this.comboProperty.Name = "comboProperty";
			this.comboProperty.Size = new System.Drawing.Size(235, 23);
			this.comboProperty.TabIndex = 1;
			this.comboProperty.SelectedIndexChanged += new System.EventHandler(this.comboProperty_SelectedIndexChanged);
			this.comboProperty.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.comboProperty_Format);
			// 
			// btnAdd
			// 
			this.btnAdd.AutoSize = true;
			this.btnAdd.FlatAppearance.BorderSize = 0;
			this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAdd.Image = global::WSTest.Properties.Resources.Add;
			this.btnAdd.Location = new System.Drawing.Point(0, 0);
			this.btnAdd.Margin = new System.Windows.Forms.Padding(0);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(22, 22);
			this.btnAdd.TabIndex = 4;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// ChoiceControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.comboProperty);
			this.Name = "ChoiceControl";
			this.Size = new System.Drawing.Size(260, 25);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private ComboBox comboProperty;
		private Button btnAdd;
	}
}
