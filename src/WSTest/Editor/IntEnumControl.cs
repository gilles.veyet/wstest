﻿namespace WSTest.Editor
{
	class IntEnumControl : ComboBox, IPropertyControl
	{
		readonly IntEnumValue EnumValue;

		public IntEnumControl(IntEnumValue intEnumValue)
		{
			MouseWheel += (o, e) => ((HandledMouseEventArgs)e).Handled = true;  // prevent  changing value with mouse wheel.

			EnumValue = intEnumValue;

			DropDownStyle = ComboBoxStyle.DropDownList;

			Items.AddRange(EnumValue.PropertyType.Values.Select(n => n.ToString()).ToArray());
			RefreshValue();

			SelectedIndexChanged += StringEnumControl_SelectedIndexChanged;
		}

		public void RefreshValue()
		{
			SelectedItem = EnumValue.Value.ToString();
		}

		private void StringEnumControl_SelectedIndexChanged(object? sender, EventArgs e)
		{
			EnumValue.Value = int.Parse((string)this.SelectedItem!);
		}
	}
}
