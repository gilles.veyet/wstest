﻿namespace WSTest.Editor
{
	internal class DateTimeControl : DateTimePicker, IPropertyControl
	{
		readonly Value _Value;

		public DateTimeControl(DateTimeValue dateTimeValue)
		{
			_Value = dateTimeValue;

			var culture = System.Threading.Thread.CurrentThread.CurrentCulture; // this is the culture defined in regional settings whereas CurrentUICulture is Windows user language. 

			Format = DateTimePickerFormat.Custom;
			CustomFormat = $"{culture.DateTimeFormat.ShortDatePattern} {culture.DateTimeFormat.LongTimePattern}";

			this.MinDate = DateTime.MinValue;
			this.MaxDate = DateTime.MaxValue;

			RefreshValue();

			this.ValueChanged += (sender, e) => { dateTimeValue.Value = this.Value; };
		}

		public DateTimeControl(DateValue dateOnlyValue)
		{
			_Value = dateOnlyValue;

			this.Format = DateTimePickerFormat.Short;
			this.MinDate = DateTime.MinValue;
			this.MaxDate = DateTime.MaxValue;

			RefreshValue();

			this.ValueChanged += (sender, e) => { dateOnlyValue.Value = DateOnly.FromDateTime(this.Value); };
		}

		public DateTimeControl(TimeValue timeOnlyValue)
		{
			_Value = timeOnlyValue;

			this.Format = DateTimePickerFormat.Short;
			this.MinDate = DateTime.MinValue;
			this.MaxDate = DateTime.MaxValue;

			RefreshValue();

			this.ValueChanged += (sender, e) => { timeOnlyValue.Value = TimeOnly.FromDateTime(this.Value); };
		}

		public void RefreshValue()
		{
			switch (_Value)
			{
				case DateTimeValue dateTimeValue:
					this.Value = dateTimeValue.Value;
					break;

				case DateValue dateOnlyValue:
					this.Value = dateOnlyValue.Value.ToDateTime(TimeOnly.MinValue);
					break;

				case TimeValue timeOnlyValue:
					this.Value = DateOnly.MinValue.ToDateTime(timeOnlyValue.Value);
					break;
			}
		}

	}

}
