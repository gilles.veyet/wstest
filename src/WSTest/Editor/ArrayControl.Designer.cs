﻿namespace WSTest.Editor
{
	partial class ArrayControl : UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			btnAdd = new Button();
			txtProperty = new TextBox();
			comboItemType = new ComboBox();
			SuspendLayout();
			// 
			// btnAdd
			// 
			btnAdd.AutoSize = true;
			btnAdd.FlatAppearance.BorderSize = 0;
			btnAdd.FlatStyle = FlatStyle.Flat;
			btnAdd.Image = Properties.Resources.Add;
			btnAdd.Location = new Point(0, 0);
			btnAdd.Margin = new Padding(0);
			btnAdd.Name = "btnAdd";
			btnAdd.Size = new Size(22, 22);
			btnAdd.TabIndex = 3;
			btnAdd.Click += btnAdd_Click;
			// 
			// txtProperty
			// 
			txtProperty.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtProperty.BorderStyle = BorderStyle.None;
			txtProperty.Location = new Point(25, 3);
			txtProperty.Name = "txtProperty";
			txtProperty.ReadOnly = true;
			txtProperty.Size = new Size(232, 16);
			txtProperty.TabIndex = 2;
			// 
			// comboItemType
			// 
			comboItemType.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			comboItemType.DropDownStyle = ComboBoxStyle.DropDownList;
			comboItemType.FormattingEnabled = true;
			comboItemType.Location = new Point(25, 0);
			comboItemType.Name = "comboItemType";
			comboItemType.Size = new Size(235, 23);
			comboItemType.TabIndex = 4;
			comboItemType.Format += comboItemType_Format;
			// 
			// ArrayControl
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			Controls.Add(comboItemType);
			Controls.Add(btnAdd);
			Controls.Add(txtProperty);
			Name = "ArrayControl";
			Size = new Size(260, 25);
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private Button btnAdd;
		private TextBox txtProperty;
		private ComboBox comboItemType;
	}
}
