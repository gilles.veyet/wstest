﻿using System.Diagnostics;

namespace WSTest.Editor
{
	internal delegate void OneOfItemSelectEventHandler(OneOfValue value, PropertyType propertyType);

	internal partial class OneOfItemControl : UserControl, IPropertyControl
	{
		public bool ExtendRight => true;

		public event OneOfItemSelectEventHandler? OnSelect;

		readonly OneOfValue _OneOfValue;

		public OneOfItemControl(OneOfValue value)
		{
			MouseWheel += (sender, ev) =>
			{
				Debug.WriteLine($"MouseWheel {ev.Location}");
			};

			_OneOfValue = value;
			InitializeComponent();
			this.comboItemType.Items.AddRange([.. value.PropertyTypeList]);

			// Does not trigger OnSelect event as is is null in constructor but children property rows will be created in PropertyEditor.AddGridChildrenValues.
			RefreshValue();
		}

		public void RefreshValue()
		{
			this.comboItemType.SelectedItem = _OneOfValue.SelectedValue?.Property.Type;
		}

		private void comboItemType_SelectedIndexChanged(object sender, EventArgs e)
		{
			var propertyType = comboItemType.SelectedItem as PropertyType;

			if (propertyType != null)
				OnSelect?.Invoke(_OneOfValue, propertyType);
		}

		private void comboItemType_Format(object sender, ListControlConvertEventArgs e)
		{
			e.Value = (e.ListItem as PropertyType)!.Name;
		}
	}
}
