﻿using System.Diagnostics;

namespace WSTest.Editor
{

	internal interface IPropertyRowContainer
	{
		public List<PropertyRow> Children { get; }

		public IPropertyRowContainer? Parent { get; }

		public int Depth { get; }
	}

	internal class PropertyRowContainer : IPropertyRowContainer
	{
		public List<PropertyRow> Children { get; } = [];

		public IPropertyRowContainer? Parent => null;

		public int Depth => 0;
	}

	internal delegate void ExpandChangedEventHandler(PropertyRow propertyRow, bool expand);
	internal delegate void AddArrayItemEventHandler(PropertyRow propertyRow, PropertyType? propertyType = null);
	internal delegate void RemoveArrayItemEventHandler(PropertyRow propertyRow);
	internal delegate void SelectOneOfItemEventHandler(PropertyRow propertyRow);
	internal delegate void SelectChoiceEventHandler(PropertyRow propertyRow);
	internal delegate void AddItemChoiceEventHandler(PropertyRow propertyRow, Value value);

	internal enum RowTypes
	{
		Simple,
		ArrayItem,
		OneOfItem,
		Choice,
	}


	[Flags]
	internal enum RowComponents
	{
		Specified = 1,
		Expand = 2,
		Remove = 4,
		Label = 8,
	}

	internal class PropertyRow : IPropertyRowContainer
	{
		readonly static Padding LabelPadding = new(5, 0, 0, 0);

		public bool Expanded => Children.Count > 0;

		public event ExpandChangedEventHandler? ExpandChanged;
		public event AddArrayItemEventHandler? OnAddArrayItem;
		public event RemoveArrayItemEventHandler? OnRemoveArrayItem;
		public event SelectOneOfItemEventHandler? OnSelectOneOfItem;
		public event SelectChoiceEventHandler? OnSelectChoice;
		public event AddItemChoiceEventHandler? OnAddItemChoice;

		public List<Control> Controls = [];
		public List<PropertyRow> Children { get; } = [];
		public IPropertyRowContainer Parent { get; }
		public int Depth { get; }
		public int RowIndex { get; set; } = 0;   // See PropertyEditor ResumePanelLayout

		public Value RowValue { get; set; }
		public Control PropertyControl { get; set; } = null!;
		public Button? ButtonExpand { get; set; }


		// Simple row
		public Label? LabelRow { get; set; }
		public CheckBox? CheckBoxSpecified { get; set; }

		//ArrayItem Row
		public PropertyRow ParentRow => (PropertyRow)Parent;
		public Button? ButtonRemove { get; set; }

		//OneOf Row
		public OneOfValue RowOneOfValue => (OneOfValue)RowValue;

		//Choice Row
		public ChoiceValue RowChoiceValue => (ChoiceValue)RowValue;

		public RowTypes RowType { get; }            // for debug purpose only.
		public RowComponents Components { get; }

		public PropertyRow(Value value, IPropertyRowContainer parent, RowTypes propertyRowType, RowComponents components)
		{
			this.Components = components;
			this.RowValue = value;
			this.Parent = parent;
			this.Depth = parent.Depth + 1;
			this.RowType = propertyRowType;

			Init();
		}


		private void Init()
		{
			int rowHeight = PropertyEditor.RowHeight;

			var prop = RowValue.Property;

			if (Components.HasFlag(RowComponents.Expand))
			{
				ButtonExpand = new Button()
				{
					Size = new System.Drawing.Size(PropertyEditor.WidthButtonExpand, PropertyEditor.RowHeight),
					FlatStyle = FlatStyle.Flat,
				};

				ButtonExpand.FlatAppearance.BorderSize = 0;

				Controls.Add(ButtonExpand);

				UpdateExpandButton();

				ButtonExpand.Click += ButtonExpand_Click;
			}

			if (Components.HasFlag(RowComponents.Label))
			{
				LabelRow = new Label()
				{
					Width = PropertyEditor.WidthLabel,
					AutoEllipsis = true,
					Padding = LabelPadding,
					Height = rowHeight - 5,
					TextAlign = ContentAlignment.MiddleLeft,
					Text = prop.Name,
					BackColor = Color.AntiqueWhite,
				};

				Controls.Add(LabelRow);
			}

			// Hide specified checkbox for string values unless value is required (keep as indicator).
			bool hideSpecified = RowValue.Specified && !prop.Required && RowValue.HandleEmptyAsNull;
			if (Components.HasFlag(RowComponents.Specified) && !hideSpecified)
			{
				var locked = RowValue.Specified && prop.Mandatory;

				CheckBoxSpecified = new CheckBox()
				{
					Checked = RowValue.Specified,
					Size = new System.Drawing.Size(PropertyEditor.WidthSpecified, rowHeight),
					Enabled = !locked,
				};

				Controls.Add(CheckBoxSpecified);

				CheckBoxSpecified.CheckedChanged += CheckBoxSpecified_CheckedChanged;
			}

			if (Components.HasFlag(RowComponents.Remove))
			{
				ButtonRemove = new Button()
				{
					Size = new System.Drawing.Size(PropertyEditor.WidthButtonRemove, rowHeight),
					UseVisualStyleBackColor = true,
					FlatStyle = FlatStyle.Flat,
					Image = global::WSTest.Properties.Resources.Delete,
				};

				ButtonRemove.FlatAppearance.BorderSize = 0;
				Controls.Add(ButtonRemove);

				ButtonRemove.Click += (sender, e) =>
				{
					OnRemoveArrayItem?.Invoke(this);
				};
			}

			PropertyControl = GetPropertyControl(RowValue);
			Controls.Add(PropertyControl);

			PropertyControl.Enabled = RowValue.Specified;
		}

		private void CheckBoxSpecified_CheckedChanged(object? sender, EventArgs e)
		{
			if (sender is not CheckBox checkbox)
				return;

			bool check = checkbox.Checked;
			PropertyControl.Enabled = check;
			RowValue.Specified = check;

			OnExpand(check);
		}

		private void UpdateExpandButton()
		{
			Debug.Assert(ButtonExpand != null);

			bool visible = RowValue.Specified && RowValue.HasChildren;
			ButtonExpand.Visible = visible;
			this.RowIndex = -1; // force redraw.

			//TraceHelper.WriteVerbose($"UpdateExpandButton Visible{visible} Expanded:{Expanded}");

			if (visible)
				ButtonExpand.Image = Expanded ? global::WSTest.Properties.Resources.Collapse : global::WSTest.Properties.Resources.Expand;
		}

		public void UpdateParentExpand()
		{
			if (Parent is PropertyRow propertyRow)
				propertyRow.UpdateExpandButton();
		}

		private void ButtonExpand_Click(object? sender, EventArgs e)
		{
			OnExpand(!Expanded);
		}

		private Control GetPropertyControl(Value value)
		{
			return value switch
			{
				StringValue stringValue => new StringControl(stringValue),
				StringBase64Value stringBase64Value => new StringControl(stringBase64Value),
				StringEnumValue stringEnumValue => new StringEnumControl(stringEnumValue),
				IntEnumValue intEnumValue => new IntEnumControl(intEnumValue),
				BooleanValue booleanValue => new BooleanControl(booleanValue),
				IntegerValue longValue => new IntegerControl(longValue),
				NumberValue decimalValue => new NumberControl(decimalValue),
				DateTimeValue dateTimeValue => new DateTimeControl(dateTimeValue),
				DateValue dateOnlyValue => new DateTimeControl(dateOnlyValue),
				TimeValue timeOnlyValue => new DateTimeControl(timeOnlyValue),
				DurationValue timeSpanValue => new TimeSpanControl(timeSpanValue),
				ObjectValue objectValue => new ObjectPropertyControl(objectValue),
				OneOfValue oneOfValue => GetOneOfItemControl(oneOfValue),
				ChoiceValue choiceValue => GetChoiceControl(choiceValue),
				ArrayValue arrayValue => GetArrayItemControl(arrayValue),
				_ => throw new Exception($"Unknow value type '{value}'")
			};
		}

		public ChoiceControl GetChoiceControl(ChoiceValue value)
		{
			var control = new ChoiceControl(value);
			control.OnSelect += ChoiceControl_OnSelect;
			control.OnAddItem += ChoiceControl_OnAddItem;
			return control;
		}

		private void ChoiceControl_OnAddItem(ChoiceValue choiceValue, Property selectedProperty)
		{
			var value = ValueFactory.AddChoiceItem(choiceValue, selectedProperty);
			OnAddItemChoice?.Invoke(this, value);
		}

		private void ChoiceControl_OnSelect(ChoiceValue choiceValue, Property selectedProperty)
		{
			choiceValue.SetValue(ValueFactory.GetChoiceValue(choiceValue, selectedProperty, false));
			OnSelectChoice?.Invoke(this);
		}

		public OneOfItemControl GetOneOfItemControl(OneOfValue value)
		{
			var control = new OneOfItemControl(value);
			control.OnSelect += OneOfItemControl_OnSelect;
			return control;
		}

		private void OneOfItemControl_OnSelect(OneOfValue value, PropertyType propertyType)
		{
			ValueFactory.SetOneOfValue(value, propertyType);
			OnSelectOneOfItem?.Invoke(this);
		}

		public ArrayControl GetArrayItemControl(ArrayValue value)
		{
			var control = new ArrayControl(value);
			control.OnAddItem += () => OnAddArrayItem?.Invoke(this);
			control.OnAddOneOfItem += (propertyType) => OnAddArrayItem?.Invoke(this, propertyType);

			return control;
		}


		public void OnExpand(bool value)
		{
			ExpandChanged?.Invoke(this, value);
			UpdateExpandButton();
		}

		public void SetYPos(int y)
		{
			foreach (var ctl in Controls)
				ctl.Top = y;
		}

		public override string ToString()
		{
			return $"{RowType} {RowValue?.Property}";
		}
	}
}
