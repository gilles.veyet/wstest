﻿namespace WSTest.Editor
{
	internal partial class ObjectPropertyControl : UserControl, IPropertyControl
	{
		public bool ExtendRight => true;

		public ObjectPropertyControl(Value value)
		{
			InitializeComponent();
			var prop = value.Property;
			this.txtProperty.Text = prop.ActualType.Name;
		}

		public void RefreshValue()
		{
			// nothing to do.
		}

	}
}
