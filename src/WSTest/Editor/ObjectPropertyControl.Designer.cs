﻿namespace WSTest.Editor
{
	partial class ObjectPropertyControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			txtProperty = new TextBox();
			SuspendLayout();
			// 
			// txtProperty
			// 
			txtProperty.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtProperty.BorderStyle = BorderStyle.None;
			txtProperty.Location = new Point(0, 3);
			txtProperty.Name = "txtProperty";
			txtProperty.ReadOnly = true;
			txtProperty.Size = new Size(291, 16);
			txtProperty.TabIndex = 0;
			txtProperty.TabStop = false;
			// 
			// ObjectPropertyControl
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			Controls.Add(txtProperty);
			Name = "ObjectPropertyControl";
			Size = new Size(291, 23);
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private TextBox txtProperty;
	}
}
