﻿namespace WSTest.Editor
{
	internal class NumberControl : NumericUpDown, IPropertyControl
	{
		readonly NumberValue DecimalValue;

		public NumberControl(NumberValue decimalValue)
		{
			MouseWheel += (o, e) => ((HandledMouseEventArgs)e).Handled = true;  // prevent  changing value with mouse wheel.

			this.DecimalValue = decimalValue;
			this.Minimum = decimal.MinValue;
			this.Maximum = decimal.MaxValue;
			this.TextAlign = HorizontalAlignment.Right;

			RefreshValue();

			this.ValueChanged += NumberControl_ValueChanged;
		}


		// override UpdateEditText because it calls a private function GetNumberText to format the text which does this
		//    text = num.ToString($"{(ThousandsSeparator ? "N" : "F")}{DecimalPlaces}", CultureInfo.CurrentCulture);
		//  here we do not wany to want to display all decimal digits 
		// see https://learn.microsoft.com/en-us/dotnet/api/system.windows.forms.numericupdown , there is link to open NumericUpDown.cs source on github.
		protected override void UpdateEditText()
		{
			Text = Value.ToString();
		}

		public void RefreshValue()
		{
			this.Value = DecimalValue.Value;
		}

		private void NumberControl_ValueChanged(object? sender, EventArgs e)
		{
			this.DecimalValue.Value = this.Value;
		}
	}
}
