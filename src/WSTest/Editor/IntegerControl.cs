﻿namespace WSTest.Editor
{

	internal class IntegerControl : NumericUpDown, IPropertyControl
	{
		readonly IntegerValue LongValue;

		public IntegerControl(IntegerValue longValue)
		{
			MouseWheel += (o, e) => ((HandledMouseEventArgs)e).Handled = true;  // prevent  changing value with mouse wheel.

			this.LongValue = longValue;

			this.TextAlign = HorizontalAlignment.Right;
			this.DecimalPlaces = 0;
			this.Minimum = long.MinValue;
			this.Maximum = long.MaxValue;

			RefreshValue();

			this.ValueChanged += IntegerControl_ValueChanged;
		}

		public void RefreshValue()
		{
			this.Value = LongValue.Value;
		}

		private void IntegerControl_ValueChanged(object? sender, EventArgs e)
		{
			this.LongValue.Value = (long)this.Value;
		}
	}

}
