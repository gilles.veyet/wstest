﻿using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using WSTest.Helpers;

namespace WSTest.Editor;

public partial class PropertyEditor : UserControl
{
	public const int ROW_HEIGHT = 28;
	public const int WIDTH_LABEL = 200;
	public const int WIDTH_SPECIFIED = 13;

	public const int WidthButtonExpand = 16;
	public const int MarginTop = 5;
	public const int MarginLeft = 5;
	public const int WidthButtonRemove = 17;    // icon is 16x16 but is not fully drawn if width is 16.
	public const int HMargin = 5;

	public static int RowHeight => Program.ScaleToDpi(ROW_HEIGHT);
	public static int WidthLabel => Program.ScaleToDpi(WIDTH_LABEL);
	public static int WidthSpecified => Program.ScaleToDpi(WIDTH_SPECIFIED);
	public static int HOffset => WidthSpecified + WidthButtonExpand + HMargin;


	PropertyRowContainer RootPropertyRowContainer = new();

	//List<PropertyRow> RootPropertyRows => RootPropertyRowContainer.Children;

	//public event EventHandler? ValueChanged;

	[Description("Show Specified checkbox"), Category("Data")]
	public bool ShowSpecifiedCheckbox { get; set; } = true;

	public PropertyEditor()
	{
		InitializeComponent();
	}

	bool IsSuspended = false;

	void SuspendPanelLayout()
	{
		Debug.Assert(!IsSuspended, $"{MethodBase.GetCurrentMethod()}: layout already suspended");
		IsSuspended = true;
		panelMain.SuspendLayout();
		TraceHelper.WriteVerbose("SuspendLayout");
	}

	void ResumePanelLayout()
	{
		Debug.Assert(IsSuspended, $"{MethodBase.GetCurrentMethod()}: layout is not suspended");
		IsSuspended = false;

		int offset = panelMain.AutoScrollPosition.Y + MarginTop;
		int index = 0;
		foreach (var pr in Traverse())
		{
			int newIndex = index++;

			if (newIndex != pr.RowIndex)
			{
				pr.SetYPos(newIndex * RowHeight + offset);
				pr.RowIndex = newIndex;
			}
		}

		panelMain.ResumeLayout();
		TraceHelper.WriteVerbose("ResumeLayout");
	}

	IEnumerable<PropertyRow> Traverse()
	{
		var stack = new Stack<PropertyRow>();

		for (int count = RootPropertyRowContainer.Children.Count, i = count - 1; i >= 0; --i)
			stack.Push(RootPropertyRowContainer.Children[i]);

		while (stack.Count != 0)
		{
			var next = stack.Pop();
			yield return next;

			for (int count = next.Children.Count, i = count - 1; i >= 0; --i)
				stack.Push(next.Children[i]);
		}
	}

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		if (DesignMode)
			BackColor = SystemColors.ControlLight;
	}

	//void OnChange()
	//{
	//	ValueChanged?.Invoke(this, new EventArgs());
	//}

	internal void Clear()
	{
		SetRootGridValues([]);
	}

	internal void Set(IEnumerable<Value> values, int expandLevel = -1)
	{
		if (values.Count() == 1)
		{
			// usually only one value, e.g. SOAP Request
			var value = values.First();

			if (!value.Specified)
				value.Specified = true;

			if (value is ObjectValue objectValue)
			{
				values = objectValue.PropertyValues;

				// Workaround for some cases when single property of request has been declared with MinOccurs=0 e.g. DTA GetOrders
				if (values.Count() == 1)
				{
					var child = values.First();

					if (!child.Specified)
					{
						child.Specified = true;
					}
				}
			}
		}

		SetRootGridValues(values, expandLevel);
	}

	public void RefreshValues()
	{
		foreach (var control in Traverse().Select(r => r.PropertyControl).OfType<IPropertyControl>())
			control.RefreshValue();
	}

	void SetRootGridValues(IEnumerable<Value> values, int expandLevel = -1)
	{
		SuspendPanelLayout();

		RootPropertyRowContainer = new PropertyRowContainer();

		panelMain.Controls.Clear();
		panelMain.VerticalScroll.Value = 0;

		AddGridValuesAndChildren(values, RootPropertyRowContainer, expandLevel);

		ResumePanelLayout();
	}

	void AddGridValuesAndChildren(IEnumerable<Value> values, IPropertyRowContainer parentRow, int expandLevel)
	{
		if (expandLevel == 0)
			return;

		foreach (var value in values)
			AddGridValueAndChildren(value, parentRow, expandLevel - 1);
	}

	void AddGridValueAndChildren(Value value, IPropertyRowContainer parentRow, int expandLevel)
	{
		var row = AddGridValue(value, parentRow);
		row.UpdateParentExpand();

		if (value.Specified)
			AddGridChildrenValues(row, expandLevel);
	}

	void AddGridChildrenValues(PropertyRow parentRow, int expandLevel)
	{
		var value = parentRow.RowValue;

		if (value is ObjectValue objectValue)
		{
			AddGridValuesAndChildren(objectValue.PropertyValues, parentRow, expandLevel);
		}
		else if (value is OneOfValue oneOfValue)
		{
			if (oneOfValue.SelectedValue != null)
				AddGridValueAndChildren(oneOfValue.SelectedValue, parentRow, expandLevel);
		}
		else if (value is ChoiceValue choiceValue)
		{
			AddGridValuesAndChildren(choiceValue.SelectedValues, parentRow, expandLevel);
		}
		else if (value is ArrayValue arrayValue)
		{
			AddGridValuesAndChildren(arrayValue.Values, parentRow, expandLevel);
		}
	}

	PropertyRow AddGridValue(Value value, IPropertyRowContainer parent)
	{
		const int top = 0;  // all new rows are added with RowIndex=0 and top=0  then they are moved in ResumePanelLayout.
		int x = MarginLeft + parent.Depth * HOffset;

		PropertyRow propertyRow;

		if (value is ChoiceValue)       // check first because it also handles array item
		{
			var flags = RowComponents.Expand | RowComponents.Label;

			if (value.IsArrayItem)
				flags |= RowComponents.Remove;
			else
				flags |= RowComponents.Specified;

			propertyRow = new PropertyRow(value, parent, RowTypes.Choice, flags);
			propertyRow.OnSelectChoice += PropertyRowChoice_OnSelect;
			propertyRow.OnAddItemChoice += PropertyRowChoice_OnAddItemChoice;
			propertyRow.OnRemoveArrayItem += PropertyRow_OnRemoveArrayItem;
		}
		else if (value.IsArrayItem)     // check second (after ChoiceValue but before all other Value type).
		{
			var flags = RowComponents.Expand | RowComponents.Remove;

			if (parent is PropertyRow parentPropertyRow && parentPropertyRow.RowValue is ChoiceValue)
				flags |= RowComponents.Label;

			propertyRow = new PropertyRow(value, parent, RowTypes.ArrayItem, flags);
			propertyRow.OnRemoveArrayItem += PropertyRow_OnRemoveArrayItem;
		}
		else if (value is OneOfValue)
		{
			propertyRow = new PropertyRow(value, parent, RowTypes.OneOfItem, RowComponents.Expand | RowComponents.Specified | RowComponents.Label);
			propertyRow.OnSelectOneOfItem += PropertyRowOneOfItem_OnSelect;
		}
		else
		{
			propertyRow = new PropertyRow(value, parent, RowTypes.Simple, RowComponents.Expand | RowComponents.Specified | RowComponents.Label);
			propertyRow.OnAddArrayItem += PropertyRowSimple_OnAddArrayItem;
		}

		if (propertyRow.ButtonRemove != null)
		{
			AddControl(propertyRow.ButtonRemove, x, top);
			x += WidthButtonRemove + HMargin;
		}

		if (propertyRow.Components.HasFlag(RowComponents.Specified) && ShowSpecifiedCheckbox)
		{
			if (propertyRow.CheckBoxSpecified != null)
				AddControl(propertyRow.CheckBoxSpecified, x, top);

			x += WidthSpecified;
		}

		if (propertyRow.ButtonExpand != null)
		{
			AddControl(propertyRow.ButtonExpand, x, top);
			x += WidthButtonExpand;
		}

		if (propertyRow.LabelRow != null)
		{
			AddControl(propertyRow.LabelRow, x, top);
			x += WidthLabel + HMargin;
		}

		AddControl(propertyRow.PropertyControl, x, top, (propertyRow.PropertyControl as IPropertyControl)?.ExtendRight ?? false);

		propertyRow.ExpandChanged += PropertyRow_ExpandChanged;

		parent.Children.Insert(parent.Children.Count, propertyRow);
		return propertyRow;
	}

	private void PropertyRow_OnRemoveArrayItem(PropertyRow propertyRow)
	{
		if (propertyRow.ParentRow.RowValue is ArrayValue parentArrayValue)
		{
			parentArrayValue.RemoveValue(propertyRow.RowValue);
		}
		else if (propertyRow.ParentRow.RowValue is ChoiceValue parentChoiceValue)
		{
			parentChoiceValue.RemoveValue(propertyRow.RowValue);
		}
		else
		{
			throw new Exception($"Invalid parent row value '{propertyRow.ParentRow.RowValue.GetType().Name}' for array item row (must be ArrayValue or ChoiceValue");
		}

		RemoveArrayItem(propertyRow);
	}

	private void PropertyRowChoice_OnAddItemChoice(PropertyRow propertyRow, Value value)
	{
		AddArrayItem(propertyRow, value);
	}

	/// <summary>
	/// Handle Expand/Collapse button.
	/// Note: button is only visible if RowValue.Specified && RowValue.HasChildren
	/// </summary>
	private void PropertyRow_ExpandChanged(PropertyRow propertyRow, bool expand)
	{
		this.Cursor = Cursors.WaitCursor;
		SuspendPanelLayout();

		if (expand)
		{
			Debug.Assert(propertyRow.RowValue.Specified == true, $"{nameof(PropertyRow_ExpandChanged)}: Specified should be true when expand is true");
			AddGridChildrenValues(propertyRow, 5);
			propertyRow.UpdateParentExpand();
		}
		else
		{
			RemoveGridRow(propertyRow, true);
		}

		ResumePanelLayout();
		this.Cursor = Cursors.Default;
	}

	private void PropertyRowOneOfItem_OnSelect(PropertyRow propertyRow)
	{
		PropertyRowSelectNewValue(propertyRow, propertyRow.RowOneOfValue.SelectedValue);
	}

	private void PropertyRowChoice_OnSelect(PropertyRow propertyRow)
	{
		var value = propertyRow.RowChoiceValue.SelectedValue!;
		PropertyRowSelectNewValue(propertyRow, value);
	}

	private void PropertyRowSelectNewValue(PropertyRow propertyRow, Value value)
	{
		SuspendPanelLayout();

		RemoveGridRow(propertyRow, true);
		AddGridValueAndChildren(value, propertyRow, -1);

		ResumePanelLayout();
	}

	private void AddArrayItem(PropertyRow propertyRow, Value value)
	{
		SuspendPanelLayout();

		if (propertyRow.Expanded)
			AddGridValueAndChildren(value, propertyRow, -1);
		else
			AddGridChildrenValues(propertyRow, -1);

		ResumePanelLayout();
	}

	private void RemoveArrayItem(PropertyRow propertyRow)
	{
		SuspendPanelLayout();

		RemoveGridRow(propertyRow);
		propertyRow.UpdateParentExpand();

		ResumePanelLayout();
	}

	private void PropertyRowSimple_OnAddArrayItem(PropertyRow propertyRow, PropertyType? propertyType)
	{
		var arrayValue = (ArrayValue)propertyRow.RowValue;
		var value = propertyType != null ? ValueFactory.AddArrayOneOfItem(arrayValue, propertyType) : ValueFactory.AddArrayItem(arrayValue);
		AddArrayItem(propertyRow, value);
	}

	void RemoveGridRow(PropertyRow row, bool onlyDescendant = false)
	{
		RemoveRows(GetAllDescendantRows(row));

		if (!onlyDescendant)
			RemoveRow(row);

		if (onlyDescendant)
			row.Children.Clear();
		else
			row.Parent.Children.Remove(row);
	}

	/// <summary>
	/// Get all descendant rows of container.
	/// </summary>
	/// <param name="row"></param>
	/// <param name="upToRow">Index of row in container (optional). If specified only get rows after this row. A value of '0' means that function returns an empty collection.</param>
	static List<PropertyRow> GetAllDescendantRows(PropertyRow row, int? upToRow = null)
	{
		List<PropertyRow> result = [];

		foreach (var child in row.Children.Take(upToRow ?? row.Children.Count))
		{
			result.Add(child);
			result.AddRange(GetAllDescendantRows(child));
		}

		return result;
	}

	void RemoveRows(IEnumerable<PropertyRow> propertyRows)
	{
		foreach (var row in propertyRows)
			RemoveRow(row);
	}

	void RemoveRow(PropertyRow row)
	{
		foreach (var ctl in row.Controls)
			panelMain.Controls.Remove(ctl);
	}

	void AddControl(Control control, int x, int y, bool extendRight = false)
	{
		const int RIGHT_BORDER_MARGIN = 5;

		SetLocation(control, x, y);

		panelMain.Controls.Add(control);

		if (extendRight)
		{
			control.Width = this.ClientSize.Width - x - RIGHT_BORDER_MARGIN;
			control.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
		}
	}

	private void SetLocation(Control control, int x, int y)
	{
		var offset = panelMain.AutoScrollPosition;
		control.Location = new Point(x + offset.X, y + offset.Y);
	}

	//private void panelMain_Resize(object sender, EventArgs e)
	//{
	//	Console.WriteLine($"panelMain_Resize {e}");
	//}

	//private void panelMain_Scroll(object sender, ScrollEventArgs e)
	//{
	//	TraceHelper.WriteVerbose($"panelMain_Scroll AutoScrollOffset:{panelMain.AutoScrollOffset}  AutoScrollPosition:{panelMain.AutoScrollPosition}  VerticalScroll:{panelMain.VerticalScroll.Value} {panelMain.VerticalScroll.Visible}");
	//}
}


