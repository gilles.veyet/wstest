﻿namespace WSTest.Editor
{

	class StringControl : TextBox, IPropertyControl
	{
		public bool ExtendRight => true;

		readonly StringBaseValue StringValue;

		public StringControl(StringBaseValue stringValue)
		{
			this.StringValue = stringValue;
			this.TextChanged += StringControl_TextChanged;
			this.Width = 500;

			RefreshValue();
		}

		public void RefreshValue()
		{
			this.Text = StringValue.Value;
		}

		private void StringControl_TextChanged(object? sender, EventArgs e)
		{
			this.StringValue.Value = this.Text;
		}
	}
}
