﻿namespace WSTest.Editor
{
	class StringEnumControl : ComboBox, IPropertyControl
	{
		public bool ExtendRight => true;

		readonly StringEnumValue EnumValue;

		public StringEnumControl(StringEnumValue stringEnumValue)
		{
			MouseWheel += (o, e) => ((HandledMouseEventArgs)e).Handled = true;  // prevent  changing value with mouse wheel.

			EnumValue = stringEnumValue;

			DropDownStyle = ComboBoxStyle.DropDownList;

			Items.AddRange([.. EnumValue.PropertyType.Values]);
			RefreshValue();

			SelectedIndexChanged += StringEnumControl_SelectedIndexChanged;
		}

		public void RefreshValue()
		{
			SelectedItem = EnumValue.Value;
		}

		private void StringEnumControl_SelectedIndexChanged(object? sender, EventArgs e)
		{
			EnumValue.Value = (string)this.SelectedItem!;
		}
	}
}
