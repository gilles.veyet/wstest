﻿namespace WSTest.Editor
{
	partial class TimeSpanControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.numDuration = new System.Windows.Forms.NumericUpDown();
			this.lblSeconds = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.numDuration)).BeginInit();
			this.SuspendLayout();
			// 
			// numDuration
			// 
			this.numDuration.Location = new System.Drawing.Point(0, 0);
			this.numDuration.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
			this.numDuration.Name = "numDuration";
			this.numDuration.Size = new System.Drawing.Size(116, 23);
			this.numDuration.TabIndex = 0;
			this.numDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// lblSeconds
			// 
			this.lblSeconds.AutoSize = true;
			this.lblSeconds.Location = new System.Drawing.Point(122, 2);
			this.lblSeconds.Name = "lblSeconds";
			this.lblSeconds.Size = new System.Drawing.Size(51, 15);
			this.lblSeconds.TabIndex = 1;
			this.lblSeconds.Text = "Seconds";
			// 
			// TimeSpanControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lblSeconds);
			this.Controls.Add(this.numDuration);
			this.Name = "TimeSpanControl";
			this.Size = new System.Drawing.Size(186, 22);
			((System.ComponentModel.ISupportInitialize)(this.numDuration)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private NumericUpDown numDuration;
		private Label lblSeconds;
	}
}
