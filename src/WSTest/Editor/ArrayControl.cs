﻿namespace WSTest.Editor
{
	internal delegate void ArrayAddOneOfItemEventHandler(PropertyType propertyType);
	internal delegate void ArrayAddItemEventHandler();

	internal partial class ArrayControl : UserControl, IPropertyControl
	{
		public bool ExtendRight => true;

		public event ArrayAddOneOfItemEventHandler? OnAddOneOfItem;
		public event ArrayAddItemEventHandler? OnAddItem;

		private readonly Value ArrayItemValue;

		public ArrayControl(Value value)
		{
			InitializeComponent();
			comboItemType.MouseWheel += (o, e) => ((HandledMouseEventArgs)e).Handled = true;  // prevent  changing value with mouse wheel.


			this.ArrayItemValue = value;
			var prop = value.Property;

			txtProperty.Visible = false;
			comboItemType.Visible = false;

			if (value.ActualType is OneOfPropertyType oneOf)
			{
				this.comboItemType.Items.AddRange([.. oneOf.PropertyTypes]);
				comboItemType.Visible = true;
				this.comboItemType.SelectedIndex = 0;
			}
			else
			{
				this.txtProperty.Text = prop.ActualType.Name;
				txtProperty.Visible = true;
			}
		}

		public void RefreshValue()
		{
			// nothing to do.
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			if (ArrayItemValue.ActualType is OneOfPropertyType oneOf)
			{
				OnAddOneOfItem?.Invoke((PropertyType)this.comboItemType.SelectedItem!);
			}
			else
			{
				OnAddItem?.Invoke();
			}
		}

		private void comboItemType_Format(object sender, ListControlConvertEventArgs e)
		{
			e.Value = (e.ListItem as PropertyType)!.Name;
		}
	}
}
