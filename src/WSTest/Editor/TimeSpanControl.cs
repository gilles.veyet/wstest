﻿namespace WSTest.Editor
{
	internal partial class TimeSpanControl : UserControl, IPropertyControl
	{
		public bool ExtendRight => true;
		readonly DurationValue TimeSpanValue;

		public TimeSpanControl(DurationValue timeSpanValue)
		{
			InitializeComponent();
			TimeSpanValue = timeSpanValue;
			RefreshValue();

			numDuration.ValueChanged += (sender, e) => { timeSpanValue.Value = TimeSpan.FromSeconds((double)numDuration.Value); };
		}

		public void RefreshValue()
		{
			numDuration.Value = (decimal)TimeSpanValue.Value.TotalSeconds;
		}
	}
}
