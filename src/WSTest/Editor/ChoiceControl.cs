﻿namespace WSTest.Editor
{
	internal delegate void ChoiceSelectEventHandler(ChoiceValue choiceValue, Property selectedProperty);
	internal delegate void ChoiceAddItemEventHandler(ChoiceValue choiceValue, Property selectedProperty);

	partial class ChoiceControl : UserControl, IPropertyControl
	{
		public bool ExtendRight => true;

		public event ChoiceSelectEventHandler? OnSelect;
		public event ChoiceAddItemEventHandler? OnAddItem;

		readonly ChoiceValue _ChoiceValue;
		Property? SelectedProperty => comboProperty.SelectedItem as Property;

		public ChoiceControl(ChoiceValue value)
		{
			_ChoiceValue = value;
			InitializeComponent();
			comboProperty.MouseWheel += (o, e) => ((HandledMouseEventArgs)e).Handled = true;  // prevent  changing value with mouse wheel.


			this.comboProperty.Items.AddRange([.. value.PropertyList]);

			if (value.IsArray)
			{
				this.comboProperty.SelectedIndex = 0;
			}
			else
			{
				btnAdd.Visible = false;
				comboProperty.Dock = DockStyle.Fill;

				// Does not trigger OnSelect event as it is null in constructor but children property rows will be created in PropertyEditor.AddGridChildrenValues.
				RefreshValue();
			}
		}

		public void RefreshValue()
		{
			if (!_ChoiceValue.IsArray)
			{
				this.comboProperty.SelectedItem = _ChoiceValue.SelectedValue?.Property;
			}
		}

		private void comboProperty_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (SelectedProperty != null && !_ChoiceValue.IsArray)
				OnSelect?.Invoke(_ChoiceValue, SelectedProperty);
		}

		private void comboProperty_Format(object sender, ListControlConvertEventArgs e)
		{
			if (e.ListItem != null)
				e.Value = ((Property)e.ListItem).Name;
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			OnAddItem?.Invoke(_ChoiceValue, SelectedProperty!);
		}
	}
}
