﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace WSTest.Editor
{
	partial class PropertyEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			panelMain = new Panel();
			SuspendLayout();
			// 
			// panelMain
			// 
			panelMain.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			panelMain.AutoScroll = true;
			panelMain.AutoScrollMinSize = new Size(600, 0);
			panelMain.Location = new Point(0, 0);
			panelMain.Name = "panelMain";
			panelMain.Size = new Size(968, 423);
			panelMain.TabIndex = 5;
			// 
			// PropertyEditor
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			Controls.Add(panelMain);
			Name = "PropertyEditor";
			Size = new Size(968, 423);
			ResumeLayout(false);
		}

		#endregion
		private Panel panelMain;
	}
}
