﻿namespace WSTest.Editor
{
	internal interface IPropertyControl
	{
		virtual bool ExtendRight => false;
		void RefreshValue();
	}
}
