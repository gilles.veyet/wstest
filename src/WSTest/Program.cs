using System.Diagnostics;
using System.Reflection;
using WSTest.Data;
using WSTest.Helpers;
using WSTest.UI;

namespace WSTest;

internal static class Program
{
	//const bool UseCache = false;    // actually faster without cache.

	public static int DeviceDpi = 96;

	public static bool DebugMode { get; } = CommandLineHelper.IsKeyPresent("debug");

	// suppress CS8618 (Non-nullable field must contain a non-null value when exiting constructor) using null! for properties that are set in Main().
	public static string AppName => Application.ProductName ?? "WSTest";
	public static string AppLocalFolder => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), AppName);
	public static Settings Settings { get; private set; } = null!;
	public static string DirectoryRoot { get; } = GetDirectoryRoot();
	public static string DirectoryServiceDescriptors => Path.Combine(DirectoryRoot, "ServiceDescriptors");
	public static string DirectoryCustom => Path.Combine(DirectoryRoot, "Custom");

	public static string DirectoryCache => Path.Combine(AppLocalFolder, "Cache");
	public static string DirectoryTemp => Path.Combine(AppLocalFolder, "Temp");
	public static string DefaultDirectoryImportServices => Path.Combine(AppLocalFolder, "Services");

	public static string DirectoryMonaco => Path.Combine(DirectoryRoot, "Monaco");

	public static CustomInfo? CustomInfo;

	private static Dictionary<string, HttpService> ServiceByIds = [];
	public static HttpService GetService(string id) => ServiceByIds[id];
	public static IEnumerable<HttpServiceDescriptor> ServiceDescriptors => ServiceByIds.Values.Select(s => s.Descriptor);


	public static int ScaleToDpi(int value)
	{
		const int REF_DPI = 96;
		return DeviceDpi == REF_DPI ? value : value * DeviceDpi / REF_DPI;
	}

	/// <summary>
	///  The main entry point for the application.
	/// </summary>
	[STAThread]
	static void Main()
	{
		using Mutex mutex = new(true, AppName, out var createdNew);
		if (!createdNew)
		{
			ShowError($"{AppName} is already running");
			return;
		}

		Directory.CreateDirectory(AppLocalFolder);
		Directory.CreateDirectory(DirectoryTemp);

		Application.ThreadException += Application_ThreadException;                     // Event handler for UI thread exceptions.
		Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);   // Force all Windows Forms errors to go through ThreadException handler.
		AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; // Event handler for handling non-UI thread exceptions.

		TraceHelper.InitTrace();
		TraceHelper.WriteInfo($"App Start - Version:{Application.ProductVersion} - Root:'{DirectoryRoot}' - AppLocalFolder:{AppLocalFolder}");

		Settings = Settings.Load();
		TraceHelper.MinLevel = Settings.TraceLevel;

		CustomInfo = CustomInfo.LoadDefaultFile();

#if false
		if (UseCache)
		{
			// Application.ProductVersion can be set in Project properties / Package / General / Package Version
			// And also in csproj file : e..g. <Version>0.5.0</Version>	
			if (Settings.AppVersion != Application.ProductVersion || Settings.CustomFullVersion != CustomInfo?.FullVersion)
			{
				TraceHelper.WriteInfo($"Version:{Application.ProductVersion} CustomVersion:{CustomInfo?.FullVersion} - Previous Version:{Settings.AppVersion} Previous Custom:{Settings.CustomFullVersion}  - clear cache as version is different");

				if (Directory.Exists(DirectoryCache))
					Directory.Delete(DirectoryCache, true);

				Settings.Save();
			}
		}
#endif

		CustomSettings? customSettings = null;

		if (Settings.FirstStart)
		{
			customSettings = CustomSettings.LoadDefaultFile();
			if (customSettings != null)
				Settings.ApplyFirstStartSettings(customSettings);
		}

		LoadServiceCache().Wait();

		Settings.EnsureDefaultProfile(customSettings);


		// To customize application configuration such as set high DPI settings or default font,
		// see https://aka.ms/applicationconfiguration.
		ApplicationConfiguration.Initialize();
		Application.Run(new FormMain());
	}

	private static string GetDirectoryRoot()
	{
		// When running solution, StartupPath ends with "bin/Debug"
		// In normal install, StartupPath ends with "bin"
		// DirectoryRoot must be set to directory above bin.
		var startupDir = new DirectoryInfo(Application.StartupPath);

		// skip "Debug" / "Release"
		if (startupDir.Parent != null && ("debug".Equals(startupDir.Name, StringComparison.OrdinalIgnoreCase) || "release".Equals(startupDir.Name, StringComparison.OrdinalIgnoreCase)))
			startupDir = startupDir.Parent;

		// skip "bin"
		if (startupDir.Parent != null && "bin".Equals(startupDir.Name, StringComparison.OrdinalIgnoreCase))
			startupDir = startupDir.Parent;

		return startupDir.FullName;
	}

	private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
	{
		string error = e.ExceptionObject.ToString() ?? "CRASH";
		AppendCrashLog(error);

		if (MessageBox.Show(error, AppName, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop) == DialogResult.Abort)
			Application.Exit();
	}

	private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
	{
		string error = e.Exception.ToString() ?? "CRASH";

		AppendCrashLog(error);

		if (MessageBox.Show(error, AppName, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop) == DialogResult.Abort)
			Application.Exit();
	}

	static void AppendCrashLog(string message)
	{
		try
		{
			File.AppendAllText(Path.Combine(AppLocalFolder, "crash.log"), $"{DateTime.Now} V{Application.ProductVersion} CRASH: {message}\r\n\r\n");
		}
		catch (Exception ex)
		{
			Debug.WriteLine(ex.ToString());
		}
	}

	static async Task LoadServiceCache()
	{

#if false
		if (UseCache)
		{
			Stopwatch sw = new();
			sw.Start();
			var cache = await ServiceCache.Load();
			sw.Stop();

			if (cache != null && cache.Services.Count != 0)
			{
				foreach (var svc in cache.Services.Values)
					svc.UpdateActualTypes();

				ServiceByIds = cache.Services;
				TraceHelper.WriteInfo($"{nameof(LoadServiceCache)}: load cache - services:{ServiceByIds.Count}  Time:{sw.ElapsedMilliseconds} ms");
				return;
			}
		}
#endif
		await BuildServiceCache();
	}

	public static async Task BuildServiceCache()
	{
		bool useSyncMode = CommandLineHelper.IsKeyPresent("Sync");
		int errorCount = TraceHelper.ErrorCount;
		int warningCount = TraceHelper.WarningCount;

		Stopwatch sw = new();
		sw.Start();

		if (useSyncMode)
		{
			ServiceByIds = [];

			foreach (var ext in GetAllServiceDescriptors())
			{
				var service = await LoadService(ext);
				ServiceByIds[service.Id] = service;
			}
		}
		else
		{
			var services = await Task.WhenAll(GetAllServiceDescriptors().Select(d => LoadService(d)));
			ServiceByIds = services.ToDictionary(s => s.Id);
		}

		long tsLoad = sw.ElapsedMilliseconds;

#if false
		if (UseCache)
		{
			await SaveServiceCache();
		}
#endif
		sw.Stop();
		TraceHelper.WriteInfo($"{nameof(BuildServiceCache)}: build cache synchronously:{useSyncMode} - services:{ServiceByIds.Count} - Errors:{TraceHelper.ErrorCount - errorCount} - Warnings:{TraceHelper.WarningCount - warningCount} - Time:{tsLoad} ms - Save:{sw.ElapsedMilliseconds - tsLoad} ms");

		CheckProfiles();
	}

	public static async Task ReloadService(HttpServiceDescriptor serviceDescriptor)
	{
		var service = await LoadService(serviceDescriptor);
		ServiceByIds[service.Id] = service;
	}

	// CheckProfiles: remove service that no longer exists. 
	static void CheckProfiles()
	{
		var serviceIds = Program.ServiceDescriptors.Select(x => x.Id).ToList();
		var setting = Program.Settings;

		bool save = false;

		foreach (var profile in Program.Settings.Profiles)
		{
			int initialCount = profile.ServiceConfigs.Keys.Count;
			profile.ServiceConfigs = profile.ServiceConfigs.Where(sc => serviceIds.Contains(sc.Key)).ToDictionary(p => p.Key, p => p.Value);

			int countRemoved = initialCount - profile.ServiceConfigs.Keys.Count;
			if (countRemoved > 0)
			{
				save = true;
				TraceHelper.WriteWarning($"{MethodBase.GetCurrentMethod()?.Name}: Profile '{profile.Name}' has been purged from {countRemoved} services");
			}
		}

		if (save)
			setting.Save();
	}

	public static async Task UpdateServiceCache(IEnumerable<HttpService> services, IEnumerable<string> deletedServiceIds)
	{
		var setting = Program.Settings;

		// clean profile from removed services.
		foreach (var profile in setting.Profiles)
			profile.ServiceConfigs = profile.ServiceConfigs.Where(sc => !deletedServiceIds.Contains(sc.Key)).ToDictionary(p => p.Key, p => p.Value);

		setting.Save();

		// remove deleted services.
		foreach (var serviceId in deletedServiceIds)
			ServiceByIds.Remove(serviceId);

		//add new services.
		foreach (var service in services)
			ServiceByIds[service.Id] = service;

		await SaveServiceCache();
	}

	public static async Task SaveServiceCache()
	{
		var cache = new ServiceCache
		{
			Services = ServiceByIds
		};
		await cache.Save();
	}

	public async static Task<HttpService> LoadService(HttpServiceDescriptor descriptor)
	{
		var service = descriptor.MakeService();
		await Task.Run(() => service.Load(Path.GetDirectoryName(service.Descriptor.ActualPath)!));
		return service;
	}

	public static List<HttpServiceDescriptor> GetAllServiceDescriptors()
	{
		var descriptors = new List<HttpServiceDescriptor>();

		foreach (string directory in GetAllServiceLocations())
			descriptors.AddRange(DescriptorHelper.LoadAllInDirectory(directory, true));

		return descriptors;
	}

	public static List<string> GetAllServiceLocations()
	{
		var directories = new List<string>();
		if (Settings.LoadDefaultServiceDescriptorDir)
			directories.Add(Program.DirectoryServiceDescriptors);

		directories.AddRange(Settings.AltServiceDescriptorDirs);
		return directories;
	}

	public static void ShowError(string message)
	{
		TraceHelper.WriteError(message);
		MessageBox.Show(message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
	}

	public static void ShowInfo(string message)
	{
		TraceHelper.WriteInfo(message);
		MessageBox.Show(message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
	}
}