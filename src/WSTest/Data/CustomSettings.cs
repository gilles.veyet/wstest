﻿using Newtonsoft.Json;

namespace WSTest.Data
{
	internal class CustomProfile
	{
		public string Name { get; set; } = String.Empty;
		public string Group { get; set; } = String.Empty;
		public string BaseUrl { get; set; } = String.Empty;
		public List<string> ServiceCategories { get; set; } = [];
		public Dictionary<string, string> PropertyMappings { get; set; } = [];
	}

	internal class CustomSettings
	{
		public int? RequestTimeout { get; set; }
		public bool? LoadDefaultServiceDescriptorDir { get; set; }
		public List<string> AltServiceDescriptorDirs { get; set; } = [];
		public List<CustomProfile> Profiles { get; set; } = [];
		//public List<PropertyFill> PropertyFills { get; set; } = new();

		static JsonSerializerSettings SerializerSettings => new()
		{
			NullValueHandling = NullValueHandling.Ignore,
			TypeNameHandling = TypeNameHandling.Auto
		};

		public void Save(string path)
		{
			string json = JsonConvert.SerializeObject(this, Formatting.Indented, SerializerSettings);
			System.IO.File.WriteAllText(path, json);
		}

		public const string CustomSettingsFilename = "CustomSettings.json";

		public static CustomSettings? LoadDefaultFile()
		{
			return Load(Path.Combine(Program.DirectoryCustom, CustomSettingsFilename));
		}

		public static CustomSettings? Load(string path)
		{
			if (!System.IO.File.Exists(path))
				return null;

			try
			{
				string json = File.ReadAllText(path);
				return JsonConvert.DeserializeObject<CustomSettings>(json, SerializerSettings);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), $"Error loading custom settings from file '{path}'", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return null;
			}
		}
	}
}
