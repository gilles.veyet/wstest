﻿using Newtonsoft.Json;

namespace WSTest.Data
{
	internal class CustomInfo
	{
		public string Name { get; set; } = string.Empty;
		public string Version { get; set; } = string.Empty;

		[Newtonsoft.Json.JsonIgnore]
		public string FullVersion => !string.IsNullOrEmpty(Name) ? $"{Name}-{Version}" : Version;


		static JsonSerializerSettings SerializerSettings => new()
		{
			NullValueHandling = NullValueHandling.Ignore,
			TypeNameHandling = TypeNameHandling.Auto
		};

		public void Save(string path)
		{
			string json = JsonConvert.SerializeObject(this, Formatting.Indented, SerializerSettings);
			System.IO.File.WriteAllText(path, json);
		}

		public const string CustomInfoFilename = "CustomInfo.json";

		public static CustomInfo? LoadDefaultFile()
		{
			return Load(Path.Combine(Program.DirectoryCustom, CustomInfoFilename));
		}

		public static CustomInfo? Load(string path)
		{
			if (!System.IO.File.Exists(path))
				return null;

			try
			{
				string json = File.ReadAllText(path);
				return JsonConvert.DeserializeObject<CustomInfo>(json, SerializerSettings);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), $"Error loading custom info from file '{path}'", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return null;
			}
		}

	}
}
