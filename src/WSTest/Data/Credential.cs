﻿namespace WSTest.Data
{
	internal class Credential(string domain)
	{
		public string Domain { get; init; } = domain;

		public string UserName { get; set; } = string.Empty;
		public string Password { get; set; } = string.Empty;
	}
}
