﻿using Newtonsoft.Json;
using System.Diagnostics;
using WSTest.Data;
using WSTest.Helpers;
using WSTest.UI;

namespace WSTest
{
	class Settings
	{
		public string? AppVersion { get; set; }
		public string? CustomFullVersion { get; set; }
		public bool FirstStart { get; set; } = true;
		public TraceLevel TraceLevel { get; set; } = TraceLevel.Info;
		public int RequestTimeout { get; set; } = 100;

		public bool LoadDefaultServiceDescriptorDir { get; set; } = true;       // Load service descriptors from default folder (relative to bin).
		public List<string> AltServiceDescriptorDirs { get; set; } = [];     // Alt folders to load service descriptors.
		public List<Profile> Profiles { get; set; } = [];

		public string? LastServiceId { get; set; }
		public string? LastMethodName { get; set; }
		public string? LastProfileId { get; set; }

		public MonacoTheme CodeViewerTheme { get; set; } = MonacoTheme.Dark;

		// Service Import
		public List<string> ServiceImportDefaultCheckMethods { get; set; } = [];
		public required string ServiceImportDefaultDir { get; set; }

		public string? LastModelServiceDescriptorDirectory { get; set; }
		public string? LastImportPackageDirectory { get; set; }
		public string? LastUpdatePackageDirectory { get; set; }

		[Newtonsoft.Json.JsonIgnore]
		public List<PropertyFill> PropertyFills { get; set; } = [];

		public const string SaveFilename = "WSTestSettings.json";
		public static string SaveFilePath => Path.Combine(Program.AppLocalFolder, SaveFilename);

		static JsonSerializerSettings SerializerSettings => new()
		{
			NullValueHandling = NullValueHandling.Ignore,
			TypeNameHandling = TypeNameHandling.Auto,
			//PreserveReferencesHandling = PreserveReferencesHandling.Objects,	// not useful for Settings (at least for now).
			Converters = [new JSonXmlQualifiedNameConverter(), new JsonProtectableValueConverter()]
		};


		public void Save()
		{
			Save(SaveFilePath);
		}

		public void Save(string path)
		{
			AppVersion = Application.ProductVersion;
			CustomFullVersion = Program.CustomInfo?.FullVersion;

			string json = JsonConvert.SerializeObject(this, Formatting.Indented, SerializerSettings);
			System.IO.File.WriteAllText(path, json);
		}

		public static Settings Load()
		{
			Settings? settings = null;

			if (System.IO.File.Exists(SaveFilePath))
			{
				try
				{
					string json = File.ReadAllText(SaveFilePath);
					settings = JsonConvert.DeserializeObject<Settings>(json, SerializerSettings);
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.ToString(), "Error loading settings", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
			}

			settings ??= new Settings() { ServiceImportDefaultDir = Program.DefaultDirectoryImportServices };

			if (string.IsNullOrEmpty(settings.ServiceImportDefaultDir))
				settings.ServiceImportDefaultDir = Program.DefaultDirectoryImportServices;

			if (settings.ServiceImportDefaultCheckMethods.Count == 0)
				settings.ServiceImportDefaultCheckMethods = ["GetVersion"];

			if (settings.PropertyFills.Count == 0)
			{
				foreach (string text in new string[]
					{
						"**|*PagingRequest|BlockSize=50",
						"**|Paging|BlockSize=50"
					})
				{
					settings.PropertyFills.Add(PropertyFill.FromString(text));
				}
			}

			return settings;
		}

		/// <summary>
		/// ApplyFirstStartSettings
		/// </summary>
		public void ApplyFirstStartSettings(CustomSettings? customSettings)
		{
			if (customSettings != null)
			{
				if (customSettings.RequestTimeout != null)
					RequestTimeout = customSettings.RequestTimeout.Value;

				if (customSettings.LoadDefaultServiceDescriptorDir != null)
					LoadDefaultServiceDescriptorDir = customSettings.LoadDefaultServiceDescriptorDir.Value;

				if (customSettings.AltServiceDescriptorDirs != null)
					AltServiceDescriptorDirs = customSettings.AltServiceDescriptorDirs;

				//if (customSettings.PropertyFills != null)
				//	PropertyFills = customSettings.PropertyFills;
			}
		}

		/// <summary>
		/// Create default profile if there is no profile.
		/// Must be called after Service Descriptors are loaded.
		/// </summary>
		public void EnsureDefaultProfile(CustomSettings? customSettings)
		{
			if (Profiles.Count != 0)
				return;

			if (customSettings != null && customSettings.Profiles.Count > 0)
			{
				Profiles = customSettings.Profiles.Select(p => LoadCustomProfile(p)).ToList();
			}
			else
			{
				var profile = new Profile();
				Profiles.Add(profile);

				profile.Name = "Default profile";
				profile.Group = "Default group";
				profile.BaseUrl = "http://localhost:5000";
				profile.SetDefaultServices();
			}
		}

		private static Profile LoadCustomProfile(CustomProfile custom)
		{
			Profile profile = new() { Name = custom.Name, Group = custom.Group, BaseUrl = custom.BaseUrl };
			profile.SetDefaultServices(custom.ServiceCategories);
			profile.PropertyMappings = custom.PropertyMappings.ToDictionary(m => m.Key, m => new ProtectableValue(m.Value));
			return profile;
		}

	}


}
