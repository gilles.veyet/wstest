﻿using Newtonsoft.Json;
using System.IO.Compression;
using WSTest.Helpers;

namespace WSTest
{
	internal class ServiceCache
	{
		public Dictionary<string, HttpService> Services { get; set; } = [];

		private static string SaveFilePath => Path.Combine(Program.DirectoryCache, "ServiceCache.gzip");

		static JsonSerializerSettings SerializerSettings => new()
		{
			Formatting = Formatting.None,
			NullValueHandling = NullValueHandling.Ignore,
			TypeNameHandling = TypeNameHandling.Auto,
			PreserveReferencesHandling = PreserveReferencesHandling.Objects,
			Converters = [new JSonXmlQualifiedNameConverter(), new JsonProtectableValueConverter()]
		};

		public async Task Save()
		{
			await Task.Run(() =>
			{
				Directory.CreateDirectory(Program.DirectoryCache);

				//string json = JsonConvert.SerializeObject(this, SerializerSettings);
				//System.IO.File.WriteAllText(SaveFilePath, json);

				using var fs = File.Create(SaveFilePath);
				using var zs = new GZipStream(fs, CompressionMode.Compress);
				using var sw = new StreamWriter(zs);
				using var writer = new JsonTextWriter(sw);
				var serializer = JsonSerializer.Create(SerializerSettings);
				serializer.Serialize(writer, this);
			});
		}

		public static async Task<ServiceCache?> Load()
		{
			ServiceCache? cache = null;

			if (System.IO.File.Exists(SaveFilePath))
			{
				try
				{
					await Task.Run(() =>
					{
						//string json = File.ReadAllText(SaveFilePath);
						//cache = JsonConvert.DeserializeObject<ServiceCache>(json, SerializerSettings);

						using var fs = File.Open(SaveFilePath, FileMode.Open);
						using var zs = new GZipStream(fs, CompressionMode.Decompress);
						using var sr = new StreamReader(zs);
						using var reader = new JsonTextReader(sr);
						var serializer = JsonSerializer.Create(SerializerSettings);
						cache = serializer.Deserialize<ServiceCache>(reader);
					});
				}
				catch (Exception ex)
				{
					const string err = "Error loading service cache";
					TraceHelper.WriteException(err, ex);
					MessageBox.Show(ex.ToString(), err, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
			}

			return cache;
		}

	}
}
