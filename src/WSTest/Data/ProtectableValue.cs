﻿namespace WSTest
{
	internal class ProtectableValue(string value, bool protect = false)
	{
		public string Value { get; set; } = value;
		public bool Protected { get; init; } = protect;

		public override string ToString()
		{
			return $"{Value}{(Protected ? " (protected)" : "")}";
		}

		public ProtectableValue Clone()
		{
			return new ProtectableValue(Value, Protected);
		}
	}
}
