﻿using System.Text;
using WSTest.Helpers;

namespace WSTest;
using PropertyMapping = KeyValuePair<string, ProtectableValue>;

class Profile : IComparable<Profile>
{
	public string Id { get; set; } = Guid.NewGuid().ToString();
	public string Name { get; set; } = String.Empty;
	public string Group { get; set; } = String.Empty;
	public string BaseUrl { get; set; } = String.Empty;

	public Dictionary<string, ProtectableValue> PropertyMappings { get; set; } = [];
	public Dictionary<string, ProfileServiceConfig> ServiceConfigs { get; set; } = [];

	public Profile Clone()
	{
		var mappings = PropertyMappings.ToDictionary(m => m.Key, m => m.Value.Clone());
		var configs = ServiceConfigs.ToDictionary(m => m.Key, m => m.Value.Clone());
		return new Profile() { Id = this.Id, Name = this.Name, Group = this.Group, BaseUrl = this.BaseUrl, PropertyMappings = mappings, ServiceConfigs = configs };
	}

	[Newtonsoft.Json.JsonIgnore]
	public List<PropertyMapping> SortedPropertyMappings => [.. PropertyMappings.OrderBy(pm => pm, Comparer<PropertyMapping>.Create((a, b) => ComparePropertyMappings(a, b)))];

	static int ComparePropertyMappings(PropertyMapping kv1, PropertyMapping kv2)
	{
		string prop1 = kv1.Key;
		string prop2 = kv2.Key;

		int index1 = Array.IndexOf(WellKnownProperties.PreferredOrder, prop1);
		int index2 = Array.IndexOf(WellKnownProperties.PreferredOrder, prop2);

		if (index1 < 0)
			index1 = int.MaxValue;

		if (index2 < 0)
			index2 = int.MaxValue;

		return index1 == index2 ? prop1.CompareTo(prop2) : index1 - index2;
	}

	/// <summary>
	/// Return list of properties, joined with ", "
	/// Only Properties that are not protected (no password)
	/// </summary>
	[Newtonsoft.Json.JsonIgnore]
	public string PropertyListInfo => String.Join(", ", PropertyMappings.Where(p => !p.Value.Protected && !string.IsNullOrEmpty(p.Value.Value)).Select(p => p.Value.Value));

	public void SetDefaultServices(List<string>? serviceCategories = null)
	{
		ServiceConfigs.Clear();
		PropertyMappings.Clear();

		foreach (var svc in Program.ServiceDescriptors)
		{
			if (serviceCategories == null || serviceCategories.Count == 0 || serviceCategories.Contains(svc.Category))
				ServiceConfigs[svc.Id] = ProfileServiceConfig.MakeDefault(svc);
		}

		UpdatePropertyMappings();
	}


	public void UpdatePropertyMappings()
	{
		var services = GetServices();
		var properties = services.SelectMany(s => s.Descriptor.AllProperties);

		foreach (var k in PropertyMappings.Keys)
		{
			if (!properties.Contains(k))
				PropertyMappings.Remove(k);
		}

		foreach (var svc in services)
		{
			foreach (var prop in svc.Descriptor.Properties)
			{
				if (!PropertyMappings.ContainsKey(prop))
					PropertyMappings[prop] = new ProtectableValue(String.Empty);
			}

			foreach (var prop in svc.Descriptor.SecretProperties)
			{
				if (!PropertyMappings.ContainsKey(prop))
					PropertyMappings[prop] = new ProtectableValue(String.Empty, true);
			}
		}
	}

	public bool HasAllProperties(IEnumerable<string> properties)
	{
		foreach (var prop in properties)
		{
			if (!PropertyMappings.ContainsKey(prop))
				return false;
		}

		return true;
	}

	public IEnumerable<HttpService> GetServices()
	{
		return ServiceConfigs.Keys.Select(k => Program.GetService(k));
	}

	public HttpService? GetService(string serviceId)
	{
		return ServiceConfigs.ContainsKey(serviceId) ? Program.GetService(serviceId) : null;
	}

	public string InterpolateStringWithMappings(string text)
	{
		return MappingHelper.InterpolateStringWithMappings(PropertyMappings, text);
	}

	/// <summary>
	/// Get Default Service url
	/// </summary>
	/// <returns></returns>
	public string GetServiceBaseUrl(HttpServiceDescriptor descriptor)
	{
		char SEP = '/';

		var serviceConfig = ServiceConfigs[descriptor.Id];

		if (serviceConfig.UrlMode == ProfileServiceConfigUrlMode.Absolute)
			return serviceConfig.Url!.TrimEnd(SEP);

		StringBuilder sb = new(BaseUrl.TrimEnd(SEP));

		if (serviceConfig.UrlMode == ProfileServiceConfigUrlMode.RelativeToBaseAndPrefix && !string.IsNullOrWhiteSpace(descriptor.DefaultUrlPrefix))
		{
			sb.Append(SEP);
			sb.Append(descriptor.DefaultUrlPrefix);
		}

		if (!string.IsNullOrEmpty(serviceConfig.Url))
		{
			sb.Append(SEP);
			sb.Append(serviceConfig.Url.TrimEnd(SEP));
		}

		return sb.ToString();
	}

	public override string ToString()
	{
		return !string.IsNullOrEmpty(Group) ? $"{Group} / {Name}" : Name;
	}

	public int CompareTo(Profile? other)
	{
		int r = this.Group.CompareTo(other?.Group);

		if (r == 0)
			r = this.Name.CompareTo(other?.Name);

		return r;
	}
}

