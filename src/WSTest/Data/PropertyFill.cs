﻿using System.Reflection;

namespace WSTest.Data
{
	internal record PropertyFill
	{
		public required PropertyPattern Pattern { get; init; }
		public required string Value { get; init; }

		[Newtonsoft.Json.JsonIgnore]
		public int? DecimalValue => int.TryParse(Value, out var val) ? val : null;

		public static PropertyFill FromString(string text)
		{
			int index = text.LastIndexOf('=');
			if (index < 0)
				throw new ArgumentException($"{MethodBase.GetCurrentMethod()}: Invalid argument '{text}'");

			var pattern = new PropertyPattern() { PatternProperty = text[..index] };
			string value = text[(index + 1)..];

			return new PropertyFill() { Pattern = pattern, Value = value };
		}
	}
}