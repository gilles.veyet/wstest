﻿using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace WSTest.Data
{
	internal record PropertyPattern
	{
		const char CharStar = '*';
		const char CharColon = ':';
		const string StringStar = "*";
		const string StringDoubleStar = "**";

		const string RegexPipe = @"\|";
		const string RegexAnyButPipe = "[^|]*";
		const string RegexAnyButPipeOrColon = "[^|:]*";
		const string RegexOptionalNamespace = "([^|]*:)?";

		//public string? PatternServiceCategory { get; init; }
		//public string? PatternServiceName { get; init; }
		//public string? PatternPackage { get; init; }
		//public string? PatternProfileName { get; init; }
		//public string? PatternProfileGroup { get; init; }

		public required string PatternProperty { get; init; }

		[Newtonsoft.Json.JsonIgnore]
		private Regex? _RegexProperty;

		[Newtonsoft.Json.JsonIgnore]
		public Regex RegexProperty => _RegexProperty ??= new Regex(BuildRegexProperty(PatternProperty), RegexOptions.Compiled | RegexOptions.IgnoreCase);

		static string BuildRegexProperty(string pattern)
		{
			var arr = pattern.Split('|');
			if (arr.Length == 0 || arr.Any(t => t.Length == 0))
				throw new ArgumentException($"{MethodBase.GetCurrentMethod()}: Invalid pattern '{pattern}'");

			StringBuilder sbRegEx = new();
			sbRegEx.Append('^');

			foreach (var pt in arr)
			{
				if (pt == StringDoubleStar)
				{
					sbRegEx.Append(".*");
				}
				else if (pt == StringStar)
				{
					sbRegEx.Append(RegexAnyButPipe);
				}
				else
				{
					string? ns;
					string name;

					int index = pt.LastIndexOf(':');

					if (index > 0)
					{
						ns = pt[..index];
						name = pt[(index + 1)..];
					}
					else
					{
						name = pt;
						ns = null;
					}

					if (ns != null)
					{
						sbRegEx.Append(MakeRegExPropertyNamespace(ns));
						sbRegEx.Append(CharColon);
						sbRegEx.Append(MakeRegExPropertyName(name));
					}
					else
					{
						sbRegEx.Append(RegexOptionalNamespace);
						sbRegEx.Append(MakeRegExPropertyName(name));
					}
				}

				sbRegEx.Append(RegexPipe);
			}

			sbRegEx.Length -= RegexPipe.Length;
			sbRegEx.Append('$');

			return sbRegEx.ToString();
		}

		static string MakeRegExPropertyName(string text) => MakeRegExWithStar(text, RegexAnyButPipeOrColon);
		static string MakeRegExPropertyNamespace(string text) => MakeRegExWithStar(text, RegexAnyButPipe);
		//static string MakeRegExServiceNameOrCategory(string text) => MakeRegExWithStar(text, "*.");

		static string MakeRegExWithStar(string text, string regexStar)
		{
			StringBuilder sb = new();

			foreach (string p in text.Split(CharStar))
			{
				if (!string.IsNullOrEmpty(p))
					sb.Append(Regex.Escape(p));

				sb.Append(regexStar);
			}

			return sb.ToString();
		}

	}
}
