﻿namespace WSTest;

public enum ProfileServiceConfigUrlMode
{
	RelativeToBase,
	RelativeToBaseAndPrefix,
	Absolute,
}

class ProfileServiceConfig
{
	public ProfileServiceConfigUrlMode UrlMode { get; set; } = ProfileServiceConfigUrlMode.RelativeToBaseAndPrefix;
	public string? Url { get; set; }

	public static ProfileServiceConfig MakeDefault(HttpServiceDescriptor svc)
	{
		return new ProfileServiceConfig() { UrlMode = !string.IsNullOrEmpty(svc.DefaultUrlPrefix) ? ProfileServiceConfigUrlMode.RelativeToBaseAndPrefix : ProfileServiceConfigUrlMode.RelativeToBase };
	}

	public ProfileServiceConfig Clone()
	{
		return new ProfileServiceConfig() { UrlMode = UrlMode, Url = Url };
	}
}