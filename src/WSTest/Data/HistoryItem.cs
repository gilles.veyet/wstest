﻿using WSTest.Helpers;

namespace WSTest
{
	public enum HistoryItemModes
	{
		/// <summary>
		/// Normal
		/// </summary>
		Normal,

		/// <summary>
		/// Preview request body. 
		/// </summary>
		Preview,
	}

	internal class HistoryItem
	{
		public required int Id { get; set; }
		public HistoryItemModes Mode { get; set; } = HistoryItemModes.Normal;
		public required string Name { get; set; }
		public required string ProfileId { get; set; }
		public required string ServiceId { get; set; }
		public required string MethodName { get; set; }
		public required ServiceTypes ServiceType { get; set; }
		public required HttpSendData SendData { get; set; }
		public HttpResult? Result { get; set; }

		[Newtonsoft.Json.JsonIgnore]
		public string ModeAndName => ToString();

		//[Newtonsoft.Json.JsonIgnore]
		//public bool InMemory => string.IsNullOrEmpty(FilePath);

		//public string? FilePath { get; set; }

		public override string ToString() => GetModePrefix() + Name;

		private string GetModePrefix()
		{
			return Mode == HistoryItemModes.Normal ? string.Empty : $"[{Mode}] ";
		}

		public HistoryItem CloneForEdition()
		{
			HistoryItem item = new()
			{
				Id = -1,    // will be set when Run is used.

				Mode = HistoryItemModes.Normal,
				Name = Name,
				ProfileId = ProfileId,
				ServiceId = ServiceId,
				MethodName = MethodName,
				ServiceType = ServiceType,
				SendData = SendData,
			};

			return item;
		}
	}
}
