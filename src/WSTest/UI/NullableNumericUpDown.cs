﻿using System.ComponentModel;


namespace WSTest.UI
{

	/// <summary>
	/// NumericUpDown control with support for nullable value.
	/// From https://stackoverflow.com/questions/15406322/windows-forms-customizing-numericupdown-to-be-nullable
	/// </summary>
	[DefaultBindingProperty("Value")]
	[DefaultEvent("ValueChanged")]
	[DefaultProperty("Value")]
	public class NullableNumericUpDown : NumericUpDown
	{
		private decimal? _value;

		public NullableNumericUpDown() : base()
		{
			Text = string.Empty;
		}

		public NullableNumericUpDown(IContainer container) : this()
		{
			container.Add(this);
		}

		/// <summary>
		/// Gets or sets the value assigned to the spin box (also known as an up-down control).
		/// 
		/// Returns:
		///      The numeric value of the System.Windows.Forms.NumericUpDown control.
		/// Exceptions:
		///    T:System.ArgumentOutOfRangeException:
		///       The assigned value is less than the System.Windows.Forms.NumericUpDown.Minimum
		///       property value.-or- The assigned value is greater than the System.Windows.Forms.NumericUpDown.Maximum property value.
		/// </summary>
		[Bindable(true)]
		public new decimal? Value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;

				if (base.Value != value.GetValueOrDefault())
				{
					base.Value = value.GetValueOrDefault();
				}

				Text = value?.ToString();
			}
		}

		/// <summary>
		/// OnValueChanged override.
		/// </summary>
		protected override void OnValueChanged(EventArgs e)
		{
			if (Value != base.Value)
			{
				Value = base.Value;
			}
		}

		/// <summary>
		/// OnTextChanged override
		/// </summary>
		protected override void OnTextChanged(EventArgs e)
		{
			base.OnTextChanged(e);

			if (string.IsNullOrEmpty(Text))
			{
				Value = null;
			}
		}
	}
}
