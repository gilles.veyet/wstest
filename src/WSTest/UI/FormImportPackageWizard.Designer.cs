﻿namespace WSTest.UI
{
	partial class FormImportPackageWizard
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormImportPackageWizard));
			btnCancel = new Button();
			btnSave = new Button();
			folderInputImport = new FolderInput();
			tabControl = new TabControl();
			tabPageLocation = new TabPage();
			labelImportPath = new Label();
			fileInputImport = new FileInput();
			optImportTypeZip = new RadioButton();
			optImportTypeSingle = new RadioButton();
			optImportTypeFolder = new RadioButton();
			labelImportType = new Label();
			labelServiceType = new Label();
			comboServiceType = new ComboBox();
			fileInputServiceDescriptorModel = new FileInput();
			labelServiceDescriptorModel = new Label();
			tabPageParameters = new TabPage();
			listEditServerUrls = new StringListEditControl();
			labelServerUrls = new Label();
			txtUrlPrefix = new TextBox();
			labelUrlPrefix = new Label();
			labelServiceCategory = new Label();
			btnReadPackageVersionFromPom = new Button();
			comboServiceCategory = new ComboBox();
			btnReadPackageNameFromPom = new Button();
			txtPackageName = new TextBox();
			labelPackageVersion = new Label();
			txtPackageVersion = new TextBox();
			labelPackageName = new Label();
			tabPageProperties = new TabPage();
			labelSecretProperties = new Label();
			listEditSecretProperties = new StringListEditControl();
			labelProperties = new Label();
			listEditProperties = new StringListEditControl();
			tabPageAuthentication = new TabPage();
			txtCustomBasicAuthentication = new TextBox();
			chkUseBasicAuthentication = new CheckBox();
			labelCustomBasicAuthentication = new Label();
			labelUseBasicAuthentication = new Label();
			labelMappings = new Label();
			listEditMappings = new StringListEditControl();
			tabPageServices = new TabPage();
			groupCheckServices = new GroupBox();
			btnLoadProfileCheckServices = new Button();
			propertyEditCheckServices = new Editor.PropertyEditor();
			btnStopCheck = new Button();
			txtCheckServiceBaseUrl = new TextBox();
			btnCheckAllEnabled = new Button();
			labelCheckServiceBaseUrl = new Label();
			comboCheckProfile = new ComboBox();
			btnCheck = new Button();
			dataGridServices = new DataGridView();
			tabPageSave = new TabPage();
			folderOutputDirectory = new FolderInput();
			labelOutputDirectory = new Label();
			btnNext = new Button();
			btnPrev = new Button();
			panelNavigate = new Panel();
			tabControl.SuspendLayout();
			tabPageLocation.SuspendLayout();
			tabPageParameters.SuspendLayout();
			tabPageProperties.SuspendLayout();
			tabPageAuthentication.SuspendLayout();
			tabPageServices.SuspendLayout();
			groupCheckServices.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)dataGridServices).BeginInit();
			tabPageSave.SuspendLayout();
			panelNavigate.SuspendLayout();
			SuspendLayout();
			// 
			// btnCancel
			// 
			btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnCancel.DialogResult = DialogResult.Cancel;
			btnCancel.Location = new Point(258, 6);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new Size(75, 23);
			btnCancel.TabIndex = 5;
			btnCancel.Text = "Cancel";
			btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnSave
			// 
			btnSave.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnSave.Location = new Point(177, 6);
			btnSave.Name = "btnSave";
			btnSave.Size = new Size(75, 23);
			btnSave.TabIndex = 4;
			btnSave.Text = "Save";
			btnSave.UseVisualStyleBackColor = true;
			btnSave.Click += btnSave_Click;
			// 
			// folderInputImport
			// 
			folderInputImport.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			folderInputImport.Folder = "";
			folderInputImport.InitialBrowseDirectory = "";
			folderInputImport.Location = new Point(142, 63);
			folderInputImport.Name = "folderInputImport";
			folderInputImport.Readonly = false;
			folderInputImport.ShowNewFolderButton = true;
			folderInputImport.Size = new Size(1325, 23);
			folderInputImport.TabIndex = 4;
			folderInputImport.FolderChanged += folderInputImport_ValueChanged;
			// 
			// tabControl
			// 
			tabControl.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			tabControl.Controls.Add(tabPageLocation);
			tabControl.Controls.Add(tabPageParameters);
			tabControl.Controls.Add(tabPageProperties);
			tabControl.Controls.Add(tabPageAuthentication);
			tabControl.Controls.Add(tabPageServices);
			tabControl.Controls.Add(tabPageSave);
			tabControl.ItemSize = new Size(61, 20);
			tabControl.Location = new Point(0, 0);
			tabControl.Name = "tabControl";
			tabControl.SelectedIndex = 0;
			tabControl.Size = new Size(1484, 620);
			tabControl.TabIndex = 18;
			// 
			// tabPageLocation
			// 
			tabPageLocation.Controls.Add(labelImportPath);
			tabPageLocation.Controls.Add(fileInputImport);
			tabPageLocation.Controls.Add(folderInputImport);
			tabPageLocation.Controls.Add(optImportTypeZip);
			tabPageLocation.Controls.Add(optImportTypeSingle);
			tabPageLocation.Controls.Add(optImportTypeFolder);
			tabPageLocation.Controls.Add(labelImportType);
			tabPageLocation.Controls.Add(labelServiceType);
			tabPageLocation.Controls.Add(comboServiceType);
			tabPageLocation.Controls.Add(fileInputServiceDescriptorModel);
			tabPageLocation.Controls.Add(labelServiceDescriptorModel);
			tabPageLocation.Location = new Point(4, 24);
			tabPageLocation.Name = "tabPageLocation";
			tabPageLocation.Padding = new Padding(3);
			tabPageLocation.Size = new Size(1476, 592);
			tabPageLocation.TabIndex = 0;
			tabPageLocation.Text = "Location";
			tabPageLocation.UseVisualStyleBackColor = true;
			// 
			// labelImportPath
			// 
			labelImportPath.AutoSize = true;
			labelImportPath.Location = new Point(9, 66);
			labelImportPath.Name = "labelImportPath";
			labelImportPath.Size = new Size(57, 15);
			labelImportPath.TabIndex = 12;
			labelImportPath.Text = "Path / Url";
			// 
			// fileInputImport
			// 
			fileInputImport.CheckFileExists = true;
			fileInputImport.FileName = "";
			fileInputImport.Filter = "";
			fileInputImport.FilterIndex = 1;
			fileInputImport.InitialDirectory = "";
			fileInputImport.Location = new Point(141, 504);
			fileInputImport.Name = "fileInputImport";
			fileInputImport.Readonly = false;
			fileInputImport.Size = new Size(1326, 23);
			fileInputImport.TabIndex = 11;
			fileInputImport.FileNameChanged += fileInputImport_FileNameChanged;
			// 
			// optImportTypeZip
			// 
			optImportTypeZip.AutoSize = true;
			optImportTypeZip.Location = new Point(261, 38);
			optImportTypeZip.Name = "optImportTypeZip";
			optImportTypeZip.Size = new Size(161, 19);
			optImportTypeZip.TabIndex = 10;
			optImportTypeZip.TabStop = true;
			optImportTypeZip.Text = "Multiple / Zip ( File or Url)";
			optImportTypeZip.UseVisualStyleBackColor = true;
			optImportTypeZip.CheckedChanged += optImportTypeZip_CheckedChanged;
			// 
			// optImportTypeSingle
			// 
			optImportTypeSingle.AutoSize = true;
			optImportTypeSingle.Location = new Point(428, 38);
			optImportTypeSingle.Name = "optImportTypeSingle";
			optImportTypeSingle.Size = new Size(118, 19);
			optImportTypeSingle.TabIndex = 9;
			optImportTypeSingle.TabStop = true;
			optImportTypeSingle.Text = "Single / File or Url";
			optImportTypeSingle.UseVisualStyleBackColor = true;
			optImportTypeSingle.CheckedChanged += optImportTypeSingle_CheckedChanged;
			// 
			// optImportTypeFolder
			// 
			optImportTypeFolder.AutoSize = true;
			optImportTypeFolder.Location = new Point(142, 38);
			optImportTypeFolder.Name = "optImportTypeFolder";
			optImportTypeFolder.Size = new Size(113, 19);
			optImportTypeFolder.TabIndex = 8;
			optImportTypeFolder.TabStop = true;
			optImportTypeFolder.Text = "Multiple / Folder";
			optImportTypeFolder.UseVisualStyleBackColor = true;
			optImportTypeFolder.CheckedChanged += optImportTypeFolder_CheckedChanged;
			// 
			// labelImportType
			// 
			labelImportType.AutoSize = true;
			labelImportType.Location = new Point(8, 38);
			labelImportType.Name = "labelImportType";
			labelImportType.Size = new Size(70, 15);
			labelImportType.TabIndex = 7;
			labelImportType.Text = "Import Type";
			// 
			// labelServiceType
			// 
			labelServiceType.AutoSize = true;
			labelServiceType.Location = new Point(8, 9);
			labelServiceType.Name = "labelServiceType";
			labelServiceType.Size = new Size(71, 15);
			labelServiceType.TabIndex = 0;
			labelServiceType.Text = "Service Type";
			// 
			// comboServiceType
			// 
			comboServiceType.DropDownStyle = ComboBoxStyle.DropDownList;
			comboServiceType.FormattingEnabled = true;
			comboServiceType.Location = new Point(142, 6);
			comboServiceType.Name = "comboServiceType";
			comboServiceType.Size = new Size(320, 23);
			comboServiceType.TabIndex = 1;
			comboServiceType.SelectedIndexChanged += comboServiceType_SelectedIndexChanged;
			// 
			// fileInputServiceDescriptorModel
			// 
			fileInputServiceDescriptorModel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			fileInputServiceDescriptorModel.CheckFileExists = true;
			fileInputServiceDescriptorModel.FileName = "";
			fileInputServiceDescriptorModel.Filter = "";
			fileInputServiceDescriptorModel.FilterIndex = 1;
			fileInputServiceDescriptorModel.InitialDirectory = "";
			fileInputServiceDescriptorModel.Location = new Point(141, 92);
			fileInputServiceDescriptorModel.Name = "fileInputServiceDescriptorModel";
			fileInputServiceDescriptorModel.Readonly = false;
			fileInputServiceDescriptorModel.Size = new Size(1326, 23);
			fileInputServiceDescriptorModel.TabIndex = 0;
			// 
			// labelServiceDescriptorModel
			// 
			labelServiceDescriptorModel.AutoSize = true;
			labelServiceDescriptorModel.Location = new Point(7, 96);
			labelServiceDescriptorModel.Name = "labelServiceDescriptorModel";
			labelServiceDescriptorModel.Size = new Size(101, 15);
			labelServiceDescriptorModel.TabIndex = 5;
			labelServiceDescriptorModel.Text = "Descriptor Model ";
			// 
			// tabPageParameters
			// 
			tabPageParameters.Controls.Add(listEditServerUrls);
			tabPageParameters.Controls.Add(labelServerUrls);
			tabPageParameters.Controls.Add(txtUrlPrefix);
			tabPageParameters.Controls.Add(labelUrlPrefix);
			tabPageParameters.Controls.Add(labelServiceCategory);
			tabPageParameters.Controls.Add(btnReadPackageVersionFromPom);
			tabPageParameters.Controls.Add(comboServiceCategory);
			tabPageParameters.Controls.Add(btnReadPackageNameFromPom);
			tabPageParameters.Controls.Add(txtPackageName);
			tabPageParameters.Controls.Add(labelPackageVersion);
			tabPageParameters.Controls.Add(txtPackageVersion);
			tabPageParameters.Controls.Add(labelPackageName);
			tabPageParameters.Location = new Point(4, 24);
			tabPageParameters.Name = "tabPageParameters";
			tabPageParameters.Padding = new Padding(3);
			tabPageParameters.Size = new Size(1476, 592);
			tabPageParameters.TabIndex = 1;
			tabPageParameters.Text = "Parameters";
			tabPageParameters.UseVisualStyleBackColor = true;
			// 
			// listEditServerUrls
			// 
			listEditServerUrls.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			listEditServerUrls.Items = (List<string>)resources.GetObject("listEditServerUrls.Items");
			listEditServerUrls.Location = new Point(133, 122);
			listEditServerUrls.Name = "listEditServerUrls";
			listEditServerUrls.Readonly = false;
			listEditServerUrls.Size = new Size(1334, 59);
			listEditServerUrls.TabIndex = 15;
			// 
			// labelServerUrls
			// 
			labelServerUrls.AutoSize = true;
			labelServerUrls.Location = new Point(5, 122);
			labelServerUrls.Name = "labelServerUrls";
			labelServerUrls.Size = new Size(62, 15);
			labelServerUrls.TabIndex = 14;
			labelServerUrls.Text = "Server Urls";
			// 
			// txtUrlPrefix
			// 
			txtUrlPrefix.Location = new Point(133, 93);
			txtUrlPrefix.Name = "txtUrlPrefix";
			txtUrlPrefix.Size = new Size(248, 23);
			txtUrlPrefix.TabIndex = 9;
			// 
			// labelUrlPrefix
			// 
			labelUrlPrefix.AutoSize = true;
			labelUrlPrefix.Location = new Point(5, 96);
			labelUrlPrefix.Name = "labelUrlPrefix";
			labelUrlPrefix.Size = new Size(55, 15);
			labelUrlPrefix.TabIndex = 8;
			labelUrlPrefix.Text = "Url Prefix";
			// 
			// labelServiceCategory
			// 
			labelServiceCategory.AutoSize = true;
			labelServiceCategory.Location = new Point(5, 67);
			labelServiceCategory.Name = "labelServiceCategory";
			labelServiceCategory.Size = new Size(60, 15);
			labelServiceCategory.TabIndex = 6;
			labelServiceCategory.Text = "Category*";
			// 
			// btnReadPackageVersionFromPom
			// 
			btnReadPackageVersionFromPom.Location = new Point(537, 34);
			btnReadPackageVersionFromPom.Name = "btnReadPackageVersionFromPom";
			btnReadPackageVersionFromPom.Size = new Size(139, 23);
			btnReadPackageVersionFromPom.TabIndex = 5;
			btnReadPackageVersionFromPom.Text = "Read from pom.xml";
			btnReadPackageVersionFromPom.UseVisualStyleBackColor = true;
			btnReadPackageVersionFromPom.Click += btnReadPackageVersionFromPom_Click;
			// 
			// comboServiceCategory
			// 
			comboServiceCategory.FormattingEnabled = true;
			comboServiceCategory.Location = new Point(133, 64);
			comboServiceCategory.Name = "comboServiceCategory";
			comboServiceCategory.Size = new Size(248, 23);
			comboServiceCategory.TabIndex = 7;
			comboServiceCategory.TextChanged += comboServiceCategory_TextChanged;
			// 
			// btnReadPackageNameFromPom
			// 
			btnReadPackageNameFromPom.Location = new Point(537, 5);
			btnReadPackageNameFromPom.Name = "btnReadPackageNameFromPom";
			btnReadPackageNameFromPom.Size = new Size(139, 23);
			btnReadPackageNameFromPom.TabIndex = 2;
			btnReadPackageNameFromPom.Text = "Read from pom.xml";
			btnReadPackageNameFromPom.UseVisualStyleBackColor = true;
			btnReadPackageNameFromPom.Click += btnReadPackageNameFromPom_Click;
			// 
			// txtPackageName
			// 
			txtPackageName.Location = new Point(133, 6);
			txtPackageName.Name = "txtPackageName";
			txtPackageName.Size = new Size(393, 23);
			txtPackageName.TabIndex = 1;
			txtPackageName.TextChanged += txtPackageName_TextChanged;
			// 
			// labelPackageVersion
			// 
			labelPackageVersion.AutoSize = true;
			labelPackageVersion.Location = new Point(5, 38);
			labelPackageVersion.Name = "labelPackageVersion";
			labelPackageVersion.Size = new Size(92, 15);
			labelPackageVersion.TabIndex = 3;
			labelPackageVersion.Text = "Package Version";
			// 
			// txtPackageVersion
			// 
			txtPackageVersion.Location = new Point(133, 35);
			txtPackageVersion.Name = "txtPackageVersion";
			txtPackageVersion.Size = new Size(393, 23);
			txtPackageVersion.TabIndex = 4;
			// 
			// labelPackageName
			// 
			labelPackageName.AutoSize = true;
			labelPackageName.Location = new Point(5, 9);
			labelPackageName.Name = "labelPackageName";
			labelPackageName.Size = new Size(91, 15);
			labelPackageName.TabIndex = 0;
			labelPackageName.Text = "Package Name*";
			// 
			// tabPageProperties
			// 
			tabPageProperties.Controls.Add(labelSecretProperties);
			tabPageProperties.Controls.Add(listEditSecretProperties);
			tabPageProperties.Controls.Add(labelProperties);
			tabPageProperties.Controls.Add(listEditProperties);
			tabPageProperties.Location = new Point(4, 24);
			tabPageProperties.Name = "tabPageProperties";
			tabPageProperties.Padding = new Padding(3);
			tabPageProperties.Size = new Size(1476, 592);
			tabPageProperties.TabIndex = 2;
			tabPageProperties.Text = "Properties";
			tabPageProperties.UseVisualStyleBackColor = true;
			// 
			// labelSecretProperties
			// 
			labelSecretProperties.AutoSize = true;
			labelSecretProperties.Location = new Point(8, 112);
			labelSecretProperties.Name = "labelSecretProperties";
			labelSecretProperties.Size = new Size(95, 15);
			labelSecretProperties.TabIndex = 3;
			labelSecretProperties.Text = "Secret Properties";
			// 
			// listEditSecretProperties
			// 
			listEditSecretProperties.Items = (List<string>)resources.GetObject("listEditSecretProperties.Items");
			listEditSecretProperties.Location = new Point(124, 112);
			listEditSecretProperties.Name = "listEditSecretProperties";
			listEditSecretProperties.Readonly = false;
			listEditSecretProperties.Size = new Size(469, 100);
			listEditSecretProperties.TabIndex = 2;
			// 
			// labelProperties
			// 
			labelProperties.AutoSize = true;
			labelProperties.Location = new Point(6, 6);
			labelProperties.Name = "labelProperties";
			labelProperties.Size = new Size(60, 15);
			labelProperties.TabIndex = 1;
			labelProperties.Text = "Properties";
			// 
			// listEditProperties
			// 
			listEditProperties.Items = (List<string>)resources.GetObject("listEditProperties.Items");
			listEditProperties.Location = new Point(124, 6);
			listEditProperties.Name = "listEditProperties";
			listEditProperties.Readonly = false;
			listEditProperties.Size = new Size(469, 100);
			listEditProperties.TabIndex = 0;
			// 
			// tabPageAuthentication
			// 
			tabPageAuthentication.Controls.Add(txtCustomBasicAuthentication);
			tabPageAuthentication.Controls.Add(chkUseBasicAuthentication);
			tabPageAuthentication.Controls.Add(labelCustomBasicAuthentication);
			tabPageAuthentication.Controls.Add(labelUseBasicAuthentication);
			tabPageAuthentication.Controls.Add(labelMappings);
			tabPageAuthentication.Controls.Add(listEditMappings);
			tabPageAuthentication.Location = new Point(4, 24);
			tabPageAuthentication.Name = "tabPageAuthentication";
			tabPageAuthentication.Padding = new Padding(3);
			tabPageAuthentication.Size = new Size(1476, 592);
			tabPageAuthentication.TabIndex = 4;
			tabPageAuthentication.Text = "Authentication";
			tabPageAuthentication.UseVisualStyleBackColor = true;
			// 
			// txtCustomBasicAuthentication
			// 
			txtCustomBasicAuthentication.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtCustomBasicAuthentication.Location = new Point(348, 6);
			txtCustomBasicAuthentication.Name = "txtCustomBasicAuthentication";
			txtCustomBasicAuthentication.Size = new Size(1119, 23);
			txtCustomBasicAuthentication.TabIndex = 17;
			// 
			// chkUseBasicAuthentication
			// 
			chkUseBasicAuthentication.AutoSize = true;
			chkUseBasicAuthentication.Location = new Point(129, 10);
			chkUseBasicAuthentication.Name = "chkUseBasicAuthentication";
			chkUseBasicAuthentication.Size = new Size(15, 14);
			chkUseBasicAuthentication.TabIndex = 15;
			chkUseBasicAuthentication.UseVisualStyleBackColor = true;
			chkUseBasicAuthentication.CheckedChanged += chkUseBasicAuthentication_CheckedChanged;
			// 
			// labelCustomBasicAuthentication
			// 
			labelCustomBasicAuthentication.AutoSize = true;
			labelCustomBasicAuthentication.Location = new Point(181, 9);
			labelCustomBasicAuthentication.Name = "labelCustomBasicAuthentication";
			labelCustomBasicAuthentication.Size = new Size(161, 15);
			labelCustomBasicAuthentication.TabIndex = 16;
			labelCustomBasicAuthentication.Text = "Custom Basic Authentication";
			// 
			// labelUseBasicAuthentication
			// 
			labelUseBasicAuthentication.AutoSize = true;
			labelUseBasicAuthentication.Location = new Point(4, 9);
			labelUseBasicAuthentication.Name = "labelUseBasicAuthentication";
			labelUseBasicAuthentication.Size = new Size(116, 15);
			labelUseBasicAuthentication.TabIndex = 14;
			labelUseBasicAuthentication.Text = "Basic Authentication";
			// 
			// labelMappings
			// 
			labelMappings.AutoSize = true;
			labelMappings.Location = new Point(6, 35);
			labelMappings.Name = "labelMappings";
			labelMappings.Size = new Size(60, 15);
			labelMappings.TabIndex = 5;
			labelMappings.Text = "Mappings";
			// 
			// listEditMappings
			// 
			listEditMappings.Items = (List<string>)resources.GetObject("listEditMappings.Items");
			listEditMappings.Location = new Point(129, 35);
			listEditMappings.Name = "listEditMappings";
			listEditMappings.Readonly = false;
			listEditMappings.Size = new Size(611, 100);
			listEditMappings.TabIndex = 4;
			// 
			// tabPageServices
			// 
			tabPageServices.Controls.Add(groupCheckServices);
			tabPageServices.Controls.Add(dataGridServices);
			tabPageServices.Location = new Point(4, 24);
			tabPageServices.Name = "tabPageServices";
			tabPageServices.Padding = new Padding(3);
			tabPageServices.Size = new Size(1476, 592);
			tabPageServices.TabIndex = 5;
			tabPageServices.Text = "Services";
			tabPageServices.UseVisualStyleBackColor = true;
			// 
			// groupCheckServices
			// 
			groupCheckServices.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			groupCheckServices.Controls.Add(btnLoadProfileCheckServices);
			groupCheckServices.Controls.Add(propertyEditCheckServices);
			groupCheckServices.Controls.Add(btnStopCheck);
			groupCheckServices.Controls.Add(txtCheckServiceBaseUrl);
			groupCheckServices.Controls.Add(btnCheckAllEnabled);
			groupCheckServices.Controls.Add(labelCheckServiceBaseUrl);
			groupCheckServices.Controls.Add(comboCheckProfile);
			groupCheckServices.Controls.Add(btnCheck);
			groupCheckServices.Location = new Point(8, 8);
			groupCheckServices.Name = "groupCheckServices";
			groupCheckServices.Size = new Size(1465, 171);
			groupCheckServices.TabIndex = 27;
			groupCheckServices.TabStop = false;
			groupCheckServices.Text = "Check Services";
			// 
			// btnLoadProfileCheckServices
			// 
			btnLoadProfileCheckServices.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnLoadProfileCheckServices.Location = new Point(1108, 49);
			btnLoadProfileCheckServices.Name = "btnLoadProfileCheckServices";
			btnLoadProfileCheckServices.Size = new Size(94, 23);
			btnLoadProfileCheckServices.TabIndex = 27;
			btnLoadProfileCheckServices.Text = "Load Profile";
			btnLoadProfileCheckServices.UseVisualStyleBackColor = true;
			btnLoadProfileCheckServices.Click += btnLoadProfileCheckServices_Click;
			// 
			// propertyEditCheckServices
			// 
			propertyEditCheckServices.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			propertyEditCheckServices.BackColor = SystemColors.ControlLight;
			propertyEditCheckServices.Location = new Point(6, 50);
			propertyEditCheckServices.Name = "propertyEditCheckServices";
			propertyEditCheckServices.ShowSpecifiedCheckbox = true;
			propertyEditCheckServices.Size = new Size(1096, 117);
			propertyEditCheckServices.TabIndex = 24;
			// 
			// btnStopCheck
			// 
			btnStopCheck.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnStopCheck.BackColor = Color.Orange;
			btnStopCheck.Location = new Point(1396, 14);
			btnStopCheck.Name = "btnStopCheck";
			btnStopCheck.Size = new Size(69, 23);
			btnStopCheck.TabIndex = 23;
			btnStopCheck.Text = "STOP";
			btnStopCheck.UseVisualStyleBackColor = false;
			btnStopCheck.Click += btnStopCheck_Click;
			// 
			// txtCheckServiceBaseUrl
			// 
			txtCheckServiceBaseUrl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtCheckServiceBaseUrl.Location = new Point(150, 16);
			txtCheckServiceBaseUrl.Name = "txtCheckServiceBaseUrl";
			txtCheckServiceBaseUrl.Size = new Size(952, 23);
			txtCheckServiceBaseUrl.TabIndex = 25;
			txtCheckServiceBaseUrl.TextChanged += txtCheckServiceBaseUrl_TextChanged;
			// 
			// btnCheckAllEnabled
			// 
			btnCheckAllEnabled.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnCheckAllEnabled.Location = new Point(1252, 14);
			btnCheckAllEnabled.Name = "btnCheckAllEnabled";
			btnCheckAllEnabled.Size = new Size(138, 23);
			btnCheckAllEnabled.TabIndex = 22;
			btnCheckAllEnabled.Text = "Check All Enabled";
			btnCheckAllEnabled.UseVisualStyleBackColor = true;
			btnCheckAllEnabled.Click += btnCheckAllEnabled_Click;
			// 
			// labelCheckServiceBaseUrl
			// 
			labelCheckServiceBaseUrl.AutoSize = true;
			labelCheckServiceBaseUrl.Location = new Point(6, 19);
			labelCheckServiceBaseUrl.Name = "labelCheckServiceBaseUrl";
			labelCheckServiceBaseUrl.Size = new Size(127, 15);
			labelCheckServiceBaseUrl.TabIndex = 26;
			labelCheckServiceBaseUrl.Text = "Base Url (without /xxx)";
			// 
			// comboCheckProfile
			// 
			comboCheckProfile.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			comboCheckProfile.DropDownStyle = ComboBoxStyle.DropDownList;
			comboCheckProfile.FormattingEnabled = true;
			comboCheckProfile.Location = new Point(1208, 50);
			comboCheckProfile.Name = "comboCheckProfile";
			comboCheckProfile.Size = new Size(251, 23);
			comboCheckProfile.TabIndex = 21;
			comboCheckProfile.SelectedIndexChanged += comboCheckProfile_SelectedIndexChanged;
			// 
			// btnCheck
			// 
			btnCheck.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnCheck.Location = new Point(1108, 14);
			btnCheck.Name = "btnCheck";
			btnCheck.Size = new Size(138, 23);
			btnCheck.TabIndex = 20;
			btnCheck.Text = "Check selected services";
			btnCheck.UseVisualStyleBackColor = true;
			btnCheck.Click += btnCheck_Click;
			// 
			// dataGridServices
			// 
			dataGridServices.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			dataGridServices.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridServices.Location = new Point(6, 185);
			dataGridServices.Name = "dataGridServices";
			dataGridServices.Size = new Size(1461, 401);
			dataGridServices.TabIndex = 0;
			// 
			// tabPageSave
			// 
			tabPageSave.Controls.Add(folderOutputDirectory);
			tabPageSave.Controls.Add(labelOutputDirectory);
			tabPageSave.Location = new Point(4, 24);
			tabPageSave.Name = "tabPageSave";
			tabPageSave.Padding = new Padding(3);
			tabPageSave.Size = new Size(1476, 592);
			tabPageSave.TabIndex = 6;
			tabPageSave.Text = "Save";
			tabPageSave.UseVisualStyleBackColor = true;
			// 
			// folderOutputDirectory
			// 
			folderOutputDirectory.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			folderOutputDirectory.Folder = "";
			folderOutputDirectory.InitialBrowseDirectory = "";
			folderOutputDirectory.Location = new Point(108, 6);
			folderOutputDirectory.Name = "folderOutputDirectory";
			folderOutputDirectory.Readonly = false;
			folderOutputDirectory.ShowNewFolderButton = true;
			folderOutputDirectory.Size = new Size(1359, 23);
			folderOutputDirectory.TabIndex = 31;
			folderOutputDirectory.FolderChanged += folderOutputDirectory_FolderChanged;
			// 
			// labelOutputDirectory
			// 
			labelOutputDirectory.AutoSize = true;
			labelOutputDirectory.Location = new Point(6, 9);
			labelOutputDirectory.Name = "labelOutputDirectory";
			labelOutputDirectory.Size = new Size(96, 15);
			labelOutputDirectory.TabIndex = 30;
			labelOutputDirectory.Text = "Output Directory";
			// 
			// btnNext
			// 
			btnNext.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnNext.Location = new Point(96, 6);
			btnNext.Name = "btnNext";
			btnNext.Size = new Size(75, 23);
			btnNext.TabIndex = 19;
			btnNext.Text = "Next >";
			btnNext.UseVisualStyleBackColor = true;
			btnNext.Click += btnNext_Click;
			// 
			// btnPrev
			// 
			btnPrev.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnPrev.Location = new Point(15, 6);
			btnPrev.Name = "btnPrev";
			btnPrev.Size = new Size(75, 23);
			btnPrev.TabIndex = 20;
			btnPrev.Text = "< Previous";
			btnPrev.UseVisualStyleBackColor = true;
			btnPrev.Click += btnPrev_Click;
			// 
			// panelNavigate
			// 
			panelNavigate.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			panelNavigate.Controls.Add(btnPrev);
			panelNavigate.Controls.Add(btnSave);
			panelNavigate.Controls.Add(btnNext);
			panelNavigate.Controls.Add(btnCancel);
			panelNavigate.Location = new Point(1138, 626);
			panelNavigate.Name = "panelNavigate";
			panelNavigate.Size = new Size(334, 32);
			panelNavigate.TabIndex = 21;
			// 
			// FormImportPackageWizard
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(1484, 661);
			Controls.Add(panelNavigate);
			Controls.Add(tabControl);
			MaximizeBox = false;
			MinimizeBox = false;
			Name = "FormImportPackageWizard";
			ShowIcon = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.CenterParent;
			Text = "Add New Package";
			Load += frmAddNewPackage_Load;
			Shown += FormImportPackageWizard_Shown;
			tabControl.ResumeLayout(false);
			tabPageLocation.ResumeLayout(false);
			tabPageLocation.PerformLayout();
			tabPageParameters.ResumeLayout(false);
			tabPageParameters.PerformLayout();
			tabPageProperties.ResumeLayout(false);
			tabPageProperties.PerformLayout();
			tabPageAuthentication.ResumeLayout(false);
			tabPageAuthentication.PerformLayout();
			tabPageServices.ResumeLayout(false);
			groupCheckServices.ResumeLayout(false);
			groupCheckServices.PerformLayout();
			((System.ComponentModel.ISupportInitialize)dataGridServices).EndInit();
			tabPageSave.ResumeLayout(false);
			tabPageSave.PerformLayout();
			panelNavigate.ResumeLayout(false);
			ResumeLayout(false);
		}

		#endregion

		private Button btnCancel;
		private Button btnSave;
		private FolderInput folderInputImport;
		private TabControl tabControl;
		private TabPage tabPageLocation;
		private TabPage tabPageParameters;
		private TabPage tabPageProperties;
		private TabPage tabPageAuthentication;
		private TabPage tabPageServices;
		private Button btnNext;
		private Button btnPrev;
		private Label labelServiceCategory;
		private Button btnReadPackageVersionFromPom;
		private ComboBox comboServiceCategory;
		private Button btnReadPackageNameFromPom;
		private TextBox txtPackageName;
		private Label labelPackageVersion;
		private TextBox txtPackageVersion;
		private Label labelPackageName;
		private Label labelServiceDescriptorModel;
		private FileInput fileInputServiceDescriptorModel;
		private Label labelServiceType;
		private ComboBox comboServiceType;
		private Label labelUrlPrefix;
		private Label labelProperties;
		private StringListEditControl listEditProperties;
		private Label labelSecretProperties;
		private StringListEditControl listEditSecretProperties;
		private TextBox txtUrlPrefix;
		private Label labelServerUrls;
		private StringListEditControl listEditServerUrls;
		private Label labelMappings;
		private StringListEditControl listEditMappings;
		private TabPage tabPageSave;
		private Label labelOutputDirectory;
		private TextBox txtCustomBasicAuthentication;
		private CheckBox chkUseBasicAuthentication;
		private Label labelCustomBasicAuthentication;
		private Label labelUseBasicAuthentication;
		private DataGridView dataGridServices;
		private FolderInput folderOutputDirectory;
		private ComboBox comboCheckProfile;
		private Button btnCheck;
		private Button btnStopCheck;
		private Panel panelNavigate;
		private RadioButton optImportTypeZip;
		private RadioButton optImportTypeSingle;
		private RadioButton optImportTypeFolder;
		private Label labelImportType;
		private FileInput fileInputImport;
		private Label labelImportPath;
		private Editor.PropertyEditor propertyEditCheckServices;
		private Label labelCheckServiceBaseUrl;
		private TextBox txtCheckServiceBaseUrl;
		private GroupBox groupCheckServices;
		private Button btnCheckAllEnabled;
		private Button btnLoadProfileCheckServices;
	}
}