﻿using System.Data;
using System.Diagnostics;
using WSTest.Helpers;

namespace WSTest.UI
{
	public partial class FormSettings : Form
	{
		public static (bool, bool) EditSettings()
		{
			var frm = new FormSettings();
			return (frm.ShowDialog() == DialogResult.OK, frm.ServicesWereChanged);
		}

		bool ServicesWereChanged;

		public FormSettings()
		{
			InitializeComponent();
		}


		private void frmSettings_Load(object sender, EventArgs e)
		{
			var settings = Program.Settings;

			comboTraceLevel.Items.AddRange(Enum.GetValues(typeof(TraceLevel)).Cast<object>().ToArray());
			comboTraceLevel.SelectedItem = settings.TraceLevel;

			comboCodeViewerTheme.Items.AddRange(Enum.GetValues(typeof(MonacoTheme)).Cast<object>().ToArray());
			comboCodeViewerTheme.SelectedItem = settings.CodeViewerTheme;

			numRequestTimeout.Value = settings.RequestTimeout;

			folderInputServiceImportDefaultDir.Folder = settings.ServiceImportDefaultDir;

			chkLoadDefaultServiceDescriptorDir.Checked = settings.LoadDefaultServiceDescriptorDir;
			lstAltServiceDescriptorDirs.Items.AddRange(settings.AltServiceDescriptorDirs.ToArray());


			UpdateContext();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			var settings = Program.Settings;
			bool descriptorConfigChanged = false;

			settings.ServiceImportDefaultDir = folderInputServiceImportDefaultDir.Folder;

			bool loadDefaultServiceDescriptorDir = chkLoadDefaultServiceDescriptorDir.Checked;

			if (settings.LoadDefaultServiceDescriptorDir != loadDefaultServiceDescriptorDir)
			{
				settings.LoadDefaultServiceDescriptorDir = loadDefaultServiceDescriptorDir;
				descriptorConfigChanged = true;
			}

			var altServiceDescriptorDirs = lstAltServiceDescriptorDirs.Items.Cast<string>().ToList();

			if (!settings.AltServiceDescriptorDirs.SequenceEqual(altServiceDescriptorDirs))
			{
				settings.AltServiceDescriptorDirs = altServiceDescriptorDirs;
				descriptorConfigChanged = true;
			}

			settings.TraceLevel = (TraceLevel)comboTraceLevel.SelectedItem!;
			settings.CodeViewerTheme = (MonacoTheme)comboCodeViewerTheme.SelectedItem!;
			settings.RequestTimeout = (int)numRequestTimeout.Value;

			TraceHelper.MinLevel = settings.TraceLevel;

			settings.Save();

			if (descriptorConfigChanged)
				ServicesWereChanged = true;

			this.DialogResult = DialogResult.OK;
		}

		void UpdateContext()
		{
			btnRemoveDescriptorFolder.Enabled = lstAltServiceDescriptorDirs.SelectedIndex >= 0;
		}

		private void btnAddDescriptorFolder_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog fd = new()
			{
				ShowNewFolderButton = false
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			lstAltServiceDescriptorDirs.Items.Add(fd.SelectedPath);
		}

		private void btnRemoveDescriptorFolder_Click(object sender, EventArgs e)
		{
			lstAltServiceDescriptorDirs.Items.RemoveAt(lstAltServiceDescriptorDirs.SelectedIndex);
		}

		private void lstAltServiceDescriptorDirs_SelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}
	}
}
