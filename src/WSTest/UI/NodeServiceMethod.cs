﻿namespace WSTest.UI
{
	internal record NodeServiceMethod
	{
		public HttpServiceMethod Method { get; init; }
		public HttpService Service { get; init; }

		public NodeServiceMethod(HttpServiceMethod method, HttpService service)
		{
			this.Method = method;
			this.Service = service;
		}

		public string Category => Service.Category;
		public string? PackageNameAndVersion => Service.PackageNameAndVersion;
		public string ServiceName => Service.Name;
		public string MethodName => Method.Name;
	}
}
