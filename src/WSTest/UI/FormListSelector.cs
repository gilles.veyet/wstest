﻿namespace WSTest.UI
{
	public partial class FormListSelector : Form
	{
		readonly object[] Items;

		public FormListSelector(object[] items)
		{
			Items = items;
			InitializeComponent();
		}

		public static T? Select<T>(IEnumerable<T> items, string title) where T : class
		{
			var frm = new FormListSelector(items.ToArray())
			{
				Text = title
			};

			if (frm.ShowDialog() != DialogResult.OK)
				return null;

			return frm.listItems.SelectedItem as T;
		}

		private void frmSelector_Load(object sender, EventArgs e)
		{
			btnOK.Enabled = false;
			FilterList();
		}

		private void FilterList()
		{
			listItems.Items.Clear();

			object[] items = Items;

			string filter = txtFilter.Text.Trim();

			if (filter.Length > 0)
				items = Items.Where(t => t.ToString()?.Contains(filter, StringComparison.OrdinalIgnoreCase) ?? false).ToArray();

			listItems.Items.AddRange(items);
		}

		private void listItems_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}

		private void txtFilter_TextChanged(object sender, EventArgs e)
		{
			FilterList();
		}

		private void listItems_SelectedIndexChanged(object sender, EventArgs e)
		{
			btnOK.Enabled = listItems.SelectedIndex >= 0;
		}

	}
}
