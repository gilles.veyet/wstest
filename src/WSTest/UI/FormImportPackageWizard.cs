﻿using System.ComponentModel;
using System.Diagnostics;
using System.IO.Compression;
using System.Xml;
using WSTest.Helpers;

namespace WSTest.UI
{
	record PackageService : IComparable<PackageService>
	{
		public HttpService Service { get; }
		public LoadServiceResult LoadResult { get; }
		public bool Enabled { get; set; }


		public string Name { get; set; } = string.Empty;
		public string? CheckMethod { get; set; } = string.Empty;
		public string SoapServiceUrl { get; set; } = string.Empty;

		public List<string> MethodsWithoutParameters;

		public HttpServiceDescriptor Descriptor => Service.Descriptor;

		public PackageService(HttpService service, LoadServiceResult loadResult, bool enabled)
		{
			Service = service;
			LoadResult = loadResult;
			Enabled = enabled;

			Name = service.Name;
			CheckMethod = Descriptor.CheckMethod;
			MethodsWithoutParameters = service.GetMethodsWithoutParameters().Select(m => m.Name).ToList();

			var defaultCheckMethods = Program.Settings.ServiceImportDefaultCheckMethods;

			if (string.IsNullOrEmpty(CheckMethod) || !MethodsWithoutParameters.Contains(CheckMethod))
			{
				CheckMethod = MethodsWithoutParameters.Where(m => defaultCheckMethods.Contains(m, StringComparer.OrdinalIgnoreCase)).FirstOrDefault();
			}

			if (Descriptor is SoapServiceDescriptor soapServiceDescriptor)
			{
				SoapServiceUrl = !string.IsNullOrEmpty(soapServiceDescriptor.ServiceUrl) ? soapServiceDescriptor.ServiceUrl : GetServiceUrl(soapServiceDescriptor, (LoadSoapServiceResult)LoadResult) ?? string.Empty;
			}
		}

		string? GetServiceUrl(SoapServiceDescriptor descriptor, LoadSoapServiceResult loadResult)
		{
			if (string.IsNullOrWhiteSpace(loadResult.SoapAddressLocation))
			{
				TraceHelper.WriteError($"{nameof(GetServiceUrl)}: missing SoapAddressLocation in service '{descriptor.Name}' - File:'{descriptor.ServiceDefinitionPath}'");
				return null;
			}

			string url = loadResult.SoapAddressLocation;

			try
			{
				var uri = new Uri(url);
				string path = uri.PathAndQuery.Trim('/');

				if (!string.IsNullOrEmpty(descriptor.DefaultUrlPrefix) && path.StartsWith(descriptor.DefaultUrlPrefix + '/'))
				{
					path = path[(descriptor.DefaultUrlPrefix.Length + 1)..];
				}

				if (string.IsNullOrWhiteSpace(path))
				{
					TraceHelper.WriteError($"{nameof(GetServiceUrl)}: invalid SoapAddressLocation in service '{descriptor.Name}' - File:'{descriptor.ServiceDefinitionPath}'");
					return null;
				}

				return path;

			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"{nameof(GetServiceUrl)}: Invalid Url '{url}' in service '{descriptor.Name}' - File:'{descriptor.ServiceDefinitionPath}'", ex);
				return null;
			}
		}
		public void UpdateDescriptor()
		{
			Descriptor.Name = Name;
			Descriptor.CheckMethod = CheckMethod;

			if (Descriptor is SoapServiceDescriptor soapServiceDescriptor)
				soapServiceDescriptor.ServiceUrl = SoapServiceUrl;
		}

		public void SaveDescriptor(string outputDirectory)
		{
			UpdateDescriptor();

			string descriptorFilename = Helper.MakeValidFileName(Descriptor.Name) + DescriptorHelper.GetDescriptorFileExtension(Descriptor.ServiceType);
			string descriptorOutputPath = Path.Combine(outputDirectory, descriptorFilename);
			DescriptorHelper.SaveDescriptor(Descriptor, descriptorOutputPath);
		}

		public async Task CheckAsync(Profile profile, CancellationToken cancellationToken)
		{
			UpdateDescriptor();

			var method = Service.GetCheckMethod();
			if (method != null)
			{
				try
				{
					var result = await Service.RunMethodWithoutParameters(method, profile, cancellationToken);
					CheckSuccess = result.Success;
					CheckResult = result.Success ? "OK" : "Fail: " + result.ErrorResult;
				}
				catch (Exception ex)
				{
					CheckSuccess = false;
					CheckResult = ex.Message;
				}
			}
			else
			{
				CheckSuccess = false;
				CheckResult = "Check Method does not exist on service";
			}

			Enabled = CheckSuccess;
		}

		public int CompareTo(PackageService? other)
		{
			return this.Name.CompareTo(other?.Name);
		}

		public string CheckResult { get; private set; } = string.Empty;
		public bool CheckSuccess { get; private set; } = false;
	}

	public partial class FormImportPackageWizard : Form
	{
		#region Declaration

		enum TabPageEnum
		{
			Location = 0,
			Params,
			Properties,
			Authentication,
			Services,
			Save
		}

		PackageImportDefinition? PackageToUpdate;
		string? UpdateDirectory;
		bool UpdateExistingPackageOrDirectory => PackageToUpdate != null || !string.IsNullOrEmpty(UpdateDirectory);

		TabPageEnum CurrentTab = TabPageEnum.Location;
		readonly DataGrid GridServices;
		BindingList<PackageService> AllServices = [];
		private Dictionary<string, HttpServiceDescriptor> ExistingDescriptorsByServiceDefinitionPath = [];
		Profile? SelectedCheckProfile => comboCheckProfile.SelectedItem as Profile;
		IEnumerable<PackageService> EnabledServices => AllServices.Where(s => s.Enabled);

		ServiceTypes ServiceType => (ServiceTypes)comboServiceType.SelectedItem!;
		HttpServiceDescriptor? ServiceDescriptorModel;

		bool StopCheck = false;
		CancellationTokenSource? CancelMethodTokenSource;

		PackageImportDefinition GetPackageImportDefinition()
		{
			var packageDef = new PackageImportDefinition()
			{
				ServiceCategory = ServiceCategory,
				PackageName = PackageName,
				PackageVersion = PackageVersion,
				ImportType = ImportType,
				ImportPath = ImportPath,
			};

			return packageDef;
		}

		PackageImportType ImportType => optImportTypeSingle.Checked ? PackageImportType.Single : optImportTypeZip.Checked ? PackageImportType.Zip : PackageImportType.Directory;
		string ImportPath => ImportType == PackageImportType.Directory ? folderInputImport.Folder : fileInputImport.FileName;
		string LocalImportPath = string.Empty;
		string ServiceCategory => comboServiceCategory.Text.Trim();
		string UrlPrefix => txtUrlPrefix.Text.Trim();
		List<string> Properties => this.listEditProperties.Items.Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
		List<string> SecretProperties => this.listEditSecretProperties.Items.Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
		IEnumerable<string> AllProperties => Properties.Concat(SecretProperties);
		List<string> Mappings => this.listEditMappings.Items.Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
		List<string> ServerUrls => this.listEditServerUrls.Items;
		string? CustomBasicAuthenticationString => chkUseBasicAuthentication.Checked ? txtCustomBasicAuthentication.Text.Trim() : null;
		string PackageName => txtPackageName.Text.Trim();
		string PackageVersion => txtPackageVersion.Text.Trim();
		string OutputDirectory => folderOutputDirectory.Folder;
		List<XmlQualifiedName> ExcludeDerivedTypes { get; set; } = [];

		#endregion

		#region Main

		void SetMappings(Dictionary<string, string> mappings)
		{
			List<string> list = [];

			foreach (var kv in mappings)
				list.Add($"{kv.Key}={kv.Value}");

			listEditMappings.Items = list;
		}

		Dictionary<string, string> GetMappings()
		{
			Dictionary<string, string> dict = [];

			foreach (string map in Mappings)
			{
				int index = map.IndexOf('=');

				if (index < 0)
					continue;

				string key = map[..index];              // same as map.Substring(0, index);
				string value = map[(index + 1)..];      // same as map.Substring(index + 1);
				dict[key] = value;
			}

			return dict;
		}

		internal static bool ShowPackageWizard(PackageImportDefinition? packageToUpdate = null)
		{
			var frm = new FormImportPackageWizard
			{
				PackageToUpdate = packageToUpdate
			};
			return frm.ShowDialog() == DialogResult.OK;
		}

		internal static bool ShowPackageWizard(string updateDirectory)
		{
			var frm = new FormImportPackageWizard
			{
				UpdateDirectory = updateDirectory
			};
			return frm.ShowDialog() == DialogResult.OK;
		}

		internal FormImportPackageWizard()
		{
			InitializeComponent();
			GridServices = new DataGrid(dataGridServices, new DataGridOptions { ReadOnly = false, MultiSelect = true });

			dataGridServices.DataError += (sender, e) => { };   // required otherwise DataGridView will complain that "DataGridViewComboBoxCell value is not valid"
																//Debug:  dataGridServices.DataError += (sender, e) => Debug.WriteLine($"DataGridServices_DataError Col:{e.ColumnIndex} Row:{e.RowIndex} {e.Context} '{e.Exception?.Message}'");

			dataGridServices.DataBindingComplete += DataGridServices_DataBindingComplete;
		}

		private void DataGridServices_DataBindingComplete(object? sender, DataGridViewBindingCompleteEventArgs e)
		{
			// OK if ComboBox items are set here in DataBindingComplete.
			// But not OK if set in LoadServices just after DataSource.
			int nCol = dataGridServices.Columns[nameof(PackageService.CheckMethod)].Index;
			int nRow = 0;
			foreach (var packageService in AllServices)
				GridServices.SetCellComboBoxItems(nRow++, nCol, packageService.MethodsWithoutParameters);
		}

		private void frmAddNewPackage_Load(object sender, EventArgs e)
		{
			// Hide tab headers. From https://stackoverflow.com/questions/10316567/how-do-i-create-a-tab-control-with-no-tab-header-in-windows-form
			tabControl.Appearance = TabAppearance.FlatButtons;
			tabControl.ItemSize = new Size(0, 1);
			tabControl.SizeMode = TabSizeMode.Fixed;

			comboServiceType.Items.AddRange(Enum.GetValues(typeof(ServiceTypes)).Cast<object>().ToArray());
			comboServiceType.SelectedItem = ServiceTypes.Soap;

			comboServiceCategory.Items.AddRange([.. Program.ServiceDescriptors.Select(s => s.Category).Distinct().OrderBy(n => n, StringComparer.InvariantCultureIgnoreCase)]);

			btnStopCheck.Visible = false;

			txtCustomBasicAuthentication.Enabled = false;
			fileInputServiceDescriptorModel.InitialDirectory = Program.Settings.LastModelServiceDescriptorDirectory ?? string.Empty;
			folderInputImport.ShowNewFolderButton = false;

			optImportTypeFolder.Checked = true;

			if (PackageToUpdate != null)
			{
				this.Text = $"Update package '{PackageToUpdate.PackageName}' in folder '{PackageToUpdate.ActualDirectory}'";

				InitImportType(PackageToUpdate);

				if (!InitWithFirstDescriptor(PackageToUpdate.ActualDirectory!))
				{
					DialogResult = DialogResult.Abort;
					return;
				}
			}
			else if (!string.IsNullOrEmpty(UpdateDirectory))
			{
				this.Text = $"Update package in folder '{UpdateDirectory}'";

				if (!InitWithFirstDescriptor(UpdateDirectory!))
				{
					DialogResult = DialogResult.Abort;
					return;
				}
			}
			else
			{
				folderInputImport.InitialBrowseDirectory = Program.Settings.LastImportPackageDirectory ?? string.Empty;
				fileInputImport.InitialDirectory = Program.Settings.LastImportPackageDirectory ?? string.Empty;
			}

			UpdateNavigation();
		}

		private void FormImportPackageWizard_Shown(object sender, EventArgs e)
		{
			fileInputImport.Location = folderInputImport.Location;
		}

		void InitImportType(PackageImportDefinition packageDef)
		{
			switch (packageDef.ImportType)
			{
				case PackageImportType.Single:
					optImportTypeSingle.Checked = true;
					fileInputImport.FileName = packageDef.ImportPath;
					break;

				case PackageImportType.Zip:
					fileInputImport.FileName = packageDef.ImportPath;
					optImportTypeZip.Checked = true;
					break;

				default:
					folderInputImport.Folder = packageDef.ImportPath;
					optImportTypeFolder.Checked = true;
					break;
			}
		}

		bool InitWithFirstDescriptor(string directory)
		{
			LoadExistingDescriptors(directory!);

			var existingDescriptor = ExistingDescriptorsByServiceDefinitionPath.Values.FirstOrDefault();
			if (existingDescriptor == null)
			{
				MessageBox.Show($"Error: folder '{directory}' does not contains any service descriptors", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return false;
			}

			InitValuesWithExistingDescriptor(existingDescriptor, directory);
			return true;
		}

		void LoadExistingDescriptors(string directory)
		{
			ExistingDescriptorsByServiceDefinitionPath = DescriptorHelper.LoadAllInDirectory(directory, false).ToDictionary(d => d.ServiceDefinitionPath);
		}

		void InitValuesWithExistingDescriptor(HttpServiceDescriptor descriptor, string importDirectory)
		{
			comboServiceType.SelectedItem = descriptor.ServiceType;
			comboServiceType.Enabled = false;

			labelServiceDescriptorModel.Visible = fileInputServiceDescriptorModel.Visible = false;

			txtPackageName.Text = descriptor.PackageName;
			txtPackageName.ReadOnly = true;
			btnReadPackageNameFromPom.Visible = false;

			txtPackageVersion.Text = descriptor.PackageVersion ?? string.Empty;

			comboServiceCategory.Text = descriptor.Category;
			comboServiceCategory.Enabled = false;

			txtUrlPrefix.Text = descriptor.DefaultUrlPrefix;

			listEditServerUrls.Items = descriptor.ServerUrls;

			listEditProperties.Items = descriptor.Properties;
			listEditProperties.Readonly = true;
			listEditSecretProperties.Items = descriptor.SecretProperties;
			listEditSecretProperties.Readonly = true;

			chkUseBasicAuthentication.Checked = descriptor.UseBasicAuthentication;
			chkUseBasicAuthentication.Enabled = false;

			if (descriptor.UseBasicAuthentication)
			{
				txtCustomBasicAuthentication.Text = descriptor.CustomBasicAuthenticationString;
				txtCustomBasicAuthentication.ReadOnly = true;
			}

			SetMappings(descriptor.Mappings);
			listEditMappings.Readonly = true;

			ExcludeDerivedTypes = descriptor.ExcludeDerivedTypes;

			folderOutputDirectory.Folder = importDirectory;
			folderOutputDirectory.Readonly = true;
		}

		void SetupGridServices()
		{
			if (dataGridServices.Columns.Count == 0)
			{
				GridServices.AddCheckboxColumn(nameof(PackageService.Enabled));
				GridServices.AddTextColumn(nameof(PackageService.Name), new ColumnOptions() { Width = 250 });   // allow to resize.

				if (ServiceType == ServiceTypes.Soap)
				{
					// set manual size (re-sizable) because auto-size (not re-sizable) is not great when all initial values are empty (SoapAddressLocation not set in WSDL).
					GridServices.AddTextColumn(nameof(PackageService.SoapServiceUrl), new ColumnOptions() { Width = 500 });
				}

				GridServices.AddComboBoxColumn(nameof(PackageService.CheckMethod));

				GridServices.AddTextColumn(nameof(PackageService.CheckResult), new ColumnOptions() { AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill });
			}
		}

		private void btnPrev_Click(object sender, EventArgs e)
		{
			--CurrentTab;
			UpdateNavigation();
		}

		private async void btnNext_Click(object sender, EventArgs e)
		{
			if (!await ApplyPage())
				return;

			++CurrentTab;
			UpdateNavigation();
		}

		void UpdateNavigation()
		{
			tabControl.SelectedIndex = (int)CurrentTab;
			var first = TabPageEnum.Location;
			var last = TabPageEnum.Save;

			btnPrev.Enabled = CurrentTab != first;
			btnNext.Enabled = CurrentTab != last;
			btnSave.Enabled = CurrentTab == last;

			UpdateContext();
		}

		void UpdateContext()
		{
			bool ok = true;

			switch (CurrentTab)
			{
				case TabPageEnum.Location:
					ok = !string.IsNullOrEmpty(ImportPath);
					break;

				case TabPageEnum.Params:
					ok = !string.IsNullOrEmpty(ServiceCategory) && !string.IsNullOrEmpty(PackageName);
					break;

				case TabPageEnum.Save:
					ok = !string.IsNullOrEmpty(OutputDirectory);
					break;
			}

			var button = CurrentTab == TabPageEnum.Save ? btnSave : btnNext;
			button.Enabled = ok;
		}

		async Task<bool> ApplyPage()
		{
			try
			{
				this.Cursor = Cursors.WaitCursor;

				switch (CurrentTab)
				{
					case TabPageEnum.Location:
						{
							SaveLastImportPackageDirectory();

							if (!CheckServiceDescriptorModel())
								return false;

							ApplyServiceDescriptorModel();
							btnReadPackageNameFromPom.Visible = ImportType == PackageImportType.Directory && !txtPackageName.ReadOnly;
							btnReadPackageVersionFromPom.Visible = ImportType == PackageImportType.Directory;
						}
						break;

					case TabPageEnum.Params:
						if (string.IsNullOrWhiteSpace(OutputDirectory))
							folderOutputDirectory.Folder = Path.Combine(Program.Settings.ServiceImportDefaultDir, ServiceCategory, PackageName);
						break;

					case TabPageEnum.Properties:
						Debug.WriteLine($"Properties: {string.Join(", ", Properties)}");
						break;

					case TabPageEnum.Authentication:
						{
							Debug.WriteLine($"Mappings: {string.Join(", ", Mappings)}");

							if (!await PreparePageServices())
								return false;
						}
						break;

					case TabPageEnum.Services:
						break;

					case TabPageEnum.Save:
						if (!UpdateExistingPackageOrDirectory && Directory.Exists(OutputDirectory))
						{
							MessageBox.Show($"Folder '{OutputDirectory}' already exists. Please select another folder", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
							return false;
						}
						break;
				}

				return true;
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private async Task<bool> PreparePageServices()
		{
			SetupGridServices();

			if (!await LoadServices())
				return false;

			CheckServiceValues = ValueFactory.MakeValuesFromKeyValues(AllProperties.Select(p => new KeyValuePair<string, string>(p, string.Empty)).ToList());
			propertyEditCheckServices.Set(CheckServiceValues);

			labelCheckServiceBaseUrl.Text = "Base Url";

			if (!string.IsNullOrEmpty(UrlPrefix))
				labelCheckServiceBaseUrl.Text += $" (without /{UrlPrefix})";

			LoadCompatibleProfiles();
			return true;
		}

		void SaveLastImportPackageDirectory()
		{
			string? directory = null;

			if (ImportType == PackageImportType.Directory)
			{
				directory = ImportPath;
			}
			else
			{
				if (!Helper.IsUrlValid(ImportPath))
					directory = Path.GetDirectoryName(ImportPath);
			}

			if (!string.IsNullOrEmpty(directory))
			{
				Program.Settings.LastImportPackageDirectory = directory;
				Program.Settings.Save();

			}
		}

		bool CheckServiceDescriptorModel()
		{
			string path = fileInputServiceDescriptorModel.FileName;

			if (string.IsNullOrWhiteSpace(path))
			{
				ServiceDescriptorModel = null;
				return true;
			}

			if (ServiceDescriptorModel?.ActualPath == path)
				return true;

			try
			{
				ServiceDescriptorModel = DescriptorHelper.LoadSingle(path);
			}
			catch (Exception ex)
			{
				Program.ShowError(ex.Message);
				return false;
			}

			Program.Settings.LastModelServiceDescriptorDirectory = Path.GetDirectoryName(path);
			Program.Settings.Save();
			return true;
		}

		void ApplyServiceDescriptorModel()
		{
			var model = ServiceDescriptorModel;

			if (model == null)
				return;

			if (string.IsNullOrWhiteSpace(ServiceCategory))
				comboServiceCategory.Text = model.Category;

			if (string.IsNullOrWhiteSpace(txtUrlPrefix.Text))
				txtUrlPrefix.Text = model.DefaultUrlPrefix;

			if (Properties.Count == 0)
			{
				listEditProperties.Items = model.Properties;
				listEditSecretProperties.Items = model.SecretProperties;
				SetMappings(model.Mappings);
			}

			chkUseBasicAuthentication.Checked = model.UseBasicAuthentication;

			if (string.IsNullOrWhiteSpace(txtCustomBasicAuthentication.Text))
				txtCustomBasicAuthentication.Text = model.CustomBasicAuthenticationString;

			if (ServerUrls.Count == 0)
				listEditServerUrls.Items = model.ServerUrls;

			ExcludeDerivedTypes = model.ExcludeDerivedTypes;
		}

		#endregion

		#region Location

		private void comboServiceType_SelectedIndexChanged(object sender, EventArgs e)
		{
			string ext = DescriptorHelper.GetDescriptorFileExtension(ServiceType);
			fileInputServiceDescriptorModel.Filter = $"{ServiceType} (*{ext})|*.json";

			UpdateFileInputImportPathFilter();
		}

		void UpdateFileInputImportPathFilter()
		{
			if (!fileInputImport.Visible)
				return;

			fileInputImport.Filter = optImportTypeZip.Checked ? "Zip file (*.zip)|*.zip" : DescriptorHelper.GetServiceDefinitionFileDialogFilter(ServiceType);
		}

		void ShowFileInputImport(bool visible)
		{
			fileInputImport.Visible = visible;
			folderInputImport.Visible = !visible;

			UpdateFileInputImportPathFilter();
			UpdateContext();
		}

		private void optImportTypeFolder_CheckedChanged(object sender, EventArgs e)
		{
			ShowFileInputImport(false);
		}

		private void optImportTypeSingle_CheckedChanged(object sender, EventArgs e)
		{
			ShowFileInputImport(true);
		}

		private void optImportTypeZip_CheckedChanged(object sender, EventArgs e)
		{
			ShowFileInputImport(true);
		}

		private void folderInputImport_ValueChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}

		private void fileInputImport_FileNameChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}

		#endregion

		#region Parameters

		private void txtPackageName_TextChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}

		private void comboServiceCategory_TextChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}

		PomReader? GetPomReader()
		{
			if (string.IsNullOrWhiteSpace(folderInputImport.Folder))
				return null;

			return PomReader.GetPomReaderInParentDirectories(folderInputImport.Folder);
		}

		private void btnReadPackageNameFromPom_Click(object sender, EventArgs e)
		{
			var reader = GetPomReader();
			if (reader == null)
				return;

			txtPackageName.Text = reader.GetArtifactId();

			if (string.IsNullOrWhiteSpace(PackageVersion))
				txtPackageVersion.Text = reader.GetVersion();
		}

		private void btnReadPackageVersionFromPom_Click(object sender, EventArgs e)
		{
			var reader = GetPomReader();
			if (reader != null)
				txtPackageVersion.Text = reader.GetVersion();
		}

		#endregion

		#region Authentication

		private void chkUseBasicAuthentication_CheckedChanged(object sender, EventArgs e)
		{
			txtCustomBasicAuthentication.Enabled = chkUseBasicAuthentication.Checked;
		}


		#endregion

		#region Services / Check

		IEnumerable<StringValue> CheckServiceValues = [];
		void LoadCompatibleProfiles()
		{
			btnLoadProfileCheckServices.Enabled = false;

			var profiles = Program.Settings.Profiles.Where(p => p.HasAllProperties(AllProperties)).ToArray();
			comboCheckProfile.Items.Clear();
			comboCheckProfile.Items.AddRange(profiles);

			if (profiles.Length > 0)
				comboCheckProfile.SelectedIndex = 0;

			UpdateContextCheckServices();
		}

		private void btnLoadProfileCheckServices_Click(object sender, EventArgs e)
		{
			if (SelectedCheckProfile == null)
				return;

			txtCheckServiceBaseUrl.Text = SelectedCheckProfile.BaseUrl;

			foreach (var mapping in SelectedCheckProfile.PropertyMappings)
			{
				var value = CheckServiceValues.FirstOrDefault(v => v.Property.Name == mapping.Key);
				if (value != null)
					value.Value = mapping.Value.Value;
			}

			propertyEditCheckServices.RefreshValues();
		}

		private void comboCheckProfile_SelectedIndexChanged(object sender, EventArgs e)
		{
			btnLoadProfileCheckServices.Enabled = comboCheckProfile.SelectedIndex >= 0;
		}

		private void txtCheckServiceBaseUrl_TextChanged(object sender, EventArgs e)
		{
			UpdateContextCheckServices();
		}

		void UpdateContextCheckServices()
		{
			bool ok = txtCheckServiceBaseUrl.Text.Trim().Length > 0;
			btnCheckAllEnabled.Enabled = btnCheck.Enabled = ok;
		}

		private async void btnCheckAllEnabled_Click(object sender, EventArgs e)
		{
			await CheckServices(EnabledServices);
		}

		private async void btnCheck_Click(object sender, EventArgs e)
		{
			await CheckServices(GridServices.SelectedRowIndexes.Select(row => AllServices[row]));
		}

		private async Task CheckServices(IEnumerable<PackageService> packageServices)
		{
			var profile = new Profile
			{
				BaseUrl = txtCheckServiceBaseUrl.Text.Trim()
			};

			foreach (var stringValue in CheckServiceValues)
			{
				profile.PropertyMappings[stringValue.Property.Name] = new ProtectableValue(stringValue.Value);
			}

			StopCheck = false;
			BlockUI();

			try
			{
				foreach (var svc in packageServices)
				{
					if (StopCheck)
						break;

					var service = svc.Service;
					profile.ServiceConfigs[service.Id] = ProfileServiceConfig.MakeDefault(service.Descriptor);

					CancelMethodTokenSource = new();
					await svc.CheckAsync(profile, CancelMethodTokenSource.Token);
					CancelMethodTokenSource = null;
				}

				dataGridServices.Refresh();

			}
			finally
			{
				RestoreUI();
			}
		}

		private void btnStopCheck_Click(object sender, EventArgs e)
		{
			StopCheck = true;

			if (CancelMethodTokenSource == null)
				return;

			try
			{
				CancelMethodTokenSource.Cancel();
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"{nameof(btnStopCheck_Click)}", ex);
			}
		}

		IEnumerable<Control> PageServiceControls => tabPageServices.Controls.OfType<Control>().Where(ctl => ctl != btnStopCheck);

		void BlockUI(bool showCancel = true)
		{
			this.UseWaitCursor = true;  // OK on whole form ( not OK on forms or tabPage controls)

			btnStopCheck.Visible = showCancel;

			panelNavigate.Enabled = false;
			//panelNavigate.UseWaitCursor = true;

			foreach (var ctl in PageServiceControls)
			{
				ctl.Enabled = false;
				//ctl.UseWaitCursor = true;
			}
		}

		void RestoreUI()
		{
			this.UseWaitCursor = false;

			btnStopCheck.Visible = false;

			panelNavigate.Enabled = true;
			//panelNavigate.UseWaitCursor = false;

			foreach (var ctl in PageServiceControls)
			{
				ctl.Enabled = true;
				//ctl.UseWaitCursor = false;
			}
		}

		#endregion

		#region Save

		private void folderOutputDirectory_FolderChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}

		private async void btnSave_Click(object sender, EventArgs e)
		{
			if (!await ApplyPage())
				return;

			this.Cursor = Cursors.WaitCursor;

			var deletedServiceIds = ExistingDescriptorsByServiceDefinitionPath.Values.Select(d => d.Id).ToList();

			// Move existing descriptors to backup dir.
			if (ExistingDescriptorsByServiceDefinitionPath.Count > 0)
			{
				string backupDir = Path.Combine(Program.DirectoryTemp, $"BackupPackage");
				Helper.CleanDirectory(backupDir);
				Directory.CreateDirectory(backupDir);

				foreach (var descriptor in ExistingDescriptorsByServiceDefinitionPath.Values)
				{
					string filePath = descriptor.ActualPath;
					File.Move(filePath, Path.Combine(backupDir, Path.GetFileName(filePath)), true);
				}
			}

			Helper.CleanDirectory(OutputDirectory);
			Directory.CreateDirectory(OutputDirectory);

			HashSet<string> packageDefinitionFiles = [];

			foreach (var svc in EnabledServices)
			{
				if (deletedServiceIds.Contains(svc.Descriptor.Id))
					deletedServiceIds.Remove(svc.Descriptor.Id);

				svc.SaveDescriptor(OutputDirectory);

				foreach (var file in svc.LoadResult.DefinitionFiles)
					packageDefinitionFiles.Add(file);
			}

			// copy service definition ( *.wsdl, *.xsd, *.yaml) to output dir.
			foreach (string file in packageDefinitionFiles)
			{
				string relPath = Path.GetRelativePath(LocalImportPath, file);
				string destPath = Path.GetFullPath(Path.Combine(OutputDirectory, relPath));

				Directory.CreateDirectory(Path.GetDirectoryName(destPath)!);
				File.Copy(file, destPath, true);
			}

			var packageDef = GetPackageImportDefinition();
			packageDef.Save(OutputDirectory);

			await Program.UpdateServiceCache(EnabledServices.Select(s => s.Service), deletedServiceIds);

			this.Cursor = Cursors.Default;

			Program.ShowInfo($"Import successful");

			this.DialogResult = DialogResult.OK;    // close 
		}

		#endregion

		#region Load Services

		async Task<bool> LoadServices()
		{
			int orgErrorCount = TraceHelper.ErrorCount;
			int orgWarningCount = TraceHelper.WarningCount;
			Stopwatch sw = new();
			sw.Start();
			try
			{
				List<PackageService> packageServices = ImportType switch
				{
					PackageImportType.Directory => LoadServicesInDirectory(ImportPath),
					PackageImportType.Single => await LoadServicesSingle(ImportPath),
					PackageImportType.Zip => await LoadServicesFromZipArchive(ImportPath),
					_ => throw new Exception($"{nameof(LoadServices)}:Invalid ImportType {ImportType}"),
				};

				packageServices.Sort();
				AllServices = new(packageServices);
				dataGridServices.DataSource = AllServices;

				sw.Stop();
				int errorCount = TraceHelper.ErrorCount - orgErrorCount;
				int warningCount = TraceHelper.WarningCount - orgWarningCount;
				TraceHelper.WriteInfo($"LoadServices done in {sw.ElapsedMilliseconds} ms - Errors:{errorCount} - Warnings:{warningCount}");

				return true;
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"{nameof(LoadServices)}: {ImportType} '{ImportPath}'", ex);
				Program.ShowError($"Error loading services: {ex.Message}");
				return false;
			}
		}

		static string GetTempImportDirectory()
		{
			string tempDir = Path.Combine(Program.DirectoryTemp, $"Import");
			Helper.CleanDirectory(tempDir);
			Directory.CreateDirectory(tempDir);
			return tempDir;
		}

		async Task<List<PackageService>> LoadServicesSingle(string importPath)
		{
			string path = importPath;

			if (Helper.IsUrlValid(path))
			{
				string tempDir = GetTempImportDirectory();
				path = Path.Combine(tempDir, Helper.MakeValidFileName(PackageName) + DescriptorHelper.GetServiceDefinitionFileExtension(ServiceType));
				await DownloadHelper.Instance.DownloadFileAsync(importPath, path);
			}

			LocalImportPath = Path.GetDirectoryName(path)!;
			return [LoadService(path, LocalImportPath)];
		}

		async Task<List<PackageService>> LoadServicesFromZipArchive(string importPath)
		{
			using ZipArchive zip = Helper.IsUrlValid(importPath) ? new ZipArchive(await DownloadHelper.Instance.GetStreamAsync(importPath), ZipArchiveMode.Read) : ZipFile.OpenRead(importPath);

			string tempDir = GetTempImportDirectory();
			zip.ExtractToDirectory(tempDir);
			return LoadServicesInDirectory(tempDir);
		}

		List<PackageService> LoadServicesInDirectory(string importDirectory)
		{
			LocalImportPath = importDirectory;
			return Directory.GetFiles(importDirectory, DescriptorHelper.GetServiceDefinitionFileFilter(ServiceType), SearchOption.AllDirectories).Select(f => LoadService(f, importDirectory)).ToList();
		}

		private (HttpServiceDescriptor, bool) MakeDescriptor(string relPath)
		{
			HttpServiceDescriptor descriptor;
			bool existing = false;

			if (ExistingDescriptorsByServiceDefinitionPath.TryGetValue(relPath, out var tmp))
			{
				descriptor = tmp;
				existing = true;
			}
			else
			{
				string name = MakeServiceNameFromRelPath(relPath);
				string id = $"{ServiceCategory}.{name}.{Guid.NewGuid()}";
				descriptor = DescriptorHelper.MakeNewDescriptor(ServiceType, id, name, ServiceCategory, relPath);
			}

			descriptor.PackageName = PackageName;
			descriptor.PackageVersion = PackageVersion;
			descriptor.Properties = Properties;
			descriptor.SecretProperties = SecretProperties;
			descriptor.Mappings = GetMappings();
			descriptor.DefaultUrlPrefix = UrlPrefix;
			descriptor.UseBasicAuthentication = chkUseBasicAuthentication.Checked;
			descriptor.CustomBasicAuthenticationString = CustomBasicAuthenticationString;
			descriptor.ServerUrls = ServerUrls;
			descriptor.ExcludeDerivedTypes = ExcludeDerivedTypes;

			return (descriptor, existing);
		}

		PackageService LoadService(string serviceDefinitionPath, string contentDirectory)
		{
			string relativePath = Path.GetRelativePath(contentDirectory, serviceDefinitionPath);

			(var descriptor, bool existing) = MakeDescriptor(relativePath);

			HttpService service = descriptor.MakeService();

			int errorCount = TraceHelper.ErrorCount;
			int warningCount = TraceHelper.WarningCount;

			var loadServiceResult = service.Load(contentDirectory);
			TraceHelper.WriteInfo($"Service '{service}' loaded from file '{descriptor.ServiceDefinitionPath}' in '{ImportPath}' - Methods:{service.Methods.Count()} - Errors:{TraceHelper.ErrorCount - errorCount} - Warnings:{TraceHelper.WarningCount - warningCount}");

			bool enabled = !UpdateExistingPackageOrDirectory || existing;
			PackageService packageService = new(service, loadServiceResult, enabled);

			return packageService;
		}

		static string MakeServiceNameFromRelPath(string relPath)
		{
			string filename = Path.GetFileNameWithoutExtension(relPath);

			if (Path.AltDirectorySeparatorChar != Path.DirectorySeparatorChar)
				relPath = relPath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

			var parts = relPath.Split(Path.DirectorySeparatorChar);

			string? version = null;

			foreach (string part in parts)
			{
				if (part.StartsWith("V", StringComparison.OrdinalIgnoreCase) && int.TryParse(part[1..], out _))
				{
					version = part;
					break;
				}
			}

			return version != null ? $"{filename}.{version}" : filename;
		}


		#endregion
	}
}
