﻿using System.ComponentModel;
using System.Windows.Forms.Design;

namespace WSTest.UI
{
	[Designer(typeof(FolderInputDesigner))]
	[DefaultEvent(nameof(FolderChanged))]
	public partial class FolderInput : UserControl
	{
		[Category("Property Changed"), Browsable(true), Description("Folder was changed")]
		public event EventHandler? FolderChanged;

		[Category("Appearance"), Browsable(true), Description("Selected Folder / Initial folder")]
		public string Folder
		{
			get { return txtFolder.Text.Trim(); }
			set { txtFolder.Text = value; }
		}

		[Category("Appearance"), Browsable(true), Description("The InitialDirectory for FolderBrowserDialog")]
		public string InitialBrowseDirectory { get; set; } = string.Empty;

		[Category("Appearance"), Browsable(true), Description("Controls whether the New Folder button appears in the folder browser dialog box.")]
		public bool ShowNewFolderButton { get; set; } = true;

		[Category("Behavior"), Browsable(true), Description("Controls whether the folder can be changed or not")]
		public bool Readonly
		{
			get { return txtFolder.ReadOnly; }
			set
			{
				txtFolder.ReadOnly = value;
				btnBrowseFolder.Visible = !value;
			}
		}

		public FolderInput()
		{
			InitializeComponent();
		}

		private void btnBrowseFolder_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog fd = new()
			{
				ShowNewFolderButton = ShowNewFolderButton
			};

			if (!string.IsNullOrEmpty(InitialBrowseDirectory))
				fd.InitialDirectory = InitialBrowseDirectory;
			else if (!string.IsNullOrEmpty(Folder))
			{
				fd.InitialDirectory = Folder;
			}

			if (fd.ShowDialog() == DialogResult.OK)
			{
				txtFolder.Text = fd.SelectedPath;
			}
		}

		private void txtFolder_TextChanged(object sender, EventArgs e)
		{
			FolderChanged?.Invoke(this, EventArgs.Empty);
		}

		// Control has a fixed height
		protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
		{
			height = 23;
			base.SetBoundsCore(x, y, width, height, specified);
		}

		// Designer that remove top and bottom resize handles.
		internal class FolderInputDesigner : ControlDesigner
		{
			FolderInputDesigner()
			{
				base.AutoResizeHandles = true;
			}

			public override SelectionRules SelectionRules
			{
				get
				{
					return SelectionRules.LeftSizeable | SelectionRules.RightSizeable | SelectionRules.Moveable;
				}
			}
		}


	}
}
