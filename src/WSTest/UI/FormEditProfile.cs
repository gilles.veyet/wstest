﻿using System.Collections.Immutable;
using System.ComponentModel;
using System.Data;

namespace WSTest.UI
{
	public partial class FormEditProfile : Form
	{
		readonly DataGrid GridServices;

		class ServiceAndConfig(HttpServiceDescriptor descriptor, ProfileServiceConfig config) : IComparable<ServiceAndConfig>
		{
			public HttpServiceDescriptor Descriptor { get; init; } = descriptor;
			public ProfileServiceConfig Config { get; init; } = config;
			public string Name => Descriptor.Name;
			public string Category => Descriptor.Category;
			public string Package => Descriptor.PackageName;
			public string? DefaultUrlPrefix => Descriptor.DefaultUrlPrefix;
			public ProfileServiceConfigUrlMode UrlMode => Config.UrlMode;

			public string BaseUrl { get; set; } = string.Empty;

			public int CompareTo(ServiceAndConfig? other) => this.Descriptor.CompareTo(other?.Descriptor);

			public override string ToString()   // for debugging
			{
				return Descriptor.ToString();
			}
		}

		readonly Profile _Profile;
		BindingList<ServiceAndConfig> ServiceAndConfigList = [];
		IEnumerable<StringValue> _Values = [];

		internal static bool EditProfile(Profile profile)
		{
			var frm = new FormEditProfile(profile);
			return frm.ShowDialog() == DialogResult.OK;
		}

		internal FormEditProfile(Profile profile)
		{
			_Profile = profile;

			InitializeComponent();

			GridServices = new DataGrid(dataGridServices, new DataGridOptions() { MultiSelect = true });
			propertyEdit.ShowSpecifiedCheckbox = false;
		}


		private void frmEditProfile_Load(object sender, EventArgs e)
		{
			this.Text = string.IsNullOrEmpty(_Profile.Name) ? "New profile" : $"Edit {_Profile.Name}";

			comboGroup.Items.AddRange([.. Program.Settings.Profiles.Select(p => p.Group).Distinct().OrderBy(n => n, StringComparer.InvariantCultureIgnoreCase)]);

			btnCancel.Enabled = !Program.Settings.FirstStart;

			txtName.Text = _Profile.Name;
			comboGroup.Text = _Profile.Group;
			txtBaseUrl.Text = _Profile.BaseUrl;

			_Profile.UpdatePropertyMappings();

			PrepareEditPropertyMappings();

			SetupGridColumns();

			UpdateServiceAndConfigList();
			UpdateContext();
		}

		void UpdateServiceAndConfigList()
		{
			var services = _Profile.ServiceConfigs.Select(kv => new ServiceAndConfig(Program.GetService(kv.Key).Descriptor, kv.Value)).ToList();
			services.Sort();

			ServiceAndConfigList = new(services);
			UpdateServiceAndConfigBaseUrl();

			dataGridServices.DataSource = ServiceAndConfigList;
		}

		void UpdateServiceAndConfigBaseUrl()
		{
			foreach (var sc in ServiceAndConfigList)
			{
				sc.BaseUrl = _Profile.GetServiceBaseUrl(sc.Descriptor);
			}
		}

		void PrepareEditPropertyMappings()
		{
			_Values = ValueFactory.MakeValuesFromSimpleMappings(_Profile.SortedPropertyMappings);
			propertyEdit.Set(_Values);
		}

		void UpdatePropertyMappingsFromEditor()
		{
			ValueFactory.UpdateSimpleMappingsWithValues(_Profile.PropertyMappings, _Values);
		}

		void UpdateContext()
		{
			btnOK.Enabled = !string.IsNullOrEmpty(_Profile.Name) && !string.IsNullOrEmpty(_Profile.BaseUrl);
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			UpdatePropertyMappingsFromEditor();

			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void btnChooseServices_Click(object sender, EventArgs e)
		{
			var services = FormChooseServices.ChooseServices(_Profile.ServiceConfigs.Select(sc => sc.Key));
			if (!services.Any())
				return;

			var ids = services.Select(s => s.Id);

			foreach (var key in _Profile.ServiceConfigs.Keys)
			{
				if (!ids.Contains(key))
					_Profile.ServiceConfigs.Remove(key);
			}

			foreach (var svc in services)
			{
				if (!_Profile.ServiceConfigs.ContainsKey(svc.Id))
				{
					_Profile.ServiceConfigs[svc.Id] = ProfileServiceConfig.MakeDefault(svc);
				}
			}

			UpdatePropertyMappingsFromEditor();
			_Profile.UpdatePropertyMappings();
			PrepareEditPropertyMappings();

			UpdateServiceAndConfigList();
		}


		void SetupGridColumns()
		{
			GridServices.AddTextColumn(nameof(ServiceAndConfig.Category));
			GridServices.AddTextColumn(nameof(ServiceAndConfig.Package));
			GridServices.AddTextColumn(nameof(ServiceAndConfig.Name));
			GridServices.AddTextColumn(nameof(ServiceAndConfig.DefaultUrlPrefix), new ColumnOptions() { HeaderText = "Prefix" });
			GridServices.AddTextColumn(nameof(ServiceAndConfig.UrlMode));
			GridServices.AddTextColumn(nameof(ServiceAndConfig.BaseUrl), new ColumnOptions() { AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill });
		}

		//private void grid_CellFormatting(object sender, System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
		//{
		//	ServiceAndConfig sc = ServiceAndConfigList[e.RowIndex];
		//	switch (gridServices.Columns[e.ColumnIndex].Name)
		//	{
		//		case nameof(ServiceAndConfig.Category):
		//			e.Value = sc.Category;
		//			e.FormattingApplied = true;
		//			break;
		//	}
		//}

		private void txtName_Validated(object sender, EventArgs e)
		{
			_Profile.Name = txtName.Text.Trim();
			UpdateContext();
		}

		private void comboGroup_Validated(object sender, EventArgs e)
		{
			_Profile.Group = comboGroup.Text.Trim();
			UpdateContext();
		}

		private void txtBaseUrl_Validated(object sender, EventArgs e)
		{
			_Profile.BaseUrl = txtBaseUrl.Text.Trim();
			UpdateServiceAndConfigBaseUrl();
			ServiceAndConfigList.ResetBindings();
			UpdateContext();
		}

	}
}
