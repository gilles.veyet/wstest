﻿using WSTest.Helpers;

namespace WSTest.UI
{
	public partial class FormAbout : Form
	{
		const string ProjectUrl = "https://gitlab.com/gilles.veyet/wstest/";
		const string ChangelogUrl = "https://gitlab.com/gilles.veyet/wstest/-/blob/master/CHANGELOG.adoc";

		public static void ShowAbout()
		{
			var frm = new FormAbout();
			frm.ShowDialog();
		}

		public FormAbout()
		{
			InitializeComponent();
		}

		private async void FormAbout_Load(object sender, EventArgs e)
		{
			linkLabelProjectUrl.Text = ProjectUrl;
			labelVersion.Text = $"{Program.AppName} {Application.ProductVersion}";
			await LoadCustomAbout();
		}

		async Task LoadCustomAbout()
		{
			string customAboutFile = Path.Combine(Program.DirectoryCustom, "CustomAbout.html");

			if (!File.Exists(customAboutFile))
			{
				this.Height -= webViewCustomAbout.Height;
				webViewCustomAbout.Visible = false;
				return;
			}

			await WebViewHelper.InitWebViewAsync(webViewCustomAbout);
			webViewCustomAbout.CoreWebView2.NewWindowRequested += CoreWebView2_NewWindowRequested;

			string html = await File.ReadAllTextAsync(customAboutFile);

			string customChangelog = Path.Combine(Program.DirectoryCustom, "Changelog.adoc");

			if (File.Exists(customChangelog))
			{
				string changelog = await File.ReadAllTextAsync(customChangelog);
				html += $"<pre>{changelog}</pre>";
			}

			webViewCustomAbout.NavigateToString(html);
		}

		private void FormAbout_FormClosed(object sender, FormClosedEventArgs e)
		{
			if (webViewCustomAbout.CoreWebView2 != null)
				webViewCustomAbout.CoreWebView2.NewWindowRequested -= CoreWebView2_NewWindowRequested;
		}

		private void CoreWebView2_NewWindowRequested(object? sender, Microsoft.Web.WebView2.Core.CoreWebView2NewWindowRequestedEventArgs e)
		{
			e.Handled = true;
			Helper.ShowInBrowser(e.Uri);
		}

		private void linkLabelGitProject_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Helper.ShowInBrowser(ProjectUrl);
		}
		private void linkLabelChangelog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Helper.ShowInBrowser(ChangelogUrl);
		}

	}
}
