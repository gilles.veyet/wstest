using Newtonsoft.Json;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using WSTest.Data;
using WSTest.Helpers;

namespace WSTest.UI
{
	public partial class FormMain : Form
	{
		#region Declarations

		List<Value> _Values = [];

		Profile? PrevProfileChooseMethod;
		Profile? CurrentProfile;    // cannot be null after form is shown since we start with a default profile and you cannot delete last profile.
		HttpService? SelectedService;
		HttpServiceMethod? SelectedMethod;
		CancellationTokenSource? CancelMethodTokenSource;
		readonly DataGrid GridHistory;
		readonly BindingList<HistoryItem> HistoryItems = [];
		int NextHistoryId => HistoryItems.LastOrDefault()?.Id + 1 ?? 0;

		#endregion

		#region Static functions
		static string GetFileDialogFilter(ServiceTypes serviceType)
		{
			return serviceType == ServiceTypes.Soap ? "XML (*.xml)|*.xml" : "JSON (*.json)|*.json";
		}

		#endregion

		#region Display

		async Task ShowRequestCodeAsync(MonacoLanguage language, string content, MonacoOptions? options = null)
		{
			if (codeViewRequest.InvokeRequired)
				codeViewRequest.Invoke(new MethodInvoker(async delegate () { await ShowRequestCodeAsync(language, content, options); }));

			await codeViewRequest.ShowCodeAsync(language, content, options);
		}
		async Task ShowRequestXmlAsync(string xml, MonacoOptions options) => await ShowRequestCodeAsync(MonacoLanguage.xml, xml, options);
		async Task ShowRequestJsonAsync(string json, MonacoOptions options) => await ShowRequestCodeAsync(MonacoLanguage.json, json, options);
		async Task ShowRequestTextAsync(string text, MonacoOptions options) => await ShowRequestCodeAsync(MonacoLanguage.plaintext, text, options);
		async Task ClearRequestAsync(MonacoOptions options) => await ShowRequestTextAsync(String.Empty, options);

		async Task ShowResponseCodeAsync(MonacoLanguage language, string content, MonacoOptions? options = null)
		{
			if (codeViewResponse.InvokeRequired)
				codeViewResponse.Invoke(new MethodInvoker(async delegate () { await ShowResponseCodeAsync(language, content, options); }));

			await codeViewResponse.ShowCodeAsync(language, content, options);
		}

		async Task ShowResponseXmlAsync(string xml, MonacoOptions options) => await ShowResponseCodeAsync(MonacoLanguage.xml, xml, options);
		async Task ShowResponseJsonAsync(string json, MonacoOptions options) => await ShowResponseCodeAsync(MonacoLanguage.json, json, options);
		async Task ShowResponseTextAsync(string text, MonacoOptions options) => await ShowResponseCodeAsync(MonacoLanguage.plaintext, text, options);
		async Task ClearResponseAsync(MonacoOptions options) => await ShowResponseTextAsync(String.Empty, options);

		#endregion

		#region Main UI
		public FormMain()
		{
			Program.DeviceDpi = this.DeviceDpi;
			TraceHelper.WriteInfo($"DeviceDpi:{this.DeviceDpi}");

			InitializeComponent();
			GridHistory = new DataGrid(dataGridHistory, new DataGridOptions() { ColumnHeadersVisible = false });
		}

		private async void frmMain_Load(object sender, EventArgs e)
		{
			if (await WebViewHelper.GetCoreWebView2Environment() == null)
				this.Close();

			panelEditor.Dock = DockStyle.Fill;
			panelCodeEdit.Dock = DockStyle.Fill;
			panelCodeEdit.Visible = false;

			SetupGridHistory();

			btnRunMethod.Enabled = btnPreviewMethod.Enabled = false;
			btnRequestEdit.Enabled = false;
			splitRequestResponse.Visible = false;

			mnuCancel.Visible = false;
			mnuTest.Visible = Program.DebugMode;
		}

		private void FormMain_DpiChanged(object sender, DpiChangedEventArgs e)
		{
			Program.DeviceDpi = e.DeviceDpiNew;
			TraceHelper.WriteInfo($"FormMain_DpiChanged New:{e.DeviceDpiNew} Old:{e.DeviceDpiOld}");
		}

		void SetupGridHistory()
		{
			GridHistory.AddTextColumn(nameof(HistoryItem.ModeAndName));
			dataGridHistory.SelectionChanged += GridHistory_SelectionChanged;
			dataGridHistory.DataSource = HistoryItems;
		}

		private void frmMain_Shown(object sender, EventArgs e)
		{
			var settings = Program.Settings;

			if (settings.FirstStart)
			{
				FormEditProfile.EditProfile(settings.Profiles[0]);
				settings.FirstStart = false;
				settings.Save();
			}

			LoadProfiles();

			TraceHelper.WriteInfo($"{nameof(frmMain_Shown)}:ready");
		}

		private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			SaveSelectionSettings(true);
		}

		private async void mnuSettings_Click(object sender, EventArgs e)
		{
			(bool ok, bool servicesWereChanged) = FormSettings.EditSettings();

			if (ok && servicesWereChanged)
			{
				await RebuildServiceCache();
			}
		}
		private void mnuAbout_Click(object sender, EventArgs e)
		{
			FormAbout.ShowAbout();
		}

		void LoadProfiles()
		{
			var settings = Program.Settings;
			var profile = settings.Profiles.FirstOrDefault(p => p.Id == Program.Settings.LastProfileId) ?? Program.Settings.Profiles[0];
			SelectProfile(profile);
		}

		void SelectProfile(Profile profile)
		{
			SetCurrentProfile(profile);
			LoadServices();
		}

		void SetCurrentProfile(Profile profile)
		{
			CurrentProfile = profile;
			labelCurrentProfile.Text = profile.ToString();
		}

		void LoadServices()
		{
			if (CurrentProfile == null)
				return;

			var settings = Program.Settings;
			var services = CurrentProfile.GetServices();
			HttpService? service;

			HttpService? lastService;
			if (!string.IsNullOrEmpty(settings.LastServiceId) && (lastService = services.FirstOrDefault(s => s.Id == settings.LastServiceId)) != null)
				service = lastService;
			else
				service = services.FirstOrDefault();

			if (service != null)
			{
				var methods = service.Methods;
				HttpServiceMethod? method;

				HttpServiceMethod? lastMethod;
				if (!string.IsNullOrEmpty(settings.LastMethodName) && (lastMethod = methods.FirstOrDefault(m => m.Name == settings.LastMethodName)) != null)
					method = lastMethod;
				else
					method = methods.FirstOrDefault();

				LoadMethod(service, method);
			}
			else
				ResetEditor();
		}

		void SaveSelectionSettings(bool save = false)
		{
			var settings = Program.Settings;

			settings.LastServiceId = SelectedService?.Descriptor.Id;
			settings.LastMethodName = SelectedMethod?.Name;
			settings.LastProfileId = CurrentProfile?.Id;

			if (save)
				settings.Save();
		}

		private void labelCurrentProfile_Click(object sender, EventArgs e)
		{
			ShowSelectProfile();
		}

		private void btnSelectProfile_Click(object sender, EventArgs e)
		{
			ShowSelectProfile();
		}

		private void ShowSelectProfile()
		{
			var profile = FormSelectProfile.SelectProfile(this.labelCurrentProfile.PointToScreen(new Point(0, 30)), CurrentProfile);
			if (profile != null)
			{
				SelectProfile(profile);
			}
		}


		private void btnManageProfiles_Click(object sender, EventArgs e)
		{
			(var profile, var modified) = FormManageProfiles.ManageProfiles(CurrentProfile);
			if (profile != null)
			{
				SelectProfile(profile);
			}
			else if (modified)
				LoadProfiles();     // Current profile could have been modified or deleted => try to reload.
		}

		private void ResetEditor(HttpService? service = null)
		{
			SelectedService = service;
			SelectedMethod = null;
			labelCurrentMethod.Text = string.Empty;
			btnRunMethod.Enabled = btnPreviewMethod.Enabled = false;

			propertyEditor.Clear();

			RemovePreviewHistoryItem();
			SaveSelectionSettings();
		}

		private void SetCurrentMethod(HttpService service, HttpServiceMethod method)
		{
			SelectedService = service;
			SelectedMethod = method;
			labelCurrentMethod.Text = $"{service.Category} {SelectedMethod.Name} - {service.Name} ({service.PackageName})";
			btnRunMethod.Enabled = btnPreviewMethod.Enabled = true;
			btnLoad.Enabled = method.Request != null;

			RemovePreviewHistoryItem();
			SaveSelectionSettings();
		}

		private void LoadMethod(HttpService service, HttpServiceMethod? method)
		{
			if (method == null)
			{
				ResetEditor(service);
				return;
			}

			SetCurrentMethod(service, method);

			_Values = SelectedMethod!.AllProperties.Select(p => ValueFactory.MakeValueFromProperty(p)).ToList();
			propertyEditor.Set(_Values);
		}

		private void labelCurrentMethod_Click(object sender, EventArgs e)
		{
			ShowSelectMethod();
		}

		private void btnSelectMethod_Click(object sender, EventArgs e)
		{
			ShowSelectMethod();
		}

		private void ShowSelectMethod()
		{
			if (CurrentProfile == null)
				return;

			bool restoreFilters = PrevProfileChooseMethod == CurrentProfile;
			PrevProfileChooseMethod = CurrentProfile;

			(var service, var method) = FormChooseMethod.ChooseMethod(this.labelCurrentMethod.PointToScreen(new Point(0, 30)), CurrentProfile, SelectedService, SelectedMethod, restoreFilters);

			if (service != null && method != null)
			{
				LoadMethod(service, method);
			}
		}


		private async void btnRunMethod_Click(object sender, EventArgs e)
		{
			if (CurrentProfile == null || SelectedService == null || SelectedMethod == null)
				throw new Exception($"{nameof(btnRunMethod_Click)}: profile/service/method must be selected");

			BlockUI();
			RemovePreviewHistoryItem();
			dataGridHistory.ClearSelection();   // trigger GridHistory_SelectionChanged =>  splitRequestResponse.Visible = false;

			try
			{
				HttpServiceRunner runner = SelectedService.CreateRunner();
				CancelMethodTokenSource = new();

				var sendData = runner.PrepareMethod(SelectedMethod, CurrentProfile, _Values);
				var result = await HttpClientHelper.SendAsync(sendData, CancelMethodTokenSource.Token);
				CancelMethodTokenSource = null;

				TraceHelper.WriteInfo($"Run {SelectedService.Descriptor.ServiceType} {SelectedMethod.Name} - {sendData} - {result}");

				var historyItem = MakeHistoryItem(HistoryItemModes.Normal, CurrentProfile, SelectedService, SelectedMethod, sendData, result);
				AddHistoryItemAndShow(historyItem);
			}
			catch (Exception ex)
			{
				TraceHelper.WriteError($"RunMethod error: {ex}");
				Program.ShowError(ex.Message);
			}
			finally
			{
				RestoreUI();
			}
		}

		HistoryItem MakeHistoryItem(HistoryItemModes mode, Profile profile, HttpService service, HttpServiceMethod method, HttpSendData httpSendData, HttpResult? httpResult)
		{
			return new HistoryItem()
			{
				Mode = mode,
				Id = NextHistoryId,
				Name = MakeHistoryItemName(profile, service, method),
				ServiceType = service.Descriptor.ServiceType,
				ProfileId = profile.Id,
				ServiceId = service.Id,
				MethodName = method.Name,
				SendData = httpSendData,
				Result = httpResult,
			};
		}


		private static string MakeHistoryItemName(Profile profile, HttpService service, HttpServiceMethod method) => $"{service.Category} {method.Name} - {service.Name} '{profile}'";

		private void mnuCancel_Click(object sender, EventArgs e)
		{
			if (CancelMethodTokenSource == null)
				return;

			try
			{
				CancelMethodTokenSource.Cancel();
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"{nameof(mnuCancel_Click)}", ex);
			}
		}

		IEnumerable<Control> MainUiControls => Controls.OfType<Control>().Where(ctl => ctl != menuStripMain);
		IEnumerable<ToolStripMenuItem> MainMenuItems => menuStripMain.Items.OfType<ToolStripMenuItem>().Where(m => m != mnuCancel);

		void BlockUI(bool showCancel = true)
		{
			this.UseWaitCursor = true;  // OK on whole form ( not OK on forms or tabPage controls)

			mnuCancel.Visible = showCancel;

			foreach (var ctl in MainUiControls)
			{
				ctl.Enabled = false;
				//ctl.UseWaitCursor = true;
			}

			foreach (var item in MainMenuItems)
				item.Enabled = false;
		}

		void RestoreUI()
		{
			this.UseWaitCursor = false;

			mnuCancel.Visible = false;

			foreach (var control in MainUiControls)
			{
				control.Enabled = true;
				//control.UseWaitCursor = false;
			}

			foreach (var item in MainMenuItems)
				item.Enabled = true;
		}

		private void btnPreviewMethod_Click(object sender, EventArgs e)
		{
			if (CurrentProfile == null || SelectedService == null || SelectedMethod == null)
				throw new Exception($"{nameof(btnPreviewMethod_Click)}: profile/service/method must be selected");

			try
			{
				HttpServiceRunner runner = SelectedService.CreateRunner();

				var sendData = runner.PrepareMethod(SelectedMethod, CurrentProfile, _Values);
				RemovePreviewHistoryItem();

				var historyItem = MakeHistoryItem(HistoryItemModes.Preview, CurrentProfile, SelectedService, SelectedMethod, sendData, null);
				AddHistoryItemAndShow(historyItem);
			}
			catch (Exception ex)
			{
				TraceHelper.WriteError($"RunMethod error: {ex}");
				Program.ShowError(ex.Message);
			}
		}

		#endregion

		#region Code Edit

		async Task ShowCodeEditAsync(MonacoLanguage language, string content, MonacoOptions? options = null) => await codeViewEdit.ShowCodeAsync(language, content, options);
		async Task ShowCodeEditXmlAsync(string xml, MonacoOptions options) => await ShowCodeEditAsync(MonacoLanguage.xml, xml, options);
		async Task ShowCodeEditJsonAsync(string json, MonacoOptions options) => await ShowCodeEditAsync(MonacoLanguage.json, json, options);

		void ShowCodeEditPanel(bool show)
		{
			panelEditor.Visible = !show;
			panelCodeEdit.Visible = show;
		}

		private async void btnRequestEdit_Click(object sender, EventArgs e)
		{
			await ShowInEditor(SelectedHistoryItem?.CloneForEdition() ?? throw new Exception($"{btnRequestEdit_Click}: {nameof(SelectedHistoryItem)} is null!"));
		}

		async Task ShowInEditor(HistoryItem item)
		{
			var data = item.SendData;

			txtCodeEditMethod.Text = data.Method.ToString();
			txtCodeEditUrl.Text = data.Url;
			txtCodeEditUrl.ReadOnly = item.ServiceType != ServiceTypes.Rest;
			btnEditorSave.Enabled = btnEditorLoad.Enabled = HttpClientHelper.CanMethodHaveBody(data.Method);

			var monacoOptions = new MonacoOptions() { Readonly = false, Theme = Program.Settings.CodeViewerTheme };

			ShowCodeEditPanel(true);
			codeViewEdit.Visible = true;

			switch (item.ServiceType)
			{
				case ServiceTypes.Rest:
					{
						if (HttpClientHelper.CanMethodHaveBody(data.Method))
							await ShowCodeEditJsonAsync(data.BodyContent ?? string.Empty, monacoOptions);
						else
							codeViewEdit.Visible = false;
					}
					break;

				case ServiceTypes.Soap:
					await ShowCodeEditXmlAsync(data.BodyContent!, monacoOptions);
					break;
			}
		}

		private async void btnEditorRun_Click(object sender, EventArgs e)
		{
			if (SelectedService == null || SelectedMethod == null || CurrentProfile == null)
				return;

			string content = await codeViewEdit.GetEditorContentAsync();
			HttpServiceRunner runner = SelectedService.CreateRunner();

			BlockUI();

			try
			{
				var sendData = runner.PrepareMethod(SelectedMethod, CurrentProfile, content, txtRequestUrl.Text.Trim());

				CancelMethodTokenSource = new();
				var result = await HttpClientHelper.SendAsync(sendData, CancelMethodTokenSource.Token);
				CancelMethodTokenSource = null;

				TraceHelper.WriteInfo($"Run {SelectedService.Descriptor.ServiceType} {SelectedMethod.Name} - {sendData} - {result}");

				var historyItem = MakeHistoryItem(HistoryItemModes.Normal, CurrentProfile, SelectedService, SelectedMethod, sendData, result);
				AddHistoryItemAndShow(historyItem);
			}
			catch (Exception ex)
			{
				TraceHelper.WriteError($"Run error: {ex}");
				Program.ShowError(ex.Message);
			}
			finally
			{
				RestoreUI();
			}
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			if (SelectedService == null || SelectedMethod == null)
				return;

			string? content = LoadFile(SelectedService);
			if (string.IsNullOrEmpty(content))
				return;

			LoadContent(SelectedService, SelectedMethod, content, null);
		}

		private void btnReplay_Click(object sender, EventArgs e)
		{
			var item = SelectedHistoryItem;
			if (item == null)
				return;

			if (panelCodeEdit.Visible)
			{
				if (MessageBox.Show($"This will discard the xml/json request edit panel\r\nDo you want proceed?", "Discard xml/json request edit panel?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
					return;

				ShowCodeEditPanel(false);
			}

			ReplayMethod(item);
		}

		private bool ReplayMethod(HistoryItem historyItem)
		{
			var profile = Program.Settings.Profiles.Find(p => p.Id == historyItem.ProfileId);
			if (profile == null)
			{
				Program.ShowError($"Cannot replay as profile {historyItem.ServiceId} no longer exists");
				return false;
			}

			if (profile != CurrentProfile)
			{
				if (MessageBox.Show($"Do you want to switch to profile {profile.Group} / {profile.Name} to replay method ?", Program.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
					return false;

				SelectProfile(profile);
			}

			var service = profile.GetService(historyItem.ServiceId);
			if (service == null)
			{
				Program.ShowError($"Cannot load service {historyItem.ServiceId} for  method '{historyItem.MethodName}' : no longer part of profile");
				return false;
			}

			var method = service.Methods.First(m => m.Name == historyItem.MethodName);
			if (method == null)
			{
				Program.ShowError($"Cannot load method '{historyItem.MethodName}' : not found in service {service}");
				return false;
			}

			SetCurrentMethod(service, method);

			string content = historyItem.SendData.BodyContent ?? string.Empty;

			return LoadContent(service, method, content, historyItem.SendData.Url);
		}

		async private void btnEditorReplay_Click(object sender, EventArgs e)
		{
			if (SelectedService == null || SelectedMethod == null)
				return;

			string content = codeViewEdit.Visible ? await codeViewEdit.GetEditorContentAsync() : string.Empty;

			if (!LoadContent(SelectedService, SelectedMethod, content, txtCodeEditUrl.Text))
				return;

			ShowCodeEditPanel(false);
		}

		bool LoadContent(HttpService service, HttpServiceMethod method, string content, string? url)
		{
			this.Cursor = Cursors.WaitCursor;
			propertyEditor.Clear();

			try
			{
				_Values = service.GetValueFromContent(method, content, url);
			}
			catch (Exception ex)
			{
				this.Cursor = Cursors.Default;
				TraceHelper.WriteException($"{nameof(LoadContent)}: Error loading content '{content}'", ex);
				Program.ShowError(ex.Message);
				return false;
			}

			propertyEditor.Set(_Values, 5); // this can a while if request is large.
			this.Cursor = Cursors.Default;
			return true;
		}

		private async void btnEditorLoad_Click(object sender, EventArgs e)
		{
			if (SelectedService == null)
				return;

			string? content = LoadFile(SelectedService);
			if (string.IsNullOrEmpty(content))
				return;

			await codeViewEdit.SetEditorContent(content, SelectedService.Descriptor.ServiceType == ServiceTypes.Rest ? MonacoLanguage.json : MonacoLanguage.xml);
		}

		static string? LoadFile(HttpService service)
		{
			OpenFileDialog fd = new()
			{
				Filter = GetFileDialogFilter(service.Descriptor.ServiceType)
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return null;

			return File.ReadAllText(fd.FileName);
		}


		private async void btnEditorSave_Click(object sender, EventArgs e)
		{
			if (SelectedService == null || SelectedMethod == null)
				return;

			SaveFileDialog fd = new()
			{
				FileName = $"{Helper.MakeValidFileName(SelectedMethod.Name)}_Request",
				Filter = GetFileDialogFilter(SelectedService.Descriptor.ServiceType)
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			string fileName = fd.FileName;
			string content = await codeViewEdit.GetEditorContentAsync();

			try
			{
				await File.WriteAllTextAsync(fileName, content);
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"Error saving '{fileName}'", ex);
				Program.ShowError(ex.Message);
			}
		}


		private void btnEditorDiscard_Click(object sender, EventArgs e)
		{
			ShowCodeEditPanel(false);
		}

		#endregion

		#region History

		HistoryItem? SelectedHistoryItem
		{
			get
			{
				int? index = GridHistory.SelectedRowIndex;
				//issue with MethodBase.GetCurrentMethod() in async method : it returns "MoveNext" instead of method name, see  https://github.com/Daddoon/BlazorMobile/issues/68
				//Debug.Print($"{nameof(GridHistory_SelectionChanged)}: index:{index}");
				return index != null ? HistoryItems[index.Value] : null;
			}
		}

		void RemovePreviewHistoryItem()
		{
			var historyItem = HistoryItems.FirstOrDefault(h => h.Mode == HistoryItemModes.Preview);
			if (historyItem != null)
				HistoryItems.Remove(historyItem);
		}

		void AddHistoryItemAndShow(HistoryItem historyItem)
		{
			HistoryItems.Add(historyItem);
			GridHistory.SelectRow(HistoryItems.Count - 1);
		}

		async Task ShowHistoryItem(HistoryItem item)
		{
			//Stopwatch sw = new();
			//sw.Start();

			var data = item.SendData;

			splitRequestResponse.Visible = true;

			txtRequestUrl.Text = data.Url;
			txtRequestMethod.Text = data.Method.ToString();
			btnRequestEdit.Enabled = true;
			btnReplay.Enabled = true;

			var monacoOptions = new MonacoOptions() { Readonly = true, Theme = Program.Settings.CodeViewerTheme };

			List<Task> codeViewTasks = [];
			Task? requestTask = null;

			switch (item.ServiceType)
			{
				case ServiceTypes.Rest:
					{
						if (HttpClientHelper.CanMethodHaveBody(data.Method))
							requestTask = ShowRequestJsonAsync(JsonHelper.PrettyPrintJson(data.BodyContent ?? String.Empty), monacoOptions);
					}
					break;

				case ServiceTypes.Soap:
					requestTask = ShowRequestXmlAsync(data.BodyContent!, monacoOptions);
					break;
			}

			btnRequestSave.Enabled = codeViewRequest.Visible = requestTask != null;

			if (requestTask != null)
				codeViewTasks.Add(requestTask);

			var result = item.Result;

			if (result != null)     // item.Mode == HistoryItemModes.Normal || item.Mode == HistoryItemModes.Edited
			{
				Task? responseTask = null;
				var sbResponseStatus = new StringBuilder();

				if (result.HttpStatus != null)
					sbResponseStatus.Append($"Status:{result.HttpStatus} ({(int)result.HttpStatus}) ");

				if (result.IsResponsePresent)
				{
					sbResponseStatus.Append($"Length:{result.Response?.Length}  Type:{result.ResponseMediaType}");

					if (result.IsResponseText)
					{
						string text = result.ResponseAsString;

						responseTask = result.ResponseMediaType switch
						{
							MediaTypes.Xml => ShowResponseXmlAsync(XmlHelper.PrettyPrintXml(text), monacoOptions),
							MediaTypes.Json => ShowResponseJsonAsync(JsonHelper.PrettyPrintJson(text), monacoOptions),
							_ => ShowResponseTextAsync(text, monacoOptions),
						};
					}
				}
				else
				{
					if (result.Exception != null)
					{
						var err = $"Exception:{result.ExceptionMessage}";
						sbResponseStatus.Append(err);
						responseTask = ShowResponseTextAsync(err, monacoOptions);
					}
					else
						sbResponseStatus.Append("No Response");
				}

				txtResponseStatus.Text = sbResponseStatus.ToString();
				codeViewResponse.Visible = responseTask != null;
				btnResponseSave.Enabled = result.IsResponsePresent;

				if (responseTask != null)
					codeViewTasks.Add(responseTask);
			}

			splitRequestResponse.Panel2Collapsed = result == null;

			if (codeViewTasks.Count > 0)
				await Task.WhenAll(codeViewTasks);

			//sw.Stop();
			//TraceHelper.WriteInfo($"{nameof(ShowHistoryItem)} done in {sw.ElapsedMilliseconds} ms");
		}

		private async void GridHistory_SelectionChanged(object? sender, EventArgs e)
		{
			var item = SelectedHistoryItem;

			if (item != null)
			{
				await ShowHistoryItem(item);
			}
			else
			{
				// Note: GridHistory_SelectionChanged is triggered twice when a new entry is added.
				//   GridHistory_SelectionChanged: index:       - triggered by ClearSelection which is called before selecting new row.
				//   GridHistory_SelectionChanged: index: 8     - triggered when new row is selected.
				splitRequestResponse.Visible = false;
			}
		}

		private async void btnRequestSave_Click(object sender, EventArgs e)
		{
			var item = SelectedHistoryItem;
			if (item == null || string.IsNullOrEmpty(item.SendData.BodyContent))
				return;

			SaveFileDialog fd = new()
			{
				FileName = $"{Helper.MakeValidFileName(item.MethodName)}_Request",
				Filter = GetFileDialogFilter(item.ServiceType)
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			string fileName = fd.FileName;
			try
			{
				await File.WriteAllTextAsync(fileName, item.SendData.BodyContent);
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"Error saving '{fileName}'", ex);
				Program.ShowError(ex.Message);
			}
		}

		private async void btnResponseSave_Click(object sender, EventArgs e)
		{
			var item = SelectedHistoryItem;
			if (item == null || item.Result?.IsResponsePresent != true)
				return;

			var type = item.Result.ResponseMediaType;
			var ext = type switch
			{
				MediaTypes.Text => "*.txt",
				MediaTypes.RichText => "*.rtf",
				MediaTypes.Json or MediaTypes.Xml or MediaTypes.Html or MediaTypes.PDF or MediaTypes.Zip => $"*.{type.ToString().ToLower()}",
				MediaTypes.Image => item.Result.ResponseImageType != null ? $"*.{item.Result.ResponseImageType.ToString()?.ToLower()}" : "*.*",
				_ => "*.*"
			};

			SaveFileDialog fd = new()
			{
				FileName = $"{Helper.MakeValidFileName(item.MethodName)}_Response",
				Filter = $"{type} ({ext})|{ext}"
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			string fileName = fd.FileName;
			try
			{
				await (item.Result.IsResponseText ? File.WriteAllTextAsync(fileName, item.Result.PrettyResponse) : File.WriteAllBytesAsync(fileName, item.Result.Response!));
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"Error saving '{fileName}'", ex);
				Program.ShowError(ex.Message);
			}
		}


		#endregion

		#region Menu Tools

		private void mnuExportSettings_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog fd = new()
			{
				Description = "Select folder to export settings",
				UseDescriptionForTitle = true
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			string path = Path.Combine(fd.SelectedPath, Settings.SaveFilename);

			JsonProtectableValueConverter.SaveProtectedValuesWithoutEncryption = true;
			Program.Settings.Save(path);
			JsonProtectableValueConverter.SaveProtectedValuesWithoutEncryption = false;

			Program.ShowInfo($"Settings have been exported to file {path}");
		}

		#endregion

		#region Test

		private void mnuSaveCustomSettingsSample_Click(object sender, EventArgs e)
		{
			SaveFileDialog fd = new()
			{
				Title = "Save CustomSettings sample",
				InitialDirectory = Program.DirectoryCustom,
				FileName = CustomSettings.CustomSettingsFilename,
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			const string DST_SDFR = "DST-SDFR";
			CustomProfile profile = new()
			{
				Name = DST_SDFR,
				Group = "Production",
				BaseUrl = "https://service.webhost.skidata.com",
				ServiceCategories = ["DTA", "DCM"]
			};

			profile.PropertyMappings[WellKnownProperties.ClientName] = DST_SDFR;
			profile.PropertyMappings[WellKnownProperties.WorkingClientName] = DST_SDFR;

			CustomSettings settings = new()
			{
				LoadDefaultServiceDescriptorDir = false,
				AltServiceDescriptorDirs = [@"D:\Workspaces\PAC\pac-tools\wstest.skidata\Services"],
				Profiles = [profile]
			};


			settings.Save(fd.FileName);
		}


		private void mnuSaveCustomInfoSample_Click(object sender, EventArgs e)
		{
			SaveFileDialog fd = new()
			{
				Title = "Save CustomInfo sample",
				InitialDirectory = Program.DirectoryCustom,
				FileName = CustomInfo.CustomInfoFilename,
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			CustomInfo info = new()
			{
				Version = "2023.08.00",
				Name = "WSTest.Skidata"
			};

			info.Save(fd.FileName);
		}


		private void mnuTestSerializeServices_Click(object sender, EventArgs e)
		{
			if (SelectedService == null)
				return;

			var jsonSettings = new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore,
				TypeNameHandling = TypeNameHandling.Auto,
				PreserveReferencesHandling = PreserveReferencesHandling.Objects,
				Converters = [new JSonXmlQualifiedNameConverter(), new JsonProtectableValueConverter()]
			};

			string json = JsonConvert.SerializeObject(new List<HttpService>() { SelectedService }, Formatting.Indented, jsonSettings);
			File.WriteAllText("test_service.json", json);
			Debug.WriteLine(json);

			var readBack = JsonConvert.DeserializeObject<List<HttpService>>(json, jsonSettings);
			string readJson = JsonConvert.SerializeObject(readBack, Formatting.Indented, jsonSettings);

			//if (!_Service.Equals(readBack))	//does not work for some unknown reason.
			if (readJson != json)
			{
				File.WriteAllText("read_service.json", readJson);
				MessageBox.Show($"Save service error: Read object does not match original object");
				return;
			}

			MessageBox.Show("Save service OK");
		}

		private void mnuTestSerializeProfile_Click(object sender, EventArgs e)
		{
			var jsonSettings = new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore,
				TypeNameHandling = TypeNameHandling.Auto,
				PreserveReferencesHandling = PreserveReferencesHandling.Objects,
				Converters = [new JSonXmlQualifiedNameConverter(), new JsonProtectableValueConverter()]
			};

			string json = JsonConvert.SerializeObject(new List<Profile>() { GetTempProfile() }, Formatting.Indented, jsonSettings);
			File.WriteAllText("test_profile.json", json);
			Debug.WriteLine(json);

			var readBack = JsonConvert.DeserializeObject<List<Profile>>(json, jsonSettings);
			string readJson = JsonConvert.SerializeObject(readBack, Formatting.Indented, jsonSettings);

			if (readJson != json)
			{
				File.WriteAllText("read_profile.json", readJson);
				MessageBox.Show($"Save profile error: Read object does not match original object");
				return;
			}

			MessageBox.Show("Save profile OK");
		}

		static Profile GetTempProfile()
		{
			var profile = new Profile() { BaseUrl = "http:/dummy.url", Name = "dummy" };
			profile.SetDefaultServices();
			profile.PropertyMappings[WellKnownProperties.ClientName] = new ProtectableValue("D-Work");
			profile.PropertyMappings[WellKnownProperties.WorkingClientName] = new ProtectableValue("D-Work");
			profile.PropertyMappings[WellKnownProperties.UserName] = new ProtectableValue("vegi");
			//do not set password because encrypted password have some random part so comparison before/after serialization would fail.
			//profile.PropertyMappings[WellKnownProperties.Password] = new ProtectableValue("password", true);
			profile.PropertyMappings[WellKnownProperties.PointOfSaleName] = new ProtectableValue("POS-VEGI");
			profile.PropertyMappings[WellKnownProperties.SalesChannelName] = new ProtectableValue("D-Work-SC");

			return profile;
		}

		private async void mnuSaveServiceDescriptorSample_Click(object sender, EventArgs e)
		{
			await TestSaveRestDescriptor();
			await TestSaveSoapDescriptor();
		}

		private async Task TestSaveRestDescriptor()
		{
			RestServiceDescriptor restDs = new()
			{
				Id = "Skidata.Summit.ContractsAndInvoicing.v1",
				Name = "ContractsAndInvoicing",
				Category = "Summit",
				UseBasicAuthentication = false,
				ServiceDefinitionPath = "http://localhost:5000/ContractsAndInvoicingJobs/api/v1/openapi/yaml",
			};

			restDs.ServerUrls.Add("http://localhost:5000/ContractsAndInvoicingJobs");
			await TestSaveServiceDescriptor(restDs);
		}

		async Task TestSaveSoapDescriptor()
		{
			SoapServiceDescriptor soapDs = new()
			{
				Id = "gilles.wstest.samples.Foo",
				Name = "Foo",
				Category = "Test",
				ServiceDefinitionPath = "Foo.wsdl",
				ServiceUrl = "/webservice/foobar/v1/services/FooBarServicePort",
				UseBasicAuthentication = false,
			};

			soapDs.ServerUrls.Add("http://localhost/foo");

			soapDs.Properties.AddRange([WellKnownProperties.ClientName, WellKnownProperties.UserName, WellKnownProperties.PointOfSaleName, WellKnownProperties.SalesChannelName]);
			soapDs.SecretProperties.AddRange([WellKnownProperties.Password]);

			soapDs.Mappings["http://www.foo.com/foobar/v1/soapheader:AuthenticationHeader|ClientName"] = $"{{{WellKnownProperties.ClientName}}}";
			soapDs.Mappings["http://www.foo.com/foobar/v1/soapheader:AuthenticationHeader|UserName"] = $"{{{WellKnownProperties.UserName}}}";
			soapDs.Mappings["http://www.foo.com/foobar/v1/soapheader:AuthenticationHeader|Password"] = $"{{{WellKnownProperties.Password}}}";
			soapDs.Mappings["http://www.foo.com/foobar/v1/soapheader:AuthenticationHeader|SalesContext|PointOfSaleName"] = $"{{{WellKnownProperties.PointOfSaleName}}}";
			soapDs.Mappings["http://www.foo.com/foobar/v1/soapheader:AuthenticationHeader|SalesContext|SalesChannelName"] = $"{{{WellKnownProperties.SalesChannelName}}}";

			await TestSaveServiceDescriptor(soapDs);
		}

		async Task TestSaveServiceDescriptor<T>(T svcDescriptor) where T : HttpServiceDescriptor
		{
			var options = MonacoOptions.Default;

			await ClearRequestAsync(options);
			await ClearResponseAsync(options);

			var jsonSettings = new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore,
				//TypeNameHandling = TypeNameHandling.Auto,
			};

			string json = JsonConvert.SerializeObject(svcDescriptor, Formatting.Indented, jsonSettings);
			await ShowRequestJsonAsync(json, options);

			var readBack = JsonConvert.DeserializeObject<T>(json, jsonSettings);
			string readJson = JsonConvert.SerializeObject(readBack, Formatting.Indented, jsonSettings);

			//if (!svcDescriptor.Equals(readBack))	does not work for some unknown reason.
			if (readJson != json)
			{
				await ShowResponseJsonAsync(readJson, options);
				MessageBox.Show($"TestSaveServiceDescriptor {typeof(T).Name} Read object does not match original object");
				return;
			}
		}

		private void mnuLoadService_Click(object sender, EventArgs e)
		{
			OpenFileDialog fd = new()
			{
				CheckFileExists = true,
				Filter = string.Join('|', [DescriptorHelper.GetServiceDefinitionFileDialogFilter(ServiceTypes.Soap), DescriptorHelper.GetServiceDefinitionFileDialogFilter(ServiceTypes.Rest)])
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			string path = fd.FileName;

			if (Path.GetExtension(path).Equals(".yaml", StringComparison.CurrentCultureIgnoreCase))
				TestLoadRestService(path);
			else
				TestLoadSoapService(path);
		}

		private async void mnuTestReloadService_Click(object sender, EventArgs e)
		{
			var serviceDescriptor = FormListSelector.Select(Program.GetAllServiceDescriptors(), "Select service to reload");
			if (serviceDescriptor is null)
				return;

			await Program.ReloadService(serviceDescriptor);
			LoadServices();
		}

		private void TestLoadRestService(string path)
		{
			Debug.Print($"Load '{path}'");

			this.Cursor = Cursors.WaitCursor;

			var dummyDescriptor = new RestServiceDescriptor() { Id = Guid.NewGuid().ToString(), Name = Path.GetFileNameWithoutExtension(path), ServiceDefinitionPath = Path.GetFileName(path) };
			var svc = new RestService(dummyDescriptor);

			int orgErrorCount = TraceHelper.ErrorCount;
			svc.Load(Path.GetDirectoryName(path)!);

			int errorCount = TraceHelper.ErrorCount - orgErrorCount;

			if (errorCount > 0)
				Program.ShowError($"Service {svc.Name} loaded with {errorCount} errors");

			this.Cursor = Cursors.Default;
		}

		private void TestLoadSoapService(string path)
		{
			Debug.Print($"Load '{path}'");

			this.Cursor = Cursors.WaitCursor;
			var svc = TestGetSoapServiceFromPath(path);

			var method = svc.Methods[0] as SoapMethod;

			if (method?.SoapHeader != null)
			{
				Debug.Print("Soap headers property chains");
				ShowSoapHeader(new StringBuilder(), svc, method.SoapHeader);
			}

			this.Cursor = Cursors.Default;
		}

		static void ShowSoapHeader(StringBuilder sb, SoapService svc, Property prop, Property? parent = null)
		{
			if (prop.IsPropertyOrActualTypeArray)
			{
				Program.ShowError($"Error ShowSoapHeader : array of '{prop.Name}' cannot be handled");
				return;
			}

			var ptype = prop.ActualType;

			if (ptype is ChoicePropertyType choicePropertyType)
			{
				foreach (var child in choicePropertyType.Properties)
					ShowSoapHeader(new StringBuilder(sb.ToString()), svc, child, parent);
			}
			else
			{
				if (parent != null)
					sb.Append('|');

				if (parent != null && parent.QualifiedName.Namespace == prop.QualifiedName.Namespace)
					sb.Append(prop.Name);
				else
					sb.Append(prop.QualifiedName.ToString());

				if (ptype is ObjectPropertyType objectPropertyType)
				{
					foreach (var child in objectPropertyType.Properties)
						ShowSoapHeader(new StringBuilder(sb.ToString()), svc, child, prop);
				}
				else if (ptype is OneOfPropertyType)
					Program.ShowError($"Error ShowSoapHeader : type {ptype.GetType().Name} '{ptype.Name}' cannot be handled");
				else
				{
					Debug.Print(sb.ToString());
				}
			}
		}

		private static SoapService TestGetSoapServiceFromPath(string path)
		{
			var dummyDescriptor = new SoapServiceDescriptor() { Id = Guid.NewGuid().ToString(), Name = Path.GetFileNameWithoutExtension(path), ServiceDefinitionPath = Path.GetFileName(path) };
			//dummyDescriptor.DescriptorPath = ??
			var svc = new SoapService(dummyDescriptor);

			int orgErrorCount = TraceHelper.ErrorCount;
			svc.Load(Path.GetDirectoryName(path)!);

			int errorCount = TraceHelper.ErrorCount - orgErrorCount;

			if (errorCount > 0)
				Program.ShowError($"Service {svc.Name} loaded with {errorCount} errors");

			return svc;
		}

		#endregion

		#region Services / Packages

		private async void mnuServicesRebuildCache_Click(object sender, EventArgs e)
		{
			await RebuildServiceCache();
		}

		async Task RebuildServiceCache()
		{
			this.Cursor = Cursors.WaitCursor;
			await Program.BuildServiceCache();
			LoadServices();                         // reload current service.
			this.Cursor = Cursors.Default;
		}


		private void mnuServicesAddNewPackage_Click(object sender, EventArgs e)
		{
			FormImportPackageWizard.ShowPackageWizard();
		}

		private void mnuServicesUpdateFolder_Click(object sender, EventArgs e)
		{
			var settings = Program.Settings;

			FolderBrowserDialog fd = new()
			{
				Description = "Select folder to update",
				InitialDirectory = settings.LastUpdatePackageDirectory ?? string.Empty,
				ShowNewFolderButton = false
			};

			if (fd.ShowDialog() != DialogResult.OK)
				return;

			string directory = fd.SelectedPath;

			settings.LastUpdatePackageDirectory = directory;
			settings.Save();

			var packageDefinition = PackageImportDefinition.FindInDirectory(directory);

			bool result;

			if (packageDefinition != null)
				result = FormImportPackageWizard.ShowPackageWizard(packageDefinition);
			else
				result = FormImportPackageWizard.ShowPackageWizard(directory);

			if (result)
				LoadServices(); // reload current service.
		}

		private void mnuServicesReloadPackage_Click(object sender, EventArgs e)
		{
			List<PackageImportDefinition> packageDefinitions = [];

			foreach (string dir in Program.GetAllServiceLocations())
				packageDefinitions.AddRange(PackageImportDefinition.LoadAllInDirectory(dir, true));

			var packageDef = FormListSelector.Select(packageDefinitions, "Select Package to reload");
			if (packageDef == null)
				return;

			if (FormImportPackageWizard.ShowPackageWizard(packageDef))
				LoadServices(); // reload current service.
		}

		#endregion

	}
}