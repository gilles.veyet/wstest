﻿using System.ComponentModel;

namespace WSTest.UI
{
	public partial class FormManageProfiles : Form
	{
		readonly Profile? InitialProfile;
		readonly DataGrid Grid;
		BindingList<Profile> Profiles = [];
		bool Modified = false;

		Profile? SelectedProfile
		{
			get
			{
				int? index = Grid.SelectedRowIndex;
				return index != null ? Profiles[index.Value] : null;
			}
		}

		internal static (Profile?, bool) ManageProfiles(Profile? currentProfile)
		{
			var frm = new FormManageProfiles(currentProfile);
			return (frm.ShowDialog() == DialogResult.OK ? frm.SelectedProfile : null, frm.Modified);
		}

		internal FormManageProfiles(Profile? currentProfile)
		{
			InitialProfile = currentProfile;
			InitializeComponent();
			Grid = new DataGrid(dataGridProfiles, DataGridOptions.Default);
		}

		private void frmManageProfiles_Load(object sender, EventArgs e)
		{
			SetupGridColumns();

			Profiles = new BindingList<Profile>(Program.Settings.Profiles);
			dataGridProfiles.SelectionChanged += DataGridProfiles_SelectionChanged;
			dataGridProfiles.DataSource = Profiles;

			if (InitialProfile != null)
				Grid.SelectRow(Profiles.IndexOf(InitialProfile));

			UpdateContext();
		}

		void SetupGridColumns()
		{
			Grid.AddTextColumn(nameof(Profile.Group));
			Grid.AddTextColumn(nameof(Profile.Name));
			Grid.AddTextColumn(nameof(Profile.BaseUrl));
			Grid.AddTextColumn(nameof(Profile.PropertyListInfo), new ColumnOptions() { HeaderText = "Properties", AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill });
		}

		private void DataGridProfiles_SelectionChanged(object? sender, EventArgs e)
		{
			UpdateContext();
		}

		void UpdateContext()
		{
			bool selectOk = SelectedProfile != null;
			btnSelect.Enabled = selectOk;
			btnEdit.Enabled = selectOk;
			btnDuplicate.Enabled = selectOk;
			btnDelete.Enabled = selectOk && Profiles.Count > 1;
		}

		private void btnSelect_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}

		private void btnNew_Click(object sender, EventArgs e)
		{
			var profile = new Profile();
			//Do not select all services  - profile.SetDefaultServices();

			EditAndSaveNewProfile(profile);
		}

		private void dataGridProfiles_DoubleClick(object sender, EventArgs e)
		{
			EditProfile();
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			EditProfile();
		}

		private void EditProfile()
		{
			var profile = SelectedProfile;

			if (profile == null)
				return;

			var clone = profile.Clone();

			if (FormEditProfile.EditProfile(clone))
			{
				int index = Profiles.IndexOf(profile);
				Profiles[index] = clone;
				SaveProfiles();
			}
		}


		private void btnDuplicate_Click(object sender, EventArgs e)
		{
			var profile = SelectedProfile;

			if (profile == null)
				return;

			var clone = profile.Clone();
			clone.Id = Guid.NewGuid().ToString();
			clone.Name = String.Empty;

			EditAndSaveNewProfile(clone);
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			var profile = SelectedProfile;

			if (profile == null)
				return;

			if (MessageBox.Show($"Confirm deletion of profile {profile}", $"Delete profile {profile}", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
				return;

			Profiles.Remove(profile);
			SaveProfiles();
		}

		private void EditAndSaveNewProfile(Profile profile)
		{
			if (!FormEditProfile.EditProfile(profile))
				return;

			Profiles.Add(profile);
			SaveProfiles();
			Grid.SelectRow(Profiles.IndexOf(profile));
		}

		void SaveProfiles()
		{
			var settings = Program.Settings;
			settings.Profiles = [.. Profiles];
			settings.Profiles.Sort();
			settings.Save();

			Modified = true;
			UpdateContext();
		}

	}
}
