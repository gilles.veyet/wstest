﻿namespace WSTest.UI
{
	partial class PasswordInput
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			txtPassword = new TextBox();
			btnShow = new Button();
			btnHide = new Button();
			SuspendLayout();
			// 
			// txtPassword
			// 
			txtPassword.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtPassword.Location = new Point(0, 0);
			txtPassword.Name = "txtPassword";
			txtPassword.Size = new Size(227, 23);
			txtPassword.TabIndex = 0;
			txtPassword.TextChanged += txtPassword_TextChanged;
			// 
			// btnShow
			// 
			btnShow.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnShow.Location = new Point(233, -1);
			btnShow.Name = "btnShow";
			btnShow.Size = new Size(75, 23);
			btnShow.TabIndex = 1;
			btnShow.Text = "Show";
			btnShow.UseVisualStyleBackColor = true;
			btnShow.Click += btnShow_Click;
			// 
			// btnHide
			// 
			btnHide.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnHide.Location = new Point(117, 0);
			btnHide.Name = "btnHide";
			btnHide.Size = new Size(75, 23);
			btnHide.TabIndex = 2;
			btnHide.Text = "Hide";
			btnHide.UseVisualStyleBackColor = true;
			btnHide.Visible = false;
			btnHide.Click += btnHide_Click;
			// 
			// PasswordInput
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			Controls.Add(btnHide);
			Controls.Add(btnShow);
			Controls.Add(txtPassword);
			Name = "PasswordInput";
			Size = new Size(308, 23);
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private TextBox txtPassword;
		private Button btnShow;
		private Button btnHide;
	}
}
