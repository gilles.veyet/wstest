﻿using System.Data;
using WSTest.Extensions;
using WSTest.Helpers;

namespace WSTest.UI
{
	public partial class FormChooseServices : Form
	{
		readonly IEnumerable<string> ServiceDescriptorIds;
		readonly TreeViewHolder<ServiceDescriptor> TreeHolderServices;
		bool FormLoaded = false;

		internal static IEnumerable<HttpServiceDescriptor> ChooseServices(IEnumerable<string> serviceDescriptorIds)
		{
			var frm = new FormChooseServices(serviceDescriptorIds);

			if (frm.ShowDialog() != DialogResult.OK)
				return [];

			return frm.SelectedServices;
		}

		IEnumerable<HttpServiceDescriptor> SelectedServices => treeServices.Nodes.Descendants().Where(n => n.Checked && n.Tag is HttpServiceDescriptor).Select(n => (HttpServiceDescriptor)n.Tag);

		public FormChooseServices(IEnumerable<string> serviceDescriptorIds)
		{
			this.ServiceDescriptorIds = serviceDescriptorIds;
			InitializeComponent();
			TreeHolderServices = new(treeServices, Program.ServiceDescriptors, s => s.Name, s => s.Category, s => s.PackageNameAndVersion);
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}

		private void frmChooseServices_Load(object sender, EventArgs e)
		{
			treeServices.CheckBoxes = true;

			btnOK.Enabled = false;

			this.Cursor = Cursors.WaitCursor;

			TreeHolderServices.LoadItems(-1);
			TreeHolderServices.CheckItems(Program.ServiceDescriptors.Where(s => ServiceDescriptorIds.Contains(s.Id)));

			FormLoaded = true;

			this.Cursor = Cursors.Default;
		}

		private bool IsInsideAfterCheck = false;

		private void treeServices_AfterCheck(object sender, TreeViewEventArgs e)
		{
			if (!FormLoaded || IsInsideAfterCheck)
				return;

			IsInsideAfterCheck = true;  // prevent stack overflow when we change node.Parent.Checked 

			var node = e.Node;

			if (node == null)
				return;

			treeServices.BeginUpdate();
			TreeHelper.SetCheckedDescendantNode(node, node.Checked);
			treeServices.EndUpdate();

			btnOK.Enabled = SelectedServices.Any();
			IsInsideAfterCheck = false;
		}
	}
}
