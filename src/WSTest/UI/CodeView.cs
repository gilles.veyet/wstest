﻿using Microsoft.Web.WebView2.Core;
using System.Diagnostics;
using System.Text;
using System.Text.Json.Nodes;
using WSTest.Helpers;

namespace WSTest.UI
{

	public enum MonacoLanguage
	{
		json,
		xml,
		plaintext,
	}

	public enum MonacoTheme
	{
		Dark,
		Light,
	}

	public record MonacoOptions
	{
		public bool Readonly { get; init; } = false;
		public MonacoTheme Theme { get; init; } = MonacoTheme.Dark;

		public static readonly MonacoOptions Default = new() { Readonly = true, Theme = MonacoTheme.Dark };
		internal static readonly MonacoOptions Initial = Default;  // see initial options in Monaco\index.html
	}

	public partial class CodeView : UserControl
	{
		#region Main

		bool EditorReady { get; set; } = false;
		MonacoOptions Options = MonacoOptions.Initial;

		public CodeView()
		{
			InitializeComponent();
		}

		protected override async void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (!DesignMode)
			{
				await WebViewHelper.InitWebViewAsync(webView);

				if (!Program.DebugMode)
				{
					webView.CoreWebView2.Settings.AreBrowserAcceleratorKeysEnabled = false;
					// Browser context menu is not available when monaco-editor is loaded so Inspect is not available to show dev tools.
					// So dev tools can be only shown with keyboard F12 or Control-Shift-I
					// So this line would be mostly useless as default is true:  webView.CoreWebView2.Settings.AreDevToolsEnabled = false;
				}

				// init monaco-editor in background as CodeView Request & Response are loaded but initially not visible
				// so it is faster when we display first request / response
				await InitEditor();
			}
		}

		#endregion

		#region Public methods

		public async Task ClearCodeAsync(MonacoOptions? options = null)
		{
			await ShowCodeAsync(MonacoLanguage.plaintext, string.Empty, options);
		}

		public async Task ShowCodeAsync(MonacoLanguage language, string text, MonacoOptions? options = null)
		{
			if (options == null)
				options = MonacoOptions.Default;

			await SetEditorOptionsAsync(options);
			await SetEditorContent(text, language);
		}

		public async Task<string> GetEditorContentAsync()
		{
			if (!EditorReady)
				return string.Empty;

			var raw = await webView.ExecuteScriptAsync($"editor.getValue();");
			return JsonNode.Parse(raw)!.ToString();
		}

		public async Task<string> GetEditorViewState()
		{
			if (!EditorReady)
				throw new Exception("Editor not ready");

			return await webView.ExecuteScriptAsync($"editor.saveViewState();");
		}

		// Is able to restore folding state provided that code is fully expanded before calling this method because it looks like it is able to collapse regions but not to expand regions which have been collapsed after saveViewState has been called.
		public async Task RestoreEditorViewState(string json)
		{
			if (!EditorReady)
				throw new Exception("Editor not ready");

			await webView.ExecuteScriptAsync($"editor.restoreViewState({json});");
		}

		/// <summary>
		/// Display html.
		/// WARNING : 2 MB max. Seems to be 2MB chars actually as I successfully loaded a 1.2 MB text file.
		/// </summary>
		public void DisplayHtml(string html)
		{
			webView.NavigateToString(html);
		}

		public async Task NavigateUrlAsync(Uri uri) => await NavigateAsync(uri);

		#endregion

		#region Monaco Editor

		readonly SemaphoreSlim semaphoreInitEditor = new(1);

		private async Task InitEditor()
		{
			await semaphoreInitEditor.WaitAsync();

			try
			{
				if (EditorReady)
					return;

				string index = CommandLineHelper.IsKeyPresent("monacodev") ? "index_dev.html" : "index.html";
				string url = Path.Combine(Program.DirectoryMonaco, index);

				TraceHelper.WriteVerbose($"{Name} Load {url}");
				await NavigateAsync(new Uri(url));
				Options = MonacoOptions.Initial;
				EditorReady = true;
			}
			finally
			{
				semaphoreInitEditor.Release();
			}
		}

		public async Task SetEditorContent(string content, MonacoLanguage language)
		{
			await InitEditor();

			string json = ((JsonNode)content!).ToJsonString();
			await webView.ExecuteScriptAsync($"monaco.editor.setModelLanguage(editor.getModel(),'{language}');editor.setValue({json});");
		}

		private async Task SetEditorOptionsAsync(MonacoOptions options)
		{
			await InitEditor();

			if (!options.Equals(Options))
			{
				StringBuilder sb = new();
				sb.Append("editor.updateOptions({");
				sb.Append($"readOnly: {((JsonNode)options.Readonly).ToJsonString()}");
				sb.Append(',');
				sb.Append($"theme: '{GetMonacoThemeName(options.Theme)}'");
				sb.Append("})");

				await webView.ExecuteScriptAsync(sb.ToString());
				Options = options;
			}
		}

		static string GetMonacoThemeName(MonacoTheme theme)
		{
			return theme switch
			{
				MonacoTheme.Light => "vs",
				_ => "vs-dark"
			};
		}

		#endregion

		#region WebView

		private async Task NavigateAsync(Uri uri)
		{
			if (webView.Source == uri)
			{
				TraceHelper.WriteVerbose($"{Name} NavigateAsync: skip as '{uri}' is already current page");
				return;
			}

			Stopwatch sw = new();
			sw.Start();

			AutoResetEvent wait = new(false);
			CoreWebView2NavigationCompletedEventArgs? result = null;

			EditorReady = false;  // will be set to true in InitEditor()

			webView.NavigationCompleted += Completed;
			webView.Source = uri;

			await Task.Run(() => wait.WaitOne(Program.DebugMode ? 600000 : 5000));  // allow 10 minutes in debug mode to prevent crash when app is in break mode.
			sw.Stop();

			webView.NavigationCompleted -= Completed;

			TraceHelper.WriteVerbose($"{Name} Navigate to '{uri}' result:{result?.IsSuccess} done in {sw.ElapsedMilliseconds} ms");

			if (result == null)
				throw new Exception($"{nameof(NavigateAsync)}: timeout navigating to {uri}");

			if (!result.IsSuccess)
				throw new Exception($"{nameof(NavigateAsync)}: error navigating to {uri} - Status:{result.WebErrorStatus}");

			void Completed(object? sender, CoreWebView2NavigationCompletedEventArgs e)
			{
				TraceHelper.WriteVerbose($"{Name} NavigationCompleted {uri}");
				result = e;
				wait.Set();
			}
		}

		#endregion
	}
}
