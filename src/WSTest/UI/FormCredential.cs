﻿using WSTest.Data;

namespace WSTest.UI
{
	public partial class FormCredential : Form
	{
		string UserName => txtUser.Text.Trim();
		string Password => passwordInput.Password;

		internal static Credential? AskCredential(Credential credential)
		{
			var frm = new FormCredential();
			frm.txtDomain.Text = credential.Domain;
			frm.txtUser.Text = credential.UserName;

			if (frm.ShowDialog() != DialogResult.OK)
				return null;

			return new Credential(credential.Domain) { UserName = frm.UserName, Password = frm.Password };
		}

		public FormCredential()
		{
			InitializeComponent();
		}

		private void FormCredential_Shown(object sender, EventArgs e)
		{
			txtUser.Select();   // set Focus
		}

		private void passwordInput_PasswordChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}

		private void txtUser_TextChanged(object sender, EventArgs e)
		{
			UpdateContext();
		}

		void UpdateContext()
		{
			btnOK.Enabled = !string.IsNullOrWhiteSpace(UserName) && !string.IsNullOrWhiteSpace(Password);
		}
	}
}
