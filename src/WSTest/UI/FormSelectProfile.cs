﻿namespace WSTest.UI
{
	public partial class FormSelectProfile : Form
	{
		bool Loaded = false;
		readonly TreeViewHolder<Profile> TreeHolderProfiles;
		readonly Profile? InitialProfile;

		Profile? SelectedProfile => treeViewProfiles.SelectedNode?.Tag as Profile;

		bool FilteredTree = false;
		string? SelectedFilterCategory => comboFilterGroup.SelectedItem as string;

		internal static Profile? SelectProfile(Point location, Profile? currentProfile)
		{
			var form = new FormSelectProfile(location, currentProfile);

			if (form.ShowDialog() == DialogResult.OK)
				return form.SelectedProfile;
			else
				return null;
		}

		internal FormSelectProfile(Point location, Profile? currentProfile)
		{
			InitialProfile = currentProfile;
			InitializeComponent();
			this.Location = location;

			TreeHolderProfiles = new(treeViewProfiles, Program.Settings.Profiles, p => p.Name, p => p.Group);
			btnOK.Enabled = false;

			comboFilterGroup.Items.AddRange(Program.Settings.Profiles.Where(s => !string.IsNullOrEmpty(s.Group)).Select(s => s.Group).Distinct().OrderBy(n => n, StringComparer.InvariantCultureIgnoreCase).ToArray());
		}

		private void FormSelectProfile_Load(object sender, EventArgs e)
		{
			TreeHolderProfiles.LoadItems(-1);
			TreeHolderProfiles.SelectItem(InitialProfile);
		}

		private void FormSelectProfile_Shown(object sender, EventArgs e)
		{
			treeViewProfiles.Select();  // same effect as Focus but Select is a better high-level function.
			Loaded = true;
		}

		private void treeViewProfiles_AfterSelect(object sender, TreeViewEventArgs e)
		{
			btnOK.Enabled = SelectedProfile != null;

			if (Loaded && btnOK.Enabled)
				this.DialogResult = DialogResult.OK;
		}

		private void comboFilterGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			ApplyFilter();
		}

		private void txtFilterName_TextChanged(object sender, EventArgs e)
		{
			ApplyFilter();
		}

		private void btnResetFilterGroup_Click(object sender, EventArgs e)
		{
			comboFilterGroup.SelectedIndex = -1;
		}

		private void btnResetFilterName_Click(object sender, EventArgs e)
		{
			txtFilterName.ResetText();
		}

		static bool FilterNode(Profile profile, string? filterGroup, string? filterName)
		{
			if (filterGroup != null && profile.Group != filterGroup)
				return false;

			if (filterName != null && !profile.Name.Contains(filterName, StringComparison.CurrentCultureIgnoreCase))
				return false;

			return true;
		}

		void ApplyFilter()
		{
			string? filterName = txtFilterName.Text.Trim();
			if (filterName.Length < 3)
				filterName = null;

			string? filterGroup = SelectedFilterCategory;

			bool filter = filterName != null || filterGroup != null;

			if (filter || FilteredTree)
			{
				FilteredTree = filter;
				TreeHolderProfiles.LoadItems(-1, n => FilterNode(n, filterGroup, filterName));
			}

		}
	}
}
