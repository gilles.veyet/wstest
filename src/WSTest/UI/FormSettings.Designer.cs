﻿namespace WSTest.UI
{
	partial class FormSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			lstAltServiceDescriptorDirs = new ListBox();
			btnCancel = new Button();
			btnOK = new Button();
			label1 = new Label();
			btnAddDescriptorFolder = new Button();
			btnRemoveDescriptorFolder = new Button();
			chkLoadDefaultServiceDescriptorDir = new CheckBox();
			labelTraceLevel = new Label();
			comboTraceLevel = new ComboBox();
			label2 = new Label();
			groupServiceImport = new GroupBox();
			folderInputServiceImportDefaultDir = new FolderInput();
			labelRequestTimeout = new Label();
			numRequestTimeout = new NumericUpDown();
			labelCodeViewerTheme = new Label();
			comboCodeViewerTheme = new ComboBox();
			groupServiceImport.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)numRequestTimeout).BeginInit();
			SuspendLayout();
			// 
			// lstAltServiceDescriptorDirs
			// 
			lstAltServiceDescriptorDirs.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			lstAltServiceDescriptorDirs.FormattingEnabled = true;
			lstAltServiceDescriptorDirs.ItemHeight = 15;
			lstAltServiceDescriptorDirs.Location = new Point(18, 159);
			lstAltServiceDescriptorDirs.Name = "lstAltServiceDescriptorDirs";
			lstAltServiceDescriptorDirs.Size = new Size(573, 109);
			lstAltServiceDescriptorDirs.TabIndex = 8;
			lstAltServiceDescriptorDirs.SelectedIndexChanged += lstAltServiceDescriptorDirs_SelectedIndexChanged;
			// 
			// btnCancel
			// 
			btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnCancel.DialogResult = DialogResult.Cancel;
			btnCancel.Location = new Point(540, 388);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new Size(75, 23);
			btnCancel.TabIndex = 13;
			btnCancel.Text = "Cancel";
			btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnOK
			// 
			btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnOK.Location = new Point(459, 388);
			btnOK.Name = "btnOK";
			btnOK.Size = new Size(75, 23);
			btnOK.TabIndex = 12;
			btnOK.Text = "OK";
			btnOK.UseVisualStyleBackColor = true;
			btnOK.Click += btnOK_Click;
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Location = new Point(18, 141);
			label1.Name = "label1";
			label1.Size = new Size(294, 15);
			label1.TabIndex = 7;
			label1.Text = "Load service descriptors from these  additional folders:";
			// 
			// btnAddDescriptorFolder
			// 
			btnAddDescriptorFolder.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnAddDescriptorFolder.FlatAppearance.BorderSize = 0;
			btnAddDescriptorFolder.FlatStyle = FlatStyle.Flat;
			btnAddDescriptorFolder.Image = Properties.Resources.Add;
			btnAddDescriptorFolder.Location = new Point(595, 159);
			btnAddDescriptorFolder.Name = "btnAddDescriptorFolder";
			btnAddDescriptorFolder.Size = new Size(20, 20);
			btnAddDescriptorFolder.TabIndex = 9;
			btnAddDescriptorFolder.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnAddDescriptorFolder.Click += btnAddDescriptorFolder_Click;
			// 
			// btnRemoveDescriptorFolder
			// 
			btnRemoveDescriptorFolder.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnRemoveDescriptorFolder.FlatAppearance.BorderSize = 0;
			btnRemoveDescriptorFolder.FlatStyle = FlatStyle.Flat;
			btnRemoveDescriptorFolder.Image = Properties.Resources.Delete;
			btnRemoveDescriptorFolder.Location = new Point(595, 185);
			btnRemoveDescriptorFolder.Name = "btnRemoveDescriptorFolder";
			btnRemoveDescriptorFolder.Size = new Size(20, 20);
			btnRemoveDescriptorFolder.TabIndex = 10;
			btnRemoveDescriptorFolder.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnRemoveDescriptorFolder.Click += btnRemoveDescriptorFolder_Click;
			// 
			// chkLoadDefaultServiceDescriptorDir
			// 
			chkLoadDefaultServiceDescriptorDir.AutoSize = true;
			chkLoadDefaultServiceDescriptorDir.Location = new Point(18, 108);
			chkLoadDefaultServiceDescriptorDir.Name = "chkLoadDefaultServiceDescriptorDir";
			chkLoadDefaultServiceDescriptorDir.Size = new Size(255, 19);
			chkLoadDefaultServiceDescriptorDir.TabIndex = 6;
			chkLoadDefaultServiceDescriptorDir.Text = "Load service descriptors from default folder";
			chkLoadDefaultServiceDescriptorDir.UseVisualStyleBackColor = true;
			// 
			// labelTraceLevel
			// 
			labelTraceLevel.AutoSize = true;
			labelTraceLevel.Location = new Point(18, 15);
			labelTraceLevel.Name = "labelTraceLevel";
			labelTraceLevel.Size = new Size(64, 15);
			labelTraceLevel.TabIndex = 0;
			labelTraceLevel.Text = "Trace Level";
			// 
			// comboTraceLevel
			// 
			comboTraceLevel.DropDownStyle = ComboBoxStyle.DropDownList;
			comboTraceLevel.FormattingEnabled = true;
			comboTraceLevel.Location = new Point(174, 7);
			comboTraceLevel.Name = "comboTraceLevel";
			comboTraceLevel.Size = new Size(204, 23);
			comboTraceLevel.TabIndex = 1;
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Location = new Point(6, 30);
			label2.Name = "label2";
			label2.Size = new Size(96, 15);
			label2.TabIndex = 0;
			label2.Text = "Default Directory";
			// 
			// groupServiceImport
			// 
			groupServiceImport.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			groupServiceImport.Controls.Add(folderInputServiceImportDefaultDir);
			groupServiceImport.Controls.Add(label2);
			groupServiceImport.Location = new Point(12, 290);
			groupServiceImport.Name = "groupServiceImport";
			groupServiceImport.Size = new Size(599, 76);
			groupServiceImport.TabIndex = 11;
			groupServiceImport.TabStop = false;
			groupServiceImport.Text = "Service Import";
			// 
			// folderInputServiceImportDefaultDir
			// 
			folderInputServiceImportDefaultDir.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			folderInputServiceImportDefaultDir.Folder = "";
			folderInputServiceImportDefaultDir.InitialBrowseDirectory = "";
			folderInputServiceImportDefaultDir.Location = new Point(108, 22);
			folderInputServiceImportDefaultDir.Name = "folderInputServiceImportDefaultDir";
			folderInputServiceImportDefaultDir.Readonly = false;
			folderInputServiceImportDefaultDir.ShowNewFolderButton = true;
			folderInputServiceImportDefaultDir.Size = new Size(471, 23);
			folderInputServiceImportDefaultDir.TabIndex = 1;
			// 
			// labelRequestTimeout
			// 
			labelRequestTimeout.AutoSize = true;
			labelRequestTimeout.Location = new Point(18, 70);
			labelRequestTimeout.Name = "labelRequestTimeout";
			labelRequestTimeout.Size = new Size(150, 15);
			labelRequestTimeout.TabIndex = 4;
			labelRequestTimeout.Text = "Request Timeout (seconds)";
			// 
			// numRequestTimeout
			// 
			numRequestTimeout.Location = new Point(174, 68);
			numRequestTimeout.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
			numRequestTimeout.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
			numRequestTimeout.Name = "numRequestTimeout";
			numRequestTimeout.Size = new Size(59, 23);
			numRequestTimeout.TabIndex = 5;
			numRequestTimeout.TextAlign = HorizontalAlignment.Right;
			numRequestTimeout.Value = new decimal(new int[] { 1, 0, 0, 0 });
			// 
			// labelCodeViewerTheme
			// 
			labelCodeViewerTheme.AutoSize = true;
			labelCodeViewerTheme.Location = new Point(18, 42);
			labelCodeViewerTheme.Name = "labelCodeViewerTheme";
			labelCodeViewerTheme.Size = new Size(140, 15);
			labelCodeViewerTheme.TabIndex = 2;
			labelCodeViewerTheme.Text = "Viewer (xml, json) Theme";
			// 
			// comboCodeViewerTheme
			// 
			comboCodeViewerTheme.DropDownStyle = ComboBoxStyle.DropDownList;
			comboCodeViewerTheme.FormattingEnabled = true;
			comboCodeViewerTheme.Location = new Point(174, 39);
			comboCodeViewerTheme.Name = "comboCodeViewerTheme";
			comboCodeViewerTheme.Size = new Size(204, 23);
			comboCodeViewerTheme.TabIndex = 3;
			// 
			// FormSettings
			// 
			AcceptButton = btnOK;
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			CancelButton = btnCancel;
			ClientSize = new Size(623, 423);
			Controls.Add(comboCodeViewerTheme);
			Controls.Add(labelCodeViewerTheme);
			Controls.Add(numRequestTimeout);
			Controls.Add(labelRequestTimeout);
			Controls.Add(groupServiceImport);
			Controls.Add(comboTraceLevel);
			Controls.Add(labelTraceLevel);
			Controls.Add(chkLoadDefaultServiceDescriptorDir);
			Controls.Add(btnAddDescriptorFolder);
			Controls.Add(btnRemoveDescriptorFolder);
			Controls.Add(label1);
			Controls.Add(btnCancel);
			Controls.Add(btnOK);
			Controls.Add(lstAltServiceDescriptorDirs);
			FormBorderStyle = FormBorderStyle.FixedSingle;
			MaximizeBox = false;
			MinimizeBox = false;
			Name = "FormSettings";
			ShowInTaskbar = false;
			Text = "Settings";
			Load += frmSettings_Load;
			groupServiceImport.ResumeLayout(false);
			groupServiceImport.PerformLayout();
			((System.ComponentModel.ISupportInitialize)numRequestTimeout).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private ListBox lstAltServiceDescriptorDirs;
		private Button btnCancel;
		private Button btnOK;
		private Label label1;
		private Button btnAddDescriptorFolder;
		private Button btnRemoveDescriptorFolder;
		private CheckBox chkLoadDefaultServiceDescriptorDir;
		private Label labelTraceLevel;
		private ComboBox comboTraceLevel;
		private Label label2;
		private GroupBox groupServiceImport;
		private FolderInput folderInputServiceImportDefaultDir;
		private Label labelRequestTimeout;
		private NumericUpDown numRequestTimeout;
		private Label labelCodeViewerTheme;
		private ComboBox comboCodeViewerTheme;
	}
}