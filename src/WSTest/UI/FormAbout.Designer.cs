﻿namespace WSTest.UI
{
	partial class FormAbout
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			btnOK = new Button();
			webViewCustomAbout = new Microsoft.Web.WebView2.WinForms.WebView2();
			labelVersion = new Label();
			linkLabelProjectUrl = new LinkLabel();
			labelGitProject = new Label();
			linkLabelChangelog = new LinkLabel();
			((System.ComponentModel.ISupportInitialize)webViewCustomAbout).BeginInit();
			SuspendLayout();
			// 
			// btnOK
			// 
			btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnOK.DialogResult = DialogResult.OK;
			btnOK.Location = new Point(844, 489);
			btnOK.Name = "btnOK";
			btnOK.Size = new Size(75, 23);
			btnOK.TabIndex = 0;
			btnOK.Text = "OK";
			btnOK.UseVisualStyleBackColor = true;
			// 
			// webViewCustomAbout
			// 
			webViewCustomAbout.AllowExternalDrop = false;
			webViewCustomAbout.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			webViewCustomAbout.CreationProperties = null;
			webViewCustomAbout.DefaultBackgroundColor = Color.White;
			webViewCustomAbout.Location = new Point(12, 73);
			webViewCustomAbout.Name = "webViewCustomAbout";
			webViewCustomAbout.Size = new Size(907, 410);
			webViewCustomAbout.TabIndex = 1;
			webViewCustomAbout.ZoomFactor = 1D;
			// 
			// labelVersion
			// 
			labelVersion.AutoSize = true;
			labelVersion.Location = new Point(12, 9);
			labelVersion.Name = "labelVersion";
			labelVersion.Size = new Size(71, 15);
			labelVersion.TabIndex = 2;
			labelVersion.Text = "WSTest x.x.x";
			// 
			// linkLabelProjectUrl
			// 
			linkLabelProjectUrl.AutoSize = true;
			linkLabelProjectUrl.Location = new Point(89, 40);
			linkLabelProjectUrl.Name = "linkLabelProjectUrl";
			linkLabelProjectUrl.Size = new Size(106, 15);
			linkLabelProjectUrl.TabIndex = 3;
			linkLabelProjectUrl.TabStop = true;
			linkLabelProjectUrl.Text = "linkLabelProjectUrl";
			linkLabelProjectUrl.LinkClicked += linkLabelGitProject_LinkClicked;
			// 
			// labelGitProject
			// 
			labelGitProject.AutoSize = true;
			labelGitProject.Location = new Point(12, 40);
			labelGitProject.Name = "labelGitProject";
			labelGitProject.Size = new Size(64, 15);
			labelGitProject.TabIndex = 5;
			labelGitProject.Text = "Project url:";
			// 
			// linkLabelChangelog
			// 
			linkLabelChangelog.AutoSize = true;
			linkLabelChangelog.Location = new Point(89, 9);
			linkLabelChangelog.Name = "linkLabelChangelog";
			linkLabelChangelog.Size = new Size(65, 15);
			linkLabelChangelog.TabIndex = 6;
			linkLabelChangelog.TabStop = true;
			linkLabelChangelog.Text = "Changelog";
			linkLabelChangelog.LinkClicked += linkLabelChangelog_LinkClicked;
			// 
			// FormAbout
			// 
			AcceptButton = btnOK;
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(931, 521);
			ControlBox = false;
			Controls.Add(linkLabelChangelog);
			Controls.Add(labelGitProject);
			Controls.Add(linkLabelProjectUrl);
			Controls.Add(labelVersion);
			Controls.Add(webViewCustomAbout);
			Controls.Add(btnOK);
			Name = "FormAbout";
			ShowIcon = false;
			ShowInTaskbar = false;
			Text = "About WSTest";
			FormClosed += FormAbout_FormClosed;
			Load += FormAbout_Load;
			((System.ComponentModel.ISupportInitialize)webViewCustomAbout).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private Button btnOK;
		private Microsoft.Web.WebView2.WinForms.WebView2 webViewCustomAbout;
		private Label labelVersion;
		private LinkLabel linkLabelProjectUrl;
		private Label labelGitProject;
		private LinkLabel linkLabelChangelog;
	}
}