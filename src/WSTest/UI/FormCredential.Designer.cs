﻿namespace WSTest.UI
{
	partial class FormCredential
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			btnOK = new Button();
			labelDomain = new Label();
			txtDomain = new TextBox();
			txtUser = new TextBox();
			labelUser = new Label();
			label2 = new Label();
			passwordInput = new PasswordInput();
			btnCancel = new Button();
			SuspendLayout();
			// 
			// btnOK
			// 
			btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnOK.DialogResult = DialogResult.OK;
			btnOK.Location = new Point(441, 116);
			btnOK.Name = "btnOK";
			btnOK.Size = new Size(75, 23);
			btnOK.TabIndex = 6;
			btnOK.Text = "OK";
			btnOK.UseVisualStyleBackColor = true;
			// 
			// labelDomain
			// 
			labelDomain.AutoSize = true;
			labelDomain.Location = new Point(12, 15);
			labelDomain.Name = "labelDomain";
			labelDomain.Size = new Size(49, 15);
			labelDomain.TabIndex = 0;
			labelDomain.Text = "Domain";
			// 
			// txtDomain
			// 
			txtDomain.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtDomain.Location = new Point(102, 12);
			txtDomain.Name = "txtDomain";
			txtDomain.ReadOnly = true;
			txtDomain.Size = new Size(495, 23);
			txtDomain.TabIndex = 1;
			// 
			// txtUser
			// 
			txtUser.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtUser.Location = new Point(102, 41);
			txtUser.Name = "txtUser";
			txtUser.Size = new Size(495, 23);
			txtUser.TabIndex = 3;
			txtUser.TextChanged += txtUser_TextChanged;
			// 
			// labelUser
			// 
			labelUser.AutoSize = true;
			labelUser.Location = new Point(12, 44);
			labelUser.Name = "labelUser";
			labelUser.Size = new Size(30, 15);
			labelUser.TabIndex = 2;
			labelUser.Text = "User";
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Location = new Point(12, 73);
			label2.Name = "label2";
			label2.Size = new Size(57, 15);
			label2.TabIndex = 4;
			label2.Text = "Password";
			// 
			// passwordInput
			// 
			passwordInput.Location = new Point(102, 73);
			passwordInput.Name = "passwordInput";
			passwordInput.Password = "";
			passwordInput.Size = new Size(495, 23);
			passwordInput.TabIndex = 5;
			passwordInput.PasswordChanged += passwordInput_PasswordChanged;
			// 
			// btnCancel
			// 
			btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnCancel.DialogResult = DialogResult.Cancel;
			btnCancel.Location = new Point(522, 116);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new Size(75, 23);
			btnCancel.TabIndex = 7;
			btnCancel.Text = "Cancel";
			btnCancel.UseVisualStyleBackColor = true;
			// 
			// FormCredential
			// 
			AcceptButton = btnOK;
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(609, 151);
			ControlBox = false;
			Controls.Add(btnCancel);
			Controls.Add(passwordInput);
			Controls.Add(label2);
			Controls.Add(txtUser);
			Controls.Add(labelUser);
			Controls.Add(txtDomain);
			Controls.Add(labelDomain);
			Controls.Add(btnOK);
			Name = "FormCredential";
			StartPosition = FormStartPosition.CenterParent;
			Text = "Credential";
			Shown += FormCredential_Shown;
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private Button btnOK;
		private Label labelDomain;
		private TextBox txtDomain;
		private TextBox txtUser;
		private Label labelUser;
		private Label label2;
		private PasswordInput passwordInput;
		private Button btnCancel;
	}
}