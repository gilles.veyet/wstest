﻿using System.Net;
using WSTest.Data;

namespace WSTest.UI
{
	internal class DownloadHelper
	{
		public static DownloadHelper Instance => new();

		delegate Task<T> GetDataAsync<T>(HttpClient httpClient, Uri uri);

		private readonly Dictionary<string, Credential> Credentials = [];

		static Uri StringToUri(string url)
		{
			if (!Uri.TryCreate(url, UriKind.Absolute, out var uri))
				throw new ArgumentException($"Invalid {url}");

			return uri;
		}

		private async Task<T> HandleRetry<T>(string url, GetDataAsync<T> Get)
		{
			var uri = StringToUri(url);
			var host = uri.Host;

			Credentials.TryGetValue(host, out var credential);

			for (; ; )
			{
				using HttpClientHandler httpClientHandler = new();
				using HttpClient httpClient = new(httpClientHandler);

				if (credential != null)
					httpClientHandler.Credentials = new NetworkCredential(credential.UserName, credential.Password);

				try
				{
					var result = await Get(httpClient, uri);

					if (credential != null)
						Credentials[host] = credential;
					else
						Credentials.Remove(host);

					return result;
				}
				catch (HttpRequestException ex)
				{
					if (ex.StatusCode != HttpStatusCode.Unauthorized)
						throw;

					if (credential == null)
						credential = new Credential(host) { UserName = Environment.UserName };
					else
						credential.Password = string.Empty;

					credential = FormCredential.AskCredential(credential);
					if (credential == null)
						throw new Exception("Canceled by user");
				}
			}

		}
		public async Task<string> GetStringAsync(string url)
		{
			return await HandleRetry(url, (HttpClient httpClient, Uri uri) => httpClient.GetStringAsync(uri));
		}

		public async Task<Stream> GetStreamAsync(string url)
		{
			return await HandleRetry(url, (HttpClient httpClient, Uri uri) => httpClient.GetStreamAsync(uri));
		}
		public async Task DownloadFileAsync(string url, string path)
		{
			using var stream = new FileStream(path, FileMode.Create, FileAccess.Write);
			using var content = await GetStreamAsync(url);
			await content.CopyToAsync(stream);
		}
	}
}
