﻿namespace WSTest.UI
{
	record DataGridOptions
	{
		public bool MultiSelect { get; set; } = false;

		public bool ReadOnly { get; set; } = true;

		public bool ColumnHeadersVisible { get; set; } = true;

		public bool RowHeadersVisible { get; set; } = false;

		public static DataGridOptions Default => new();
	}
	record ColumnOptions
	{
		public string? HeaderText { get; init; } = null;
		public DataGridViewContentAlignment Alignment { get; init; } = DataGridViewContentAlignment.MiddleLeft;
		public bool Frozen { get; init; } = false;
		public bool Readonly { get; init; } = false;
		public bool Visible { get; init; } = true;
		public bool Wrap { get; init; } = false;
		public int Width { get; init; } = 0;
		public DataGridViewAutoSizeColumnMode AutoSizeMode { get; init; } = DataGridViewAutoSizeColumnMode.NotSet;

		public DataGridViewAutoSizeColumnMode ActualAutoSizeMode => AutoSizeMode == DataGridViewAutoSizeColumnMode.NotSet && Width == 0 ? DataGridViewAutoSizeColumnMode.AllCells : AutoSizeMode;

		public static ColumnOptions Default => new();
	}

	internal class DataGrid
	{
		readonly DataGridView Grid;

		public DataGrid(DataGridView grid, DataGridOptions options)
		{
			this.Grid = grid;
			SetupDataGridView(options);
		}

		private void SetupDataGridView(DataGridOptions options)
		{
			//Grid.AllowUserToResizeColumns = true;		default is true.

			Grid.AllowDrop = false;
			Grid.AllowUserToDeleteRows = false;
			Grid.AllowUserToAddRows = false;
			Grid.ReadOnly = options.ReadOnly;
			Grid.EditMode = DataGridViewEditMode.EditOnEnter;

			Grid.ColumnHeadersVisible = options.ColumnHeadersVisible;
			Grid.RowHeadersVisible = options.RowHeadersVisible;
			Grid.RowHeadersWidth = 28;  // default is 43

			Grid.ColumnHeadersDefaultCellStyle.Font = new Font(Grid.Font, FontStyle.Bold);
			Grid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
			Grid.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
			Grid.CellBorderStyle = DataGridViewCellBorderStyle.Single;
			Grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			Grid.MultiSelect = options.MultiSelect;
			Grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
			Grid.AutoGenerateColumns = false;
		}

		public DataGridViewTextBoxColumn AddTextColumn(string dataPropertyName, ColumnOptions? options = null)
		{
			if (options == null)
				options = ColumnOptions.Default;

			DataGridViewTextBoxColumn column = new()
			{
				Name = dataPropertyName,
				DataPropertyName = dataPropertyName,
				HeaderText = options.HeaderText ?? dataPropertyName,
				Width = options.Width,
				Frozen = options.Frozen,
				ReadOnly = options.Readonly,
				Visible = options.Visible,
				AutoSizeMode = options.ActualAutoSizeMode,
			};

			column.DefaultCellStyle.WrapMode = options.Wrap ? DataGridViewTriState.True : DataGridViewTriState.False; // default is NotSet,  same effect as False ?
			column.DefaultCellStyle.Alignment = options.Alignment;

			Grid.Columns.Add(column);
			return column;
		}


		public DataGridViewCheckBoxColumn AddCheckboxColumn(string dataPropertyName, ColumnOptions? options = null)
		{
			if (options == null)
				options = ColumnOptions.Default;

			DataGridViewCheckBoxColumn column = new()
			{
				Name = dataPropertyName,
				DataPropertyName = dataPropertyName,
				HeaderText = options.HeaderText ?? dataPropertyName,
				//Width = options.Width,
				Frozen = options.Frozen,
				ReadOnly = options.Readonly,
				Visible = options.Visible,
				AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader,
			};

			//column.DefaultCellStyle.WrapMode = options.Wrap ? DataGridViewTriState.True : DataGridViewTriState.False; // default is NotSet,  same effect as False ?
			//column.DefaultCellStyle.Alignment = options.Alignment;

			Grid.Columns.Add(column);
			return column;
		}

		public DataGridViewComboBoxColumn AddComboBoxColumn(string dataPropertyName, ColumnOptions? options = null)
		{
			if (options == null)
				options = ColumnOptions.Default;

			DataGridViewComboBoxColumn column = new()
			{
				Name = dataPropertyName,
				DataPropertyName = dataPropertyName,
				HeaderText = options.HeaderText ?? dataPropertyName,
				Width = options.Width,
				Frozen = options.Frozen,
				ReadOnly = options.Readonly,
				Visible = options.Visible,
				AutoSizeMode = options.ActualAutoSizeMode,
			};

			//column.DefaultCellStyle.WrapMode = options.Wrap ? DataGridViewTriState.True : DataGridViewTriState.False; // default is NotSet,  same effect as False ?
			column.DefaultCellStyle.Alignment = options.Alignment;

			Grid.Columns.Add(column);
			return column;
		}

		public void SetCellComboBoxItems(int rowIndex, int colIndex, IEnumerable<string> items)
		{
			DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)Grid.Rows[rowIndex].Cells[colIndex];
			cell.MaxDropDownItems = 100;  // not sure if useful. Default is 8, max is 100
			cell.Items.Clear();
			cell.Items.AddRange(items.ToArray());
		}

		public void ClearSelection()
		{
			Grid.ClearSelection();
		}

		public void SelectRow(int nRow)
		{
			if (nRow < 0)
			{
				Grid.ClearSelection();
				return;
			}

			if (Grid.Rows[nRow].Selected)    // do nothing if row is already selected : prevent double SelectionChange when 1st item is added to grid.
				return;

			Grid.ClearSelection();
			Grid.Rows[nRow].Selected = true;
			Grid.CurrentCell = Grid.Rows[nRow].Cells[0];  // cell must not be hidden otherwise this will throw an exception. 
		}

		public int[] SelectedRowIndexes => Grid.SelectedRows.Cast<DataGridViewRow>().Select(r => r.Index).ToArray();

		public int? SelectedRowIndex => SelectedRowIndexes.Length > 0 ? SelectedRowIndexes[0] : null;
	}
}
