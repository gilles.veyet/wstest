﻿namespace WSTest.UI
{
	partial class FormSelectProfile
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			btnCancel = new Button();
			btnOK = new Button();
			treeViewProfiles = new TreeView();
			groupFilter = new GroupBox();
			btnResetFilterName = new Button();
			btnResetFilterGroup = new Button();
			comboFilterGroup = new ComboBox();
			labelGroup = new Label();
			labelName = new Label();
			txtFilterName = new TextBox();
			groupFilter.SuspendLayout();
			SuspendLayout();
			// 
			// btnCancel
			// 
			btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnCancel.DialogResult = DialogResult.Cancel;
			btnCancel.Location = new Point(713, 415);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new Size(75, 23);
			btnCancel.TabIndex = 8;
			btnCancel.Text = "Cancel";
			btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnOK
			// 
			btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnOK.DialogResult = DialogResult.OK;
			btnOK.Location = new Point(632, 415);
			btnOK.Name = "btnOK";
			btnOK.Size = new Size(75, 23);
			btnOK.TabIndex = 7;
			btnOK.Text = "OK";
			btnOK.UseVisualStyleBackColor = true;
			// 
			// treeViewProfiles
			// 
			treeViewProfiles.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			treeViewProfiles.Location = new Point(12, 99);
			treeViewProfiles.Name = "treeViewProfiles";
			treeViewProfiles.Size = new Size(776, 310);
			treeViewProfiles.TabIndex = 9;
			treeViewProfiles.AfterSelect += treeViewProfiles_AfterSelect;
			// 
			// groupFilter
			// 
			groupFilter.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			groupFilter.Controls.Add(btnResetFilterName);
			groupFilter.Controls.Add(btnResetFilterGroup);
			groupFilter.Controls.Add(comboFilterGroup);
			groupFilter.Controls.Add(labelGroup);
			groupFilter.Controls.Add(labelName);
			groupFilter.Controls.Add(txtFilterName);
			groupFilter.Location = new Point(12, 12);
			groupFilter.Name = "groupFilter";
			groupFilter.Size = new Size(776, 81);
			groupFilter.TabIndex = 12;
			groupFilter.TabStop = false;
			groupFilter.Text = "Filter";
			// 
			// btnResetFilterName
			// 
			btnResetFilterName.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnResetFilterName.FlatAppearance.BorderSize = 0;
			btnResetFilterName.FlatStyle = FlatStyle.Flat;
			btnResetFilterName.Image = Properties.Resources.Delete;
			btnResetFilterName.Location = new Point(752, 51);
			btnResetFilterName.Name = "btnResetFilterName";
			btnResetFilterName.Size = new Size(18, 18);
			btnResetFilterName.TabIndex = 60;
			btnResetFilterName.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnResetFilterName.Click += btnResetFilterName_Click;
			// 
			// btnResetFilterGroup
			// 
			btnResetFilterGroup.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnResetFilterGroup.FlatAppearance.BorderSize = 0;
			btnResetFilterGroup.FlatStyle = FlatStyle.Flat;
			btnResetFilterGroup.Image = Properties.Resources.Delete;
			btnResetFilterGroup.Location = new Point(752, 19);
			btnResetFilterGroup.Name = "btnResetFilterGroup";
			btnResetFilterGroup.Size = new Size(18, 18);
			btnResetFilterGroup.TabIndex = 59;
			btnResetFilterGroup.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnResetFilterGroup.Click += btnResetFilterGroup_Click;
			// 
			// comboFilterGroup
			// 
			comboFilterGroup.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			comboFilterGroup.DropDownStyle = ComboBoxStyle.DropDownList;
			comboFilterGroup.FormattingEnabled = true;
			comboFilterGroup.Location = new Point(52, 19);
			comboFilterGroup.Name = "comboFilterGroup";
			comboFilterGroup.Size = new Size(694, 23);
			comboFilterGroup.TabIndex = 12;
			comboFilterGroup.SelectedIndexChanged += comboFilterGroup_SelectedIndexChanged;
			// 
			// labelGroup
			// 
			labelGroup.AutoSize = true;
			labelGroup.Location = new Point(6, 23);
			labelGroup.Name = "labelGroup";
			labelGroup.Size = new Size(40, 15);
			labelGroup.TabIndex = 11;
			labelGroup.Text = "Group";
			// 
			// labelName
			// 
			labelName.AutoSize = true;
			labelName.Location = new Point(6, 51);
			labelName.Name = "labelName";
			labelName.Size = new Size(39, 15);
			labelName.TabIndex = 10;
			labelName.Text = "Name";
			// 
			// txtFilterName
			// 
			txtFilterName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtFilterName.Location = new Point(51, 48);
			txtFilterName.Name = "txtFilterName";
			txtFilterName.Size = new Size(695, 23);
			txtFilterName.TabIndex = 9;
			txtFilterName.TextChanged += txtFilterName_TextChanged;
			// 
			// FormSelectProfile
			// 
			AcceptButton = btnOK;
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			CancelButton = btnCancel;
			ClientSize = new Size(800, 450);
			Controls.Add(groupFilter);
			Controls.Add(treeViewProfiles);
			Controls.Add(btnCancel);
			Controls.Add(btnOK);
			MaximizeBox = false;
			MinimizeBox = false;
			Name = "FormSelectProfile";
			ShowIcon = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.Manual;
			Text = "Select Profile";
			Load += FormSelectProfile_Load;
			Shown += FormSelectProfile_Shown;
			groupFilter.ResumeLayout(false);
			groupFilter.PerformLayout();
			ResumeLayout(false);
		}

		#endregion
		private Button btnCancel;
		private Button btnOK;
		private TreeView treeViewProfiles;
		private GroupBox groupFilter;
		private ComboBox comboFilterGroup;
		private Label labelGroup;
		private Label labelName;
		private Button btnResetFilterName;
		private Button btnResetFilterGroup;
		private TextBox txtFilterName;
	}
}