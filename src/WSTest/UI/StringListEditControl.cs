﻿using System.ComponentModel;

namespace WSTest.UI
{
	public partial class StringListEditControl : UserControl
	{
		readonly DataGrid Grid;
		BindingList<string> ItemList = [];

		string? SelectedItem
		{
			get
			{
				int? index = Grid.SelectedRowIndex;
				return index != null ? ItemList[index.Value] : null;
			}
		}

		[Category("Behavior"), Browsable(true), Description("Controls whether the Items can be changed or not")]
		public bool Readonly
		{
			get { return dataGrid.ReadOnly; }
			set
			{
				dataGrid.ReadOnly = value;
				btnAdd.Visible = !value;
				btnRemove.Visible = !value;
			}
		}

		[Category("Data"), Browsable(true), Description("The list of items")]
		public List<string> Items
		{
			get { return [.. ItemList]; }
			set
			{
				ItemList = new(value);
				dataGrid.DataSource = ItemList;
			}
		}

		public StringListEditControl()
		{
			InitializeComponent();
			dataGrid.VirtualMode = true;    // Use Virtual mode => CellValueNeeded/CellValuePushed are used to get/store values.

			Grid = new DataGrid(dataGrid, new DataGridOptions() { ReadOnly = false, ColumnHeadersVisible = false, RowHeadersVisible = true });
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			ItemList.Add(string.Empty);
		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			var prop = SelectedItem;

			if (prop == null)
				return;

			ItemList.Remove(prop);
		}

		private void StringListEditControl_Load(object sender, EventArgs e)
		{
			Grid.AddTextColumn("dummy", new ColumnOptions() { AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill });

			ItemList = new BindingList<string>(Items);

			dataGrid.SelectionChanged += DataGrid_SelectionChanged;
			dataGrid.DataSource = ItemList;

			UpdateContext();
		}


		private void DataGrid_SelectionChanged(object? sender, EventArgs e)
		{
			UpdateContext();
		}

		void UpdateContext()
		{
			bool selectOk = SelectedItem != null;
			btnRemove.Enabled = selectOk;
		}

		private void dataGrid_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			e.Value = ItemList[e.RowIndex];
		}

		private void dataGrid_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
		{
			ItemList[e.RowIndex] = (string)e.Value!;
		}
	}
}
