﻿namespace WSTest.UI
{
	partial class FormMain
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			btnRunMethod = new Button();
			propertyEditor = new Editor.PropertyEditor();
			splitMain = new SplitContainer();
			panelCodeEdit = new Panel();
			btnEditorSave = new Button();
			btnEditorLoad = new Button();
			btnEditorReplay = new Button();
			txtCodeEditMethod = new TextBox();
			txtCodeEditUrl = new TextBox();
			btnEditorDiscard = new Button();
			btnEditorRun = new Button();
			codeViewEdit = new CodeView();
			panelEditor = new Panel();
			btnLoad = new Button();
			labelCurrentMethod = new Label();
			labelCurrentProfile = new Label();
			btnManageProfiles = new Button();
			btnPreviewMethod = new Button();
			btnSelectMethod = new Button();
			btnSelectProfile = new Button();
			splitRequestResponse = new SplitContainer();
			btnReplay = new Button();
			btnRequestSave = new Button();
			btnRequestEdit = new Button();
			txtRequestMethod = new TextBox();
			txtRequestUrl = new TextBox();
			codeViewRequest = new CodeView();
			txtResponseStatus = new TextBox();
			btnResponseSave = new Button();
			codeViewResponse = new CodeView();
			splitFull = new SplitContainer();
			dataGridHistory = new DataGridView();
			menuStripMain = new MenuStrip();
			mnuCancel = new ToolStripMenuItem();
			mnuSettings = new ToolStripMenuItem();
			mnuServices = new ToolStripMenuItem();
			mnuServicesRebuildCache = new ToolStripMenuItem();
			mnuServicesAddNewPackage = new ToolStripMenuItem();
			mnuServicesReloadPackage = new ToolStripMenuItem();
			mnuServicesUpdateFolder = new ToolStripMenuItem();
			mnuTools = new ToolStripMenuItem();
			mnuExportSettings = new ToolStripMenuItem();
			mnuTest = new ToolStripMenuItem();
			mnuSaveServiceDescriptorSample = new ToolStripMenuItem();
			mnuSaveCustomSettingsSample = new ToolStripMenuItem();
			mnuSaveCustomInfoSample = new ToolStripMenuItem();
			mnuTestLoadService = new ToolStripMenuItem();
			mnuTestReloadService = new ToolStripMenuItem();
			mnuTestSerializeServices = new ToolStripMenuItem();
			mnuTestSerializeProfile = new ToolStripMenuItem();
			mnuAbout = new ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)splitMain).BeginInit();
			splitMain.Panel1.SuspendLayout();
			splitMain.Panel2.SuspendLayout();
			splitMain.SuspendLayout();
			panelCodeEdit.SuspendLayout();
			panelEditor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)splitRequestResponse).BeginInit();
			splitRequestResponse.Panel1.SuspendLayout();
			splitRequestResponse.Panel2.SuspendLayout();
			splitRequestResponse.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)splitFull).BeginInit();
			splitFull.Panel1.SuspendLayout();
			splitFull.Panel2.SuspendLayout();
			splitFull.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)dataGridHistory).BeginInit();
			menuStripMain.SuspendLayout();
			SuspendLayout();
			// 
			// btnRunMethod
			// 
			btnRunMethod.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnRunMethod.Location = new Point(369, 33);
			btnRunMethod.Margin = new Padding(4, 3, 4, 3);
			btnRunMethod.Name = "btnRunMethod";
			btnRunMethod.Size = new Size(44, 23);
			btnRunMethod.TabIndex = 32;
			btnRunMethod.Text = "Run";
			btnRunMethod.Click += btnRunMethod_Click;
			// 
			// propertyEditor
			// 
			propertyEditor.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			propertyEditor.BackColor = SystemColors.ControlLight;
			propertyEditor.Location = new Point(0, 62);
			propertyEditor.Name = "propertyEditor";
			propertyEditor.ShowSpecifiedCheckbox = true;
			propertyEditor.Size = new Size(534, 219);
			propertyEditor.TabIndex = 42;
			// 
			// splitMain
			// 
			splitMain.BackColor = Color.DimGray;
			splitMain.Dock = DockStyle.Fill;
			splitMain.Location = new Point(0, 0);
			splitMain.Name = "splitMain";
			// 
			// splitMain.Panel1
			// 
			splitMain.Panel1.BackColor = SystemColors.Control;
			splitMain.Panel1.Controls.Add(panelCodeEdit);
			splitMain.Panel1.Controls.Add(panelEditor);
			// 
			// splitMain.Panel2
			// 
			splitMain.Panel2.Controls.Add(splitRequestResponse);
			splitMain.Size = new Size(1229, 684);
			splitMain.SplitterDistance = 550;
			splitMain.TabIndex = 46;
			// 
			// panelCodeEdit
			// 
			panelCodeEdit.Controls.Add(btnEditorSave);
			panelCodeEdit.Controls.Add(btnEditorLoad);
			panelCodeEdit.Controls.Add(btnEditorReplay);
			panelCodeEdit.Controls.Add(txtCodeEditMethod);
			panelCodeEdit.Controls.Add(txtCodeEditUrl);
			panelCodeEdit.Controls.Add(btnEditorDiscard);
			panelCodeEdit.Controls.Add(btnEditorRun);
			panelCodeEdit.Controls.Add(codeViewEdit);
			panelCodeEdit.Location = new Point(3, 347);
			panelCodeEdit.Name = "panelCodeEdit";
			panelCodeEdit.Size = new Size(531, 318);
			panelCodeEdit.TabIndex = 69;
			// 
			// btnEditorSave
			// 
			btnEditorSave.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnEditorSave.Location = new Point(351, 4);
			btnEditorSave.Margin = new Padding(4, 3, 4, 3);
			btnEditorSave.Name = "btnEditorSave";
			btnEditorSave.Size = new Size(44, 23);
			btnEditorSave.TabIndex = 41;
			btnEditorSave.Text = "Save";
			btnEditorSave.Click += btnEditorSave_Click;
			// 
			// btnEditorLoad
			// 
			btnEditorLoad.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnEditorLoad.Location = new Point(299, 4);
			btnEditorLoad.Margin = new Padding(4, 3, 4, 3);
			btnEditorLoad.Name = "btnEditorLoad";
			btnEditorLoad.Size = new Size(44, 23);
			btnEditorLoad.TabIndex = 40;
			btnEditorLoad.Text = "Load";
			btnEditorLoad.Click += btnEditorLoad_Click;
			// 
			// btnEditorReplay
			// 
			btnEditorReplay.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnEditorReplay.Location = new Point(403, 4);
			btnEditorReplay.Margin = new Padding(4, 3, 4, 3);
			btnEditorReplay.Name = "btnEditorReplay";
			btnEditorReplay.Size = new Size(58, 23);
			btnEditorReplay.TabIndex = 39;
			btnEditorReplay.Text = "Replay";
			btnEditorReplay.Click += btnEditorReplay_Click;
			// 
			// txtCodeEditMethod
			// 
			txtCodeEditMethod.Location = new Point(3, 3);
			txtCodeEditMethod.Name = "txtCodeEditMethod";
			txtCodeEditMethod.ReadOnly = true;
			txtCodeEditMethod.Size = new Size(52, 23);
			txtCodeEditMethod.TabIndex = 38;
			// 
			// txtCodeEditUrl
			// 
			txtCodeEditUrl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtCodeEditUrl.Location = new Point(61, 3);
			txtCodeEditUrl.Name = "txtCodeEditUrl";
			txtCodeEditUrl.ReadOnly = true;
			txtCodeEditUrl.Size = new Size(179, 23);
			txtCodeEditUrl.TabIndex = 37;
			// 
			// btnEditorDiscard
			// 
			btnEditorDiscard.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnEditorDiscard.Location = new Point(469, 4);
			btnEditorDiscard.Margin = new Padding(4, 3, 4, 3);
			btnEditorDiscard.Name = "btnEditorDiscard";
			btnEditorDiscard.Size = new Size(60, 23);
			btnEditorDiscard.TabIndex = 36;
			btnEditorDiscard.Text = "Discard";
			btnEditorDiscard.Click += btnEditorDiscard_Click;
			// 
			// btnEditorRun
			// 
			btnEditorRun.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnEditorRun.Location = new Point(247, 4);
			btnEditorRun.Margin = new Padding(4, 3, 4, 3);
			btnEditorRun.Name = "btnEditorRun";
			btnEditorRun.Size = new Size(44, 23);
			btnEditorRun.TabIndex = 34;
			btnEditorRun.Text = "Run";
			btnEditorRun.Click += btnEditorRun_Click;
			// 
			// codeViewEdit
			// 
			codeViewEdit.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			codeViewEdit.Location = new Point(0, 32);
			codeViewEdit.Name = "codeViewEdit";
			codeViewEdit.Size = new Size(531, 283);
			codeViewEdit.TabIndex = 1;
			// 
			// panelEditor
			// 
			panelEditor.Controls.Add(btnLoad);
			panelEditor.Controls.Add(labelCurrentMethod);
			panelEditor.Controls.Add(labelCurrentProfile);
			panelEditor.Controls.Add(btnManageProfiles);
			panelEditor.Controls.Add(btnPreviewMethod);
			panelEditor.Controls.Add(btnSelectMethod);
			panelEditor.Controls.Add(btnRunMethod);
			panelEditor.Controls.Add(propertyEditor);
			panelEditor.Controls.Add(btnSelectProfile);
			panelEditor.Location = new Point(3, 20);
			panelEditor.Name = "panelEditor";
			panelEditor.Size = new Size(534, 284);
			panelEditor.TabIndex = 68;
			// 
			// btnLoad
			// 
			btnLoad.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnLoad.Location = new Point(419, 33);
			btnLoad.Margin = new Padding(4, 3, 4, 3);
			btnLoad.Name = "btnLoad";
			btnLoad.Size = new Size(44, 23);
			btnLoad.TabIndex = 69;
			btnLoad.Text = "Load";
			btnLoad.Click += btnLoad_Click;
			// 
			// labelCurrentMethod
			// 
			labelCurrentMethod.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			labelCurrentMethod.AutoEllipsis = true;
			labelCurrentMethod.BackColor = SystemColors.Info;
			labelCurrentMethod.BorderStyle = BorderStyle.Fixed3D;
			labelCurrentMethod.Location = new Point(70, 33);
			labelCurrentMethod.Name = "labelCurrentMethod";
			labelCurrentMethod.Size = new Size(292, 23);
			labelCurrentMethod.TabIndex = 68;
			labelCurrentMethod.Click += labelCurrentMethod_Click;
			// 
			// labelCurrentProfile
			// 
			labelCurrentProfile.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			labelCurrentProfile.AutoEllipsis = true;
			labelCurrentProfile.BackColor = SystemColors.Info;
			labelCurrentProfile.BorderStyle = BorderStyle.Fixed3D;
			labelCurrentProfile.Location = new Point(70, 3);
			labelCurrentProfile.Name = "labelCurrentProfile";
			labelCurrentProfile.Size = new Size(292, 23);
			labelCurrentProfile.TabIndex = 67;
			labelCurrentProfile.Click += labelCurrentProfile_Click;
			// 
			// btnManageProfiles
			// 
			btnManageProfiles.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnManageProfiles.Location = new Point(369, 4);
			btnManageProfiles.Name = "btnManageProfiles";
			btnManageProfiles.Size = new Size(109, 23);
			btnManageProfiles.TabIndex = 66;
			btnManageProfiles.Text = "Manage Profiles";
			btnManageProfiles.UseVisualStyleBackColor = true;
			btnManageProfiles.Click += btnManageProfiles_Click;
			// 
			// btnPreviewMethod
			// 
			btnPreviewMethod.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnPreviewMethod.Location = new Point(471, 33);
			btnPreviewMethod.Margin = new Padding(4, 3, 4, 3);
			btnPreviewMethod.Name = "btnPreviewMethod";
			btnPreviewMethod.Size = new Size(60, 23);
			btnPreviewMethod.TabIndex = 43;
			btnPreviewMethod.Text = "Preview";
			btnPreviewMethod.Click += btnPreviewMethod_Click;
			// 
			// btnSelectMethod
			// 
			btnSelectMethod.Location = new Point(6, 33);
			btnSelectMethod.Name = "btnSelectMethod";
			btnSelectMethod.Size = new Size(58, 23);
			btnSelectMethod.TabIndex = 59;
			btnSelectMethod.Text = "Method";
			btnSelectMethod.UseVisualStyleBackColor = true;
			btnSelectMethod.Click += btnSelectMethod_Click;
			// 
			// btnSelectProfile
			// 
			btnSelectProfile.Location = new Point(6, 3);
			btnSelectProfile.Name = "btnSelectProfile";
			btnSelectProfile.Size = new Size(58, 23);
			btnSelectProfile.TabIndex = 64;
			btnSelectProfile.Text = "Profile";
			btnSelectProfile.UseVisualStyleBackColor = true;
			btnSelectProfile.Click += btnSelectProfile_Click;
			// 
			// splitRequestResponse
			// 
			splitRequestResponse.BackColor = Color.DimGray;
			splitRequestResponse.Dock = DockStyle.Fill;
			splitRequestResponse.Location = new Point(0, 0);
			splitRequestResponse.Name = "splitRequestResponse";
			splitRequestResponse.Orientation = Orientation.Horizontal;
			// 
			// splitRequestResponse.Panel1
			// 
			splitRequestResponse.Panel1.BackColor = SystemColors.Control;
			splitRequestResponse.Panel1.Controls.Add(btnReplay);
			splitRequestResponse.Panel1.Controls.Add(btnRequestSave);
			splitRequestResponse.Panel1.Controls.Add(btnRequestEdit);
			splitRequestResponse.Panel1.Controls.Add(txtRequestMethod);
			splitRequestResponse.Panel1.Controls.Add(txtRequestUrl);
			splitRequestResponse.Panel1.Controls.Add(codeViewRequest);
			// 
			// splitRequestResponse.Panel2
			// 
			splitRequestResponse.Panel2.BackColor = SystemColors.Control;
			splitRequestResponse.Panel2.Controls.Add(txtResponseStatus);
			splitRequestResponse.Panel2.Controls.Add(btnResponseSave);
			splitRequestResponse.Panel2.Controls.Add(codeViewResponse);
			splitRequestResponse.Size = new Size(675, 684);
			splitRequestResponse.SplitterDistance = 229;
			splitRequestResponse.TabIndex = 0;
			// 
			// btnReplay
			// 
			btnReplay.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnReplay.Location = new Point(500, 3);
			btnReplay.Margin = new Padding(4, 3, 4, 3);
			btnReplay.Name = "btnReplay";
			btnReplay.Size = new Size(53, 23);
			btnReplay.TabIndex = 36;
			btnReplay.Text = "Replay";
			btnReplay.Click += btnReplay_Click;
			// 
			// btnRequestSave
			// 
			btnRequestSave.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnRequestSave.Location = new Point(622, 3);
			btnRequestSave.Margin = new Padding(4, 3, 4, 3);
			btnRequestSave.Name = "btnRequestSave";
			btnRequestSave.Size = new Size(53, 23);
			btnRequestSave.TabIndex = 35;
			btnRequestSave.Text = "Save";
			btnRequestSave.Click += btnRequestSave_Click;
			// 
			// btnRequestEdit
			// 
			btnRequestEdit.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnRequestEdit.Location = new Point(561, 3);
			btnRequestEdit.Margin = new Padding(4, 3, 4, 3);
			btnRequestEdit.Name = "btnRequestEdit";
			btnRequestEdit.Size = new Size(53, 23);
			btnRequestEdit.TabIndex = 33;
			btnRequestEdit.Text = "Edit";
			btnRequestEdit.Click += btnRequestEdit_Click;
			// 
			// txtRequestMethod
			// 
			txtRequestMethod.Location = new Point(2, 3);
			txtRequestMethod.Name = "txtRequestMethod";
			txtRequestMethod.ReadOnly = true;
			txtRequestMethod.Size = new Size(52, 23);
			txtRequestMethod.TabIndex = 2;
			// 
			// txtRequestUrl
			// 
			txtRequestUrl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtRequestUrl.Location = new Point(60, 3);
			txtRequestUrl.Name = "txtRequestUrl";
			txtRequestUrl.ReadOnly = true;
			txtRequestUrl.Size = new Size(433, 23);
			txtRequestUrl.TabIndex = 1;
			// 
			// codeViewRequest
			// 
			codeViewRequest.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			codeViewRequest.Location = new Point(0, 29);
			codeViewRequest.Name = "codeViewRequest";
			codeViewRequest.Size = new Size(675, 200);
			codeViewRequest.TabIndex = 0;
			// 
			// txtResponseStatus
			// 
			txtResponseStatus.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
			txtResponseStatus.Location = new Point(2, 1);
			txtResponseStatus.Name = "txtResponseStatus";
			txtResponseStatus.ReadOnly = true;
			txtResponseStatus.Size = new Size(613, 23);
			txtResponseStatus.TabIndex = 35;
			// 
			// btnResponseSave
			// 
			btnResponseSave.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnResponseSave.Location = new Point(622, 1);
			btnResponseSave.Margin = new Padding(4, 3, 4, 3);
			btnResponseSave.Name = "btnResponseSave";
			btnResponseSave.Size = new Size(53, 23);
			btnResponseSave.TabIndex = 34;
			btnResponseSave.Text = "Save";
			btnResponseSave.Click += btnResponseSave_Click;
			// 
			// codeViewResponse
			// 
			codeViewResponse.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			codeViewResponse.Location = new Point(0, 25);
			codeViewResponse.Name = "codeViewResponse";
			codeViewResponse.Size = new Size(675, 423);
			codeViewResponse.TabIndex = 0;
			// 
			// splitFull
			// 
			splitFull.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			splitFull.FixedPanel = FixedPanel.Panel2;
			splitFull.Location = new Point(0, 27);
			splitFull.Name = "splitFull";
			// 
			// splitFull.Panel1
			// 
			splitFull.Panel1.Controls.Add(splitMain);
			// 
			// splitFull.Panel2
			// 
			splitFull.Panel2.Controls.Add(dataGridHistory);
			splitFull.Size = new Size(1521, 684);
			splitFull.SplitterDistance = 1229;
			splitFull.TabIndex = 47;
			// 
			// dataGridHistory
			// 
			dataGridHistory.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridHistory.Dock = DockStyle.Fill;
			dataGridHistory.Location = new Point(0, 0);
			dataGridHistory.Name = "dataGridHistory";
			dataGridHistory.Size = new Size(288, 684);
			dataGridHistory.TabIndex = 0;
			// 
			// menuStripMain
			// 
			menuStripMain.Items.AddRange(new ToolStripItem[] { mnuCancel, mnuSettings, mnuServices, mnuTools, mnuTest, mnuAbout });
			menuStripMain.Location = new Point(0, 0);
			menuStripMain.Name = "menuStripMain";
			menuStripMain.Size = new Size(1533, 24);
			menuStripMain.TabIndex = 48;
			menuStripMain.Text = "menuStrip1";
			// 
			// mnuCancel
			// 
			mnuCancel.BackColor = Color.Orange;
			mnuCancel.Name = "mnuCancel";
			mnuCancel.Size = new Size(64, 20);
			mnuCancel.Text = "CANCEL";
			mnuCancel.Click += mnuCancel_Click;
			// 
			// mnuSettings
			// 
			mnuSettings.Name = "mnuSettings";
			mnuSettings.Size = new Size(61, 20);
			mnuSettings.Text = "Settings";
			mnuSettings.Click += mnuSettings_Click;
			// 
			// mnuServices
			// 
			mnuServices.DropDownItems.AddRange(new ToolStripItem[] { mnuServicesRebuildCache, mnuServicesAddNewPackage, mnuServicesReloadPackage, mnuServicesUpdateFolder });
			mnuServices.Name = "mnuServices";
			mnuServices.Size = new Size(61, 20);
			mnuServices.Text = "Services";
			// 
			// mnuServicesRebuildCache
			// 
			mnuServicesRebuildCache.Name = "mnuServicesRebuildCache";
			mnuServicesRebuildCache.Size = new Size(190, 22);
			mnuServicesRebuildCache.Text = "Rebuild Service Cache";
			mnuServicesRebuildCache.Click += mnuServicesRebuildCache_Click;
			// 
			// mnuServicesAddNewPackage
			// 
			mnuServicesAddNewPackage.Name = "mnuServicesAddNewPackage";
			mnuServicesAddNewPackage.Size = new Size(190, 22);
			mnuServicesAddNewPackage.Text = "Add New Package";
			mnuServicesAddNewPackage.Click += mnuServicesAddNewPackage_Click;
			// 
			// mnuServicesReloadPackage
			// 
			mnuServicesReloadPackage.Name = "mnuServicesReloadPackage";
			mnuServicesReloadPackage.Size = new Size(190, 22);
			mnuServicesReloadPackage.Text = "Update Package";
			mnuServicesReloadPackage.Click += mnuServicesReloadPackage_Click;
			// 
			// mnuServicesUpdateFolder
			// 
			mnuServicesUpdateFolder.Name = "mnuServicesUpdateFolder";
			mnuServicesUpdateFolder.Size = new Size(190, 22);
			mnuServicesUpdateFolder.Text = "Update Folder";
			mnuServicesUpdateFolder.Click += mnuServicesUpdateFolder_Click;
			// 
			// mnuTools
			// 
			mnuTools.DropDownItems.AddRange(new ToolStripItem[] { mnuExportSettings });
			mnuTools.Name = "mnuTools";
			mnuTools.Size = new Size(46, 20);
			mnuTools.Text = "Tools";
			// 
			// mnuExportSettings
			// 
			mnuExportSettings.Name = "mnuExportSettings";
			mnuExportSettings.Size = new Size(153, 22);
			mnuExportSettings.Text = "Export Settings";
			mnuExportSettings.ToolTipText = "Export Settings (include profiles) without encryption. For use on another computer.";
			mnuExportSettings.Click += mnuExportSettings_Click;
			// 
			// mnuTest
			// 
			mnuTest.DropDownItems.AddRange(new ToolStripItem[] { mnuSaveServiceDescriptorSample, mnuSaveCustomSettingsSample, mnuSaveCustomInfoSample, mnuTestLoadService, mnuTestReloadService, mnuTestSerializeServices, mnuTestSerializeProfile });
			mnuTest.Name = "mnuTest";
			mnuTest.Size = new Size(39, 20);
			mnuTest.Text = "Test";
			// 
			// mnuSaveServiceDescriptorSample
			// 
			mnuSaveServiceDescriptorSample.Name = "mnuSaveServiceDescriptorSample";
			mnuSaveServiceDescriptorSample.Size = new Size(195, 22);
			mnuSaveServiceDescriptorSample.Text = "Save Service Descriptor";
			mnuSaveServiceDescriptorSample.Click += mnuSaveServiceDescriptorSample_Click;
			// 
			// mnuSaveCustomSettingsSample
			// 
			mnuSaveCustomSettingsSample.Name = "mnuSaveCustomSettingsSample";
			mnuSaveCustomSettingsSample.Size = new Size(195, 22);
			mnuSaveCustomSettingsSample.Text = "Save Custom Settings";
			mnuSaveCustomSettingsSample.Click += mnuSaveCustomSettingsSample_Click;
			// 
			// mnuSaveCustomInfoSample
			// 
			mnuSaveCustomInfoSample.Name = "mnuSaveCustomInfoSample";
			mnuSaveCustomInfoSample.Size = new Size(195, 22);
			mnuSaveCustomInfoSample.Text = "Save CustomInfo";
			mnuSaveCustomInfoSample.Click += mnuSaveCustomInfoSample_Click;
			// 
			// mnuTestLoadService
			// 
			mnuTestLoadService.Name = "mnuTestLoadService";
			mnuTestLoadService.Size = new Size(195, 22);
			mnuTestLoadService.Text = "Load Service from file";
			mnuTestLoadService.Click += mnuLoadService_Click;
			// 
			// mnuTestReloadService
			// 
			mnuTestReloadService.Name = "mnuTestReloadService";
			mnuTestReloadService.Size = new Size(195, 22);
			mnuTestReloadService.Text = "Reload Service";
			mnuTestReloadService.Click += mnuTestReloadService_Click;
			// 
			// mnuTestSerializeServices
			// 
			mnuTestSerializeServices.Name = "mnuTestSerializeServices";
			mnuTestSerializeServices.Size = new Size(195, 22);
			mnuTestSerializeServices.Text = "Serialize Services";
			mnuTestSerializeServices.Click += mnuTestSerializeServices_Click;
			// 
			// mnuTestSerializeProfile
			// 
			mnuTestSerializeProfile.Name = "mnuTestSerializeProfile";
			mnuTestSerializeProfile.Size = new Size(195, 22);
			mnuTestSerializeProfile.Text = "Serialize Profile";
			mnuTestSerializeProfile.Click += mnuTestSerializeProfile_Click;
			// 
			// mnuAbout
			// 
			mnuAbout.Name = "mnuAbout";
			mnuAbout.Size = new Size(52, 20);
			mnuAbout.Text = "About";
			mnuAbout.Click += mnuAbout_Click;
			// 
			// FormMain
			// 
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(1533, 723);
			Controls.Add(splitFull);
			Controls.Add(menuStripMain);
			Icon = (Icon)resources.GetObject("$this.Icon");
			MainMenuStrip = menuStripMain;
			Name = "FormMain";
			Text = "WSTest";
			WindowState = FormWindowState.Maximized;
			FormClosing += frmMain_FormClosing;
			Load += frmMain_Load;
			Shown += frmMain_Shown;
			DpiChanged += FormMain_DpiChanged;
			splitMain.Panel1.ResumeLayout(false);
			splitMain.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)splitMain).EndInit();
			splitMain.ResumeLayout(false);
			panelCodeEdit.ResumeLayout(false);
			panelCodeEdit.PerformLayout();
			panelEditor.ResumeLayout(false);
			splitRequestResponse.Panel1.ResumeLayout(false);
			splitRequestResponse.Panel1.PerformLayout();
			splitRequestResponse.Panel2.ResumeLayout(false);
			splitRequestResponse.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)splitRequestResponse).EndInit();
			splitRequestResponse.ResumeLayout(false);
			splitFull.Panel1.ResumeLayout(false);
			splitFull.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)splitFull).EndInit();
			splitFull.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)dataGridHistory).EndInit();
			menuStripMain.ResumeLayout(false);
			menuStripMain.PerformLayout();
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion
		private Button btnRunMethod;
		private Editor.PropertyEditor propertyEditor;
		private SplitContainer splitMain;
		private SplitContainer splitRequestResponse;
		private SplitContainer splitFull;
		private MenuStrip menuStripMain;
		private ToolStripMenuItem mnuTest;
		private ToolStripMenuItem mnuSaveServiceDescriptorSample;
		private ToolStripMenuItem mnuTestReloadService;
		private ToolStripMenuItem mnuTestLoadService;
		private ToolStripMenuItem mnuTestSerializeProfile;
		private ToolStripMenuItem mnuTestSerializeServices;
		private ToolStripMenuItem mnuSettings;
		private Button btnSelectMethod;
		private CodeView codeViewRequest;
		private CodeView codeViewResponse;
		private TextBox txtRequestUrl;
		private TextBox txtRequestMethod;
		private Button btnRequestEdit;
		private Button btnPreviewMethod;
		private ToolStripMenuItem mnuCancel;
		private Button btnSelectProfile;
		private DataGridView dataGridHistory;
		private Button btnManageProfiles;
		private ToolStripMenuItem mnuServices;
		private ToolStripMenuItem mnuServicesAddNewPackage;
		private ToolStripMenuItem mnuServicesReloadPackage;
		private ToolStripMenuItem mnuServicesUpdateFolder;
		private ToolStripMenuItem mnuServicesRebuildCache;
		private ToolStripMenuItem mnuTools;
		private ToolStripMenuItem mnuExportSettings;
		private ToolStripMenuItem mnuSaveCustomSettingsSample;
		private Button btnRequestSave;
		private TextBox txtResponseStatus;
		private Button btnResponseSave;
		private Panel panelEditor;
		private Panel panelCodeEdit;
		private Button btnEditorRun;
		private CodeView codeViewEdit;
		private Button btnEditorDiscard;
		private TextBox txtCodeEditMethod;
		private TextBox txtCodeEditUrl;
		private Button btnReplay;
		private Label labelCurrentMethod;
		private Label labelCurrentProfile;
		private ToolStripMenuItem mnuAbout;
		private ToolStripMenuItem mnuSaveCustomInfoSample;
		private Button btnEditorReplay;
		private Button btnLoad;
		private Button btnEditorLoad;
		private Button btnEditorSave;
	}
}