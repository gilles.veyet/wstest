﻿using System.Data;
using WSTest.Helpers;

namespace WSTest.UI
{
	internal partial class FormChooseMethod : Form
	{
		const int EXPAND_LEVEL = 2; // expand category / package so expand until service (methods are hidden).
		readonly Profile _Profile;
		readonly NodeServiceMethod? InitialMethod;
		bool ShowMethodInfo = true;
		bool Loaded = false;

		readonly List<HttpService> AllServices;

		readonly TreeViewHolder<NodeServiceMethod> TreeHolderMethods;

		bool FilteredTree = false;
		readonly int WidthWithoutInfoPanel = 550;
		readonly int WidthWithInfoPanel = 1200;

		readonly bool RestoreFilters;

		static string? PrevFilterMethodValue;
		static string? PrevSelectedFilterCategory;
		static string? PrevSelectedFilterPackage;
		static HttpService? PrevSelectedFilterService;

		string? FilterMethodValue
		{
			get
			{
				var str = txtFilterMethod.Text.Trim();
				return str.Length >= 3 ? str : null;
			}
		}

		string? SelectedFilterCategory => comboFilterCategory.SelectedItem as string;
		string? SelectedFilterPackage => comboFilterPackage.SelectedItem as string;
		HttpService? SelectedFilterService => comboFilterService.SelectedItem as HttpService;

		internal static (HttpService?, HttpServiceMethod?) ChooseMethod(Point location, Profile profile, HttpService? currentService, HttpServiceMethod? currentMethod, bool restoreFilters)
		{
			var frm = new FormChooseMethod(location, profile, currentService, currentMethod, restoreFilters);
			var result = frm.ShowDialog();

			PrevFilterMethodValue = frm.FilterMethodValue;
			PrevSelectedFilterCategory = frm.SelectedFilterCategory;
			PrevSelectedFilterPackage = frm.SelectedFilterPackage;
			PrevSelectedFilterService = frm.SelectedFilterService;

			if (result != DialogResult.OK)
				return (null, null);

			var selectedNode = frm.SelectedNodeServiceMethod!;
			return (selectedNode.Service, selectedNode.Method);
		}

		NodeServiceMethod? SelectedNodeServiceMethod => (treeMethods.SelectedNode?.Tag as NodeServiceMethod);

		HttpService? SelectedService => SelectedNodeServiceMethod?.Service ?? (treeMethods.SelectedNode?.FirstNode.Tag as NodeServiceMethod)?.Service;


		public FormChooseMethod(Point location, Profile profile, HttpService? currentService, HttpServiceMethod? currentMethod, bool restoreFilters)
		{
			InitializeComponent();
			this.Location = location;

			//Debug.WriteLine($"FormChooseMethod restoreFilters:{restoreFilters} currentMethod:{currentMethod}");

			this.RestoreFilters = restoreFilters;
			btnOK.Enabled = false;
			btnShowInExplorer.Enabled = false;

			btnHideMethodInfo.Left = btnShowMethodInfo.Left;
			WidthWithoutInfoPanel = this.splitMain.Panel1.Width + 20;
			WidthWithInfoPanel = this.Width;

			DisplayMethodInfoPanel(false);

			_Profile = profile;
			AllServices =
			[
				.. _Profile.GetServices().ToList().
								OrderBy(s => s.Category, StringComparer.InvariantCultureIgnoreCase).
								ThenBy(s => s.PackageName, StringComparer.InvariantCultureIgnoreCase).
								ThenBy(s => s.Name, StringComparer.InvariantCultureIgnoreCase),
			];


			var nodes = AllServices.SelectMany(s => s.Methods.Select(m => new NodeServiceMethod(m, s)));
			TreeHolderMethods = new(treeMethods, nodes, n => n.MethodName, n => n.Category, n => n.PackageNameAndVersion, n => n.ServiceName);

			if (currentService != null && currentMethod != null && AllServices.Contains(currentService))
				InitialMethod = nodes.FirstOrDefault(n => n.Method == currentMethod);
		}

		bool LoadComboFilterCategory()
		{
			comboFilterCategory.Items.Clear();
			comboFilterCategory.Items.AddRange(AllServices.Select(s => s.Category).Distinct().OrderBy(n => n, StringComparer.InvariantCultureIgnoreCase).ToArray());

			if (comboFilterCategory.Items.Count == 1)
			{
				comboFilterCategory.SelectedIndex = 0;
				return true;
			}
			else
				return false;
		}

		bool LoadComboFilterPackage()
		{
			string? filterCategory = SelectedFilterCategory;

			comboFilterPackage.Items.Clear();
			comboFilterPackage.Items.AddRange(AllServices
					.Where(s => (filterCategory == null || s.Category == SelectedFilterCategory) && !string.IsNullOrEmpty(s.PackageName))
					.Select(s => s.PackageName).Distinct()
					.OrderBy(n => n, StringComparer.InvariantCultureIgnoreCase
				).ToArray());

			if (comboFilterPackage.Items.Count == 1)
			{
				comboFilterPackage.SelectedIndex = 0;
				return true;
			}
			else
				return false;
		}

		bool LoadComboFilterService()
		{
			string? filterCategory = SelectedFilterCategory;
			string? filterPackage = SelectedFilterPackage;

			comboFilterService.Items.Clear();   // strange : it does not trigger comboFilterService_SelectedIndexChanged when a service was selected before clearing items.
			comboFilterService.Items.AddRange(AllServices.Where(s => (filterCategory == null || s.Category == filterCategory) && (filterPackage == null || s.PackageName == filterPackage)).ToArray());

			if (comboFilterService.Items.Count == 1)
			{
				comboFilterService.SelectedIndex = 0;
				return true;
			}
			else
				return false;

		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}

		private async void frmChooseMethod_LoadAsync(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			await WebViewHelper.InitWebViewAsync(webViewMethod);

			if (!LoadComboFilterCategory() && !LoadComboFilterPackage() && !LoadComboFilterService())
				TreeHolderMethods.LoadItems(InitialMethod != null ? 0 : EXPAND_LEVEL);

			if (RestoreFilters)
			{
				//Debug.WriteLine($"frmChooseMethod_LoadAsync RestoreFilters  filterMethod:'{PrevFilterMethodValue}' - filterCategory:'{PrevSelectedFilterCategory}' - filterPackage:'{PrevSelectedFilterPackage}' - filterService:'{PrevSelectedFilterService}'");

				txtFilterMethod.Text = PrevFilterMethodValue;
				comboFilterCategory.SelectedItem = PrevSelectedFilterCategory;
				comboFilterPackage.SelectedItem = PrevSelectedFilterPackage;
				comboFilterService.SelectedItem = PrevSelectedFilterService;

				ApplyFilter();
			}

			TreeHolderMethods.SelectItem(InitialMethod);

			txtFilterMethod.Select();   // same effect as Focus but Select is a better high-level function.

			Loaded = true;
			this.Cursor = Cursors.Default;
		}

		private void treeMethods_AfterSelect(object sender, TreeViewEventArgs e)
		{
			//Debug.WriteLine($"treeMethods_AfterSelect");
			OnSelectedNodeChanged();
		}

		/// <summary>
		/// Handle selected node change
		/// As there isn't any SelectedNodeChanged event (see https://stackoverflow.com/questions/1671153/why-isnt-there-a-selectednodechanged-event-for-windows-forms-treeview )
		/// This method is called when AfterSelect is triggered and when a filter is applied (previously selected node can be filtered out).
		/// </summary>
		void OnSelectedNodeChanged()
		{
			//Debug.WriteLine($"OnSelectedNodeChanged SelectedNodeServiceMethod:{SelectedNodeServiceMethod}");
			btnOK.Enabled = SelectedNodeServiceMethod != null;
			btnShowInExplorer.Enabled = SelectedService != null;

			if (ShowMethodInfo)
				DescribeMethod();
			else if (btnOK.Enabled && Loaded)
				this.DialogResult = DialogResult.OK;
		}

		private void treeMethods_DoubleClick(object sender, EventArgs e)
		{
			if (SelectedNodeServiceMethod != null)
				this.DialogResult = DialogResult.OK;
		}

		private void txtFilterMethod_TextChanged(object sender, EventArgs e)
		{
			ApplyFilter();
		}

		private void comboFilterCategory_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!LoadComboFilterPackage() && !LoadComboFilterService())
				ApplyFilter();
		}
		private void comboFilterPackage_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!LoadComboFilterService())
				ApplyFilter();
		}

		private void comboFilterService_SelectedIndexChanged(object sender, EventArgs e)
		{
			ApplyFilter();
		}

		static bool FilterNode(NodeServiceMethod node, string? filterCategory, string? filterPackage, HttpService? filterService, string? filterMethod)
		{
			if (filterCategory != null && node.Service.Category != filterCategory)
				return false;

			if (filterPackage != null && node.Service.PackageName != filterPackage)
				return false;

			if (filterService != null && node.Service != filterService)
				return false;

			if (filterMethod != null && !node.MethodName.Contains(filterMethod, StringComparison.CurrentCultureIgnoreCase))
				return false;

			return true;
		}

		void ApplyFilter()
		{
			string? filterMethod = FilterMethodValue;
			string? filterCategory = SelectedFilterCategory;
			string? filterPackage = SelectedFilterPackage;
			HttpService? filterService = SelectedFilterService;

			bool filter = filterMethod != null || filterCategory != null || filterPackage != null || filterService != null;

			if (filter || FilteredTree)
			{
				FilteredTree = filter;
				int expandLevel = filterMethod != null || filterService != null ? -1 : EXPAND_LEVEL;
				TreeHolderMethods.LoadItems(expandLevel, n => FilterNode(n, filterCategory, filterPackage, filterService, filterMethod));

				//Debug.WriteLine($"ApplyFilter filterMethod:'{filterMethod}' - filterCategory:'{filterCategory}' - filterPackage:'{filterPackage}' - filterService:'{filterService}' - expandLevel:{expandLevel}");
				OnSelectedNodeChanged();
			}
		}


		private void btnResetFilterCategory_Click(object sender, EventArgs e)
		{
			comboFilterCategory.SelectedIndex = -1;
		}

		private void btnResetFilterPackage_Click(object sender, EventArgs e)
		{
			comboFilterPackage.SelectedIndex = -1;
		}

		private void btnResetFilterService_Click(object sender, EventArgs e)
		{
			comboFilterService.SelectedIndex = -1;
		}

		private void btnResetFilterMethod_Click(object sender, EventArgs e)
		{
			txtFilterMethod.ResetText();
		}

		void DescribeMethod()
		{
			var node = SelectedNodeServiceMethod;

			if (node != null)
				webViewMethod.NavigateToString(node.Method.DescribeMethod(node.Service));
			else
				webViewMethod.NavigateToString(string.Empty);
		}

		private void btnShowMethodInfo_Click(object sender, EventArgs e)
		{
			DisplayMethodInfoPanel(true);
		}

		private void btnHideMethodInfo_Click(object sender, EventArgs e)
		{
			DisplayMethodInfoPanel(false);
		}

		void DisplayMethodInfoPanel(bool show)
		{
			btnShowMethodInfo.Visible = !show;
			btnHideMethodInfo.Visible = show;

			ShowMethodInfo = show;

			splitMain.Panel2Collapsed = !show;
			this.Width = show ? WidthWithInfoPanel : WidthWithoutInfoPanel;

			if (show)
				DescribeMethod();
		}

		private void FormChooseMethod_FormClosed(object sender, FormClosedEventArgs e)
		{
			// Dispose WebView2 to prevent memory leak. Can be seen in task manager: expand WSTest and look for "WebView2: about.blank"
			if (!webViewMethod.IsDisposed)
				webViewMethod.Dispose();
		}

		private void btnShowInExplorer_Click(object sender, EventArgs e)
		{
			if (SelectedService != null)
				Helper.ShowInExplorer(SelectedService.Descriptor.ServiceDefinitionFullPath);
		}
	}
}
