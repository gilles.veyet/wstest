﻿namespace WSTest.UI
{
	partial class FormChooseMethod
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			treeMethods = new TreeView();
			btnCancel = new Button();
			btnOK = new Button();
			txtFilterMethod = new TextBox();
			webViewMethod = new Microsoft.Web.WebView2.WinForms.WebView2();
			groupFilter = new GroupBox();
			labelFilterPackage = new Label();
			btnResetFilterPackage = new Button();
			comboFilterPackage = new ComboBox();
			btnResetFilterMethod = new Button();
			btnResetFilterService = new Button();
			btnResetFilterCategory = new Button();
			comboFilterService = new ComboBox();
			labelFilterService = new Label();
			comboFilterCategory = new ComboBox();
			labelFilterCategory = new Label();
			labelFilterMethod = new Label();
			btnShowMethodInfo = new Button();
			btnHideMethodInfo = new Button();
			splitMain = new SplitContainer();
			btnShowInExplorer = new Button();
			((System.ComponentModel.ISupportInitialize)webViewMethod).BeginInit();
			groupFilter.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)splitMain).BeginInit();
			splitMain.Panel1.SuspendLayout();
			splitMain.Panel2.SuspendLayout();
			splitMain.SuspendLayout();
			SuspendLayout();
			// 
			// treeMethods
			// 
			treeMethods.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
			treeMethods.Location = new Point(0, 176);
			treeMethods.Name = "treeMethods";
			treeMethods.Size = new Size(553, 489);
			treeMethods.TabIndex = 3;
			treeMethods.AfterSelect += treeMethods_AfterSelect;
			treeMethods.DoubleClick += treeMethods_DoubleClick;
			// 
			// btnCancel
			// 
			btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
			btnCancel.DialogResult = DialogResult.Cancel;
			btnCancel.Location = new Point(478, 671);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new Size(75, 23);
			btnCancel.TabIndex = 5;
			btnCancel.Text = "Cancel";
			btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnOK
			// 
			btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
			btnOK.Location = new Point(397, 671);
			btnOK.Name = "btnOK";
			btnOK.Size = new Size(75, 23);
			btnOK.TabIndex = 4;
			btnOK.Text = "OK";
			btnOK.UseVisualStyleBackColor = true;
			btnOK.Click += btnOK_Click;
			// 
			// txtFilterMethod
			// 
			txtFilterMethod.Location = new Point(82, 106);
			txtFilterMethod.Name = "txtFilterMethod";
			txtFilterMethod.Size = new Size(436, 23);
			txtFilterMethod.TabIndex = 10;
			txtFilterMethod.TextChanged += txtFilterMethod_TextChanged;
			// 
			// webViewMethod
			// 
			webViewMethod.AllowExternalDrop = true;
			webViewMethod.CreationProperties = null;
			webViewMethod.DefaultBackgroundColor = Color.White;
			webViewMethod.Dock = DockStyle.Fill;
			webViewMethod.Location = new Point(0, 0);
			webViewMethod.Name = "webViewMethod";
			webViewMethod.Size = new Size(959, 706);
			webViewMethod.TabIndex = 0;
			webViewMethod.ZoomFactor = 1D;
			// 
			// groupFilter
			// 
			groupFilter.Controls.Add(labelFilterPackage);
			groupFilter.Controls.Add(btnResetFilterPackage);
			groupFilter.Controls.Add(comboFilterPackage);
			groupFilter.Controls.Add(btnResetFilterMethod);
			groupFilter.Controls.Add(btnResetFilterService);
			groupFilter.Controls.Add(btnResetFilterCategory);
			groupFilter.Controls.Add(comboFilterService);
			groupFilter.Controls.Add(labelFilterService);
			groupFilter.Controls.Add(comboFilterCategory);
			groupFilter.Controls.Add(labelFilterCategory);
			groupFilter.Controls.Add(labelFilterMethod);
			groupFilter.Controls.Add(txtFilterMethod);
			groupFilter.Location = new Point(5, 29);
			groupFilter.Name = "groupFilter";
			groupFilter.Size = new Size(548, 141);
			groupFilter.TabIndex = 2;
			groupFilter.TabStop = false;
			groupFilter.Text = "Filter";
			// 
			// labelFilterPackage
			// 
			labelFilterPackage.AutoSize = true;
			labelFilterPackage.Location = new Point(6, 51);
			labelFilterPackage.Name = "labelFilterPackage";
			labelFilterPackage.Size = new Size(51, 15);
			labelFilterPackage.TabIndex = 3;
			labelFilterPackage.Text = "Package";
			// 
			// btnResetFilterPackage
			// 
			btnResetFilterPackage.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnResetFilterPackage.FlatAppearance.BorderSize = 0;
			btnResetFilterPackage.FlatStyle = FlatStyle.Flat;
			btnResetFilterPackage.Image = Properties.Resources.Delete;
			btnResetFilterPackage.Location = new Point(524, 48);
			btnResetFilterPackage.Name = "btnResetFilterPackage";
			btnResetFilterPackage.Size = new Size(18, 18);
			btnResetFilterPackage.TabIndex = 5;
			btnResetFilterPackage.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnResetFilterPackage.Click += btnResetFilterPackage_Click;
			// 
			// comboFilterPackage
			// 
			comboFilterPackage.DropDownStyle = ComboBoxStyle.DropDownList;
			comboFilterPackage.FormattingEnabled = true;
			comboFilterPackage.Location = new Point(82, 48);
			comboFilterPackage.Name = "comboFilterPackage";
			comboFilterPackage.Size = new Size(436, 23);
			comboFilterPackage.TabIndex = 4;
			comboFilterPackage.SelectedIndexChanged += comboFilterPackage_SelectedIndexChanged;
			// 
			// btnResetFilterMethod
			// 
			btnResetFilterMethod.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnResetFilterMethod.FlatAppearance.BorderSize = 0;
			btnResetFilterMethod.FlatStyle = FlatStyle.Flat;
			btnResetFilterMethod.Image = Properties.Resources.Delete;
			btnResetFilterMethod.Location = new Point(524, 107);
			btnResetFilterMethod.Name = "btnResetFilterMethod";
			btnResetFilterMethod.Size = new Size(18, 18);
			btnResetFilterMethod.TabIndex = 11;
			btnResetFilterMethod.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnResetFilterMethod.Click += btnResetFilterMethod_Click;
			// 
			// btnResetFilterService
			// 
			btnResetFilterService.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnResetFilterService.FlatAppearance.BorderSize = 0;
			btnResetFilterService.FlatStyle = FlatStyle.Flat;
			btnResetFilterService.Image = Properties.Resources.Delete;
			btnResetFilterService.Location = new Point(524, 77);
			btnResetFilterService.Name = "btnResetFilterService";
			btnResetFilterService.Size = new Size(18, 18);
			btnResetFilterService.TabIndex = 8;
			btnResetFilterService.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnResetFilterService.Click += btnResetFilterService_Click;
			// 
			// btnResetFilterCategory
			// 
			btnResetFilterCategory.Anchor = AnchorStyles.Top | AnchorStyles.Right;
			btnResetFilterCategory.FlatAppearance.BorderSize = 0;
			btnResetFilterCategory.FlatStyle = FlatStyle.Flat;
			btnResetFilterCategory.Image = Properties.Resources.Delete;
			btnResetFilterCategory.Location = new Point(524, 20);
			btnResetFilterCategory.Name = "btnResetFilterCategory";
			btnResetFilterCategory.Size = new Size(18, 18);
			btnResetFilterCategory.TabIndex = 2;
			btnResetFilterCategory.TextImageRelation = TextImageRelation.ImageBeforeText;
			btnResetFilterCategory.Click += btnResetFilterCategory_Click;
			// 
			// comboFilterService
			// 
			comboFilterService.DropDownStyle = ComboBoxStyle.DropDownList;
			comboFilterService.FormattingEnabled = true;
			comboFilterService.Location = new Point(82, 77);
			comboFilterService.Name = "comboFilterService";
			comboFilterService.Size = new Size(436, 23);
			comboFilterService.TabIndex = 7;
			comboFilterService.SelectedIndexChanged += comboFilterService_SelectedIndexChanged;
			// 
			// labelFilterService
			// 
			labelFilterService.AutoSize = true;
			labelFilterService.Location = new Point(6, 80);
			labelFilterService.Name = "labelFilterService";
			labelFilterService.Size = new Size(44, 15);
			labelFilterService.TabIndex = 6;
			labelFilterService.Text = "Service";
			// 
			// comboFilterCategory
			// 
			comboFilterCategory.DropDownStyle = ComboBoxStyle.DropDownList;
			comboFilterCategory.FormattingEnabled = true;
			comboFilterCategory.Location = new Point(82, 19);
			comboFilterCategory.Name = "comboFilterCategory";
			comboFilterCategory.Size = new Size(436, 23);
			comboFilterCategory.TabIndex = 1;
			comboFilterCategory.SelectedIndexChanged += comboFilterCategory_SelectedIndexChanged;
			// 
			// labelFilterCategory
			// 
			labelFilterCategory.AutoSize = true;
			labelFilterCategory.Location = new Point(6, 22);
			labelFilterCategory.Name = "labelFilterCategory";
			labelFilterCategory.Size = new Size(55, 15);
			labelFilterCategory.TabIndex = 0;
			labelFilterCategory.Text = "Category";
			// 
			// labelFilterMethod
			// 
			labelFilterMethod.AutoSize = true;
			labelFilterMethod.Location = new Point(6, 109);
			labelFilterMethod.Name = "labelFilterMethod";
			labelFilterMethod.Size = new Size(49, 15);
			labelFilterMethod.TabIndex = 9;
			labelFilterMethod.Text = "Method";
			// 
			// btnShowMethodInfo
			// 
			btnShowMethodInfo.Location = new Point(410, 12);
			btnShowMethodInfo.Name = "btnShowMethodInfo";
			btnShowMethodInfo.Size = new Size(143, 23);
			btnShowMethodInfo.TabIndex = 1;
			btnShowMethodInfo.Text = "Show Method Info >>";
			btnShowMethodInfo.UseVisualStyleBackColor = true;
			btnShowMethodInfo.Click += btnShowMethodInfo_Click;
			// 
			// btnHideMethodInfo
			// 
			btnHideMethodInfo.Location = new Point(5, 12);
			btnHideMethodInfo.Name = "btnHideMethodInfo";
			btnHideMethodInfo.Size = new Size(143, 23);
			btnHideMethodInfo.TabIndex = 0;
			btnHideMethodInfo.Text = "Hide Method Info <<";
			btnHideMethodInfo.UseVisualStyleBackColor = true;
			btnHideMethodInfo.Click += btnHideMethodInfo_Click;
			// 
			// splitMain
			// 
			splitMain.Dock = DockStyle.Fill;
			splitMain.FixedPanel = FixedPanel.Panel1;
			splitMain.Location = new Point(0, 0);
			splitMain.Name = "splitMain";
			// 
			// splitMain.Panel1
			// 
			splitMain.Panel1.Controls.Add(btnShowInExplorer);
			splitMain.Panel1.Controls.Add(treeMethods);
			splitMain.Panel1.Controls.Add(btnHideMethodInfo);
			splitMain.Panel1.Controls.Add(btnOK);
			splitMain.Panel1.Controls.Add(btnShowMethodInfo);
			splitMain.Panel1.Controls.Add(btnCancel);
			splitMain.Panel1.Controls.Add(groupFilter);
			// 
			// splitMain.Panel2
			// 
			splitMain.Panel2.Controls.Add(webViewMethod);
			splitMain.Size = new Size(1518, 706);
			splitMain.SplitterDistance = 555;
			splitMain.TabIndex = 0;
			// 
			// btnShowInExplorer
			// 
			btnShowInExplorer.Location = new Point(261, 12);
			btnShowInExplorer.Name = "btnShowInExplorer";
			btnShowInExplorer.Size = new Size(143, 23);
			btnShowInExplorer.TabIndex = 6;
			btnShowInExplorer.Text = "Show in Explorer";
			btnShowInExplorer.UseVisualStyleBackColor = true;
			btnShowInExplorer.Click += btnShowInExplorer_Click;
			// 
			// FormChooseMethod
			// 
			AcceptButton = btnOK;
			AutoScaleDimensions = new SizeF(7F, 15F);
			AutoScaleMode = AutoScaleMode.Font;
			CancelButton = btnCancel;
			ClientSize = new Size(1518, 706);
			Controls.Add(splitMain);
			MinimizeBox = false;
			Name = "FormChooseMethod";
			ShowIcon = false;
			ShowInTaskbar = false;
			StartPosition = FormStartPosition.Manual;
			Text = "Choose Method";
			FormClosed += FormChooseMethod_FormClosed;
			Load += frmChooseMethod_LoadAsync;
			((System.ComponentModel.ISupportInitialize)webViewMethod).EndInit();
			groupFilter.ResumeLayout(false);
			groupFilter.PerformLayout();
			splitMain.Panel1.ResumeLayout(false);
			splitMain.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)splitMain).EndInit();
			splitMain.ResumeLayout(false);
			ResumeLayout(false);
		}

		#endregion

		private TreeView treeMethods;
		private Button btnCancel;
		private Button btnOK;
		private TextBox txtFilterMethod;
		private Microsoft.Web.WebView2.WinForms.WebView2 webViewMethod;
		private GroupBox groupFilter;
		private ComboBox comboFilterService;
		private Label labelFilterService;
		private ComboBox comboFilterCategory;
		private Label labelFilterCategory;
		private Label labelFilterMethod;
		private Button btnResetFilterMethod;
		private Button btnResetFilterService;
		private Button btnResetFilterCategory;
		private Button btnShowMethodInfo;
		private Button btnHideMethodInfo;
		private SplitContainer splitMain;
		private Label labelFilterPackage;
		private Button btnResetFilterPackage;
		private ComboBox comboFilterPackage;
		private Button btnShowInExplorer;
	}
}