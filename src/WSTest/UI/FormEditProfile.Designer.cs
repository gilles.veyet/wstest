﻿namespace WSTest.UI
{
	partial class FormEditProfile
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.labelName = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.labelGroup = new System.Windows.Forms.Label();
			this.txtBaseUrl = new System.Windows.Forms.TextBox();
			this.labelBaseUrl = new System.Windows.Forms.Label();
			this.labelProperties = new System.Windows.Forms.Label();
			this.propertyEdit = new WSTest.Editor.PropertyEditor();
			this.lblServices = new System.Windows.Forms.Label();
			this.btnChooseServices = new System.Windows.Forms.Button();
			this.mainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.panelServices = new System.Windows.Forms.Panel();
			this.dataGridServices = new System.Windows.Forms.DataGridView();
			this.panelProperties = new System.Windows.Forms.Panel();
			this.comboGroup = new System.Windows.Forms.ComboBox();
			this.mainLayoutPanel.SuspendLayout();
			this.panelServices.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridServices)).BeginInit();
			this.panelProperties.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOK.Location = new System.Drawing.Point(1216, 626);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 0;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(1297, 626);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// labelName
			// 
			this.labelName.AutoSize = true;
			this.labelName.Location = new System.Drawing.Point(12, 15);
			this.labelName.Name = "labelName";
			this.labelName.Size = new System.Drawing.Size(44, 15);
			this.labelName.TabIndex = 2;
			this.labelName.Text = "Name*";
			// 
			// txtName
			// 
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtName.Location = new System.Drawing.Point(98, 12);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(1274, 23);
			this.txtName.TabIndex = 3;
			this.txtName.Validated += new System.EventHandler(this.txtName_Validated);
			// 
			// labelGroup
			// 
			this.labelGroup.AutoSize = true;
			this.labelGroup.Location = new System.Drawing.Point(12, 44);
			this.labelGroup.Name = "labelGroup";
			this.labelGroup.Size = new System.Drawing.Size(40, 15);
			this.labelGroup.TabIndex = 5;
			this.labelGroup.Text = "Group";
			// 
			// txtBaseUrl
			// 
			this.txtBaseUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtBaseUrl.Location = new System.Drawing.Point(98, 70);
			this.txtBaseUrl.Name = "txtBaseUrl";
			this.txtBaseUrl.Size = new System.Drawing.Size(1274, 23);
			this.txtBaseUrl.TabIndex = 6;
			this.txtBaseUrl.Validated += new System.EventHandler(this.txtBaseUrl_Validated);
			// 
			// labelBaseUrl
			// 
			this.labelBaseUrl.AutoSize = true;
			this.labelBaseUrl.Location = new System.Drawing.Point(16, 73);
			this.labelBaseUrl.Name = "labelBaseUrl";
			this.labelBaseUrl.Size = new System.Drawing.Size(54, 15);
			this.labelBaseUrl.TabIndex = 7;
			this.labelBaseUrl.Text = "Base Url*";
			// 
			// labelProperties
			// 
			this.labelProperties.AutoSize = true;
			this.labelProperties.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.labelProperties.Location = new System.Drawing.Point(0, 0);
			this.labelProperties.Name = "labelProperties";
			this.labelProperties.Size = new System.Drawing.Size(68, 15);
			this.labelProperties.TabIndex = 9;
			this.labelProperties.Text = "Properties:";
			// 
			// propertyEdit
			// 
			this.propertyEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.propertyEdit.BackColor = System.Drawing.SystemColors.ControlLight;
			this.propertyEdit.Location = new System.Drawing.Point(-3, 27);
			this.propertyEdit.Name = "propertyEdit";
			this.propertyEdit.ShowSpecifiedCheckbox = true;
			this.propertyEdit.Size = new System.Drawing.Size(1353, 218);
			this.propertyEdit.TabIndex = 10;
			// 
			// lblServices
			// 
			this.lblServices.AutoSize = true;
			this.lblServices.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.lblServices.Location = new System.Drawing.Point(3, 7);
			this.lblServices.Name = "lblServices";
			this.lblServices.Size = new System.Drawing.Size(57, 15);
			this.lblServices.TabIndex = 11;
			this.lblServices.Text = "Services:";
			// 
			// btnChooseServices
			// 
			this.btnChooseServices.Location = new System.Drawing.Point(63, 3);
			this.btnChooseServices.Name = "btnChooseServices";
			this.btnChooseServices.Size = new System.Drawing.Size(75, 23);
			this.btnChooseServices.TabIndex = 13;
			this.btnChooseServices.Text = "Choose";
			this.btnChooseServices.UseVisualStyleBackColor = true;
			this.btnChooseServices.Click += new System.EventHandler(this.btnChooseServices_Click);
			// 
			// mainLayoutPanel
			// 
			this.mainLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.mainLayoutPanel.ColumnCount = 1;
			this.mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.mainLayoutPanel.Controls.Add(this.panelServices, 0, 0);
			this.mainLayoutPanel.Controls.Add(this.panelProperties, 0, 1);
			this.mainLayoutPanel.Location = new System.Drawing.Point(16, 113);
			this.mainLayoutPanel.Name = "mainLayoutPanel";
			this.mainLayoutPanel.RowCount = 2;
			this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.mainLayoutPanel.Size = new System.Drawing.Size(1356, 507);
			this.mainLayoutPanel.TabIndex = 15;
			// 
			// panelServices
			// 
			this.panelServices.Controls.Add(this.dataGridServices);
			this.panelServices.Controls.Add(this.lblServices);
			this.panelServices.Controls.Add(this.btnChooseServices);
			this.panelServices.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelServices.Location = new System.Drawing.Point(3, 3);
			this.panelServices.Name = "panelServices";
			this.panelServices.Size = new System.Drawing.Size(1350, 247);
			this.panelServices.TabIndex = 0;
			// 
			// dataGridServices
			// 
			this.dataGridServices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridServices.Location = new System.Drawing.Point(3, 32);
			this.dataGridServices.Name = "dataGridServices";
			this.dataGridServices.RowTemplate.Height = 25;
			this.dataGridServices.Size = new System.Drawing.Size(1344, 218);
			this.dataGridServices.TabIndex = 14;
			// 
			// panelProperties
			// 
			this.panelProperties.Controls.Add(this.labelProperties);
			this.panelProperties.Controls.Add(this.propertyEdit);
			this.panelProperties.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelProperties.Location = new System.Drawing.Point(3, 256);
			this.panelProperties.Name = "panelProperties";
			this.panelProperties.Size = new System.Drawing.Size(1350, 248);
			this.panelProperties.TabIndex = 1;
			// 
			// comboGroup
			// 
			this.comboGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboGroup.FormattingEnabled = true;
			this.comboGroup.Location = new System.Drawing.Point(98, 41);
			this.comboGroup.Name = "comboGroup";
			this.comboGroup.Size = new System.Drawing.Size(1274, 23);
			this.comboGroup.TabIndex = 16;
			this.comboGroup.Validated += new System.EventHandler(this.comboGroup_Validated);
			// 
			// frmEditProfile
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(1384, 661);
			this.Controls.Add(this.comboGroup);
			this.Controls.Add(this.mainLayoutPanel);
			this.Controls.Add(this.labelBaseUrl);
			this.Controls.Add(this.txtBaseUrl);
			this.Controls.Add(this.labelGroup);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.labelName);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmEditProfile";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "frmEditProfile";
			this.Load += new System.EventHandler(this.frmEditProfile_Load);
			this.mainLayoutPanel.ResumeLayout(false);
			this.panelServices.ResumeLayout(false);
			this.panelServices.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridServices)).EndInit();
			this.panelProperties.ResumeLayout(false);
			this.panelProperties.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Button btnOK;
		private Button btnCancel;
		private Label labelName;
		private TextBox txtName;
		private Label labelGroup;
		private TextBox txtBaseUrl;
		private Label labelBaseUrl;
		private Label labelProperties;
		private Editor.PropertyEditor propertyEdit;
		private Label lblServices;
		private Button btnChooseServices;
		private TableLayoutPanel mainLayoutPanel;
		private Panel panelServices;
		private Panel panelProperties;
		private ComboBox comboGroup;
		private DataGridView dataGridServices;
	}
}