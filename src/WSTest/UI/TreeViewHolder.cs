﻿namespace WSTest.UI
{
	internal class TreeViewHolder<T> where T : notnull
	{
		public delegate string FuncName(T arg);
		public delegate string? FuncGroup(T arg);
		public delegate bool FuncFilter(T arg);

		private readonly TreeView Tree;
		private readonly List<T> SortedItems;
		private readonly FuncGroup[] FuncGroups;
		private readonly FuncName ItemName;
		private Dictionary<T, TreeNode> DictLoadedItems = [];

		public TreeViewHolder(TreeView treeView, IEnumerable<T> items, FuncName itemName, params FuncGroup[] funcGroups)
		{
			Tree = treeView;
			ItemName = itemName;
			FuncGroups = funcGroups;

			Tree.HideSelection = false;  // show selected node when TreeView do not have focus.

			// sort items by function groups
			IOrderedEnumerable<T> orderedItems = items.OrderBy(x => funcGroups.First()(x), StringComparer.InvariantCultureIgnoreCase);

			foreach (var func in funcGroups.Skip(1))
				orderedItems = orderedItems.ThenBy(x => func(x), StringComparer.InvariantCultureIgnoreCase);

			// then sort by item name
			orderedItems = orderedItems.ThenBy(x => itemName(x), StringComparer.InvariantCultureIgnoreCase);

			SortedItems = [.. orderedItems];
		}

		/// <summary>
		/// Load items with optional filters
		/// </summary>
		/// <param name="expandLevel">Expand level or -1 to expand all nodes</param>
		/// <param name="filter">Filter function (optional)</param>
		public void LoadItems(int expandLevel, FuncFilter? filter = null)
		{
			DictLoadedItems = [];

			Tree.BeginUpdate();
			Tree.Nodes.Clear();

			bool expandAll = expandLevel < 0;

			TreeNode?[] groupNodes = new TreeNode[FuncGroups.Length];

			foreach (var item in SortedItems)
			{
				if (filter != null && !filter(item))
					continue;

				int levelGroup = 0;

				TreeNodeCollection parent = Tree.Nodes;

				foreach (var funcGroup in FuncGroups)
				{
					var node = groupNodes[levelGroup];
					string? text = funcGroup(item);

					if (node?.Text != text)
					{
						for (int i = levelGroup; i < groupNodes.Length; ++i)
						{
							TreeViewHolder<T>.TrimNode(groupNodes[i], expandAll || i < expandLevel);
							groupNodes[i] = null;
						}

						if (!string.IsNullOrEmpty(text))
						{
							var child = parent.Add(text);
							parent = child.Nodes;
							groupNodes[levelGroup] = child;
						}
					}
					else if (node != null)
						parent = node.Nodes;

					++levelGroup;
				}

				var nodeItem = parent.Add(ItemName(item));
				nodeItem.Tag = item;
				DictLoadedItems[item] = nodeItem;

				if (expandAll || levelGroup < expandLevel)
					nodeItem.Expand();
			}

			int levelTrim = 0;

			foreach (var node in groupNodes)
			{
				TreeViewHolder<T>.TrimNode(node, expandAll || levelTrim++ < expandLevel);
			}

			if (Tree.Nodes.Count > 0)
				Tree.Nodes[0].EnsureVisible();  // scroll to top

			Tree.EndUpdate();
		}

		static void TrimNode(TreeNode? node, bool expand)
		{
			if (node != null)
			{
				if (node.Nodes.Count == 0)
					node.Remove();
				else if (expand)
					node.Expand();
			}
		}

		public void SelectItem(T? selectedItem)
		{
			TreeNode? node = null;

			if (selectedItem != null)
				DictLoadedItems.TryGetValue(selectedItem, out node);

			// when node is not null : expand ascendants and scroll tree view so that selected node is visible.
			// when node is null : should unselect all nodes but actually first node is selected.
			Tree.SelectedNode = node;
		}

		public void CheckItems(IEnumerable<T> checkedItems)
		{
			Tree.BeginUpdate();

			foreach (var checkedItem in checkedItems)
			{
				var node = DictLoadedItems[checkedItem];
				if (node != null)
					node.Checked = true;
			}

			TreeViewHolder<T>.RecurseCheckAllNodes(Tree.Nodes);
			Tree.EndUpdate();
		}

		enum AllNoneOrSome
		{
			All,
			None,
			Some,
		}

		static AllNoneOrSome RecurseCheckAllNodes(TreeNodeCollection collection)
		{
			int countTotal = 0;
			int countAllChecked = 0;
			int countNoneChecked = 0;

			foreach (var node in collection.OfType<TreeNode>())
			{
				++countTotal;

				if (node.Nodes.Count == 0)
				{
					if (node.Checked)
						++countAllChecked;
					else
						++countNoneChecked;
				}
				else
				{
					switch (TreeViewHolder<T>.RecurseCheckAllNodes(node.Nodes))
					{
						case AllNoneOrSome.All:
							++countAllChecked;
							node.Checked = true;
							break;
						case AllNoneOrSome.None:
							++countNoneChecked;
							node.Checked = false;
							break;
						default:
							node.Checked = false;
							break;
					}
				}
			}

			var result = countNoneChecked == countTotal ? AllNoneOrSome.None : countAllChecked == countTotal ? AllNoneOrSome.All : AllNoneOrSome.Some;
			return result;
		}
	}
}
