﻿using System.ComponentModel;
using System.Windows.Forms.Design;

namespace WSTest.UI
{
	[Designer(typeof(FileInputDesigner))]
	[DefaultEvent(nameof(FileNameChanged))]
	public partial class FileInput : UserControl
	{
		[Category("Property Changed"), Browsable(true), Description("FileName was changed")]
		public event EventHandler? FileNameChanged;

		[Category("Appearance"), Browsable(true), Description("Selected FileName / Initial FileName")]
		public string FileName
		{
			get { return txtFileName.Text.Trim(); }
			set { txtFileName.Text = value; }
		}

		[Category("Appearance"), Browsable(true), Description("The InitialDirectory for OpenFileDialog")]
		public string InitialDirectory { get; set; } = string.Empty;

		[Category("Appearance"), Browsable(true), Description("The Filter for OpenFileDialog")]
		public string Filter { get; set; } = string.Empty;

		[Category("Appearance"), Browsable(true), Description("The Filter Index for OpenFileDialog")]
		public int FilterIndex { get; set; } = 1;

		[Category("Behavior"), Browsable(true), Description(" whether the dialog box displays a warning if the user specifies a file name that does not exist.")]
		public bool CheckFileExists { get; set; } = true;


		[Category("Behavior"), Browsable(true), Description("Controls whether the FileName can be changed or not")]
		public bool Readonly
		{
			get { return txtFileName.ReadOnly; }
			set
			{
				txtFileName.ReadOnly = value;
				btnBrowseFile.Visible = !value;
			}
		}

		public FileInput()
		{
			InitializeComponent();
		}

		private void btnBrowseFile_Click(object sender, EventArgs e)
		{
			OpenFileDialog fd = new();

			if (!string.IsNullOrEmpty(InitialDirectory))
				fd.InitialDirectory = InitialDirectory;
			else if (string.IsNullOrEmpty(FileName))
			{
				fd.InitialDirectory = Path.GetDirectoryName(FileName);
			}

			fd.CheckFileExists = CheckFileExists;
			fd.Filter = Filter;
			fd.FilterIndex = FilterIndex;

			if (fd.ShowDialog() == DialogResult.OK)
			{
				txtFileName.Text = fd.FileName;
			}
		}

		private void txtFileName_TextChanged(object sender, EventArgs e)
		{
			FileNameChanged?.Invoke(this, EventArgs.Empty);
		}

		// Control has a fixed height
		protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
		{
			height = 23;
			base.SetBoundsCore(x, y, width, height, specified);
		}

		// Designer that remove top and bottom resize handles.
		internal class FileInputDesigner : ControlDesigner
		{
			FileInputDesigner()
			{
				base.AutoResizeHandles = true;
			}

			public override SelectionRules SelectionRules
			{
				get
				{
					return SelectionRules.LeftSizeable | SelectionRules.RightSizeable | SelectionRules.Moveable;
				}
			}
		}

	}
}
