﻿using System.ComponentModel;
using System.Windows.Forms.Design;

namespace WSTest.UI
{
	[Designer(typeof(PasswordInputDesigner))]
	[DefaultEvent(nameof(PasswordChanged))]

	public partial class PasswordInput : UserControl
	{
		[Category("Property Changed"), Browsable(true), Description("Password was changed")]
		public event EventHandler? PasswordChanged;

		[Category("Appearance"), Browsable(true), Description("Password")]
		public string Password
		{
			get { return txtPassword.Text.Trim(); }
			set { txtPassword.Text = value; }
		}

		bool ShowPassword = false;

		public PasswordInput()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			btnHide.Location = btnShow.Location;
			UpdateContext();
		}

		void UpdateContext()
		{
			txtPassword.UseSystemPasswordChar = !ShowPassword;
			btnShow.Visible = !ShowPassword;
			btnHide.Visible = ShowPassword;
		}

		private void txtPassword_TextChanged(object sender, EventArgs e)
		{
			PasswordChanged?.Invoke(this, EventArgs.Empty);
		}

		private void btnHide_Click(object sender, EventArgs e)
		{
			ShowPassword = false;
			UpdateContext();
		}

		private void btnShow_Click(object sender, EventArgs e)
		{
			ShowPassword = true;
			UpdateContext();
		}

		// Control has a fixed height
		protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
		{
			height = 23;
			base.SetBoundsCore(x, y, width, height, specified);
		}

		internal class PasswordInputDesigner : ControlDesigner
		{
			PasswordInputDesigner()
			{
				base.AutoResizeHandles = true;
			}

			public override SelectionRules SelectionRules
			{
				get
				{
					return SelectionRules.LeftSizeable | SelectionRules.RightSizeable | SelectionRules.Moveable;
				}
			}
		}
	}

}
