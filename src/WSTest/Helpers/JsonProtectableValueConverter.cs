﻿using Newtonsoft.Json;

namespace WSTest.Helpers
{
	internal class JsonProtectableValueConverter : JsonConverter<ProtectableValue>
	{
		public static bool SaveProtectedValuesWithoutEncryption;

		public override void WriteJson(JsonWriter writer, ProtectableValue? value, JsonSerializer serializer)
		{
			if (value == null)
				writer.WriteNull();
			else if (value.Protected)
			{
				if (SaveProtectedValuesWithoutEncryption)
					writer.WriteValue('\n' + value.Value);
				else
					writer.WriteValue('\r' + Convert.ToBase64String(SecurityHelper.Encrypt(value.Value)));
			}
			else
				writer.WriteValue(value.Value);
		}

		public override ProtectableValue ReadJson(JsonReader reader, Type objectType, ProtectableValue? existingValue, bool hasExistingValue, JsonSerializer serializer)
		{
			string? value = reader.Value as string;

			if (value != null && value.StartsWith('\r'))
			{
				string password = SecurityHelper.Decrypt(Convert.FromBase64String(value[1..]));
				return new ProtectableValue(password, true);
			}
			else if (value != null && value.StartsWith('\n'))
			{
				return new ProtectableValue(value[1..], true);
			}
			else
			{
				return new ProtectableValue(value ?? string.Empty);
			}
		}
	}

}
