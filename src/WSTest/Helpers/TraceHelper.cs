﻿using System.Diagnostics;

namespace WSTest.Helpers;

public static class TraceHelper
{
	public static int ErrorCount { get; private set; }
	public static int WarningCount { get; private set; }
	public static TraceLevel MinLevel { get; set; }

	public static void InitTrace(TraceLevel minLevel = TraceLevel.Verbose)
	{
		MinLevel = minLevel;
		string tracePath = Path.Combine(Program.AppLocalFolder, Program.AppName + ".log");
		File.Delete(tracePath);

		var listener = new TextWriterTraceListener(tracePath, "appListener")
		{
			TraceOutputOptions = TraceOptions.DateTime | TraceOptions.Timestamp
		};

		Trace.AutoFlush = true;
		Trace.Listeners.Add(listener);
	}

	public static void WriteVerbose(string message)
	{
		Write(TraceLevel.Verbose, message);
	}

	public static void WriteInfo(string message)
	{
		Write(TraceLevel.Info, message);
	}

	public static void WriteWarning(string message)
	{
		Write(TraceLevel.Warning, message);
	}

	public static void WriteError(string message)
	{
		Write(TraceLevel.Error, message);
	}

	public static void WriteException(string message, Exception ex)
	{
		Write(TraceLevel.Error, $"{message}\r\n{ex}");
	}

	public static bool IsEnabled(TraceLevel traceLevel)
	{
		return traceLevel <= MinLevel;
	}

	public static void Write(TraceLevel traceLevel, string message)
	{
		switch (traceLevel)
		{
			case TraceLevel.Error:
				++ErrorCount;
				break;

			case TraceLevel.Warning:
				++WarningCount;
				break;
		}

		if (traceLevel <= MinLevel)
			Trace.WriteLine($"{DateTime.Now:yyyy.MM.dd HH:mm:ss,fff} {Enum.GetName(typeof(TraceLevel), traceLevel)?[..1]} {message}");
	}

}

