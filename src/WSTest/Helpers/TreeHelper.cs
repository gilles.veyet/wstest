﻿using WSTest.Extensions;

namespace WSTest.Helpers
{
	internal static class TreeHelper
	{
		public static void SetCheckedDescendantNode(TreeNode node, bool check)
		{
			if (node.Nodes.Count > 0)
			{
				foreach (var child in node.Nodes.Descendants())
					child.Checked = check;
			}

			var parent = node.Parent;

			while (parent != null)
			{
				parent.Checked = !parent.Nodes.Descendants().Where(n => !n.Checked).Any();
				parent = parent.Parent;
			}
		}
	}
}
