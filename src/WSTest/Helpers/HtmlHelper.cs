﻿using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace WSTest.Helpers;

class HtmlCollection : List<HtmlElement>
{
	public HtmlCollection(IEnumerable<HtmlElement>? collection = null) : base(collection ?? [])
	{ }

	public HtmlCollection(HtmlElement htmlElement) : base([htmlElement])
	{ }

	public string ToHtml()
	{
		StringBuilder sb = new();

		foreach (var element in this)
			sb.Append(element.ToHtml());

		return sb.ToString();
	}
}

internal static class HtmlElementExtensions
{
	public static T AddClass<T>(this T htmlTag, string className) where T : HtmlTag
	{
		htmlTag.ClassNames.Add(className);
		return htmlTag;
	}

	public static T AddChild<T>(this T htmlTag, HtmlElement child) where T : HtmlTag
	{
		htmlTag.Children.Add(child);
		return htmlTag;
	}
}

abstract class HtmlElement
{
	public abstract string ToHtml();
}

class HtmlText(string text) : HtmlElement
{
	public string Text = text;
	public bool IsBold { get; init; }

	public bool IsPreformatted { get; init; }

	public override string ToHtml()
	{
		HtmlElement elem = new HtmlText(Text);
		HtmlElement start = elem;

		if (IsBold)
			elem = new HtmlBold().AddChild(elem);

		if (IsPreformatted)
			elem = new HtmlPre().AddChild(elem);

		return elem == start ? HttpUtility.HtmlEncode(Text) : elem.ToHtml();
	}
}

class HtmlRaw : HtmlElement
{
	public string Content { get; }

	public HtmlRaw(string content)
	{
		this.Content = content;
	}

	public override string ToHtml()
	{
		return Content;
	}
}

class HtmlBreak : HtmlElement
{
	public static HtmlBreak Instance = new();
	public override string ToHtml()
	{
		return "<br/>";
	}
}

class HtmlSpace : HtmlElement
{
	public static HtmlSpace Instance = new();

	public override string ToHtml()
	{
		return "&nbsp;";
	}
}

abstract class HtmlTag : HtmlElement
{
	protected abstract string Tag { get; }

	public string? Id { get; init; }
	public string? Name { get; init; }
	public List<string> ClassNames { get; init; } = [];

	public Dictionary<string, string> Attributes { get; init; } = [];

	public HtmlCollection Children { get; init; } = [];

	public HtmlTag() { }

	public HtmlTag(params HtmlElement[] children)
	{
		Children = new(children);
	}

	public HtmlTag(IEnumerable<HtmlElement> children)
	{
		Children.AddRange(children);
	}


	public HtmlElement AddChildren(IEnumerable<HtmlElement> children)
	{
		Children.AddRange(children);
		return this;
	}

	public HtmlElement SetAttribute(string key, string value)
	{
		Attributes[key] = value;
		return this;
	}

	public string? GetAttribute(string key)
	{
		if (Attributes.TryGetValue(key, out string? value))
			return value;

		return null;
	}

	public bool RemoveAttribute(string key)
	{
		return Attributes.Remove(key);
	}

	public HtmlTag AddText(string text)
	{
		return this.AddChild(new HtmlText(text));
	}

	public override string ToHtml()
	{
		StringBuilder sb = new();
		sb.Append('<');
		sb.Append(Tag);

		if (!string.IsNullOrWhiteSpace(Id))
			sb.Append($" id=\"{Id}\"");

		if (!string.IsNullOrWhiteSpace(Name))
			sb.Append($" name=\"{Name}\"");

		if (ClassNames.Count > 0)
		{
			sb.Append(" class=");
			sb.Append('"');
			sb.Append(string.Join('.', ClassNames));
			sb.Append('"');
		}

		foreach (KeyValuePair<string, string> kv in Attributes)
		{
			// handle empty attribute, see https://html.spec.whatwg.org/multipage/syntax.html#syntax-attr-empty
			if (string.IsNullOrEmpty(kv.Value))
				sb.Append($" {kv.Key}");
			else
				sb.Append($" {kv.Key}=\"{kv.Value}\"");
		}

		sb.Append('>');

		sb.Append(Children.ToHtml());

		sb.Append("</");
		sb.Append(Tag);
		sb.Append('>');

		return sb.ToString();
	}
}


class HtmlBody : HtmlTag
{
	protected override string Tag => "body";

	public HtmlBody() { }
	public HtmlBody(params HtmlElement[] children) : base(children) { }
	public HtmlBody(IEnumerable<HtmlElement> children) : base(children) { }
}

class HtmlPre : HtmlTag
{
	protected override string Tag => "pre";
	public HtmlPre() { }
	public HtmlPre(params HtmlElement[] children) : base(children) { }
}

class HtmlBold : HtmlTag
{
	protected override string Tag => "b";

	public HtmlBold()
	{ }
}


class HtmlHeading1 : HtmlTag
{
	protected override string Tag => "h1";

	public HtmlHeading1() { }
	public HtmlHeading1(string text) : base(new HtmlText(text)) { }
}

class HtmlHeading2 : HtmlTag
{
	protected override string Tag => "h2";

	public HtmlHeading2() { }
	public HtmlHeading2(string text) : base(new HtmlText(text)) { }
}


class HtmlHeading3 : HtmlTag
{
	protected override string Tag => "h3";
	public HtmlHeading3() { }
	public HtmlHeading3(string text) : base(new HtmlText(text)) { }

}

partial class HtmlLink : HtmlTag
{
	protected override string Tag => "a";

	public HtmlLink() { }

	public HtmlLink(string href)
	{
		SetAttribute("href", href);
	}

	/// <summary>
	/// Make HtmlLink that we can refer to with <see cref="MakeReference"/>
	/// </summary>
	public static HtmlLink MakeAnchor(XmlQualifiedName name)
	{
		return new HtmlLink() { Id = MakeHRef(name) };
	}

	/// <summary>
	/// Make HtmlLink that refers to <see cref="MakeAnchor"/>.
	/// </summary>
	public static HtmlLink MakeReference(XmlQualifiedName name)
	{
		return new HtmlLink('#' + MakeHRef(name));
	}

	[GeneratedRegex("[:/]+")]
	private static partial Regex RegexInvalidHRefChars();

	/// <summary>
	/// Transform qualified name so it does NOT look like an url (so replace ':' and '/' by '_').
	/// </summary>
	static string MakeHRef(XmlQualifiedName name)
	{
		return RegexInvalidHRefChars().Replace(name.ToString(), "_");
	}
}

class HtmlDiv : HtmlTag
{
	protected override string Tag => "div";

	public HtmlDiv() { }

	public HtmlDiv(string text) : this(new HtmlText(text)) { }

	public HtmlDiv(params HtmlElement[] children) : base(children) { }
	public HtmlDiv(IEnumerable<HtmlElement> children) : base(children) { }
}

class HtmlSpan : HtmlTag
{
	protected override string Tag => "span";

	public HtmlSpan() { }

	public HtmlSpan(string text) : this(new HtmlText(text)) { }

	public HtmlSpan(params HtmlElement[] children) : base(children) { }
	public HtmlSpan(IEnumerable<HtmlElement> children) : base(children) { }
}


class HtmlTable : HtmlTag
{
	protected override string Tag => "table";

	public HtmlTable() { }
	public HtmlTable(params HtmlElement[] children) : base(children) { }
	public HtmlTable(IEnumerable<HtmlElement> children) : base(children) { }

}

class HtmlTableHead : HtmlTag
{
	protected override string Tag => "thead";

	public HtmlTableHead() { }
	public HtmlTableHead(params HtmlElement[] children) : base(children) { }
	public HtmlTableHead(IEnumerable<HtmlElement> children) : base(children) { }
}

class HtmlTableBody : HtmlTag
{
	protected override string Tag => "thead";

	public HtmlTableBody() { }
	public HtmlTableBody(params HtmlElement[] children) : base(children) { }
	public HtmlTableBody(IEnumerable<HtmlElement> children) : base(children) { }
}

class HtmlTableRow : HtmlTag
{
	protected override string Tag => "tr";

	public HtmlTableRow() { }
	public HtmlTableRow(params HtmlElement[] children) : base(children) { }
}

abstract class HtmlTableCell : HtmlTag
{
	const string ATTR_COLSPAN = "colspan";

	private int? _colSpan;

	public int? ColSpan
	{
		get
		{
			return _colSpan;
		}
		set
		{
			_colSpan = value;

			if (value > 1)
				SetAttribute(ATTR_COLSPAN, value.Value.ToString());
			else
				RemoveAttribute(ATTR_COLSPAN);
		}
	}

	public HtmlTableCell() { }

	public HtmlTableCell(params HtmlElement[] children) : base(children) { }
	public HtmlTableCell(IEnumerable<HtmlElement> children) : base(children) { }
}


class HtmlTableData : HtmlTableCell
{
	protected override string Tag => "td";

	public HtmlTableData() { }
	public HtmlTableData(params HtmlElement[] children) : base(children) { }
	public HtmlTableData(IEnumerable<HtmlElement> children) : base(children) { }

}

class HtmlTableHeader : HtmlTableCell
{
	protected override string Tag => "th";

	public HtmlTableHeader() { }

	public HtmlTableHeader(params HtmlElement[] children) : base(children) { }
	public HtmlTableHeader(IEnumerable<HtmlElement> children) : base(children) { }
}

class HtmlDetails : HtmlTag
{
	const string ATTR_OPEN = "open";
	protected override string Tag => "details";

	public bool IsOpen
	{
		get { return GetAttribute(ATTR_OPEN) != null; }
		set
		{
			if (value)
				SetAttribute(ATTR_OPEN, string.Empty);
			else
				RemoveAttribute(ATTR_OPEN);
		}
	}

	public HtmlDetails() { }
	public HtmlDetails(params HtmlElement[] children) : base(children) { }
	public HtmlDetails(IEnumerable<HtmlElement> children) : base(children) { }
}

class HtmlSummary : HtmlTag
{
	protected override string Tag => "summary";

	public HtmlSummary() { }
	public HtmlSummary(params HtmlElement[] children) : base(children) { }
	public HtmlSummary(IEnumerable<HtmlElement> children) : base(children) { }
}


class HtmlDescriptionList : HtmlTag
{
	protected override string Tag => "dl";

	public HtmlDescriptionList() { }
	public HtmlDescriptionList(params HtmlElement[] children) : base(children) { }
}

class HtmlDescriptionTerm : HtmlTag
{
	protected override string Tag => "dt";

	public HtmlDescriptionTerm() { }
	public HtmlDescriptionTerm(params HtmlElement[] children) : base(children) { }
}

class HtmlDescriptionDetail : HtmlTag
{
	protected override string Tag => "dd";

	public HtmlDescriptionDetail() { }
	public HtmlDescriptionDetail(params HtmlElement[] children) : base(children) { }
}
