﻿using System.Text;

namespace WSTest.Helpers
{
	internal static class MappingHelper
	{
		public static string InterpolateStringWithMappings(Dictionary<string, ProtectableValue> mappings, string text)
		{
			const char STX = '{';
			const char ETX = '}';

			StringBuilder output = new();

			int len = text.Length;
			int start = 0;

			for (; start < len;)
			{
				int indexBegin = text.IndexOf(STX, start);
				if (indexBegin < 0 || indexBegin + 1 >= len)
					break;

				output.Append(text[start..indexBegin]);

				if (text[indexBegin + 1] == STX)
				{
					start = indexBegin + 2;
					output.Append(STX);
				}
				else
				{
					++indexBegin;
					int indexEnd = text.IndexOf(ETX, indexBegin);

					if (indexEnd < 0)
						throw new Exception($"TransformMapping: invalid value '{text}' - missing '{ETX}'");

					int lkey = indexEnd - indexBegin;
					if (lkey == 0)
						throw new Exception($"TransformMapping: invalid value '{text}' - empty parameter");

					string key = text.Substring(indexBegin, lkey);

					if (mappings.TryGetValue(key, out var map))
					{
						output.Append(map.Value);
					}
					else
						throw new Exception($"TransformMapping: key  '{key}' not found in property mappings of service configuration");

					start = indexEnd + 1;
				}
			}

			if (start < len)
				output.Append(text[start..]);


			return output.ToString();
		}
	}
}
