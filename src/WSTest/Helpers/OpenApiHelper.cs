﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Interfaces;
using Microsoft.OpenApi.Models;

namespace WSTest.Helpers;

enum OpenApiPrimitiveTypes
{
	Boolean,
	Integer,
	Number,
	String
}

static class OpenApiHelper
{
	public static OpenApiPrimitiveTypes ParsePrimitiveType(string str)
	{
		return str switch
		{
			"string" => OpenApiPrimitiveTypes.String,
			"integer" => OpenApiPrimitiveTypes.Integer,
			"number" => OpenApiPrimitiveTypes.Number,
			"boolean" => OpenApiPrimitiveTypes.Boolean,
			_ => throw new ArgumentException($"Unexpected primitive type'{str}'")
		};
	}

	public static string? GetXName(IDictionary<string, IOpenApiExtension> extensions)
	{
		if (extensions == null)
			return null;

		var kv = extensions.FirstOrDefault(p => p.Key == "x-name");
		if (kv.Key == null)
			return null;

		return (kv.Value as OpenApiString)?.Value;
	}

	public static System.Net.Http.HttpMethod ConvertOperationTypeToHttpMethod(OperationType operationType)
	{
		return new System.Net.Http.HttpMethod(operationType.ToString());
	}
}
