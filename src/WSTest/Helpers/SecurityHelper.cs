﻿using System.Text;

namespace WSTest.Helpers
{
	internal class SecurityHelper
	{
		static readonly byte[] AdditionalEntropy = [0x52, 0xB7, 0x67, 0x13, 0xF9];

		public static byte[] Encrypt(string data, bool userScope = false)
		{
			if (string.IsNullOrEmpty(data))
				return [];

			return System.Security.Cryptography.ProtectedData.Protect(Encoding.UTF8.GetBytes(data), AdditionalEntropy, GetDataProtectionScope(userScope));
		}

		public static string Decrypt(byte[] data, bool userScope = false)
		{
			if (data == null || data.Length == 0)
				return string.Empty;

			return Encoding.UTF8.GetString(System.Security.Cryptography.ProtectedData.Unprotect(data, AdditionalEntropy, GetDataProtectionScope(userScope)));
		}

		static System.Security.Cryptography.DataProtectionScope GetDataProtectionScope(bool userScope) => userScope ? System.Security.Cryptography.DataProtectionScope.CurrentUser : System.Security.Cryptography.DataProtectionScope.LocalMachine;
	}
}
