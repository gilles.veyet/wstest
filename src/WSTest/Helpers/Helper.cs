﻿using System.Diagnostics;
using System.Text.RegularExpressions;

namespace WSTest.Helpers;


public static partial class Helper
{

	public static bool IsUrlValid(string source) => Uri.TryCreate(source, UriKind.Absolute, out Uri? uriResult) && (uriResult?.Scheme == Uri.UriSchemeHttps || uriResult?.Scheme == Uri.UriSchemeHttp);

	public static bool IsDirectoryInside(string child, string parent) => NormalizeDirectory(child).StartsWith(NormalizeDirectory(parent), StringComparison.OrdinalIgnoreCase);

	public static string NormalizeDirectory(string directory) => directory.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar).TrimEnd(Path.DirectorySeparatorChar);

	public static string NormalizeFilePath(string filePath) => filePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

	[GeneratedRegex("\\s+")]
	private static partial Regex RegExWhitespace();

	public static string RemoveWhitespace(string input) => RegExWhitespace().Replace(input, string.Empty);

	/// <summary>
	/// Sanitize a filename: replace invalid characters by "_".
	/// This method can only be used on a file name (not a full path as it will replace the "\" in the path by "_").
	/// </summary>
	/// <param name="filename">The filename to check.</param>
	/// <returns>the safe filename.</returns>
	public static string MakeValidFileName(string filename)
	{
		return string.Join("_", filename.Split(Path.GetInvalidFileNameChars(), StringSplitOptions.RemoveEmptyEntries));   // replace invalid chars by "_"
	}

	public static string MakeTempFile(string extension)
	{
		return Path.GetTempFileName() + extension;
	}

	public static void CleanDirectory(string directory)
	{
		if (!Directory.Exists(directory))
			return;

		foreach (var dir in Directory.GetDirectories(directory))
			Directory.Delete(dir, true);

		foreach (var file in Directory.GetFiles(directory))
			File.Delete(file);
	}

	// Unused 
	//public static string TextToHtml(string text)
	//{
	//	// from https://stackoverflow.com/questions/3991840/simple-text-to-html-conversion/16722351
	//	return "<pre>" + HttpUtility.HtmlEncode(text) + "</pre>";
	//}

	public static void ShowInBrowser(string url)
	{
		var psi = new ProcessStartInfo(url)
		{
			UseShellExecute = true
		};

		Process.Start(psi);
	}

	public static void ShowInExplorer(string path)
	{
		// from https://stackoverflow.com/questions/334630/opening-a-folder-in-explorer-and-selecting-a-file
		var psi = new ProcessStartInfo("explorer")
		{
			Arguments = $"/e, /select, \"{Path.GetFullPath(path)}\""
		};

		Process.Start(psi);
	}

}
