﻿using System.Text.Json.Nodes;

namespace WSTest.Helpers;

internal static class JsonHelper
{
	public static JsonNode? ConvertToJsonNode<T>(T value)
	{
		if (value == null)
			return null;

		return value switch
		{
			bool boolValue => (JsonNode)boolValue,
			string stringValue => (JsonNode)stringValue!,
			long longValue => (JsonNode)longValue,
			decimal decimalValue => (JsonNode)decimalValue,
			DateTime dateTimeValue => (JsonNode)XmlHelper.Serialize(dateTimeValue)!,    // convert to string. (JsonNode)dateTimeValue works but it has annoying effect that enclosing ["] are added when converting resulting JSonNode to string.
			DateOnly dateOnly => (JsonNode)XmlHelper.Serialize(dateOnly)!,              // idem 
			TimeOnly timeOnly => (JsonNode)XmlHelper.Serialize(timeOnly)!,              // idem
			_ => throw new Exception($"Cannot convert type '{typeof(T)}' to JsonNode")
		};
	}

	public static string PrettyPrintJson(string json)
	{
		if (string.IsNullOrEmpty(json))
			return json;

		var first = json[0];

		if (!(first == '{' || first == '['))
			return json;

		try
		{
			return Newtonsoft.Json.Linq.JToken.Parse(json).ToString();
		}
		catch
		{
			return json;
		}
	}

}
