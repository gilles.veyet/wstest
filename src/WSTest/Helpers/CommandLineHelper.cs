﻿namespace WSTest.Helpers
{
	internal static class CommandLineHelper
	{
		/// <summary>
		/// Get string value from command line arguments with specified key.
		/// Separators are ':' and '='
		/// For example
		///     Key=Value
		///     Key:Value
		/// </summary>
		/// <param name="key">Key (case insensitive).</param>
		/// <returns>String value or null if not found</returns>
		public static string? GetStringValueWithKey(string key)
		{
			if (string.IsNullOrWhiteSpace(key))
				return null;

			foreach (string arg in Environment.GetCommandLineArgs())
			{
				if (!arg.StartsWith(key, StringComparison.OrdinalIgnoreCase))
					continue;

				if (arg.Length == key.Length)
					return string.Empty;                        // key match, not followed by separator => return empty string.

				char sep = arg[key.Length];

				if (!(sep == ':' || sep == '='))
					continue;                                   // next char is not a separator => no match (continue).

				return arg[(key.Length + 1)..];
			}

			return null;    // key not found 
		}

		/// <summary>
		/// Check if a key is present in command line.
		/// </summary>
		/// <param name="key">Key (case insensitive).</param>
		public static bool IsKeyPresent(string key)
		{
			return GetStringValueWithKey(key) != null;
		}

	}
}
