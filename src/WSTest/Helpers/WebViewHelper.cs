﻿using Microsoft.Web.WebView2.Core;
using Microsoft.Web.WebView2.WinForms;

namespace WSTest.Helpers
{
	internal static class WebViewHelper
	{
		static CoreWebView2Environment? WebView2Environment;

		public static async Task<CoreWebView2Environment?> GetCoreWebView2Environment()
		{
			if (WebView2Environment == null)
			{
				try
				{
					// Use --allow-file-access-from-files to allow web worker on local files.
					//   - if not specified then monaco-editor shows this warning: Could not create web worker(s). Falling back to loading web worker code in main thread, which might cause UI freezes. Please see https://github.com/microsoft/monaco-editor#faq
					// See https://learn.microsoft.com/en-us/microsoft-edge/webview2/concepts/working-with-local-content?tabs=dotnetcsharp#loading-local-content-by-navigating-to-a-file-url
					// Full list of flags: see https://learn.microsoft.com/en-us/microsoft-edge/webview2/concepts/webview-features-flags?tabs=dotnetcsharp
					//   - flags must be prefixed by "--" 
					WebView2Environment = await CoreWebView2Environment.CreateAsync(null, Program.DirectoryTemp, new CoreWebView2EnvironmentOptions("--allow-file-access-from-files"));
				}
				catch (Exception ex)
				{
					TraceHelper.WriteException($"{nameof(GetCoreWebView2Environment)} failed", ex);
					AskDownloadWebViewRuntime();
					return null;
				}
			}

			return WebView2Environment;
		}

		public static async Task InitWebViewAsync(WebView2 webview)
		{
			var env = await GetCoreWebView2Environment() ?? throw new Exception($"{nameof(InitWebViewAsync)}: {nameof(CoreWebView2Environment)} is not initialized");
			await webview.EnsureCoreWebView2Async(env);
		}

		private static void AskDownloadWebViewRuntime()
		{
			string url = "https://go.microsoft.com/fwlink/p/?LinkId=2124703";

			if (MessageBox.Show($"Program cannot start because WebView runtime is not installed.\r\nDo you want to download it now from {url}", Program.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				Helper.ShowInBrowser(url);
			}
		}
	}
}
