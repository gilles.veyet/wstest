﻿using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using WSTest.Extensions;

namespace WSTest.Helpers
{
	internal static class XmlHelper
	{
		const string CUSTOM_NS_PREFIX = "ns";
		public const string SoapEnvelopeUri = "http://schemas.xmlsoap.org/soap/envelope/";

		public static readonly XName XNameTypeAttribute = (XNamespace)XmlSchema.InstanceNamespace + "type";
		public static readonly XmlQualifiedName AnyType = new("anyType", XmlSchema.Namespace);
		public static readonly XmlQualifiedName AnySimpleType = new("anySimpleType", XmlSchema.Namespace);
		public static bool IsAnyOrSimpleType(XmlQualifiedName? name) => name == AnyType || name == AnySimpleType;

		public static XName ToXName(XmlQualifiedName qualifiedName) => (XNamespace)qualifiedName.Namespace + qualifiedName.Name;

		public static string MakeQualifiedNameWithPrefix(XElement elem, XmlQualifiedName qualifiedName)
		{
			string ns = qualifiedName.Namespace;
			string? prefix = elem.GetPrefixOfNamespace(ns);

			if (string.IsNullOrEmpty(prefix))
			{
				XElement root = elem.Document!.Root!;
				int max = root.Attributes().Where(x => x.IsNamespaceDeclaration && x.Name.LocalName.StartsWith(CUSTOM_NS_PREFIX)).Select(x => int.Parse(x.Name.LocalName[CUSTOM_NS_PREFIX.Length..])).DefaultIfEmpty(0).Max();
				prefix = CUSTOM_NS_PREFIX + (max + 1).ToString();
				root.SetAttributeValue(XNamespace.Xmlns + prefix, ns);

				Debug.Assert(elem.GetPrefixOfNamespace(ns) == prefix, $"{MethodBase.GetCurrentMethod()}: prefix not found for added namespace");
			}

			return prefix + ':' + qualifiedName.Name;
		}

		public static string Serialize<T>(T value) where T : notnull
		{
			return value switch
			{
				bool boolValue => XmlConvert.ToString(boolValue),
				string stringValue => stringValue ?? string.Empty,
				long longValue => XmlConvert.ToString(longValue),
				decimal decimalValue => XmlConvert.ToString(decimalValue),
				DateTime dateTimeValue => XmlConvert.ToString(dateTimeValue, XmlDateTimeSerializationMode.Local),
				TimeSpan timeSpanValue => XmlConvert.ToString(timeSpanValue),
				//TimeOnly timeOnlyValue => XmlConvert.ToString(timeOnlyValue),	// XmlConvert does not handle TimeOnly
				//DateOnly dateOnlyValue => XmlConvert.ToString(dateOnlyValue), // XmlConvert does not handle DateOnly
				TimeOnly timeOnlyValue => timeOnlyValue.ToString("HH:mm:ss", CultureInfo.InvariantCulture),
				DateOnly dateOnlyValue => dateOnlyValue.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
				_ => throw new Exception($"{nameof(Serialize)}: Cannot convert type '{typeof(T)}' to XML value")
			};
		}


		public static T? ParseString<T>(string text) where T : struct
		{
			T dummy = default;

			try
			{
				return dummy switch
				{
					bool => (T)(object)XmlConvert.ToBoolean(text),
					long => (T)(object)XmlConvert.ToInt64(text),
					decimal => (T)(object)XmlConvert.ToDecimal(text),
					DateTime => (T)(object)XmlConvert.ToDateTime(text, XmlDateTimeSerializationMode.Local),
					TimeSpan => (T)(object)XmlConvert.ToTimeSpan(text),
					TimeOnly => (T)(object)TimeOnly.Parse(text, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.NoCurrentDateDefault),
					DateOnly => (T)(object)DateOnly.Parse(text, CultureInfo.InvariantCulture),
					_ => throw new ArgumentException($"{nameof(ParseString)}: Cannot parse text '{text}' to type '{typeof(T)}'"),
				};
			}
			catch (FormatException)
			{
				return null;
			}
		}

		// Set encoding to utf-8 otherwise it is wrong in xml declaration 
		//    Before: <?xml version="1.0" encoding="utf-16"?>
		//    After: <?xml version="1.0" encoding="utf-8"?>
		// From https://www.csharp411.com/how-to-force-xmlwriter-or-xmltextwriter-to-use-encoding-other-than-utf-16/
		class StringWriterUtf8(StringBuilder sb) : StringWriter(sb)
		{
			public override Encoding Encoding => Encoding.UTF8;
		}

		public static string GetXDocumentContent(XDocument xDoc)
		{
			StringBuilder sb = new();

			using (var xmlWriter = new StringWriterUtf8(sb))
			{
				xDoc.Save(xmlWriter, SaveOptions.None);
			}

			return sb.ToString();
		}

		public static string PrettyPrintXml(string xml)
		{
			if (string.IsNullOrEmpty(xml))
				return xml;

			try
			{
				var xDoc = XDocument.Parse(xml);
				return xDoc.ToString(); // same output as GetXDocumentContent but without xml declaration (not useful for pretty print).
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"{nameof(PrettyPrintXml)}:Parse error", ex);
				return xml;
			}
		}

		public static XmlQualifiedName MakeXmlQualifiedName(XElement elem, string nameWithNamespace)
		{
			var arr = nameWithNamespace.Split(':');
			if (arr.Length != 2)
				return new XmlQualifiedName(arr[0], elem.GetDefaultNamespace().NamespaceName);

			string prefix = arr[0];
			string name = arr[1];

			var ns = elem.GetNamespaceOfPrefix(prefix) ?? throw new Exception($"{nameof(MakeXmlQualifiedName)}: Prefix '{prefix}' not found for '{nameWithNamespace}' in '{elem.Document?.ToString()}'");
			var xn = ns + name;
			return xn.ToXmlQualifiedName();
		}
	}
}
