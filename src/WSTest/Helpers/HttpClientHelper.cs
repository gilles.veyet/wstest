﻿using System.Net;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;

namespace WSTest.Helpers;

public class HttpClientSecurityOptions
{
	public string? BasicAuthentication { get; init; }

	public static HttpClientSecurityOptions None => new();
}

public class HttpClientOptions
{
	public Dictionary<string, string> Headers = [];
	public HttpClientSecurityOptions Security { get; init; } = HttpClientSecurityOptions.None;

	public string ContentType { get; init; } = MediaTypeNames.Text.Plain;
	public string? Accept { get; init; }

	public static HttpClientOptions None => new();
}

public enum MediaTypes
{
	Unknown,
	Xml,
	Json,
	Text,
	Html,
	RichText,
	Image,
	PDF,
	Binary,
	Zip,
}

public enum ImageMediaType
{
	Bmp,
	Jpeg,
	Png,
	Tiff,
}

internal record HttpSendData
{
	public required HttpMethod Method { get; init; }
	public required string Url { get; init; }
	public required HttpClientOptions Options { get; init; }
	public string? BodyContent { get; init; }

	public override string ToString()
	{
		return $"{Method} '{Url}' Body.Length:{BodyContent?.Length}";
	}
}


public record HttpResult
{
	public bool Success { get; init; } = false;
	public Exception? Exception { get; init; }
	public HttpStatusCode? HttpStatus { get; init; }
	public byte[]? Response { get; init; }
	public string? ResponseCharSet { get; init; }
	public MediaTypes ResponseMediaType { get; init; }
	public ImageMediaType? ResponseImageType { get; init; }

	[Newtonsoft.Json.JsonIgnore]
	public bool IsResponsePresent => Response?.Length > 0;

	[Newtonsoft.Json.JsonIgnore]
	public bool IsResponseText => ResponseMediaType switch { MediaTypes.Json or MediaTypes.Xml or MediaTypes.Text or Helpers.MediaTypes.RichText or MediaTypes.Html => true, _ => false };

	public Encoding GetResponseEncoding()
	{
		if (string.IsNullOrEmpty(ResponseCharSet))
			return Encoding.UTF8;

		try
		{
			return Encoding.GetEncoding(ResponseCharSet);
		}
		catch
		{
			return Encoding.UTF8;
		}
	}

	[Newtonsoft.Json.JsonIgnore]
	public string ResponseAsString => IsResponseText && Response != null ? GetResponseEncoding().GetString(Response) : String.Empty;

	[Newtonsoft.Json.JsonIgnore]
	public string PrettyResponse => ResponseMediaType switch { MediaTypes.Json => JsonHelper.PrettyPrintJson(ResponseAsString), MediaTypes.Xml => XmlHelper.PrettyPrintXml(ResponseAsString), _ => ResponseAsString };

	[Newtonsoft.Json.JsonIgnore]
	public string ErrorResult => Success ? string.Empty : (HttpStatus != null ? $"Status:{HttpStatus} - " : "") + Exception?.Message ?? ResponseAsString;

	[Newtonsoft.Json.JsonIgnore]
	public string ExceptionMessage
	{
		get
		{
			if (Exception == null)
				return string.Empty;

			string message = Exception.Message;

			if (Exception.InnerException != null)
				message += $" - {Exception.InnerException.Message}";

			return message;
		}
	}

	public override string ToString()
	{
		return $"{(Success ? "" : $"Error '{ExceptionMessage}'")} Status:'{HttpStatus}' Response.Length:{Response?.Length}  ResponseCharSet:'{ResponseCharSet}' ResponseMediaType:{ResponseMediaType} ResponseImageType:'{ResponseImageType}'";
	}
}

static class HttpClientHelper
{
	private static System.Net.Http.HttpClient? _httpClient;

	private static System.Net.Http.HttpClient _HttpClient
	{
		get
		{
			var timeout = TimeSpan.FromSeconds(Program.Settings.RequestTimeout);

			//Timeout cannot be changed after 1st request has been made so dispose 
			if (_httpClient != null && _httpClient.Timeout != timeout)
			{
				_httpClient.Dispose();
				_httpClient = null;
			}

			if (_httpClient == null)
			{
				var handler = new HttpClientHandler
				{
					//default value is already manual - ClientCertificateOptions = ClientCertificateOption.Manual,

					// uncomment to allow invalid SSL certificates.
					//ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) =>
					//{
					//	return true;
					//}
				};

				_httpClient = new(handler)
				{
					Timeout = timeout
				};
			}

			return _httpClient;
		}
	}

	public static bool CanMethodHaveBody(HttpMethod method)
	{
		return method == HttpMethod.Post || method == HttpMethod.Put || method == HttpMethod.Patch;
	}

	public async static Task<HttpResult> SendAsync(HttpSendData sendData, CancellationToken cancellationToken)
	{
		var client = _HttpClient;

		var httpClientOptions = sendData.Options ?? new HttpClientOptions();

		var securityOptions = httpClientOptions.Security ?? HttpClientSecurityOptions.None;

		using var request = new HttpRequestMessage(sendData.Method, sendData.Url);
		if (!string.IsNullOrEmpty(sendData.BodyContent))
		{
			request.Content = new StringContent(sendData.BodyContent, Encoding.UTF8, httpClientOptions.ContentType);
		}

		if (!string.IsNullOrEmpty(httpClientOptions.Accept))
			request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(httpClientOptions.Accept));

		foreach (var kv in httpClientOptions.Headers)
			request.Headers.Add(kv.Key, kv.Value);

		if (!string.IsNullOrEmpty(securityOptions.BasicAuthentication))
		{
			// use UTF-8 encoding for basic authentication.
			// see https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#character_encoding_of_http_authentication
			// and https://httpwg.org/specs/rfc7617.html#charset
			var authToken = Encoding.UTF8.GetBytes(securityOptions.BasicAuthentication);
			request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(authToken));
		}

		HttpResponseMessage? response = null;

		try
		{
			// We can use ConfigureAwait(false) as code below await does not need to run in caller (main) thread. See https://devblogs.microsoft.com/dotnet/configureawait-faq/
			response = await client.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken).ConfigureAwait(false);
			var headers = response.Content.Headers;
			var contentType = headers.ContentType;
			(var mediaType, var imageType) = GetResponseMediaType(contentType);

			return new HttpResult() { Success = response.IsSuccessStatusCode, HttpStatus = response.StatusCode, Response = await response.Content.ReadAsByteArrayAsync(cancellationToken), ResponseCharSet = contentType?.CharSet, ResponseMediaType = mediaType, ResponseImageType = imageType };
		}
		catch (HttpRequestException ex)
		{
			return new HttpResult() { Exception = ex, HttpStatus = ex.StatusCode };
		}
		catch (Exception ex)
		{
			return new HttpResult() { Exception = ex };
		}
		finally
		{
			response?.Dispose();
		}
	}

	static (MediaTypes, ImageMediaType?) GetResponseMediaType(MediaTypeHeaderValue? value)
	{
		// See https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
		// Some samples from real Web Services
		//	"application/json"					SKIDATA DTA
		//	"text/xml"							SKIDATA DTA
		//	"application/vnd.restful+json"		SKIDATA Summit.Logic

		if (string.IsNullOrEmpty(value?.MediaType))
			return (MediaTypes.Unknown, null);

		var parts = value.MediaType.Split('/');

		if (parts.Length < 2)
			return (MediaTypes.Unknown, null);

		string main = parts[0];
		string sub = parts[1];

		return main switch
		{
			"text" => (GetTextMediaType(sub), null),
			"application" => (GetApplicationMediaType(sub), null),
			"image" => (MediaTypes.Image, GetImageMediaType(sub)),
			_ => (MediaTypes.Unknown, null),
		};
	}

	static MediaTypes GetTextMediaType(string sub)
	{
		return sub switch
		{
			"html" => MediaTypes.Html,
			"xml" => MediaTypes.Xml,
			"richtext" => MediaTypes.RichText,
			_ => MediaTypes.Text
		};
	}

	static ImageMediaType? GetImageMediaType(string sub)
	{
		return Enum.TryParse<ImageMediaType>(sub, out var imageType) ? imageType : null;
	}

	static MediaTypes GetApplicationMediaType(string sub)
	{
		foreach (var typ in sub.Split('+'))
		{
			switch (typ)
			{
				case "xml":
					return MediaTypes.Xml;

				case "json":
					return MediaTypes.Json;

				case "x-yaml":
				case "yaml":
					return MediaTypes.Text;

				case "rtf":
					return MediaTypes.RichText;

				case "pdf":
					return MediaTypes.PDF;

				case "zip":
					return MediaTypes.Zip;

				case "octet-stream":
					return MediaTypes.Binary;
			}
		}

		return MediaTypes.Unknown;
	}

}
