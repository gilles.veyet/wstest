﻿using Newtonsoft.Json;

namespace WSTest.Helpers
{
	public class JSonXmlQualifiedNameConverter : JsonConverter<System.Xml.XmlQualifiedName>
	{
		public override void WriteJson(JsonWriter writer, System.Xml.XmlQualifiedName? value, JsonSerializer serializer)
		{
			if (value != null)
				writer.WriteValue(value.ToString());
		}

		public override System.Xml.XmlQualifiedName ReadJson(JsonReader reader, Type objectType, System.Xml.XmlQualifiedName? existingValue, bool hasExistingValue, JsonSerializer serializer)
		{
			var sval = reader.Value as string;

			if (!string.IsNullOrEmpty(sval))
			{
				int index = sval.LastIndexOf(':');
				return index >= 0 ? new System.Xml.XmlQualifiedName(sval[(index + 1)..], sval[..index]) : new System.Xml.XmlQualifiedName(sval);
			}
			else
				return System.Xml.XmlQualifiedName.Empty;
		}
	}

}
