﻿
using System.Xml;
using System.Xml.Linq;
using WSTest.Helpers;

namespace WSTest
{
	[method: Newtonsoft.Json.JsonConstructor]
	class SoapService(SoapServiceDescriptor descriptor) : HttpService
	{
		public override SoapServiceDescriptor Descriptor { get; } = descriptor;
		public override List<SoapMethod> Methods { get; } = [];

		public override LoadSoapServiceResult Load(string serviceDefinitionContentDirectory)
		{
			var loader = new SoapServiceLoader(this, serviceDefinitionContentDirectory);
			var result = loader.Load();
			Methods.Sort();

			return result;
		}

		public override SoapServiceRunner CreateRunner() => new(this);

		public override List<Value> GetValueFromContent(HttpServiceMethod method, string content, string? url)
		{
			var value = ValueFactory.MakeValueFromProperty(method.Request!);

			using var reader = XmlReader.Create(new StringReader(content));
			var namespaceManager = new XmlNamespaceManager(reader.NameTable);
			namespaceManager.AddNamespace("soap", XmlHelper.SoapEnvelopeUri);
			var xdoc = XDocument.Load(reader);

			if (xdoc.Root == null)
				throw new Exception($"Invalid XML content");

			var body = xdoc.Root.Element((XNamespace)XmlHelper.SoapEnvelopeUri + "Body");
			var element = body != null ? body.Elements().First() : xdoc.Root;
			value.ParseXml(element);
			return [value];
		}
	}
}

