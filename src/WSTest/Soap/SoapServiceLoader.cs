﻿using System.Data;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.XPath;
using WSTest.Extensions;
using WSTest.Helpers;

#pragma warning disable IDE0130 // Namespace does not match folder structure
namespace WSTest;
#pragma warning restore IDE0130 // Namespace does not match folder structure

internal record LoadSoapServiceResult : LoadServiceResult
{
	public string? SoapAddressLocation { get; init; }
}

internal class SoapServiceLoader
{
	private readonly SoapService _Service;

	private string WsdlPath { get; set; }

	// suppress CS8618 (Non-nullable field must contain a non-null value when exiting constructor) using null! for properties that are set in Load().
	private XDocument XDoc = null!;
	private XmlNamespaceManager NamespaceManager = null!;
	private XmlSchemaSet SchemaSet = null!;
	private Dictionary<string, ActualPropertyType> ActualTypes = [];
	private Dictionary<string, ReferencePropertyType> ReferenceTypes = [];

	private enum MessageType
	{
		Input,
		Output,
	}

	public int ErrorCount { get; private set; } = 0;

	public SoapServiceLoader(SoapService service, string serviceDefinitionContentDirectory)
	{
		_Service = service;
		WsdlPath = Path.Combine(serviceDefinitionContentDirectory, _Service.Descriptor.ServiceDefinitionPath);
	}

	public LoadSoapServiceResult Load()
	{
		using (var reader = XmlReader.Create(WsdlPath))
		{
			NamespaceManager = new XmlNamespaceManager(reader.NameTable);
			NamespaceManager.AddNamespace("wsdl", "http://schemas.xmlsoap.org/wsdl/");
			NamespaceManager.AddNamespace("soap", "http://schemas.xmlsoap.org/wsdl/soap/");
			NamespaceManager.AddNamespace("xsd", XmlSchema.Namespace);

			XDoc = XDocument.Load(reader);
		}

		var result = new LoadSoapServiceResult() { SoapAddressLocation = GetSoapAddressLocation() };
		result.DefinitionFiles.Add(WsdlPath);

		LoadSchemas().ForEach(schema => result.DefinitionFiles.Add(schema));
		LoadDerivedTypeNames();

		HandleSoapAnyOfTypes();
		LoadMethods();

		return result;
	}

	/// <summary>
	/// LoadDerivedTypeNames
	/// This method must only be called once after service has been loaded (otherwise same derived types would be added several times).
	/// </summary>
	public void LoadDerivedTypeNames()
	{
		var service = _Service;
		Dictionary<XmlQualifiedName, PropertyType> dictPropertyTypes = [];

		foreach (var ptype in ActualTypes.Values.Where(p => p.BaseTypeName != null))
		{
			if (service.Descriptor.ExcludeDerivedTypes.Count > 0 && ptype.QualifiedName != null && service.Descriptor.ExcludeDerivedTypes.Contains(ptype.QualifiedName))
			{
				TraceHelper.WriteInfo($"{nameof(LoadDerivedTypeNames)}: Skip '{ptype.QualifiedName}' (excluded from descriptor)");
				continue;
			}

			if (ptype.QualifiedName == null)
			{
				TraceHelper.WriteWarning($"Found a property type which has a base type '{ptype.BaseTypeName}' but has no name! service:{service}");
				return;
			}

			var baseType = ActualTypes[ptype.BaseTypeName!.ToString()];
			baseType.AddDerivedTypeName(ptype.QualifiedName);
		}
	}

	List<string> LoadSchemas()
	{
		SchemaImporter schemaImporter = new();

		var schemas = XDoc.XPathSelectElements("/wsdl:definitions/wsdl:types/xsd:schema", NamespaceManager);
		foreach (var schema in schemas!)
		{
			string content = schema.InnerXml();
			if (!string.IsNullOrWhiteSpace(content))
				schemaImporter.ImportWithContent(content, WsdlPath);
		}

		this.SchemaSet = schemaImporter.GetSchemaSet();

		TraceHelper.WriteVerbose($"Load {WsdlPath} =>  Schemas:{SchemaSet.Schemas().Count} GlobalElements:{SchemaSet.GlobalElements.Count} GlobalTypes:{SchemaSet.GlobalTypes.Count} GlobalAttributes:{SchemaSet.GlobalAttributes.Count}");

		var globalElements = SchemaSet.GlobalElements.Values.OfType<XmlSchemaElement>();
		var globalTypes = SchemaSet.GlobalTypes.Values.OfType<XmlSchemaType>().Where(t => !t.QualifiedName.IsEmpty && t.QualifiedName.Namespace != XmlSchema.Namespace);


		// globalTypes.Count() is different from  SchemaSet.GlobalTypes.Count because we skip global type Type:XmlSchemaComplexType QualifiedName:'http://www.w3.org/2001/XMLSchema:anyType'
		//if (globalTypes.Count() != SchemaSet.GlobalTypes.Count)
		//	TraceHelper.WriteWarning($"File {WsdlPath} GlobalTypes count:{SchemaSet.GlobalTypes.Count} greater than XmlSchemaType element count {globalTypes.Count()}");
		//foreach (var schema in SchemaSet.GlobalTypes.Values.OfType<XmlSchemaType>().Where(t => !(!t.QualifiedName.IsEmpty && t.QualifiedName.Namespace != XmlSchema.Namespace)))
		//{
		//	TraceHelper.WriteWarning($"File {WsdlPath} Skip global type {XmlSchemaTypeToString(schema)}");
		//}

		ReferenceTypes = globalTypes.ToDictionary(t => t.QualifiedName.ToString(), t => new ReferencePropertyType(t.QualifiedName));

		if (globalElements.Count() != SchemaSet.GlobalElements.Count)
			TraceHelper.WriteWarning($"File {WsdlPath} GlobalElements count:{SchemaSet.GlobalElements.Count} greater than XmlSchemaElement element count {globalElements.Count()}");

		if (TraceHelper.IsEnabled(System.Diagnostics.TraceLevel.Verbose))
		{
			foreach (var elem in globalElements)
				TraceHelper.WriteVerbose(XmlSchemaElementToString(elem));
		}

		TraceHelper.WriteVerbose($"Load GlobalTypes:{SchemaSet.GlobalTypes.Count}");
		ActualTypes = [];

		foreach (var schemaType in globalTypes)
		{
			var ptype = MakeActualType(schemaType);
			ActualTypes[ptype.QualifiedName!.ToString()] = ptype;
			//_Service.AddPropertyTypeRef(ptype);
			TraceHelper.WriteVerbose($"Loaded type {ptype.QualifiedName} : {XmlSchemaTypeToString(schemaType)}");
		}

		_Service.ActualTypes = ActualTypes;
		_Service.ReferenceTypes = [.. ReferenceTypes.Values];
		_Service.UpdateActualTypes();

		return [.. schemaImporter.SchemaPaths];
	}

	void HandleSoapAnyOfTypes()
	{
		IEnumerable<ActualPropertyType> GetDescendantTypes(ActualPropertyType ptype)
		{
			if (ptype.DerivedTypeNames == null)
				return [];

			return ptype.DerivedTypeNames.Select(t => ActualTypes[t.ToString()]);
		}

		IEnumerable<ActualPropertyType> GetAllDescendantTypes(ActualPropertyType ptype)
		{
			var types = GetDescendantTypes(ptype);
			return types.Concat(types.SelectMany(t => GetDescendantTypes(t)));
		}


		foreach (var ptype in ActualTypes.Values.Where(p => p.DerivedTypeNames != null))
		{
			var ptypes = GetAllDescendantTypes(ptype).Where(t => !t.IsAbstract).ToList();

			if (!ptype.IsAbstract)
				ptypes.Add(ptype);

			if (ptypes.Count > 0)
			{
				ptype.SoapAnyOfPropertyTypes = ptypes.Select(p => p with { SoapAnyOfPropertyTypes = null }).ToList();
			}
			else if (ptype.IsAbstract)
			{
				TraceHelper.WriteError($"Error property type '{ptype.QualifiedName}' is abstract but no non-abstract derived types were found in schemas. WsdlPath:{WsdlPath}");
			}
		}
	}

	static string XmlSchemaElementToString(in XmlSchemaElement type)
	{
		var sb = new StringBuilder($"Type:{type.GetType().Name}");

		if (!string.IsNullOrEmpty(type.Name))
			sb.Append($" Name:'{type.Name}'");

		if (!type.QualifiedName.IsEmpty)
			sb.Append($" QualifiedName:'{type.QualifiedName}'");

		if (!type.SchemaTypeName.IsEmpty)
			sb.Append($" SchemaTypeName:{type.SchemaTypeName}");

		if (!type.RefName.IsEmpty)
			sb.Append($" RefName:{type.RefName}");

		if (type.Namespaces.Count > 0)
			sb.Append($" Namespaces:{string.Join(", ", type.Namespaces)}");

		sb.Append($" IsAbstract:{type.IsAbstract}  IsNillable:{type.IsNillable}  IsArray:{type.MaxOccurs > 1}  MinOccurs:{type.MinOccurs}  SourceUri:'{type.SourceUri}' LineNumber:{type.LineNumber}");
		return sb.ToString();
	}

	static string XmlSchemaTypeToString(in XmlSchemaType type)
	{
		var sb = new StringBuilder($"Type:{type.GetType().Name}");

		if (!string.IsNullOrEmpty(type.Name))
			sb.Append($" Name:'{type.Name}'");

		if (!type.QualifiedName.IsEmpty)
			sb.Append($" QualifiedName:'{type.QualifiedName}'");

		if (type.Datatype != null)
			sb.Append($" Datatype:{type.Datatype}");

		if (type.TypeCode != XmlTypeCode.None)
			sb.Append($" TypeCode:{type.TypeCode}");

		if (type.DerivedBy != XmlSchemaDerivationMethod.None)
			sb.Append($" DerivedBy:{type.DerivedBy}");

		if (!XmlHelper.IsAnyOrSimpleType(type.BaseXmlSchemaType?.QualifiedName))
			sb.Append($" BaseXmlSchemaType:'{type.BaseXmlSchemaType?.QualifiedName}'");

		if (type.Namespaces.Count > 0)
			sb.Append($" Namespaces:{string.Join(", ", type.Namespaces)}");

		sb.Append($" IsAbstract:{(type as XmlSchemaComplexType)?.IsAbstract ?? false} IsArray:{IsArray(type)} SourceUri:'{type.SourceUri}' LineNumber:{type.LineNumber}");
		return sb.ToString();
	}

	static string XmlSchemaObjectToString(in XmlSchemaObject type)
	{
		var sb = new StringBuilder($"Type:{type.GetType().Name}");

		if (type.Namespaces.Count > 0)
			sb.Append($" Namespaces:{string.Join(", ", type.Namespaces)}");

		sb.Append($" SourceUri:'{type.SourceUri}' LineNumber:{type.LineNumber}");
		return sb.ToString();
	}

	void LoadMethods()
	{
		var operations = XDoc.XPathSelectElements("/wsdl:definitions/wsdl:portType/wsdl:operation", NamespaceManager);

		foreach (var operation in operations!)
		{
			try
			{
				_Service.Methods.Add(GetMethod(operation));
			}
			catch (Exception ex)
			{
				TraceHelper.WriteError(ex.Message);
			}
		}
	}


	SoapMethod GetMethod(in XElement operationNode)
	{
		string? methodName = (operationNode.Attribute("name")?.Value) ?? throw new Exception($"Method not found in file '{WsdlPath}' xml:'{operationNode.Value}'");
		var schemaRequest = GetMethodElement(operationNode, MessageType.Input);
		var schemaResponse = GetMethodElement(operationNode, MessageType.Output);

		if (schemaRequest is null)
		{
			throw new Exception($"Request not found for method {methodName} in file '{WsdlPath}'");
		}

		if (schemaResponse is null)
		{
			throw new Exception($"Response not found for method {methodName} in file '{WsdlPath}'");
		}

		string soapAction = GetSoapAction(methodName);

		if (string.IsNullOrEmpty(soapAction))
		{
			TraceHelper.WriteVerbose($"SoapAction not found for method {methodName} in file '{WsdlPath}'");
		}

		SoapMethod method = new(methodName, soapAction) { Description = GetMethodDescription(operationNode) };

		try
		{
			method.Request = new RequestProperty(MakeProperty(schemaRequest!));
		}
		catch (NotImplementedException ex)
		{
			throw new Exception($"Invalid type for request in method {methodName} in file '{WsdlPath}' - {ex.Message}", ex);
		}

		try
		{
			method.Response = MakeProperty(schemaResponse!);
		}
		catch (NotImplementedException ex)
		{
			throw new Exception($"Invalid type for response in method {methodName} in file '{WsdlPath}' - {ex.Message}", ex);
		}

		try
		{
			method.SoapHeader = GetSoapHeader(methodName);
		}
		catch (NotImplementedException ex)
		{
			throw new Exception($"Invalid type for soep header in method {methodName} in file '{WsdlPath}' - {ex.Message}", ex);
		}

		TraceHelper.WriteVerbose($"Method {methodName} SoapAction:'{soapAction}' request:{method.Request?.Name} response:{method.Response?.Name} SoapHeader:'{method.SoapHeader?.Name}'");

		return method;
	}

	string? GetMethodDescription(in XElement operationNode)
	{
		var node = operationNode.XPathSelectElement($"wsdl:documentation", NamespaceManager);
		return node?.InnerXml();
	}

	string GetSoapAction(in string operationName)
	{
		var node = XDoc.XPathSelectElement($"/wsdl:definitions/wsdl:binding/wsdl:operation[@name='{operationName}']/soap:operation", NamespaceManager);
		return node?.Attribute("soapAction")?.Value ?? string.Empty;
	}

	Property? GetSoapHeader(in string operationName)
	{
		var opeNode = XDoc.XPathSelectElement($"/wsdl:definitions/wsdl:binding/wsdl:operation[@name='{operationName}']/wsdl:input/soap:header", NamespaceManager);
		if (opeNode == null)
			return null;

		string? message = opeNode.Attribute("message")?.Value;
		string? part = opeNode.Attribute("part")?.Value;

		if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(part))
		{
			TraceHelper.WriteError($"Invalid Soap header message for method '{operationName}' in file '{WsdlPath}' - message:'{message}' part:'{part}'");
			return null;
		}

		var elementName = GetMessageElement(opeNode, message, part);
		var element = GetElement(elementName);
		if (element == null)
		{
			TraceHelper.WriteError($"Soap header element not found for method '{operationName}' in file '{WsdlPath}' - message:'{message}' part:'{part}' elementName:'{elementName}'");
			return null;
		}

		return MakeProperty(element);
	}

	string? GetSoapAddressLocation()
	{
		var node = XDoc.XPathSelectElement($"/wsdl:definitions/wsdl:service/wsdl:port/soap:address", NamespaceManager);
		return node?.Attribute("location")?.Value;
	}

	XmlSchemaElement? GetElement(in XmlQualifiedName? qualifiedName)
	{
		if (qualifiedName == null)
			return null;

		return SchemaSet.GlobalElements[qualifiedName] as XmlSchemaElement;
	}

	XmlSchemaElement? GetMethodElement(in XElement operationNode, in MessageType type)
	{
		string strType = type switch { MessageType.Input => "input", MessageType.Output => "output", _ => throw new NotImplementedException($"Unknown MessageType '{type}'") };

		var node = operationNode.XPathSelectElement($"wsdl:{strType}", NamespaceManager);
		if (node == null)
			return null;

		string? message = node.Attribute("message")?.Value;
		if (string.IsNullOrEmpty(message))
			return null;

		var messageElement = GetMessageElement(node, message);
		return GetElement(messageElement);
	}

	private XmlQualifiedName? GetMessageElement(XElement node, string messageName, string? partName = null)
	{
		var qualifiedName = XmlHelper.MakeXmlQualifiedName(node, messageName);

		string xpath = $"/wsdl:definitions/wsdl:message[@name='{qualifiedName.Name}']/wsdl:part";

		if (!string.IsNullOrEmpty(partName))
			xpath += $"[@name='{partName}']";

		var partNode = XDoc.XPathSelectElement(xpath, NamespaceManager);
		string? elementValue = partNode?.Attribute("element")?.Value;

		if (string.IsNullOrEmpty(elementValue))
			return null;

		return XmlHelper.MakeXmlQualifiedName(node, elementValue);
	}

	Property MakeProperty(in XmlSchemaElement schemaElement)
	{
		string name = schemaElement.Name!;
		var schemaType = schemaElement.ElementSchemaType ?? throw new Exception($"Missing ElementSchemaType  in {XmlSchemaElementToString(schemaElement)}");
		bool required = schemaElement.MinOccurs > 0;

		try
		{
			var propertyType = MakeType(schemaType);
			TraceHelper.WriteVerbose($"Property Name:{(required ? "*" : " ")}{name,-40} {XmlSchemaElementToString(schemaElement)}");
			return new Property(schemaElement.QualifiedName, propertyType!, schemaElement.MaxOccurs > 1) { Required = required, Nullable = false, Description = GetDocumentation(schemaElement) };
		}
		catch (NotImplementedException ex)
		{
			string err = $"Exception '{ex.Message}' on property '{XmlSchemaElementToString(schemaElement)}'";
			TraceHelper.WriteError(err);
			throw new NotImplementedException(err);
		}
	}

	ActualPropertyType MakeActualType(in XmlSchemaType schemaType)
	{
		return schemaType switch
		{
			XmlSchemaSimpleType schemaSimpleType => MakeSimpleType(schemaSimpleType),
			XmlSchemaComplexType schemaComplexType => MakeComplexType(schemaComplexType),
			_ => throw new NotImplementedException($"Invalid schema type {XmlSchemaTypeToString(schemaType)}"),
		};
	}

	PropertyType MakeType(in XmlSchemaType schemaType)
	{
		XmlQualifiedName? qualifiedName = GetTypeName(schemaType);

		if (qualifiedName != null)
		{
			return ReferenceTypes[qualifiedName.ToString()];
		}
		else
			return MakeActualType(schemaType);
	}

	ActualPropertyType MakeSimpleType(in XmlSchemaSimpleType simpleType)
	{
		return simpleType.Content switch
		{
			XmlSchemaSimpleTypeUnion typeUnion => MakeSimpleTypeUnion(simpleType, typeUnion),
			XmlSchemaSimpleTypeList => MakeBasicTypeList(simpleType),
			XmlSchemaSimpleTypeRestriction typeRestriction => MakeSimpleTypeWithRestriction(simpleType, typeRestriction),
			_ => MakeSimpleTypeWithRestriction(simpleType),
		};
	}

	static ActualPropertyType MakeSimpleTypeUnion(in XmlSchemaSimpleType simpleType, in XmlSchemaSimpleTypeUnion typeUnion)
	{
		//TODO Implement XmlSchemaSimpleTypeUnion 
		throw new NotImplementedException($"MakeSimpleTypeUnion not implemented {XmlSchemaTypeToString(simpleType)}");
	}
	BasicPropertyType MakeBasicTypeList(in XmlSchemaSimpleType simpleType)
	{
		var type = GetBasicPropertyTypeFromXmlTypeCode(simpleType.TypeCode, simpleType);
		return type with { IsArray = true, SoapArrayAsList = true };
	}

	ActualPropertyType MakeSimpleTypeWithRestriction(in XmlSchemaSimpleType simpleType, in XmlSchemaSimpleTypeRestriction? typeRestriction = null)
	{
		// SimpleType cannot be array, see IsArray method.

		if (simpleType.TypeCode == XmlTypeCode.AnyAtomicType)
		{
			return OneOfPropertyType.MakeAnySimpleType(false, GetTypeName(simpleType));
		}
		else
		{
			XmlQualifiedName? baseTypeName = null;

			// do not consider base type name in case of restriction. See LoadDerivedTypes : we must only handle extension otherwise it will crash in OneOfPropertyType because of arrays.
			if (typeRestriction == null)
			{
				baseTypeName = GetBaseTypeName(simpleType);
			}

			var facets = typeRestriction?.Facets.OfType<XmlSchemaEnumerationFacet>().Where(f => !string.IsNullOrEmpty(f.Value));
			if (facets?.Count() > 0)
			{
				return new StringEnumPropertyType(facets.Select(f => f.Value!)) { IsArray = false, QualifiedName = GetTypeName(simpleType), BaseTypeName = baseTypeName, Description = GetDocumentation(simpleType) };
			}
			else
			{
				var ptype = GetBasicPropertyTypeFromXmlTypeCode(simpleType.TypeCode, simpleType) with { BaseTypeName = baseTypeName };

				if (ptype is StringPropertyType stringPropertyType)
				{
					int? minLength = null;
					int? maxLength = null;
					string? pattern = null;

					if (typeRestriction != null)
					{
						foreach (var r in typeRestriction.Facets)
						{
							if (r is XmlSchemaMinLengthFacet xmlSchemaMinLengthFacet)
								if (int.TryParse(xmlSchemaMinLengthFacet.Value, out int v))
									minLength = v;

							if (r is XmlSchemaMaxLengthFacet xmlSchemaMaxLengthFacet)
								if (int.TryParse(xmlSchemaMaxLengthFacet.Value, out int v))
									maxLength = v;

							if (r is XmlSchemaLengthFacet xmlSchemaLengthFacet)
								if (int.TryParse(xmlSchemaLengthFacet.Value, out int v))
								{
									minLength = maxLength = v;
								}

							if (r is XmlSchemaPatternFacet xmlSchemaPatternFacet)
								pattern = xmlSchemaPatternFacet.Value;
						}
					}

					return stringPropertyType with { MinLength = minLength, MaxLength = maxLength, Pattern = pattern };
				}
				else
				{
					return ptype;
				}
			}
		}
	}

	BasicPropertyType GetBasicPropertyTypeFromXmlTypeCode(in XmlTypeCode typeCode, in XmlSchemaType schemaType)
	{
		return typeCode switch
		{
			// Language  e.g. "en", "en-US", "fr", "fr-FR".
			XmlTypeCode.String or XmlTypeCode.NormalizedString or XmlTypeCode.QName or XmlTypeCode.Token or XmlTypeCode.Name or XmlTypeCode.Language or XmlTypeCode.AnyUri
			=> new StringPropertyType() { QualifiedName = GetTypeName(schemaType), BaseTypeName = GetBaseTypeName(schemaType), Description = GetDocumentation(schemaType) },
			XmlTypeCode.Base64Binary => new StringBase64PropertyType() { QualifiedName = GetTypeName(schemaType), BaseTypeName = GetBaseTypeName(schemaType), Description = GetDocumentation(schemaType) },
			_ => new SimplePropertyType(GetBasicTypeFromXmlTypeCode(typeCode)) { QualifiedName = GetTypeName(schemaType), BaseTypeName = GetBaseTypeName(schemaType), Description = GetDocumentation(schemaType) }
		};
	}

	static SimpleTypes GetBasicTypeFromXmlTypeCode(in XmlTypeCode typeCode)
	{
		return typeCode switch
		{
			XmlTypeCode.Boolean => SimpleTypes.Boolean,
			XmlTypeCode.Int or XmlTypeCode.UnsignedInt or XmlTypeCode.Integer or XmlTypeCode.Byte or XmlTypeCode.UnsignedByte or XmlTypeCode.Short or XmlTypeCode.UnsignedShort
				or XmlTypeCode.Long or XmlTypeCode.UnsignedLong or XmlTypeCode.NegativeInteger or XmlTypeCode.NonNegativeInteger or XmlTypeCode.PositiveInteger or XmlTypeCode.NonPositiveInteger
				=> SimpleTypes.Integer,
			XmlTypeCode.GDay or XmlTypeCode.GMonth or XmlTypeCode.GMonthDay or XmlTypeCode.GYear or XmlTypeCode.GYearMonth => SimpleTypes.Integer,
			XmlTypeCode.Date => SimpleTypes.Date,
			XmlTypeCode.DateTime => SimpleTypes.DateTime,
			XmlTypeCode.Time => SimpleTypes.Time,
			XmlTypeCode.Duration => SimpleTypes.Duration,
			XmlTypeCode.Float or XmlTypeCode.Double or XmlTypeCode.Decimal => SimpleTypes.Number,
			_ => throw new NotImplementedException($"Invalid type code '{typeCode}' - Not a simple type"),
		};
	}

	static bool IsArray(in XmlSchemaType schemaType)
	{
		if (schemaType is XmlSchemaComplexType complexType)
		{
			var typ = complexType.Particle ?? complexType.ContentTypeParticle;
			return typ.MaxOccurs > 1;
		}
		else
			return false;
	}

	XmlQualifiedName? GetTypeName(in XmlSchemaType? schemaType)
	{
		if (schemaType == null || schemaType.QualifiedName == null || schemaType.QualifiedName.IsEmpty || schemaType.QualifiedName.Namespace == XmlSchema.Namespace)
			return null;

		var qualifiedName = schemaType.QualifiedName;

		if (!ReferenceTypes.ContainsKey(qualifiedName.ToString()))
			throw new Exception($"Global type '{qualifiedName} does not exist");

		return qualifiedName;
	}

	XmlQualifiedName? GetBaseTypeName(in XmlSchemaType? schemaType)
	{
		// also check XmlHelper.IsAnyOrSimpleType(schemaType.BaseXmlSchemaType.QualifiedName
		if (schemaType?.BaseXmlSchemaType == null || schemaType.BaseXmlSchemaType.QualifiedName.IsEmpty || schemaType.QualifiedName.Namespace == XmlSchema.Namespace)
			return null;

		var qualifiedName = schemaType.BaseXmlSchemaType.QualifiedName;
		return ReferenceTypes.ContainsKey(qualifiedName.ToString()) ? qualifiedName : null;
	}

	ActualPropertyType MakeComplexType(in XmlSchemaComplexType complexType)
	{
		if (complexType.ContentType == XmlSchemaContentType.Empty)
			return MakeEmptyType(complexType);

		if (complexType.ContentType == XmlSchemaContentType.TextOnly)
		{
			var ptype = GetBasicPropertyTypeFromXmlTypeCode(complexType.TypeCode, complexType);

			var attributes = GetAttributes(complexType);

			if (attributes.Count > 0)
			{
				var prop = new Property(new XmlQualifiedName("(Value)"), ptype, false) { SoapHandling = SoapPropertyHandlingEnum.InnerText, Required = IsRequired(complexType) };
				List<Property> props = [prop, .. attributes];
				return new ObjectPropertyType(props) { QualifiedName = GetTypeName(complexType), BaseTypeName = GetBaseTypeName(complexType), IsAbstract = complexType.IsAbstract, Description = GetDocumentation(complexType) };
			}
			else
			{
				return ptype with { IsAbstract = complexType.IsAbstract };
			}
		}

		var typ = complexType.Particle ?? complexType.ContentTypeParticle;

		return typ switch
		{
			XmlSchemaGroupRef xmlSchemaGroupRef => MakeGroupRefType(xmlSchemaGroupRef, complexType),
			XmlSchemaChoice xmlSchemaChoice => MakeChoiceType(xmlSchemaChoice, complexType),     // it is strange to have choice here because it means that property name will not be used when generating SOAP as ChoicePropertyType is a choice between several elements.
			XmlSchemaAll xmlSchemaAll => MakeAllType(xmlSchemaAll, complexType),
			XmlSchemaSequence xmlSchemaSequence => MakeSequenceType(xmlSchemaSequence, complexType),
			_ => throw new NotImplementedException($"MakeComplexType type {typ} is not implemented"),
		};
	}

	static bool IsRequired(in XmlSchemaComplexType complexType)
	{
		var typ = complexType.Particle ?? complexType.ContentTypeParticle;
		return typ.MinOccurs > 0;
	}

	List<Property> GetAttributes(in XmlSchemaComplexType type)
	{
		List<Property> attributes = [];

		foreach (var attr in type.AttributeUses.Values.OfType<XmlSchemaAttribute>())
		{
			if (attr.AttributeSchemaType == null)
			{
				TraceHelper.WriteWarning($"{MethodBase.GetCurrentMethod()}: AttributeSchemaType is null for attribute '{attr.Name}' on type '{XmlSchemaTypeToString(type)}'");
				continue;
			}

			var ptype = MakeSimpleType(attr.AttributeSchemaType);
			var prop = new Property(attr.QualifiedName, ptype, false) { Required = attr.Use == XmlSchemaUse.Required, SoapHandling = SoapPropertyHandlingEnum.Attribute };
			attributes.Add(prop);
		}

		return attributes;
	}

	/// <summary>
	/// Empty Type.
	/// e.g. GetVersionRequest in DTA\interfaces\internals\dta-interface-backoffice\src\main\resources\v16\contractor\InventoryMessages.xsd
	/// </summary>
	ObjectPropertyType MakeEmptyType(in XmlSchemaComplexType complexType)
	{
		return new ObjectPropertyType(GetAttributes(complexType)) { IsArray = IsArray(complexType), QualifiedName = GetTypeName(complexType), BaseTypeName = GetBaseTypeName(complexType), IsAbstract = complexType.IsAbstract, Description = GetDocumentation(complexType) };
	}

	ObjectPropertyType MakeGroupRefType(in XmlSchemaGroupRef groupRef, in XmlSchemaComplexType complexType)
	{
		return MakeSequenceType(GetGroupRefElements(groupRef), complexType, groupRef);
	}

	ChoicePropertyType MakeChoiceType(in XmlSchemaChoice choice, in XmlSchemaComplexType? complexType)
	{
		var props = choice.Items.OfType<XmlSchemaElement>().Select(c => MakeProperty(c));
		return new ChoicePropertyType(props) { IsArray = choice.MaxOccurs > 1, QualifiedName = GetTypeName(complexType), BaseTypeName = GetBaseTypeName(complexType), IsAbstract = complexType?.IsAbstract ?? false, Description = GetDocumentation(complexType) };
	}

	ObjectPropertyType MakeAllType(in XmlSchemaAll sequence, in XmlSchemaComplexType complexType)
	{
		return MakeSequenceType(sequence.Items.OfType<XmlSchemaParticle>(), complexType, sequence);
	}

	ObjectPropertyType MakeSequenceType(in XmlSchemaSequence sequence, in XmlSchemaComplexType complexType)
	{
		return MakeSequenceType(GetSequenceElements(sequence), complexType, sequence);
	}

	ObjectPropertyType MakeSequenceType(in IEnumerable<XmlSchemaParticle> items, XmlSchemaComplexType complexType, XmlSchemaParticle particle)
	{
		// particle is equal to complexType.Particle ?? complexType.ContentTypeParticle, see MakeComplexType
		bool isArray = particle.MaxOccurs > 1;

		// Order of element in sequence is important: it must be same order in SOAP (XML).
		var props = items.Select(p => p switch
		{
			XmlSchemaElement xmlSchemaElement => MakeProperty(xmlSchemaElement),
			XmlSchemaChoice xmlSchemaChoice => Property.MakeChoiceProperty(MakeChoiceType(xmlSchemaChoice, null), xmlSchemaChoice.MinOccurs > 0),
			_ => throw new NotImplementedException($"{MethodBase.GetCurrentMethod()}: type '{p}' is not handled")
		}).ToList();


		var attributes = GetAttributes(complexType);
		bool soapLikeArray = !isArray && attributes.Count == 0 && props.Count == 1 && props[0].IsArray;

		props.AddRange(attributes);
		return new ObjectPropertyType(props) { IsArray = isArray, SoapLikeArray = soapLikeArray, QualifiedName = GetTypeName(complexType), BaseTypeName = GetBaseTypeName(complexType), IsAbstract = complexType.IsAbstract, Description = GetDocumentation(complexType) };
	}

	IEnumerable<XmlSchemaParticle> GetGroupRefElements(in XmlSchemaGroupRef groupRef)
	{
		bool isArray = groupRef.MaxOccurs > 1;
		if (isArray)
			TraceHelper.WriteWarning($"{MethodBase.GetCurrentMethod()}: Array is not handled");

		return groupRef.Particle switch
		{
			XmlSchemaChoice xmlSchemaChoice => [xmlSchemaChoice],
			XmlSchemaAll xmlSchemaAll => xmlSchemaAll.Items.OfType<XmlSchemaParticle>(),
			XmlSchemaSequence xmlSchemaSequence => GetSequenceElements(xmlSchemaSequence),
			_ => throw new NotImplementedException($"{MethodBase.GetCurrentMethod()}: {groupRef.Particle} is not implemented"),
		};
	}

	IEnumerable<XmlSchemaParticle> GetSequenceElements(in XmlSchemaSequence sequence)
	{
		// XmlSchemaSequence can contain: XmlSchemaElement, XmlSchemaGroupRef, XmlSchemaChoice, XmlSchemaSequence, or XmlSchemaAny.
		// Skip XmlSchemaAny
		var items = sequence.Items.OfType<XmlSchemaParticle>().Where(p => p is not XmlSchemaAny).ToList();

		if (items.Count != sequence.Items.Count)
		{
			var first = sequence.Items.OfType<XmlSchemaAny>().First();
			TraceHelper.WriteWarning($"{MethodBase.GetCurrentMethod()}: some elements of type <any> were skipped - First element:'{XmlSchemaObjectToString(first)}'");
		}

		return items.SelectMany(p => p switch
		{
			XmlSchemaGroupRef xmlSchemaGroupRef => GetGroupRefElements(xmlSchemaGroupRef),
			XmlSchemaSequence xmlSchemaSequence => GetSequenceElements(xmlSchemaSequence),
			_ => [p]
		});
	}

	static string? GetDocumentation(in XmlSchemaAnnotated? schema)
	{
		if (schema?.Annotation == null)
			return null;

		var doc = schema.Annotation.Items.OfType<XmlSchemaDocumentation>().FirstOrDefault();
		if (doc?.Markup == null)
			return null;

		StringBuilder sb = new();

		foreach (var node in doc.Markup)
		{
			if (node != null)
				sb.Append(node.OuterXml);
		}

		return sb.ToString();
	}
}