﻿namespace WSTest
{
	internal class SoapMethod : HttpServiceMethod
	{
		public string SoapAction { get; init; } = null!;
		public Property? SoapHeader { get; set; }
		public Property Response { get; set; } = null!;

		[Newtonsoft.Json.JsonConstructor]
		protected SoapMethod() { }

		public SoapMethod(string name, string soapAction) : base(name)
		{
			this.SoapAction = soapAction;
		}

		public override string DescribeMethod(ILookupReferenceType lookup)
		{
			return new SoapMethodInfo(this, lookup).DescribeMethod();
		}

		public override string ToString()
		{
			return base.ToString();
		}

	}
}
