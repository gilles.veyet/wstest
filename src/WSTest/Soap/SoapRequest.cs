﻿using System.Xml.Linq;
using System.Xml.Schema;
using WSTest.Helpers;

namespace WSTest
{
	internal class SoapRequest
	{
		static readonly XNamespace SoapEnvelopeNamespace = (XNamespace)XmlHelper.SoapEnvelopeUri;
		static readonly XName XNameSoapEnvelope = SoapEnvelopeNamespace + "Envelope";

		public XDocument Doc { get; }
		private XElement? _Body;
		private XElement? _Header;

		public XElement Body
		{
			get
			{
				if (_Body == null)
				{
					_Body = new XElement(SoapEnvelopeNamespace + "Body");
					Doc.Root!.Add(_Body);
				}

				return _Body;
			}
		}

		public XElement Header
		{
			get
			{
				if (_Header == null)
				{
					_Header = new XElement(SoapEnvelopeNamespace + "Header");
					Doc.Root!.Add(_Header);
				}

				return _Header;
			}
		}

		public SoapRequest()
		{
			Doc = new XDocument(
				new XElement(XNameSoapEnvelope,
					new XAttribute(XNamespace.Xmlns + "soap", SoapEnvelopeNamespace),
					new XAttribute(XNamespace.Xmlns + "xsd", XmlSchema.Namespace),
					new XAttribute(XNamespace.Xmlns + "xsi", XmlSchema.InstanceNamespace)));
		}

		public string GetContent()
		{
			return XmlHelper.GetXDocumentContent(Doc);
		}
	}
}
