﻿using System.Diagnostics;
using System.Net.Mime;
using System.Text;
using WSTest.Helpers;

#pragma warning disable IDE0130 // Namespace does not match folder structure
namespace WSTest
#pragma warning restore IDE0130 // Namespace does not match folder structure
{
	internal class SoapServiceRunner(SoapService service) : HttpServiceRunner
	{
		public override SoapService _Service { get; } = service;

		SoapServiceDescriptor _Descriptor => _Service.Descriptor;

		public override HttpSendData PrepareMethod(HttpServiceMethod method, Profile profile, IReadOnlyCollection<Value> values)
		{
			var soapMethod = (SoapMethod)method;
			string baseUrl = profile.GetServiceBaseUrl(_Service.Descriptor);

			return new HttpSendData
			{
				Url = !string.IsNullOrWhiteSpace(_Descriptor.ServiceUrl) ? baseUrl.TrimEnd('/') + '/' + _Descriptor.ServiceUrl.TrimStart('/') : baseUrl,
				Method = HttpMethod.Post,
				Options = GetHttpClientOptions(soapMethod, profile),
				BodyContent = GetRequestBody(soapMethod, profile, values)
			};
		}

		public override HttpSendData PrepareMethod(HttpServiceMethod method, Profile profile, string content, string url)
		{
			var soapMethod = (SoapMethod)method;
			return new HttpSendData
			{
				Url = url,
				Method = HttpMethod.Post,
				Options = GetHttpClientOptions(soapMethod, profile),
				BodyContent = content
			};

		}

		HttpClientOptions GetHttpClientOptions(SoapMethod method, Profile profile)
		{
			HttpClientOptions httpOptions = new()
			{
				Security = GetSecurityOptions(profile),
				ContentType = MediaTypeNames.Text.Xml,  // use "text/xml" ("application/xml" is OK for most systems but it is rejected by Handshake ASPI).
			};

			httpOptions.Headers["SOAPAction"] = method.SoapAction;
			return httpOptions;
		}


		string GetRequestBody(SoapMethod soapMethod, Profile profile, IReadOnlyCollection<Value> values)
		{
			var body = values.Where(v => v.Property is RequestProperty).FirstOrDefault() ?? throw new Exception("Request method is not defined");
			SoapRequest request = new();

			if (soapMethod.SoapHeader != null)
			{
				var soapValue = ValueFactory.MakeValueFromProperty(soapMethod.SoapHeader);
				if (!FillSoapHeader(new StringBuilder(), profile, soapValue))
					throw new Exception("Missing mappings for SoapHeader");

				soapValue.SoapSerialize(request.Header);
			}

			body.SoapSerialize(request.Body);
			//Debug.Print(requestContent);
			return request.GetContent();
		}

		bool FillSoapHeader(StringBuilder sbPath, Profile profile, Value value, Value? parentValue = null)
		{
			var property = value.Property;
			if (value is ArrayValue)
			{
				throw new Exception($"Error FillSoapHeader: array of '{property.Name}' cannot be handled");
			}

			if (value is ChoiceValue choiceValue)
			{
				foreach (var child in choiceValue.PropertyList.Select(cp => ValueFactory.GetChoiceValue(choiceValue, cp, false)))
				{
					if (FillSoapHeader(new StringBuilder(sbPath.ToString()), profile, child, parentValue))   // use parent = parentValue (skip value when it is ChoiceValue).
					{
						choiceValue.SetValue(child);
						return true;
					}
				}

				return false;
			}
			else
			{
				if (parentValue != null)
					sbPath.Append('|');

				var parentProperty = parentValue?.Property;

				if (parentProperty != null && parentProperty.QualifiedName.Namespace == property.QualifiedName.Namespace)
					sbPath.Append(property.Name);
				else
					sbPath.Append(property.QualifiedName.ToString());

				if (value is ObjectValue objectValue)
				{
					bool result = true;

					foreach (var child in objectValue.PropertyValues)
						result = result && FillSoapHeader(new StringBuilder(sbPath.ToString()), profile, child, value);

					return result;
				}
				else if (value is OneOfValue oneOfValue)
				{
					//throw new Exception($"Error FillSoapHeader : OneOfValue '{property.Name}' cannot be handled");

					foreach (var ptype in oneOfValue.PropertyTypeList)
					{
						var child = ValueFactory.SetOneOfValue(oneOfValue, ptype);
						if (FillSoapHeader(sbPath, profile, child, value))
						{
							return true;
						}
					}

					return false;
				}
				else if (value is BasicValue basicValue)
				{
					string fullKey = sbPath.ToString();
					string key = property.Name;
					string? mapping = FindMapping(fullKey, key);

					if (!string.IsNullOrEmpty(mapping))
					{
						basicValue.ParseString(profile.InterpolateStringWithMappings(mapping));
						return true;
					}
					else if (profile.PropertyMappings.TryGetValue(key, out var protectableValue))
					{
						basicValue.ParseString(protectableValue.Value);
						return true;
					}
					else
					{
						TraceHelper.Write(property.Required ? TraceLevel.Error : TraceLevel.Warning, $"Missing mapping for '{fullKey}' in Soap header'");
						return !property.Required;
					}
				}
				else
					throw new Exception($"Error FillSoapHeader : ArrayValue '{property.Name}' cannot be handled");
			}
		}


		string? FindMapping(string fullKey, string key)
		{
			if (_Descriptor.Mappings.TryGetValue(fullKey, out string? textFull))
			{
				return textFull;
			}

			if (_Descriptor.Mappings.TryGetValue(key, out string? textKey))
			{
				return textKey;
			}

			return null;
		}

	}
}
