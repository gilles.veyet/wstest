﻿using System.Xml.Schema;
using WSTest.Helpers;

namespace WSTest
{
	class SchemaImporter
	{

		public List<ValidationEventArgs> WarningsAndErrors { get; } = [];

		private readonly XmlSchemaSet SchemaSet = new();
		public HashSet<string> SchemaPaths { get; } = [];

		public SchemaImporter()
		{ }

		public XmlSchemaSet GetSchemaSet()
		{
			SchemaSet.Compile();
			return SchemaSet;
		}

		public void ImportWithPath(string sourcePath, string? targetNamespace)
		{
			XmlSchema? schema;

			if (string.IsNullOrEmpty(targetNamespace))
				TraceHelper.WriteWarning($"SchemaImporter: empty target namespace '{targetNamespace}' for file '{sourcePath}'");

			try
			{
				// Several files can have same targetNamespace,  see example in dcm-interface\install\v9
				schema = SchemaSet.Add(targetNamespace, sourcePath);

				if (schema == null)
					throw new Exception($"{nameof(ImportWithPath)}: adding schema '{sourcePath}' returned null");

				if (schema.TargetNamespace != targetNamespace)
					TraceHelper.WriteWarning($"SchemaImporter: schema '{sourcePath}' target namespace {schema.TargetNamespace} different from expected {targetNamespace}");
			}
			catch (Exception ex)
			{
				throw new Exception($"SchemaImporter: Invalid file '{sourcePath}' : {ex.Message}", ex);
			}

			RecurseImport(schema, sourcePath);
		}

		public void ImportWithContent(string schemaContent, string sourcePath)
		{
			using var sr = new StringReader(schemaContent);
			XmlSchema? xmlSchema = XmlSchema.Read(sr, (s, ev) =>
			{
				if (ev != null)
					TraceHelper.WriteWarning($"SchemaImporter: Import embedded schema => {ev.Severity} '{ev.Message}'");
			});

			if (xmlSchema != null)
			{
				SchemaSet.Add(xmlSchema);
				RecurseImport(xmlSchema, sourcePath);
			}
		}

		void RecurseImport(XmlSchema schema, string sourcePath)
		{
			string directory = Path.GetDirectoryName(sourcePath)!;

			foreach (var include in schema.Includes)
			{
				if (include is XmlSchemaImport import)
				{
					string? schemaLocation = import.SchemaLocation;

					if (string.IsNullOrWhiteSpace(schemaLocation))
					{
						TraceHelper.WriteWarning($"Invalid import (missing schemaLocation) in file '{sourcePath}' Line:{import.LineNumber} Pos:{import.LinePosition}  Namespace:'{import.Namespace}'");
						continue;
					}

					string path = Path.GetFullPath(schemaLocation, directory);

					if (SchemaPaths.Contains(path))
						continue;

					SchemaPaths.Add(path);
					ImportWithPath(path, import.Namespace);
				}
			}
		}
	}

}
