﻿using WSTest.Helpers;

namespace WSTest;

internal class SoapMethodInfo(SoapMethod method, ILookupReferenceType lookup) : HttpMethodInfo(lookup)
{
	protected override SoapMethod Method => method;
	protected override string MethodMoreInfo => Method.SoapAction;

	protected override IEnumerable<HtmlTableRow> DescribeResponses()
	{
		return [DescribeRootProperty(Method.Response)];
	}
}
