﻿using System.Net.Mime;
using System.Text;
using WSTest.Helpers;

namespace WSTest;

internal class RestServiceRunner(RestService service) : HttpServiceRunner()
{
	public override RestService _Service { get; } = service;

	public override HttpSendData PrepareMethod(HttpServiceMethod method, Profile profile, IReadOnlyCollection<Value> values)
	{
		var restMethod = (RestMethod)method;
		string? requestBody = null;

		var body = values.Where(v => v.Property is RequestProperty).FirstOrDefault();
		if (body != null && body.Specified)
		{
			System.Text.Json.JsonSerializerOptions jsonOptions = new() { WriteIndented = true };
			requestBody = body.ToJsonNode()?.ToJsonString(jsonOptions);
			//Debug.Print(bodyContent);
		}

		return new HttpSendData() { Url = MakeMethodUrl(restMethod, profile, values), Method = OpenApiHelper.ConvertOperationTypeToHttpMethod(restMethod.HttpMethod), Options = GetHttpClientOptions(profile), BodyContent = requestBody };
	}

	public override HttpSendData PrepareMethod(HttpServiceMethod method, Profile profile, string content, string url)
	{
		var restMethod = (RestMethod)method;
		return new HttpSendData() { Url = url!, Method = OpenApiHelper.ConvertOperationTypeToHttpMethod(restMethod.HttpMethod), Options = GetHttpClientOptions(profile), BodyContent = content };
	}

	HttpClientOptions GetHttpClientOptions(Profile profile)
	{
		return new HttpClientOptions()
		{
			Security = GetSecurityOptions(profile),
			ContentType = MediaTypeNames.Application.Json,
			Accept = MediaTypeNames.Application.Json,
		};
	}


	string MakeMethodUrl(RestMethod method, Profile profile, IReadOnlyCollection<Value> values)
	{
		StringBuilder urlBuilder = new(profile.GetServiceBaseUrl(_Service.Descriptor));

		var pathBuilder = new StringBuilder(method.EndPointPath);
		StringBuilder queryBuilder = new();

		foreach (var v in values)
		{
			var property = v.Property;

			if (property is UrlParam urlParam)
			{

				switch (urlParam.Location)
				{
					case UrlParamLocations.Path:
						{
							string strValue = ToPathString(v);
							pathBuilder.Replace("{" + property.Name + "}", strValue);
						}
						break;

					case UrlParamLocations.Query:
						{
							string strValue = ToQueryString(v);
							if (!string.IsNullOrEmpty(strValue))
							{
								queryBuilder.Append(strValue).Append('&');
							}
						}
						break;

					default:
						throw new Exception($"UrlParam location '{urlParam.Location}' is not handled yet");
				}
			}
		}

		urlBuilder.Append(pathBuilder);

		if (queryBuilder.Length > 0)
		{
			queryBuilder.Length--;  // remove last '&'
			urlBuilder.Append('?');
			urlBuilder.Append(queryBuilder);
		}

		return urlBuilder.ToString();
	}


	public static string ToPathString(Value value)
	{
		if (value is ArrayValue arrayValue)
		{
			if (!value.Specified || arrayValue.Values.Count == 0)
				return string.Empty;

			return String.Join(',', arrayValue.Values.Select(v => ToPathString(v)));
		}
		else if (value is BasicValue basicValue)
		{
			return Uri.EscapeDataString(basicValue.SerializeToString());
		}
		else
		{
			throw new Exception($"Type {value.ActualType} is not handled in path/query parameters");
		}
	}

	public static string ToQueryString(Value value)
	{
		if (value is ArrayValue arrayValue)
		{
			if (!value.Specified || arrayValue.Values.Count == 0)
				return string.Empty;

			return String.Join('&', arrayValue.Values.Select(v => ToQueryString(v)));
		}
		else
		{
			return $"{Uri.EscapeDataString(value.Property.Name)}={ToPathString(value)}";
		}
	}

}
