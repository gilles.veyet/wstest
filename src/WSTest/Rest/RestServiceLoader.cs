﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.Readers;
using System.Diagnostics;
using System.Net.Mime;
using System.Reflection;
using System.Xml;
using WSTest.Extensions;
using WSTest.Helpers;
using WSTest.Rest;

namespace WSTest;

internal class RestServiceLoader
{
	private readonly RestService _Service;
	private readonly string OpenApiDataPath;

	private Dictionary<string, ActualPropertyType> ActualTypes = [];
	private Dictionary<string, ReferencePropertyType> ReferenceTypes = [];
	private Dictionary<string, ReferencePropertyType> ArrayReferenceTypes = [];


	public RestServiceLoader(RestService service, string serviceDefinitionContentDirectory)
	{
		_Service = service;
		OpenApiDataPath = Path.Combine(serviceDefinitionContentDirectory, _Service.Descriptor.ServiceDefinitionPath);
	}

	public LoadServiceResult Load()
	{
		string openApiData = File.ReadAllText(OpenApiDataPath);
		OpenApiDocument openApiDocument = new OpenApiStringReader().Read(openApiData, out _);

		if (openApiDocument.Components == null || openApiDocument.Paths == null)
			throw new Exception($"Invalid OpenAPI (yaml/json) path '{OpenApiDataPath}'");

		LoadSchemas(openApiDocument);

		// responses schemas ( apiDoc.Components.Responses ) are not loaded but OpenApiStringReader already lookups for references when loading methods (Paths).
		// there can be an issue in case responses schemas are using recursive references.
		LoadPaths(openApiDocument);

		var result = new LoadServiceResult();
		result.DefinitionFiles.Add(OpenApiDataPath);
		return result;
	}

	bool ExistSchema(XmlQualifiedName qualifiedName)
	{
		return ReferenceTypes.ContainsKey(qualifiedName.ToString());
	}

	XmlQualifiedName? CheckTypeName(XmlQualifiedName? qualifiedName)
	{
		return qualifiedName != null && ExistSchema(qualifiedName) ? qualifiedName : null;
	}

	static XmlQualifiedName MakeTypeName(string key)
	{
		return new XmlQualifiedName(key);
	}

	void LoadSchemas(OpenApiDocument openApiDocument)
	{
		ActualTypes = [];
		ArrayReferenceTypes = [];
		var schemas = openApiDocument.Components.Schemas;
		ReferenceTypes = schemas.ToDictionary(t => t.Key, t => new ReferencePropertyType(MakeTypeName(t.Key)));

		foreach (var kv in schemas)
		{
			var qualifiedName = MakeTypeName(kv.Key);
			Debug.Assert(kv.Value.Reference.Id == kv.Key, $"{MethodBase.GetCurrentMethod()} key '{kv.Key}' is different from schema reference Id '{kv.Value.Reference.Id}'");
			var type = MakeActualType(kv.Value);

			Debug.Assert(type != null, $"{MethodBase.GetCurrentMethod()}: MakeActualType key '{kv.Key}' returned null!");

			// qualifiedName can be different from type.QualifiedName, e.g when declaring an array
			//     Pets:
			//       type: array
			//       items:
			//         $ref: "#/components/schemas/Pet"
			ActualTypes[qualifiedName.ToString()] = type;
		}

		_Service.ActualTypes = ActualTypes;
		_Service.ReferenceTypes = [.. ReferenceTypes.Values, .. ArrayReferenceTypes.Values];
		_Service.UpdateActualTypes();
	}

	void LoadPaths(OpenApiDocument openApiDocument)
	{
		foreach (var rpath in openApiDocument.Paths)
		{
			foreach (var operationKV in rpath.Value.Operations)
			{
				var operation = operationKV.Value;
				string methodName = operation.OperationId;

				// OperationId is optional in OpenApi spec so it can be null
				if (string.IsNullOrEmpty(methodName))
				{
					methodName = $"{operationKV.Key} {rpath.Key}";
				}

				var method = new RestMethod(methodName)
				{
					HttpMethod = operationKV.Key,
					EndPointPath = rpath.Key,
					Description = operation.Description?.NullIfEmpty() ?? operation.Summary,
				};

				var parameters = operation.Parameters.Count > 0 ? operation.Parameters : rpath.Value.Parameters;

				if (parameters.Count > 0)
				{
					foreach (var p in parameters)
					{
						var prop = MakeProperty(p.Name, p.Schema, p.Required);
						method.UrlParams.Add(new UrlParam(prop, ConvertLocation(p.In)));
					}
				}

				if (operation.RequestBody != null)
				{
					var kv = operation.RequestBody.Content.FirstOrDefault(k => k.Key == MediaTypeNames.Application.Json);

					if (kv.Value != null)
					{
						var ptype = MakeType(kv.Value.Schema);
						string name = OpenApiHelper.GetXName(operation.RequestBody.Extensions) ?? "Body";
						method.Request = new RequestProperty(new XmlQualifiedName(name), ptype) { Description = operation.RequestBody.Description };
					}
				}

				foreach (var kv in operation.Responses)
				{
					var ptype = MakeResponse(kv.Value);
					if (ptype != null)
					{
						int key = kv.Key == "default" ? 0 : int.Parse(kv.Key);
						method.Responses[key] = ptype;
					}
					else
						TraceHelper.WriteWarning($"Response '{kv.Key}' of method {operation.OperationId}: missing or invalid content");
				}

				_Service.Methods.Add(method);
			}
		}

	}

	RestResponse MakeResponse(OpenApiResponse resp)
	{
		PropertyType? ptype = null;
		var kv = resp.Content.FirstOrDefault(k => k.Key == MediaTypeNames.Application.Json);

		if (kv.Value != null)
		{
			string type = kv.Key;
			var mediaType = kv.Value;
			var schema = mediaType.Schema;

			ptype = type switch
			{
				MediaTypeNames.Application.Json => MakeType(schema),
				MediaTypeNames.Text.Plain or MediaTypeNames.Application.Octet => GetBasicPropertyType(schema, false, null),
				_ => null   // triggers a warning trace in parent method.
			};
		}

		return new RestResponse() { Description = resp.Description, PropertyType = ptype };
	}

	static UrlParamLocations ConvertLocation(ParameterLocation? location)
	{
		return location switch
		{
			ParameterLocation.Path => UrlParamLocations.Path,
			ParameterLocation.Query => UrlParamLocations.Query,
			ParameterLocation.Cookie => UrlParamLocations.Cookie,
			ParameterLocation.Header => UrlParamLocations.Header,
			_ => throw new ArgumentException($"Unexpected OpenApi ParameterLocation value '{location}'", nameof(location))
		};
	}

	Property MakeProperty(string name, OpenApiSchema schema, bool required)
	{
		var qualifiedName = new XmlQualifiedName(name);
		return new Property(qualifiedName, MakeType(schema), false) { Required = required, Nullable = schema.Nullable, Description = schema.Description };
	}

	ActualPropertyType MakeActualType(OpenApiSchema schema, bool isArray = false)
	{
		string? refId = schema.Reference?.Id;
		var xmlRefId = !string.IsNullOrEmpty(refId) ? MakeTypeName(refId) : null;

		// check is redundant in most case but still needed when MakeActualType is called below in case - schema.Type == "array" -
		if (xmlRefId != null && !ExistSchema(xmlRefId))
			throw new Exception($"Schema reference '{refId} does not exist");

		if (schema.Type == "object")
		{
			var properties = schema.Properties;
			var required = schema.Required;

			var props = properties.Select(p => MakeProperty(p.Key, p.Value, required.Contains(p.Key)));

			return new ObjectPropertyType(props) { IsArray = isArray, QualifiedName = xmlRefId, Description = schema.Description };
		}
		else if (schema.Enum?.Count > 0)
		{
			var values = schema.Enum.Select(e => GetEnumValue(e));
			return OpenApiHelper.ParsePrimitiveType(schema.Type) == OpenApiPrimitiveTypes.Integer ?
				new IntEnumPropertyType(values.Select(v => int.Parse(v))) { IsArray = isArray, QualifiedName = xmlRefId, Description = schema.Description } :
				new StringEnumPropertyType(values) { IsArray = isArray, QualifiedName = xmlRefId, Description = schema.Description };
		}
		else if (schema.Type == "array" && schema.Items != null)
		{
			return MakeActualType(schema.Items, true);
		}
		if (schema.OneOf != null && schema.OneOf.Count > 0)
		{
			List<PropertyType> types = [];

			foreach (var choice in schema.OneOf)
				types.Add(MakeType(choice));

			return new OneOfPropertyType(types) { IsArray = isArray, QualifiedName = xmlRefId, Description = schema.Description };
		}
		else if (schema.AllOf != null && schema.AllOf.Count > 0)
		{
			return MergeSchemas(schema.AllOf, isArray, xmlRefId);
		}
		else
		{
			return GetBasicPropertyType(schema, isArray, xmlRefId);
		}
	}

	PropertyType MakeType(OpenApiSchema schema)
	{
		string? refId = schema.Reference?.Id;
		bool isArray = false;

		if (refId == null && schema.Type == "array" && schema.Items?.Reference?.Id != null)
		{
			refId = schema.Items.Reference.Id;
			isArray = true;
		}

		if (refId != null)
		{
			if (!ReferenceTypes.TryGetValue(refId, out ReferencePropertyType? referencePropertyType))
				throw new Exception($"Schema reference '{refId} does not exist");

			if (isArray)
			{
				if (ArrayReferenceTypes.TryGetValue(refId, out ReferencePropertyType? arrayRefType))
				{
					return arrayRefType;
				}
				else
				{
					var refArray = referencePropertyType with { IsArray = true };
					ArrayReferenceTypes[refId] = refArray;
					return refArray;
				}
			}
			else
				return referencePropertyType;
		}
		else
			return MakeActualType(schema, false);
	}

	static BasicPropertyType GetBasicPropertyType(OpenApiSchema schema, bool isArray, XmlQualifiedName? referenceId)

	{
		var primitiveType = OpenApiHelper.ParsePrimitiveType(schema.Type);

		SimpleTypes? simpleType;

		if (primitiveType == OpenApiPrimitiveTypes.String)
		{
			simpleType = schema.Format switch
			{
				"date-time" => SimpleTypes.DateTime,
				"date" => SimpleTypes.Date,
				//"binary" => SimpleTypes.Binary, // TODO handle binary
				_ => null
			};

			if (simpleType == null)
			{
				if (schema.Format == "byte")
					return new StringBase64PropertyType() { IsArray = isArray, QualifiedName = referenceId, Description = schema.Description };
				else
					return new StringPropertyType() { IsArray = isArray, QualifiedName = referenceId, Description = schema.Description, MinLength = schema.MinLength, MaxLength = schema.MaxLength, Pattern = schema.Pattern };
			}
		}
		else
		{
			simpleType = primitiveType switch
			{
				OpenApiPrimitiveTypes.Integer => SimpleTypes.Integer,
				OpenApiPrimitiveTypes.Boolean => SimpleTypes.Boolean,
				OpenApiPrimitiveTypes.Number => SimpleTypes.Number,
				_ => throw new ArgumentException($"Unexpected OpenApiPrimitiveTypes '{primitiveType}'")
			};
		}

		//TODO? handle Number formats as hint : 'float', 'double', 'decimal' 
		//TODO? handle String formats as hint : 'password', 'guid'
		return new SimplePropertyType(simpleType.Value) { IsArray = isArray, QualifiedName = referenceId, Description = schema.Description };
	}

	static string GetEnumValue(IOpenApiAny e)
	{
		ArgumentNullException.ThrowIfNull(e);

		if (e is OpenApiString s)
			return s.Value;

		if (e is OpenApiInteger i)
			return i.Value.ToString();

		return e.ToString()!;
	}

	ObjectPropertyType MergeSchemas(IList<OpenApiSchema> schemas, bool isArray, XmlQualifiedName? qualifiedName)
	{
		List<Property> properties = [];
		XmlQualifiedName? baseTypeName = null;

		foreach (var schema in schemas)
		{
			var type = MakeActualType(schema, false);

			if (baseTypeName == null && type.QualifiedName != null)
				baseTypeName = CheckTypeName(type.QualifiedName);

			if (type is ObjectPropertyType objectPropertyType)
				properties.AddRange(objectPropertyType.Properties);
			else
			{
				throw new Exception($"Invalid property '{type}' in AllOf");
			}
		}

		return new ObjectPropertyType(properties) { QualifiedName = qualifiedName, BaseTypeName = baseTypeName, IsArray = isArray };
	}

}
