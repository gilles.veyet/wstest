﻿using System.Collections.Specialized;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Web;

namespace WSTest;


[method: Newtonsoft.Json.JsonConstructor]
partial class RestService(RestServiceDescriptor descriptor) : HttpService
{
	public override RestServiceDescriptor Descriptor { get; } = descriptor;
	public override List<RestMethod> Methods { get; } = [];

	public override LoadServiceResult Load(string serviceDefinitionContentDirectory)
	{
		var loader = new RestServiceLoader(this, serviceDefinitionContentDirectory);
		var result = loader.Load();
		Methods.Sort();

		return result;
	}

	public override RestServiceRunner CreateRunner() => new(this);

	public override List<Value> GetValueFromContent(HttpServiceMethod method, string content, string? url)
	{
		RestMethod restMethod = (RestMethod)method;
		List<Value> values = method.UrlParams.Select(p => ValueFactory.MakeValueFromProperty(p)).ToList();

		if (!string.IsNullOrEmpty(url))
		{
			Uri uri = new(url); // handle escape chars so it is not needed to call UnescapeDataString on AbsolutePath & Query.

			string endPoint = restMethod.EndPointPath;
			int count = endPoint.Split('/').Length - 1;
			string path = '/' + string.Join('/', uri.AbsolutePath.Split('/').TakeLast(count));  // assume that EndPointPath always starts with '/'.

			var pathParams = ExtractParameters(endPoint, path);

			NameValueCollection queryParams = HttpUtility.ParseQueryString(uri.Query);

			foreach (var value in values)
			{
				UrlParam param = (UrlParam)value.Property;

				switch (param.Location)
				{
					case UrlParamLocations.Path:
						{
							if (pathParams.TryGetValue(param.Name, out string? str))
							{
								if (value is BasicValue basicValue)
								{
									basicValue.ParseString(str);
								}
								else if (value is ArrayValue arrayValue)
								{
									var texts = str.Split(',');
									arrayValue.ParseStrings(texts);
								}
								else
								{
									throw new Exception($"Type {value.ActualType} is not handled in path parameters");
								}
							}
						}
						break;

					case UrlParamLocations.Query:
						{
							string? str = queryParams[param.Name];
							if (str != null)
							{
								if (value is BasicValue basicValue)
								{
									basicValue.ParseString(str);
								}
								else if (value is ArrayValue arrayValue)
								{
									var texts = str.Split(',');
									arrayValue.ParseStrings(texts);
								}
								else
								{
									throw new Exception($"Type {value.ActualType} is not handled in query parameters");
								}
							}
						}
						break;
				}
			}
		}

		if (method.Request != null)
		{
			using JsonDocument document = JsonDocument.Parse(content, new JsonDocumentOptions { CommentHandling = JsonCommentHandling.Skip });
			JsonElement root = document.RootElement;
			var value = ValueFactory.MakeValueFromProperty(method.Request);
			value.ParseJson(root);

			values.Add(value);
		}

		return values;
	}


	static Dictionary<string, string> ExtractParameters(string template, string path)
	{
		var result = new Dictionary<string, string>();
		string[] parameters = RegexExtractPathParameters().Matches(template).Select(m => m.Groups[1].Value).ToArray();
		string pattern = "^" + RegexExtractPathParameters().Replace(template, @"([^/]+)") + "$";
		var match = Regex.Match(path, pattern);

		if (match.Success)
		{
			for (int i = 1; i < match.Groups.Count; ++i)
			{
				result[parameters[i - 1]] = match.Groups[i].Value;
			}
		}

		return result;
	}

	[GeneratedRegex(@"\{([^}]+)\}")]
	private static partial Regex RegexExtractPathParameters();

}
