﻿using WSTest.Helpers;

namespace WSTest;


internal class RestMethodInfo(RestMethod method, ILookupReferenceType lookup) : HttpMethodInfo(lookup)
{
	protected override RestMethod Method => method;
	protected override string MethodMoreInfo => $"{Method.HttpMethod} {Method.EndPointPath}";

	protected override IEnumerable<HtmlTableRow> DescribeResponses()
	{
		List<HtmlTableRow> result = [];

		foreach (var kv in Method.Responses)
		{
			var response = kv.Value;
			var ptype = response.PropertyType;
			string key = kv.Key == 0 ? "default" : kv.Key.ToString();

			if (ptype != null)
			{
				result.Add(new HtmlTableRow(new HtmlTableData(new HtmlText(key)), new HtmlTableData(DescribeTypeWithReference(ptype, ptype.IsArray))));
			}
			else
			{
				result.Add(new HtmlTableRow(new HtmlTableData(new HtmlText(key)), new HtmlTableData(new HtmlText(response.Description))));
			}
		}

		return result;
	}



}
