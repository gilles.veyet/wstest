﻿namespace WSTest.Rest
{
	internal record RestResponse
	{
		public required string Description { get; init; }
		public PropertyType? PropertyType { get; init; }
	}
}
