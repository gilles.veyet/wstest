﻿using Microsoft.OpenApi.Models;
using WSTest.Rest;

namespace WSTest
{
	// Unused
	//public enum RestResponseTypes
	//{
	//	Void,
	//	Json,
	//	PrimitiveDataType,
	//	Binary,
	//	Base64
	//}

	class RestMethod : HttpServiceMethod
	{
		public required string EndPointPath { get; init; }
		public OperationType HttpMethod { get; init; }

		public Dictionary<int, RestResponse> Responses { get; } = [];

		[Newtonsoft.Json.JsonConstructor]
		protected RestMethod() { }

		public RestMethod(string name) : base(name)
		{ }

		// Unused
		//RestResponseTypes GetRestResponseType(int httpStatus)
		//{
		//	if (Responses.TryGetValue(httpStatus, out var type) || Responses.TryGetValue(0, out type))
		//	{
		//		if (type.IsArray || type is ObjectPropertyType || type is ReferencePropertyType)
		//			return RestResponseTypes.Json;
		//		else if (type is SimplePropertyType simplePropertyType)
		//		{
		//			return simplePropertyType.Type switch
		//			{
		//				SimpleTypes.Base64 => RestResponseTypes.Base64,
		//				SimpleTypes.Binary => RestResponseTypes.Binary,
		//				_ => RestResponseTypes.PrimitiveDataType
		//			};
		//		}
		//		else // enums
		//		{
		//			return RestResponseTypes.PrimitiveDataType;
		//		}
		//	}
		//	else
		//		return RestResponseTypes.Void;
		//}

		public override string DescribeMethod(ILookupReferenceType lookup)
		{
			return new RestMethodInfo(this, lookup).DescribeMethod();
		}

		public override string ToString()
		{
			return base.ToString();
		}

	}
}
