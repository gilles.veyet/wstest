﻿using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using WSTest.Helpers;

namespace WSTest
{
	internal class PomReader
	{
		#region Instance 

		readonly XDocument XDoc;
		readonly XmlNamespaceManager NamespaceManager;

		public PomReader(string path)
		{
			using var reader = XmlReader.Create(path);
			NamespaceManager = new XmlNamespaceManager(reader.NameTable);
			NamespaceManager.AddNamespace("ns", "http://maven.apache.org/POM/4.0.0");

			XDoc = XDocument.Load(reader);
		}

		public string? GetArtifactId()
		{
			return XDoc.XPathSelectElement("//ns:project/ns:artifactId", NamespaceManager)?.Value;
		}

		public string? GetVersion()
		{
			return XDoc.XPathSelectElement("//ns:project/ns:version", NamespaceManager)?.Value;
		}

		#endregion

		#region Static methods

		public static PomReader? GetPomReaderInParentDirectories(string directory)
		{
			string? path = SearchPomXmlInParentDirectories(directory);
			if (path == null)
				return null;

			try
			{
				return new PomReader(path);
			}
			catch (Exception ex)
			{
				TraceHelper.WriteException($"Error ReadPackageInfoFromPomXml '{path}'", ex);
				return null;
			}
		}

		public static string? SearchPomXmlInParentDirectories(string directory)
		{
			const string POM_XML = "pom.xml";

			if (!Directory.Exists(directory))
				return null;

			var dir = new DirectoryInfo(directory);

			for (int i = 0; i < 10; i++)
			{
				string path = Path.Combine(dir.FullName, POM_XML);
				if (File.Exists(path))
					return path;

				if (dir.Parent == null)
					break;

				dir = dir.Parent;
			}

			return null;
		}

		#endregion

	}
}
