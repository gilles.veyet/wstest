﻿using Newtonsoft.Json;
using WSTest.Helpers;

namespace WSTest
{
	internal static class DescriptorHelper
	{
		const string EXT_REST = ".openapi.wstest.json";
		const string EXT_SOAP = ".wsdl.wstest.json";

		public static HttpServiceDescriptor MakeNewDescriptor(ServiceTypes type, string id, String name, string category, string serviceDefinitionPath)
		{
			return type switch
			{
				ServiceTypes.Soap => new SoapServiceDescriptor() { Id = id, Name = name, Category = category, ServiceDefinitionPath = serviceDefinitionPath },
				ServiceTypes.Rest => new RestServiceDescriptor() { Id = id, Name = name, Category = category, ServiceDefinitionPath = serviceDefinitionPath },
				_ => throw new Exception($"{nameof(MakeNewDescriptor)}: Invalid descriptor type '{type}'")
			};
		}

		public static string GetDescriptorFileExtension(ServiceTypes type)
		{
			return type switch
			{
				ServiceTypes.Soap => EXT_SOAP,
				ServiceTypes.Rest => EXT_REST,
				_ => throw new Exception($"{nameof(GetDescriptorFileExtension)}: Invalid descriptor type '{type}'")
			};
		}

		public static string GetDescriptorFileExtension(ServiceDescriptor descriptor)
		{
			return GetDescriptorFileExtension(descriptor.ServiceType);
		}

		public static string GetServiceDefinitionFileFilter(ServiceTypes type)
		{
			return '*' + GetServiceDefinitionFileExtension(type);
		}

		public static string GetServiceDefinitionFileExtension(ServiceTypes type)
		{
			return type switch
			{
				ServiceTypes.Soap => ".wsdl",
				ServiceTypes.Rest => ".yaml",
				_ => throw new Exception($"{nameof(GetServiceDefinitionFileFilter)}: Invalid descriptor type '{type}'")
			};
		}


		public static string GetServiceDefinitionFileDialogFilter(ServiceTypes type)
		{
			return type switch
			{
				ServiceTypes.Soap => "WSDL (*.wsdl)|*.wsdl",
				ServiceTypes.Rest => "Open API (*.yaml)|*.yaml",
				_ => throw new Exception($"{nameof(GetServiceDefinitionFileFilter)}: Invalid descriptor type '{type}'")
			};
		}


		static JsonSerializerSettings SerializerSettings => new()
		{
			NullValueHandling = NullValueHandling.Ignore,
			Converters = [new JSonXmlQualifiedNameConverter()]
		};

		public static void SaveDescriptor(ServiceDescriptor descriptor, string path)
		{
			descriptor.ActualPath = path;
			string json = JsonConvert.SerializeObject(descriptor, Formatting.Indented, SerializerSettings);
			File.WriteAllText(path, json);
		}

		static T LoadServiceDescriptor<T>(string filePath) where T : ServiceDescriptor
		{
			try
			{
				string json = File.ReadAllText(filePath);
				var svc = JsonConvert.DeserializeObject<T>(json, SerializerSettings) ?? throw new Exception($"Error loading {typeof(T)} file:'{filePath}' - Deserialize did not return any data");

				TraceHelper.WriteVerbose($"Loaded service descriptor '{svc.ToStringExt()}'");

				svc.ActualPath = filePath;
				return svc;
			}
			catch (Exception ex)
			{
				throw new Exception($"Error loading {typeof(T)} file:'{filePath}' - {ex.Message}");
			}
		}

		public static HttpServiceDescriptor LoadSingle(string filePath)
		{
			if (filePath.EndsWith(EXT_REST, StringComparison.OrdinalIgnoreCase))
				return LoadServiceDescriptor<RestServiceDescriptor>(filePath);

			if (filePath.EndsWith(EXT_SOAP, StringComparison.OrdinalIgnoreCase))
				return LoadServiceDescriptor<SoapServiceDescriptor>(filePath);

			throw new NotImplementedException($"Error trying to load service descriptor '{filePath}' : unknown extension");
		}

		public static List<HttpServiceDescriptor> LoadAllInDirectory(string directory, bool recurse)
		{
			var searchOption = recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

			List<HttpServiceDescriptor> descriptors = [];

			if (Directory.Exists(directory))
			{
				foreach (var file in Directory.GetFiles(directory, MakePatternFromExtension(EXT_SOAP), searchOption))
					descriptors.Add(LoadServiceDescriptor<SoapServiceDescriptor>(file));

				foreach (var file in Directory.GetFiles(directory, MakePatternFromExtension(EXT_REST), searchOption))
					descriptors.Add(LoadServiceDescriptor<RestServiceDescriptor>(file));
			}

			return descriptors;
		}

		private static string MakePatternFromExtension(string ext)
		{
			return $"*{ext}";
		}
	}
}
