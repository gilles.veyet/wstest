﻿using System.Xml;
using WSTest.Helpers;

namespace WSTest;

interface ILookupReferenceType
{
	ActualPropertyType GetActualType(XmlQualifiedName referencePropertyId);
}

record LoadServiceResult
{
	public List<string> DefinitionFiles { get; } = [];
}

abstract class Service : ILookupReferenceType, IComparable<Service>
{
	[Newtonsoft.Json.JsonIgnore]
	public string Name => Descriptor.Name;

	[Newtonsoft.Json.JsonIgnore]
	public string Id => Descriptor.Id;

	[Newtonsoft.Json.JsonIgnore]
	public string Category => Descriptor.Category;

	[Newtonsoft.Json.JsonIgnore]
	public string PackageName => Descriptor.PackageName;

	[Newtonsoft.Json.JsonIgnore]
	public string PackageNameAndVersion => Descriptor.PackageNameAndVersion;

	public abstract ServiceDescriptor Descriptor { get; }
	public abstract IEnumerable<ServiceMethod> Methods { get; }

	public Dictionary<string, ActualPropertyType> ActualTypes { get; set; } = [];
	public List<ReferencePropertyType> ReferenceTypes { get; set; } = [];

	public abstract LoadServiceResult Load(string serviceDefinitionContentDirectory);

	public void UpdateActualTypes()
	{
		foreach (var ptype in ReferenceTypes)
		{
			ptype._ActualType = ActualTypes[ptype.RefQualifiedName.ToString()];
		}
	}

	public ActualPropertyType GetActualType(XmlQualifiedName referenceId)
	{
		return ActualTypes[referenceId.ToString()];
	}

	public override string ToString()
	{
		return Descriptor.ToString();
	}

	public int CompareTo(Service? other)
	{
		return Descriptor.CompareTo(other?.Descriptor);
	}

	public abstract ServiceRunner CreateRunner();
}

abstract class HttpService : Service
{
	public override abstract HttpServiceDescriptor Descriptor { get; }
	public override abstract IEnumerable<HttpServiceMethod> Methods { get; }

	public IEnumerable<HttpServiceMethod> GetMethodsWithoutParameters()
	{
		return Methods.Where(m => m.UrlParams.Count == 0 && (m.Request == null || m.Request.ActualType is ObjectPropertyType objectPropertyType && objectPropertyType.Properties.Count == 0));
	}

	[Newtonsoft.Json.JsonConstructor]
#pragma warning disable IDE0290 // Use primary constructor
	public HttpService() { }
#pragma warning restore IDE0290 // Use primary constructor

	public abstract override HttpServiceRunner CreateRunner();

	public abstract List<Value> GetValueFromContent(HttpServiceMethod method, string content, string? url);

	public HttpServiceMethod? GetCheckMethod()
	{
		string? checkMethodName = Descriptor.CheckMethod;
		if (string.IsNullOrEmpty(checkMethodName))
			return null;

		return Methods.FirstOrDefault(m => m.Name == checkMethodName);
	}

	public async Task<HttpResult> RunMethodWithoutParameters(HttpServiceMethod method, Profile profile, CancellationToken cancellationToken)
	{
		var values = method.AllProperties.Select(p => ValueFactory.MakeValueFromProperty(p)).ToList();
		HttpServiceRunner runner = CreateRunner();

		var sendData = runner.PrepareMethod(method, profile, values);
		return await HttpClientHelper.SendAsync(sendData, cancellationToken);
	}
}