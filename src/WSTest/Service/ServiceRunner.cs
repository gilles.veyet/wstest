﻿using WSTest.Helpers;

namespace WSTest
{

	internal abstract class ServiceRunner
	{
		public abstract Service _Service { get; }

		public ServiceRunner()
		{ }
	}

	internal abstract class HttpServiceRunner : ServiceRunner
	{
		public abstract override HttpService _Service { get; }

		public HttpServiceRunner() : base()
		{ }

		public abstract HttpSendData PrepareMethod(HttpServiceMethod method, Profile profile, IReadOnlyCollection<Value> values);

		public abstract HttpSendData PrepareMethod(HttpServiceMethod method, Profile profile, string content, string url);


		protected HttpClientSecurityOptions GetSecurityOptions(Profile profile)
		{
			if (_Service.Descriptor.UseBasicAuthentication)
			{
				return new HttpClientSecurityOptions() { BasicAuthentication = profile.InterpolateStringWithMappings(_Service.Descriptor.BasicAuthenticationString) };
			}
			else
				return HttpClientSecurityOptions.None;
		}
	}
}
