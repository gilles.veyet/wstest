﻿using System.Reflection;
using System.Text;
using System.Xml;
using WSTest.Helpers;

namespace WSTest
{
	internal abstract class HttpMethodInfo(ILookupReferenceType lookup)
	{
		const string CLASS_HEADER = "header";
		const string CLASS_DESCRIPTION = "description";
		const string CLASS_DESCRIBE_TABLE = "describe-table";

		protected abstract HttpServiceMethod Method { get; }
		protected abstract string MethodMoreInfo { get; }
		protected abstract IEnumerable<HtmlTableRow> DescribeResponses();

		protected ILookupReferenceType Lookup { get; } = lookup;

		private readonly List<Property> RootProperties = [];
		private Queue<ActualPropertyType> QueueReferenceTypes = new();
		private HashSet<XmlQualifiedName> SetReferenceIds = [];

		public string DescribeMethod()
		{
			this.QueueReferenceTypes = new();
			this.SetReferenceIds = [];

			var methodTable = MakeTableWithHeader(new HtmlText(Method.Name), Method.Description, [new HtmlTableRow(new HtmlTableData(new HtmlText(MethodMoreInfo)))]);

			HtmlCollection coll = [methodTable];

			if (Method.UrlParams.Count > 0)
			{
				coll.Add(MakeTableWithHeader(new HtmlText("Params"), null, Method.UrlParams.Select(p => DescribeRootProperty(p))));
			}

			if (Method.Request != null)
			{
				coll.Add(MakeTableWithHeader(new HtmlText("Request"), IsDescribeRootPropertyWithoutLink(Method.Request) ? Method.Request.Description : null, [DescribeRootProperty(Method.Request)]));
			}

			var responses = DescribeResponses();
			if (responses.Any())
			{
				coll.Add(MakeTableWithHeader(new HtmlText("Response(s)"), null, responses));
			}

			var main = new HtmlDiv(coll);
			main.AddChildren(DescribeRootProperties());
			main.AddChildren(DescribeReferencedTypes());

			HtmlBody body = new(main);
			return $"<html><head><style>{Properties.Resources.SchemaInfo}</style></head>{body.ToHtml()}</html>";
		}

		protected static HtmlTable MakeTableWithHeader(Helpers.HtmlElement header, string? description = null, IEnumerable<HtmlTableRow>? dataRows = null)
		{
			List<HtmlTableRow> rows = !string.IsNullOrEmpty(description) ? [MakeRowDescription(description, 2)] : [];

			if (dataRows != null)
				rows.AddRange(dataRows);

			var rowHeader = new HtmlTableHead(new HtmlTableRow(new HtmlTableHeader(header) { ColSpan = 2 }).AddClass(CLASS_HEADER));

			if (rows != null)
			{
				return new HtmlTable(rowHeader, new HtmlTableBody(rows)).AddClass(CLASS_DESCRIBE_TABLE);
			}
			else
				return new HtmlTable(rowHeader);
		}

		protected static HtmlTag MakeDetailsSection(Helpers.HtmlElement header, string? description, IEnumerable<HtmlTableRow>? dataRows)
		{
			List<HtmlTableRow> rows = !string.IsNullOrEmpty(description) ? [MakeRowDescription(description, 2)] : [];

			if (dataRows != null)
				rows.AddRange(dataRows);

			if (rows.Count > 0)
			{
				var summary = new HtmlSummary(header).AddClass(CLASS_HEADER);
				return new HtmlDetails(summary, new HtmlTable(new HtmlTableBody(rows)).AddClass(CLASS_DESCRIBE_TABLE)) { IsOpen = rows.Count < 20 };
			}
			else
				return MakeTableWithHeader(header);
		}


		/// <summary>
		///  IsDescribeRootPropertyWithoutLink: True if we describe RootProperty immedietely. False if we add a link.
		///     True when property type has a (qualified) name or type is a simple type.
		/// </summary>
		static bool IsDescribeRootPropertyWithoutLink(Property property) => property.ActualType.QualifiedName != null || property.ActualType is BasicPropertyType;

		protected HtmlTableRow DescribeRootProperty(Property property)
		{
			if (IsDescribeRootPropertyWithoutLink(property))
			{
				return DescribeProperty(property);
			}
			else
			{
				// so only add to RootProperties if complex type or property type does not have a (qualified) name.
				RootProperties.Add(property);
				string name = property.Name;

				if (property.IsArray)
					name += "[]";

				return new HtmlTableRow(new HtmlTableData(new HtmlText(name)), new HtmlTableData(HtmlLink.MakeReference(property.QualifiedName).AddText(name)));
			}
		}

		protected HtmlCollection DescribeRootProperties()
		{
			if (RootProperties.Count == 0)
				return [];

			HtmlCollection collection = [];

			foreach (var property in RootProperties)
			{
				var refId = property.QualifiedName;
				collection.Add(HtmlLink.MakeAnchor(refId));

				(var refInfo, var typeElements) = DescribePropertyType(property.ActualType);
				collection.Add(MakeTableWithHeader(new HtmlText($"{property.Name} : {refInfo}"), property.Description, typeElements));
			}

			return collection;
		}

		static HtmlTableRow MakeRowDescription(string description, int? colSpan = null)
		{
			var div = new HtmlDiv(new HtmlRaw(description)).AddClass(CLASS_DESCRIPTION);
			return new HtmlTableRow(new HtmlTableData(div) { ColSpan = colSpan });
		}

		protected HtmlTableRow DescribeProperty(Property property)
		{
			var type = DescribeTypeWithReference(property.ActualType, property.IsArray || property.Type.IsArray);

			if (property.ActualType is not ChoicePropertyType)
			{
				StringBuilder sb = new();
				sb.Append(property.Name);

				if (property.Required)
					sb.Append('*');

				if (property.Nullable)
					sb.Append('?');

				return new HtmlTableRow(new HtmlTableData(new HtmlText(sb.ToString())), new HtmlTableData(type));
			}
			else if (type is HtmlLink)
			{
				return new HtmlTableRow(new HtmlTableData(new HtmlText(property.Name)), new HtmlTableData(type));
			}
			else
			{
				return new HtmlTableRow(new HtmlTableData(type.Children.Take(1)), new HtmlTableData(type.Children.Skip(1)));
			}
		}

		protected HtmlCollection DescribeReferencedTypes()
		{
			if (QueueReferenceTypes.Count == 0)
				return [];

			HtmlCollection collection = [];

			for (; QueueReferenceTypes.Count > 0;)
			{
				var propertyType = QueueReferenceTypes.Dequeue();
				collection.Add(HtmlLink.MakeAnchor(propertyType.QualifiedName!));
				collection.Add(DescribeType(propertyType));
			}

			return collection;
		}

		protected HtmlTag DescribeType(ActualPropertyType propertyType)
		{
			StringBuilder sbName = new(propertyType.Name);

			if (propertyType.IsAbstract)
				sbName.Append(" (Abstract)");

			HtmlCollection listName = [];

			listName.Add(new HtmlText(sbName.ToString()));

			if (propertyType.BaseTypeName != null)
			{
				listName.Add(new HtmlText(" \u2190 ")); // left arrow
				listName.Add(HtmlLink.MakeReference(propertyType.BaseTypeName).AddText(propertyType.BaseTypeName.Name));
			}

			if (propertyType.DerivedTypeNames != null)
			{
				listName.Add(new HtmlText(" \u2192"));  // right arrow

				foreach (var derivedType in propertyType.DerivedTypeNames)
				{
					listName.Add(HtmlSpace.Instance);
					listName.Add(HtmlLink.MakeReference(derivedType).AddText(derivedType.Name));
				}
			}

			(var refInfo, var typeElements) = DescribePropertyType(propertyType);

			listName.Add(new HtmlText($" : {refInfo}"));
			return MakeDetailsSection(new HtmlDiv(listName), propertyType.Description, typeElements);
		}

		protected (string, IEnumerable<HtmlTableRow>?) DescribePropertyType(ActualPropertyType propertyType)
		{
			(var refInfo, var typeElements) = propertyType switch
			{
				ObjectPropertyType objectPropertyType => ("Object", objectPropertyType.Properties.Select(p => DescribeProperty(p))),
				OneOfPropertyType oneOfPropertyType => ("One of", oneOfPropertyType.PropertyTypes.Select(ptype => new HtmlTableRow(new HtmlTableData(DescribeTypeWithReference(ptype, false))))),
				ChoicePropertyType choicePropertyType => ("Choice", choicePropertyType.Properties.Select(p => DescribeProperty(p))),
				StringPropertyType stringPropertyType => ("String" + GetStringPropertyTypeDetail(stringPropertyType), null),
				StringBase64PropertyType stringBase64PropertyType => ("String (base64)", null),
				StringEnumPropertyType stringEnumPropertyType => ("Enum (string)", stringEnumPropertyType.Values.Select(v => new HtmlTableRow(new HtmlTableData((new HtmlText(v)))))),
				IntEnumPropertyType intEnumPropertyType => ("Enum (string)", intEnumPropertyType.Values.Select(v => new HtmlTableRow(new HtmlTableData((new HtmlText(v.ToString())))))),
				SimplePropertyType simplePropertyType => (simplePropertyType.Type.ToString(), null),
				_ => throw new Exception($"{MethodBase.GetCurrentMethod()}: type '{propertyType}' is not handled"),
			};

			return (MakeArrayLabel(refInfo, propertyType.IsArray), typeElements);
		}

		private static string MakeArrayLabel(string label, bool isArray)
		{
			return isArray ? "Array of " + label : label;
		}

		static string GetStringPropertyTypeDetail(StringPropertyType stringPropertyType)
		{
			StringBuilder sb = new();

			if (stringPropertyType.MinLength != null)
				sb.Append($" MinLength:{stringPropertyType.MinLength}");

			if (stringPropertyType.MaxLength != null)
				sb.Append($" MaxLength:{stringPropertyType.MaxLength}");

			if (!string.IsNullOrEmpty(stringPropertyType.Pattern))
				sb.Append($" Pattern:{stringPropertyType.Pattern}");

			return sb.ToString();
		}

		protected HtmlTag DescribeTypeWithReference(PropertyType propertyType, bool isPropertyArray)
		{
			if (propertyType.QualifiedName != null)
			{
				CheckReferencePropertyType(propertyType.QualifiedName);
			}

			string suffix = isPropertyArray || propertyType.ActualType.IsArray ? "[]" : string.Empty;
			var qualifiedName = propertyType.QualifiedName;

			if (qualifiedName != null)
			{
				return HtmlLink.MakeReference(qualifiedName).AddText(qualifiedName.Name + suffix);
			}
			else
			{
				if (propertyType is SimplePropertyType simplePropertyType)
				{
					return new HtmlSpan(simplePropertyType.Type.ToString() + suffix);
				}
				else if (propertyType is StringPropertyType stringPropertyType)
				{
					return new HtmlSpan("String" + suffix + GetStringPropertyTypeDetail(stringPropertyType));
				}
				else
				{
					// Used for: 
					//  - Choice / array of choice in WSTestSample / Foo / CreateFoo
					//  - StringEnumPropertyType in WSTestSample / Swagger / AddPet
					//  - DCM GetTraceConfig
					//  - Array of Object in DTA / dta-interface-back-office / ContractorInventoryService / GetPermissionTemplate / EnumPropertyDefinition / EnumItem

					// QualifiedName is null so it is an ActualPropertyType (cannot be a ReferencePropertyName).
					(var refInfo, var typeElements) = DescribePropertyType((ActualPropertyType)propertyType);

					if (isPropertyArray && !propertyType.ActualType.IsArray)
						refInfo = MakeArrayLabel(refInfo, true);

					if (typeElements != null)
						return new HtmlDiv(new HtmlText(refInfo), new HtmlTable(new HtmlBody(typeElements)));
					else
						return new HtmlSpan(refInfo);
				}
			}
		}

		protected void CheckReferencePropertyType(in XmlQualifiedName qualifiedName)
		{
			if (SetReferenceIds.Contains(qualifiedName))
				return;

			SetReferenceIds.Add(qualifiedName);

			var actualPropertyType = Lookup.GetActualType(qualifiedName);

			QueueReferenceTypes.Enqueue(actualPropertyType);

			if (actualPropertyType.BaseTypeName != null)
			{
				CheckReferencePropertyType(actualPropertyType.BaseTypeName);
			}

			if (actualPropertyType.DerivedTypeNames != null)
			{
				foreach (var derived in actualPropertyType.DerivedTypeNames)
					CheckReferencePropertyType(derived);
			}
		}

	}

}
