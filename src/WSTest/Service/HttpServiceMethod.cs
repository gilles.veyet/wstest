﻿namespace WSTest;


abstract class HttpServiceMethod : ServiceMethod
{
	public List<UrlParam> UrlParams { get; } = [];

	public RequestProperty? Request { get; set; }

	[Newtonsoft.Json.JsonConstructor]
	protected HttpServiceMethod() { }

	public HttpServiceMethod(string name) : base(name)
	{ }

	[Newtonsoft.Json.JsonIgnore]
	public override IReadOnlyCollection<Property> AllProperties
	{
		get
		{
			List<Property> list = new(UrlParams);

			if (Request != null)
			{
				list.Add(Request);
			}

			return list;
		}
	}

	public override string ToString()
	{
		return base.ToString();
	}
}
