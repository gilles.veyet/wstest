﻿using Newtonsoft.Json;
using System.Reflection;

namespace WSTest
{
	internal enum PackageImportType
	{
		Single,     // Path or url.
		Directory,  // Directory.
		Zip,        // Path or url.		
	}

	internal record PackageImportDefinition
	{
		/// <summary>
		/// Path from which the import definition was loaded.
		/// </summary>
		[Newtonsoft.Json.JsonIgnore]
		public string? ActualPath { get; set; }

		[Newtonsoft.Json.JsonIgnore]
		public string? ActualDirectory => Path.GetDirectoryName(ActualPath);

		public PackageImportType ImportType { get; set; } = PackageImportType.Directory;

		public required string ServiceCategory { get; set; }

		public required string PackageName { get; set; }

		/// <summary>
		/// Import directory.
		/// </summary>
		public required string ImportPath { get; set; }

		/// <summary>
		/// Package version.
		/// </summary>
		public string? PackageVersion { get; set; }

		public DateTimeOffset SaveDate { get; set; }

		[Newtonsoft.Json.JsonIgnore]
		public string PackageNameAndVersion => !string.IsNullOrEmpty(PackageVersion) ? $"{PackageName} ({PackageVersion})" : PackageName;

		public override string ToString() => $"{ServiceCategory} {PackageNameAndVersion}";

		#region Serialization

		const string FILENAME = nameof(PackageImportDefinition) + ".json";

		static string MakePath(string directory) => Path.Combine(directory, FILENAME);

		public void Save(string directory)
		{
			string path = MakePath(directory);

			SaveDate = DateTimeOffset.Now;
			ActualPath = path;

			string json = JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
			File.WriteAllText(path, json);
		}

		public static List<PackageImportDefinition> LoadAllInDirectory(string directory, bool recurse)
		{
			var searchOption = recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

			List<PackageImportDefinition> items = [];

			foreach (var file in Directory.GetFiles(directory, FILENAME, searchOption))
				items.Add(Load(file));

			return items;
		}

		public static PackageImportDefinition? FindInDirectory(string directory)
		{
			string path = MakePath(directory);
			return File.Exists(path) ? Load(path) : null;
		}

		public static PackageImportDefinition Load(string path)
		{
			string json = File.ReadAllText(path);
			var def = JsonConvert.DeserializeObject<PackageImportDefinition>(json) ?? throw new Exception($"{MethodBase.GetCurrentMethod()}: Invalid file '{path}");
			def.ActualPath = path;
			return def;
		}

		#endregion
	}
}
