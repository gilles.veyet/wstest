﻿using System.Xml;

namespace WSTest
{
	internal enum ServiceTypes
	{
		Rest,
		Soap,
	}

	public static class WellKnownProperties
	{
		public const string ClientName = "ClientName";
		public const string WorkingClientName = "WorkingClientName";
		public const string UserName = "UserName";
		public const string Password = "Password";
		public const string SalesChannelName = "SalesChannelName";
		public const string PointOfSaleName = "PointOfSaleName";
		public const string PartnerSystem = "PartnerSystem";            // for DCM.

		private static readonly string[] _PreferredOrder = [ClientName, WorkingClientName, UserName, Password, SalesChannelName, PointOfSaleName, PartnerSystem];
		public static string[] PreferredOrder => _PreferredOrder;
	}



	abstract record ServiceDescriptor : IComparable<ServiceDescriptor>
	{
		public const string BasicAuthenticationStringUserAndClient = $"{{{WellKnownProperties.UserName}}}@{{{WellKnownProperties.ClientName}}}:{{{WellKnownProperties.Password}}}";
		public const string BasicAuthenticationStringUser = $"{{{WellKnownProperties.UserName}}}:{{{WellKnownProperties.Password}}}";

		/// <summary>
		/// Path from which the descriptor was loaded. Part of service cache.
		/// Not stored in json file: store in Service (ServiceCache) if needed.
		/// </summary>
		[Newtonsoft.Json.JsonIgnore]
		public string ActualPath { get; set; } = null!;

		public string Id { get; init; } = Guid.NewGuid().ToString();    // Unique Id.
		public string Name { get; set; } = string.Empty;
		public string Category { get; set; } = string.Empty;           // e.g DTA, DTA-BI, DCM, OR, SKI
		public string PackageName { get; set; } = string.Empty;
		public string? PackageVersion { get; set; }
		public List<string> Properties { get; set; } = [];          // see WellKnownProperties
		public List<string> SecretProperties { get; set; } = [];    // secret properties (show password char instead or real value). e.g. Password
		public Dictionary<string, string> Mappings { get; set; } = [];
		public string? CheckMethod { get; set; }

		public List<XmlQualifiedName> ExcludeDerivedTypes { get; set; } = [];

		[Newtonsoft.Json.JsonIgnore]
		public IEnumerable<string> AllProperties => Properties.Concat(SecretProperties);

		[Newtonsoft.Json.JsonIgnore]
		public string PackageNameAndVersion => !string.IsNullOrEmpty(PackageVersion) ? $"{PackageName} ({PackageVersion})" : PackageName;

		[Newtonsoft.Json.JsonIgnore]
		public abstract ServiceTypes ServiceType { get; }

		public abstract Service MakeService();

		/// <summary>
		/// Sort by Category, Package and Name (used in frmEditProfile).
		/// </summary>
		public int CompareTo(ServiceDescriptor? other)
		{
			int r = this.Category.CompareTo(other?.Category);

			if (r == 0)
				r = this.PackageName.CompareTo(other?.PackageName);

			if (r == 0)
				r = this.Name.CompareTo(other?.Name);

			return r;
		}

		public override string ToString()
		{
			//return $"{Category} {Name}{(!string.IsNullOrEmpty(PackageName) ? $" ({PackageName})" : string.Empty)}";
			return $"{Category} {Name}";
		}

		public string ToStringExt()
		{
			return $"{this.GetType().Name} Id:'{Id}' Name:'{Name}' Category:'{Category}'";
		}
	}

	abstract record HttpServiceDescriptor : ServiceDescriptor
	{
		public override abstract HttpService MakeService();

		public required string ServiceDefinitionPath { get; set; }   // relative or absolute path to wsdl/yaml file.

		[Newtonsoft.Json.JsonIgnore]
		public string ServiceDefinitionFullPath => Path.Combine(Path.GetDirectoryName(ActualPath)!, ServiceDefinitionPath);

		public string? DefaultUrlPrefix { get; set; }                  // combined with Profile.BaseUrl and REST method url or SoapServiceDescriptor.ServiceUrl. .e.g. "dta", "or"
		public List<string> ServerUrls { get; set; } = [];
		public bool UseBasicAuthentication { get; set; }
		public string? CustomBasicAuthenticationString { get; set; }

		[Newtonsoft.Json.JsonIgnore]
		public string DefaultBasicAuthenticationString => Properties.Contains(WellKnownProperties.ClientName) ? BasicAuthenticationStringUserAndClient : BasicAuthenticationStringUser;

		[Newtonsoft.Json.JsonIgnore]
		public string BasicAuthenticationString => !string.IsNullOrWhiteSpace(CustomBasicAuthenticationString) ? CustomBasicAuthenticationString : DefaultBasicAuthenticationString;

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record RestServiceDescriptor : HttpServiceDescriptor
	{
		public override ServiceTypes ServiceType => ServiceTypes.Rest;
		public override RestService MakeService() => new(this);

		public override string ToString()
		{
			return base.ToString();
		}
	}

	record SoapServiceDescriptor : HttpServiceDescriptor
	{
		public override ServiceTypes ServiceType => ServiceTypes.Soap;
		public override SoapService MakeService() => new(this);

		public string ServiceUrl { get; set; } = string.Empty;

		public override string ToString()
		{
			return base.ToString();
		}
	}


}
