﻿namespace WSTest;


abstract class ServiceMethod : IComparable<ServiceMethod>
{
	public string Name { get; init; } = null!;
	public string? Description { get; init; }   // SOAP Documentation (string/html) - OpenAPI description/summary (string only, CommonMark/MarkDown not handled).

	[Newtonsoft.Json.JsonConstructor]
	protected ServiceMethod() { }

	public ServiceMethod(string name)
	{
		this.Name = name;
	}

	[Newtonsoft.Json.JsonIgnore]
	public abstract IReadOnlyCollection<Property> AllProperties { get; }

	/// <summary>
	/// DescribeMethod : return Html content.
	/// </summary>
	public abstract string DescribeMethod(ILookupReferenceType lookup);

	public override string ToString()
	{
		return Name;
	}

	public int CompareTo(ServiceMethod? other)
	{
		return this.Name.CompareTo(other?.Name);
	}
}

